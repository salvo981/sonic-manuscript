# This will generate a single plot with 3 columns
# 1 for each type of domain: prokaryote, eukaryote, mixed

# MMseqs msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-msens-ismb.extime.datapoint.tsv --multi-col-name empire-class --mrkr-size 10 --prefix qfo-ismb-mmseqs-msens-MULTIPLOT-extimes