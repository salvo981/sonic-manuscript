#!/bin/bash

#PBS -S /usr/bin/bash
#PBS -N proteinortho-2000mags-with-no-asterisks
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@k.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs



# This script performs additional runs using the 2,000 MAGs dataset.
# Specifically we reprocessed the input to remove the * symbols
# to avoid po6 to crash

date

# Tool name abbreviations:
# po6 -> ProteinOrtho6
po6_cmd=~/work_repos/proteinortho/proteinortho6.pl
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=reference_2000_mags_no_asterisk
cpu_cnt=128

# Output directories
output_root=$root\output_extra_2000mags_runs/
mkdir $output_root
po6_output_root=$output_root\po6/
mkdir $po6_output_root

# ProteinOrtho6 run [default]
po6_mode=default-no-asterisk # This should use the pseudo bitscores
po6_run_name_prefix=po6-$po6_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' ProteinOrtho runs '$cpu_cnt$' cpus' $input_set $po6_run_name_prefix $'@@@@\n'

echo $po6_cmd
echo $po6_output_root
echo $po6_run_name_prefix
# Create and enter the po6 output directory
mkdir $po6_output_root$po6_run_name_prefix
wait
cd $po6_output_root$po6_run_name_prefix
wait

echo hyperfine --style none --runs 1 --export-json $po6_output_root$po6_run_name_prefix.hypefine.json \"$po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* \> $po6_output_root\log.$po6_run_name_prefix.txt 2\&\>1\"
hyperfine --style none --runs 1 --export-json $po6_output_root$po6_run_name_prefix.hypefine.json "$po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* > $po6_output_root\log.$po6_run_name_prefix.txt 2>&1"
wait


echo $'\n'
date




echo $'\n'
date