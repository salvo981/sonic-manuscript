    ####### MODEL #########
    # Model: lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl
    # Test set: QfO2020
    # Model feature type: categorical

    ###### MMseqs2 ######
    # msens
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-mmseqs-ca-msens.tsv --categorical -d

    # Model performance:
    # MCC:	0.9254
    # Accuracy:	0.9627
    # F1-score:	0.9627

    # sens
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-mmseqs-ca-sens.tsv --categorical -d

    # Model performance:
    # MCC:	0.9141
    # Accuracy:	0.9570
    # F1-score:	0.9570

    # default
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-mmseqs-ca-default.tsv --categorical -d

    # Model performance:
    # MCC:	0.8988
    # Accuracy:	0.9494
    # F1-score:	0.9494

    # fast
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-mmseqs-ca-fast.tsv --categorical -d

    # Model performance:
    # MCC:	0.8661
    # Accuracy:	0.9331
    # F1-score:	0.9331


    ###### Diamond ######
    # msens
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-dmnd-ca-msens.tsv --categorical -d

    # Model performance:
    # MCC:	0.8781
    # Accuracy:	0.9391
    # F1-score:	0.9391

    # sens
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-dmnd-ca-sens.tsv --categorical -d

    # Model performance:
    # MCC:	0.8881
    # Accuracy:	0.9441
    # F1-score:	0.9441

    # default
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-dmnd-ca-default.tsv --categorical -d

    # Model performance:
    # MCC:	0.8848
    # Accuracy:	0.9424
    # F1-score:	0.9424

    # fast
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-dmnd-ca-fast.tsv --categorical -d

    # Model performance:
    # MCC:	0.8954
    # Accuracy:	0.9477
    # F1-score:	0.9477

    ### BLAST
    python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/predict_samples.py --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --model ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightbgm.mmseqs_msens.no-weights.cat.01_300_250_9features_62250samples.pckl --true-values ~/work_repos/sonic-manuscript/tables/aln-time-files/qfo2020/manuscript/aln_ex_times_s138-blast-ca-default.tsv --categorical -d

    # Model performance:
    # MCC:	0.7116
    # Accuracy:	0.8558
    # F1-score:	0.8558
