using Base: iscontiguous, Float64
#=
Procees archives with datapoints from qfo20 benchmark and generate datapoints for plotting.
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using DataStructures: OrderedDict
using JSON
using DataFrames
using CSV
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using ProcessRawQfOBenchDatapoints



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "raw-datapoints-dir"
            nargs = 1
            arg_type = String
            help = "Directory containing gzip archives with datapoints dowloaded from QfO benchmark web-server."
            required = true
        "--output-name"
            nargs = '?'
            arg_type = String
            default = "qfo-benchmark-results.tsv"
            help = "Name of the output datapoint file. "
            required = false
        "--output-dir"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Directory that will contain the output files."
            required = true
        # "--output-tbl"
        #     nargs = '?'
        #     arg_type = String
        #     default = ""
        #     help = "Table with extra information, including sequence count and proteome size"
        #     required = true
        "--all-methods"
            action = :store_true
            help = "Extract datapoints for all methods."
        "--cleanup"
            action = :store_true   # this makes it a flag
            help = "Remove temporary files."
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""
Check the difference in magnitude between two values.
This can be used for example to compare length difference,
or the arch sizes

Return a tuple of type (1, 1.5) where 1 indicates that the xDiffThr was surpassed.
The last value says how many times one value is smaller (or bigger) than the other (e.g., 2X)
If the first value is 0 then the threshold was not surpassed
"""
function check_magnitude_difference(v1::Float64, v2::Float64, xDiffThr::Float64)::Tuple{Int, Float64}
    @debug "check_magnitude_difference :: START" v1 v2 xDiffThr

    # simply return that it should be kept
    if v1 == v2
        return(0, 1.)
    else
        # how many times v2 is bigger than v1
        v1VSv2::Float64 = 1.
        # this should be a negative multiplier if v1 is bigger than v2
        # consider which value is the biggest:
        if v2 < v1
            v1VSv2 = round(v1/v2, digits=2)
            # @show v1VSv2
        else # v2 is bigger
            v1VSv2 = -round(v2/v1, digits=2)
            # @show v1VSv2
        end
        # Check if the threhold has been surpassed
        # and return the output tuple accordingly
        if abs(v1VSv2) > xDiffThr # drop it
            # if v1VSv2 < 0
            #     @debug "Debug @ check_magnitude_difference" v1VSv2, abs(v1VSv2)
            #     exit(-10)
            # end
            return(1, v1VSv2)
        else # keep it
            # @show (v1VSv2, abs(v1VSv2))
            return(0, v1VSv2)
        end
    end
end




"""
Extract run settings for SonicParanoid2 based on the run ID used in the QfO Benchmark
"""
function soniparannoid_benchmark_run_settings(participantId::String)::Tuple{String, String, String}
    @debug "soniparannoid_benchmark_run_settings :: START" participantId

    # Example of participant ID
    # s2-dm-def-graph (for graph based runs)
    # s2-dm-def-thr75 (for hybrid mode runs)
    flds::Array{String, 1} = rsplit(participantId, "-", limit=4)
    
    alignmentTool::String = flds[2]
    sensitivity::String = flds[3]
    runType::String = flds[4]
    
    # Reformat strings for aligner
    if alignmentTool == "dm"
        alignmentTool = "Diamond"
    elseif alignmentTool == "blast"
        alignmentTool = "BLAST"
    else
        alignmentTool = "MMseqs"
    end
    # Reformat strings for run type
    if runType == "ca"
        runType = "Complete"
    elseif runType == "graph"
        runType = "Graph"
    else
        runType = "Hybrid"
    end

    return (runType, alignmentTool, sensitivity)

end



"""
Process a JSON file with results from a test
Return: list of strings with results for each test
"""
function process_sonic_json_results(jsonPath::String)::String
    @debug "process_sonic_json_results :: START" jsonPath

    # The JSON file with results have the following DataStructures
    #=
    {
         "_id": "QfO:STD_Bacteria_agg",
         "challenge_ids": [
             "STD_Bacteria"
         ],
         "datalink": {
            "inline_data": {
                "challenge_participants": [
                    {
                        "metric_x": 1087,
                        "metric_y": 0.57881018,
                        "participant_id": "s136-dmnd-ca-default",
                        "stderr_x": 60.990621,
                        "stderr_y": 0.017279919
                    }
                ],
                "visualization": {
                    "type": "2D-plot",
                    "x_axis": "NR_COMPLETED_TREE_SAMPLINGS",
                    "y_axis": "RF_DISTANCE"
                }
            }
        },
        "type": "aggregation"
    }
    =#

    # Following are the information that we will extract from the results file
    challengeId::String = participantId = x = y = xerr = yerr = xlabel = ylabel =""
    resInfo::Dict{String, Any} =JSON.parsefile(jsonPath)
    # println(resInfo)
    challengeId = resInfo["challenge_ids"][1]
    participantId = resInfo["datalink"]["inline_data"]["challenge_participants"][1]["participant_id"]
    x = resInfo["datalink"]["inline_data"]["challenge_participants"][1]["metric_x"]
    y = resInfo["datalink"]["inline_data"]["challenge_participants"][1]["metric_y"]
    xerr = resInfo["datalink"]["inline_data"]["challenge_participants"][1]["stderr_x"]
    yerr = resInfo["datalink"]["inline_data"]["challenge_participants"][1]["stderr_y"]
    xlabel = resInfo["datalink"]["inline_data"]["visualization"]["x_axis"]
    ylabel = resInfo["datalink"]["inline_data"]["visualization"]["y_axis"] 
    # Run names have the following format
    # s2-dm-def-graph
    # @show participantId
    flds::Array{String, 1} = rsplit(participantId, "-", limit=4)
    alignmentTool::String = flds[2]
    runType::String = flds[3]
    sensitivity::String = flds[4]

    # Reformat strings for aligner
    if alignmentTool == "dmnd"
        alignmentTool = "Diamond"
    elseif alignmentTool == "blast"
        alignmentTool = "BLAST"
    else
        alignmentTool = "MMseqs"
    end
    # Reformat strings for run type
    if runType == "ca"
        runType = "Complete"
    else
        runType = "Essential"
    end

    # Output lines have the following format
    # participantId, challengeId, runType, alignmentTool, sensitivity, x, y, xerr, yerr, xlabel, ylabel
    return "$(participantId)\t$(challengeId)\t$(runType)\t$(alignmentTool)\t$(sensitivity)\t$(x)\t$(y)\t$(xerr)\t$(yerr)\t$(xlabel)\t$(ylabel)"

end



"""
Process a JSON file with results from a test, from different methods
Return: list of strings with results for the current test and the method strating with prefix
"""
function process_sonic_json_results_multiple_methods(jsonPath::String; targetMethod::String)::Vector{String}
    @debug "process_sonic_json_results_multiple_methods :: START" jsonPath targetMethod

    # The JSON file with results have the following DataStructures
    #=
    {
         "_id": "QfO:STD_Bacteria_agg",
         "challenge_ids": [
             "STD_Bacteria"
         ],
         "datalink": {
            "inline_data": {
                "challenge_participants": [
                    {
                        "metric_x": 1087,
                        "metric_y": 0.57881018,
                        "participant_id": "s136-dmnd-ca-default",
                        "stderr_x": 60.990621,
                        "stderr_y": 0.017279919
                    }
                ],
                "visualization": {
                    "type": "2D-plot",
                    "x_axis": "NR_COMPLETED_TREE_SAMPLINGS",
                    "y_axis": "RF_DISTANCE"
                }
            }
        },
        "type": "aggregation"
    }
    =#

    # Following are the information that we will extract from the results file
    challengeId::String = participantId = x = y = xerr = yerr = xlabel = ylabel =""
    resInfo::Dict{String, Any} = JSON.parsefile(jsonPath)
    # println(resInfo)
    challengeId = resInfo["challenge_ids"][1]
    # Find the correct indext for the participant ID
    participantIdx::Int = 0
    for (idx, participantResults) in enumerate(resInfo["datalink"]["inline_data"]["challenge_participants"])
        # @show idx participantResults typeof(participantResults)
        if participantResults["participant_id"] == targetMethod
            participantIdx = idx
            break
        end
    end

    participantId = resInfo["datalink"]["inline_data"]["challenge_participants"][participantIdx]["participant_id"]
    x = resInfo["datalink"]["inline_data"]["challenge_participants"][participantIdx]["metric_x"]
    y = resInfo["datalink"]["inline_data"]["challenge_participants"][participantIdx]["metric_y"]
    xerr = resInfo["datalink"]["inline_data"]["challenge_participants"][participantIdx]["stderr_x"]
    yerr = resInfo["datalink"]["inline_data"]["challenge_participants"][participantIdx]["stderr_y"]
    xlabel = resInfo["datalink"]["inline_data"]["visualization"]["x_axis"]
    ylabel = resInfo["datalink"]["inline_data"]["visualization"]["y_axis"] 

    # Output values have the following order
    # challengeId: e.g. GSTD_Luca
    # x: x value
    # y: y value
    # xerr: value for the errorbar for x
    # yerr: value for the errorbar for y
    # xlabel
    # ylabel
    return String[challengeId, "$x", "$y", "$xerr", "$yerr", xlabel, ylabel]

    # participantId, challengeId, runType, alignmentTool, sensitivity, x, y, xerr, yerr, xlabel, ylabel
    # return "$(participantId)\t$(challengeId)\t$(runType)\t$(alignmentTool)\t$(sensitivity)\t$(x)\t$(y)\t$(xerr)\t$(yerr)\t$(xlabel)\t$(ylabel)"


    # @warn "DEBUG :: process_sonic_json_results_multiple_methods"

    # exit(-5)

    #=
    # Example of participant ID
    # s2-dm-def-graph (for graph based runs)
    # s2-dm-def-thr75 (for hybrid mode runs)
    flds::Array{String, 1} = rsplit(participantId, "-", limit=4)
    alignmentTool::String = flds[2]
    sensitivity::String = flds[3]
    runType::String = flds[4]

    # Reformat strings for aligner
    if alignmentTool == "dm"
        alignmentTool = "Diamond"
    elseif alignmentTool == "blast"
        alignmentTool = "BLAST"
    else
        alignmentTool = "MMseqs"
    end
    # Reformat strings for run type
    if runType == "ca"
        runType = "Complete"
    elseif runType == "graph"
        runType = "Graph"
    else
        runType = "Hybrid"
    end
    
    # @warn "Debugging process_sonic_json_results_multiple_methods"

    # Output lines have the following format
    # participantId, challengeId, runType, alignmentTool, sensitivity, x, y, xerr, yerr, xlabel, ylabel
    return "$(participantId)\t$(challengeId)\t$(runType)\t$(alignmentTool)\t$(sensitivity)\t$(x)\t$(y)\t$(xerr)\t$(yerr)\t$(xlabel)\t$(ylabel)"
    =#


end



"""
Unarchive and process benchmark results files for a single participant.
"""
function process_raw_datapoints(qfoResultsArchivePath::String, outDir::String; cleanup::Bool=false, participantId::String="")::String
    @debug "process_raw_datapoints :: START" qfoResultsArchivePath outDir cleanup participantId
    
    # tmp variables
    tmpDir::String = tmpPath = resLine = ""
    # These directories and files must be in the results directory
    resultDirs::Array{String, 1} = ["EC", "GO", "G_STD2_Eukaryota", "G_STD2_Fungi", "G_STD2_Luca", "G_STD2_Vertebrata", "STD_Bacteria", "STD_Eukaryota", "STD_Fungi", "SwissTrees", "TreeFam-A", "Manifest.json"]
    bname::String = string(rsplit(basename(qfoResultsArchivePath), ".", limit=2)[1])    
    tarPath::String = joinpath(outDir, bname)
    rawResultsDir::String = rsplit(tarPath, ".", limit=2)[1]
    flatDataPointsFile::String = joinpath(outDir, "$(split(bname, '_', limit=2)[1]).tsv")
    resLine::String = ""
    jsonCnt::Int = 0

    # Check of the file is an archive and extract it in the output directory
    if is_archive(qfoResultsArchivePath)[1]
        pigz_decompress(qfoResultsArchivePath, tarPath, keep=true, force=true, errLog=false, threads=1)
        extract_tar_archive(tarPath, outDir)
        # Remove temporary files
        rm(tarPath, force=true)
    else
        @error "The provided file is not a valid archive:" qfoResultsArchivePath
        exit(-5)
    end
    
    # Check that all the directories are available
    for dirname in resultDirs[1:11]
        # @show dirname
        tmpDir = joinpath(rawResultsDir, dirname)
        if !isdir(joinpath(rawResultsDir))
            @error "A directory with datapoints is missing!" dirname tmpDir
            exit(-2)
        end
    end
    
    tmpPath = joinpath(rawResultsDir, resultDirs[12])
    if !isfile(tmpPath)
        @error "The manifest file is missing!" resultDirs[11] tmpPath
        exit(-2)
    end

    # Extract the participant ID if not provided
    if length(participantId) == 0
        # Archive files have the following format:
        # s136-blast-ca-default_20210827T034430.tar.gz
        participantId = string(rsplit(basename(qfoResultsArchivePath), "_", limit=2)[1])
    end

    # create the output file
    ofd::IO = open(flatDataPointsFile, "w")
    # Create the output file and write the hdr
    hdr::String = "method\ttest\trun_type\taligner\tsensitivity\tx\ty\txerr\tyerr\txlabel\tylabel\n"
    write(ofd, hdr)

    # Process all JSON file in each directory
    for (r, d, f) in walkdir(rawResultsDir)
        # @show (r, d, f)
        for fname in f
            if endswith(fname, r".json")
                # Skip Manifest.json
                # @show fname[1:2]
                # println(typeof(fname[1:2]))
                if fname[1:2] != "Ma"
                    tmpPath = joinpath(r, fname)
                    # @show tmpPath
                    resLine = process_sonic_json_results_multiple_methods(tmpPath, targetMethod=participantId)
                    write(ofd, "$(resLine)\n")
                    jsonCnt += 1
                end
            end
        end
        # break
    end

    close(ofd)
    
    # 32 result files are expected
    if jsonCnt != 32
        @error "Each QfO result archive must contain 32 JSON result files!" jsonCnt
        exit(-7)
    end
    
    # Remove directory with raw files
    if cleanup
        rm(rawResultsDir, recursive=true, force=true)
    end
    # return the file with datapoints
    return flatDataPointsFile
end



"""
Unarchive and process benchmark results files for all participants.
"""
function process_raw_datapoints_for_all_methods(qfoResultsArchivePath::String, outDir::String; cleanup::Bool=false)::String
    @debug "process_raw_datapoints_for_all_methods :: START" qfoResultsArchivePath outDir cleanup
    
    # tmp variables
    tmpDir::String = tmpPath = resLine = ""
    # These directories and files must be in the results directory
    resultDirs::Array{String, 1} = ["EC", "GO", "G_STD2_Eukaryota", "G_STD2_Fungi", "G_STD2_Luca", "G_STD2_Vertebrata", "STD_Bacteria", "STD_Eukaryota", "STD_Fungi", "SwissTrees", "TreeFam-A", "Manifest.json"]
    bname::String = string(rsplit(basename(qfoResultsArchivePath), ".", limit=2)[1])
    tarPath::String = joinpath(outDir, bname)
    rawResultsDir::String = rsplit(tarPath, ".", limit=2)[1]
    flatDataPointsFile::String = joinpath(outDir, "$(split(bname, '_', limit=2)[1]).tsv")
    resLine::String = ""
    jsonCnt::Int = 0

    # Check of the file is an archive and extract it in the output directory
    if is_archive(qfoResultsArchivePath)[1]
        pigz_decompress(qfoResultsArchivePath, tarPath, keep=true, force=true, errLog=false, threads=1)
        extract_tar_archive(tarPath, outDir)
        # Remove temporary files
        rm(tarPath, force=true)
    else
        @error "The provided file is not a valid archive:" qfoResultsArchivePath
        exit(-5)
    end
    
    # Check that all the directories are available
    for dirname in resultDirs[1:11]
        # @show dirname
        tmpDir = joinpath(rawResultsDir, dirname)
        if !isdir(joinpath(rawResultsDir))
            @error "A directory with datapoints is missing!" dirname tmpDir
            exit(-2)
        end
    end
    
    tmpPath = joinpath(rawResultsDir, resultDirs[12])
    if !isfile(tmpPath)
        @error "The manifest file is missing!" resultDirs[11] tmpPath
        exit(-2)
    end

    # create the output file
    ofd::IO = open(flatDataPointsFile, "w")
    # Create the output file and write the hdr
    hdr::String = "method\ttest\trun_type\taligner\tsensitivity\tx\ty\txerr\tyerr\txlabel\tylabel\n"
    write(ofd, hdr)

    # Extract the name of all participants
    tmpPath = joinpath(rawResultsDir, "Manifest.json")

    @warn "The data extractor assumes that all participants have results in all the tests in the benchmark."
    participants::Vector{String} = JSON.parsefile(tmpPath)[1]["participants"]
    # @show participants

    # This will contain the
    # run type: e.g., graph or hybrid
    # alignemnt tool: alignmnet tool used for the graph-based predictions
    # sensitivity: settings for the aligner, e.g., most-sensitive
    toolSettings::Tuple{String, String, String} = ("na", "na", "na")
    tmpMethodResults::Array{String, 1} = String[]

    # Process all JSON file in each directory
    for (r, d, f) in walkdir(rawResultsDir)
        # @show (r, d, f)
        participantId::String = ""
        for fname in f
            if endswith(fname, r".json")
                # Skip Manifest.json
                # @show fname[1:2]
                # println(typeof(fname[1:2]))
                if fname[1:2] != "Ma"
                    tmpPath = joinpath(r, fname)
                    # @show tmpPath
                    for participantId in participants
                        # @show participantId
                        if startswith(participantId, "s2-")
                            toolSettings = soniparannoid_benchmark_run_settings(participantId)
                        else
                            # Reset to unknown
                            toolSettings = ("na", "na", "na")
                        end
                        
                        # resLine = process_sonic_json_results_multiple_methods(tmpPath, targetMethod=participantId)
                        tmpMethodResults = process_sonic_json_results_multiple_methods(tmpPath, targetMethod=participantId)
                        # resLine = "$(participantId)\t"
                        resLine = "$(participantId)\t$(tmpMethodResults[1])\t$(toolSettings[1])\t$(toolSettings[2])\t$(toolSettings[3])\t$(tmpMethodResults[2])\t$(tmpMethodResults[3])\t$(tmpMethodResults[4])\t$(tmpMethodResults[5])\t$(tmpMethodResults[6])\t$(tmpMethodResults[7])"
                        # println(resLine)
                        write(ofd, "$(resLine)\n")
                    end
                    jsonCnt += 1
                end
            end
        end
        # break
    end

    close(ofd)
    
    # 32 result files are expected
    if jsonCnt != 32
        @error "Each QfO result archive must contain 32 JSON result files!" jsonCnt
        exit(-7)
    end
    
    # Remove directory with raw files
    if cleanup
        rm(rawResultsDir, recursive=true, force=true)
    end
    # return the file with datapoints
    return flatDataPointsFile
end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)


rawDataDir = realpath(args["raw-datapoints-dir"][1])
# metaDataTbl = realpath(args["meta-data"][1])
outDir = abspath(args["output-dir"])
outTblName = args["output-name"]
if length(outTblName) < 1
    @error "The name of the output table must be at least one character!" outTblName
    exit(-5)
end
makedir(outDir)
# outTbl = abspath(args["output-tbl"])
allMethods = args["all-methods"]
cleanup = args["cleanup"]
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "QfO benchmark datapoints will be processed using the following parameters:" rawDataDir outDir outTblName allMethods debug

# Process all the files in the input directory
tmpPath = ""
hasHdr = false
outTbl = joinpath(outDir, outTblName)
ofd = open(outTbl, "w")
# Process raw datapoints and generate a single datapoint file
for f in readdir(rawDataDir)
    global tmpPath = joinpath(rawDataDir, f)
    @show f
    if f == "README.md"
        @info "Skipping README file" f
        continue
    end

    if allMethods
        global tmpPath = process_raw_datapoints_for_all_methods(tmpPath, outDir, cleanup=cleanup)
    else
        global tmpPath = process_raw_datapoints(tmpPath, outDir, cleanup=cleanup, participantId="")
    end
    # @warn "Debug mode"
    # Open the data point file and dump into the output table
    ifd = open(tmpPath, "r")
    hdr = readline(ifd, keep=true)
    if !hasHdr
        write(ofd, hdr)
        global hasHdr = true
    end
    
    # Write the remaining lines
    for ln in eachline(ifd, keep=true)
        write(ofd, ln)
    end
    close(ifd)
    # Remove temporary files
    if cleanup
        rm(tmpPath, force=true)
    end
end

close(ofd)

# Remove repetitions from the DataFrame if all methods were used
@show outTbl

if allMethods
    @info "Removing duplicates from DataFrame" outTbl
    # Load the data DataFrame
    df = CSV.read(outTbl, delim="\t", DataFrame)
    # Remove repetitions
    unique!(df)
    # Sort the DataFrame
    sort!(df, [:method])
    @warn "The previous DF file will be overwritten with the filtered one." outTbl
    CSV.write(outTbl, delim="\t", df)
end
