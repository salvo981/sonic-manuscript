#!/bin/bash

# This script performs additional runs using the 2,000 MAGs dataset
# The extra runs are the following:
# - SonicParanoid2 fast
# - SonicParanoid2 fast (graph-only)
# - SonicParanoid2 default
# - SonicParanoid2 default (graph-only)

date

# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=reference_2000_mags
cpu_cnt=128

# Output directories
output_root=$root\output_extra_2000mags_runs/
mkdir $output_root
sp_output_root=$output_root\sp2/
mkdir $sp_output_root


# SonicParanoid2 (fast)
sp_mode=fast
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date


# SonicParanoid2 (fast graph-only)
sp_mode=fast
sp_run_name_prefix=sp2-$sp_mode-graph-only-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt --graph-only \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt --graph-only > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date


# SonicParanoid2 (default)
sp_mode=default
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json  \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date


# SonicParanoid2 default (graph-only)
sp_mode=default
sp_run_name_prefix=sp2-$sp_mode-graph-only-$input_set-$cpu_cnt\cpus

echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json  \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --graph-only \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --graph-only > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date