date


# Use 9 features. No weights
echo $'\n@@@@ feature-selection-mmseqs-msens-noidx no-weights @@@@'
python3 ~/work_repos/sonic-manuscript/adaboost-training/feature_selection_adaboost.py -m ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/models/mmseqs_msens.no-weights.adaboost_mdepth2_225_11_sammer_9features_62250samples.pckl -tr ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/training-data/training_smpls.no_mol_weights.adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/training-data/labels.adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv -o ./ -t 4 -d &> log.feature_selection_adaboost.txt

# All runs completed
date