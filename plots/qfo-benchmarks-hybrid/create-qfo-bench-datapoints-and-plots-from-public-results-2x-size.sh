# Generate qfo results datasets
julia /home/salvocos/work_repos/sonic-manuscript/scripts/julia/create_qfo20_benchmark_datapoints_from_public_results.jl ~/work_repos/sonic-manuscript/tables/qfo-benchmarks/sp2-qfo20-webpage-json-files-200223.tar.xz --output-dir ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/ --cleanup
wait
# Generate the plots

python3 /home/salvocos/work_repos/sonic-manuscript/scripts/python/plot_qfo20_benchmark_public_results-2x-size.py -i ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/qfo20-public-results.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/ --prefix qfo20-bench-public-results-2x-size-manuscript --mrkr-size 10 --font-size 17 --pareto-line-width 3 --fig-width-inches 5.4 --fig-height-inches 3.6 --pareto-script ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl --format svg

