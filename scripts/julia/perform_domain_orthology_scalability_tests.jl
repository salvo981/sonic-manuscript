using Base: iscontiguous, Float64
#=
Perform multiple domain-orthology-runs using sonicparanoid script.
This is used to generate the datapoints to generate the scalability figure for DBO
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using DataStructures: OrderedDict
using Printf:@printf,@sprintf
using DataFrames
using CSV
using Serialization
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using DomainOrthologyScalability



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath

    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings()
    @add_arg_table! s begin
    "input-dir"
        arg_type = String
        help = "Directory containing all the input proteomes."
        required = true
    "run-dir"
        arg_type = String
        help = "Directory a reference SonicPAranoid run with auxiliary files."
        required = true
    "python-script"
        arg_type = String
        help = "Path to the python script to be executed."
        required = true
    "output-dir"
        arg_type = String
        help = "Directory that will contain the output files."
        required = true
    "--points"
        # nargs = "?"
        arg_type = Int
        default = 20
        help = "Number of points to be generated"
        required = false
    "--prefix"
        arg_type = String
        default = "benchmarkrun"
        help = "General prefix for the run"
        required = false
    "--cleanup"
        action = :store_true   # this makes it a flag
        help = "Remove temporary files."
    "--threads"
        arg_type = Int
        default = 4
        help = "Number of points to be generated"
        required = false
    "--debug", "-d"
        action = :store_true   # this makes it a flag
        help = "Show debug information."
    end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""
Computes the input size for each run
"""
function compute_incremental_run_datasets(points::Int, inputProteomes::Array{String, 1})::Array{Int, 1}
    @debug "compute_incremental_run_datasets :: START" points length(inputProteomes)
    spCnt::Int = length(inputProteomes)

    if points >= spCnt
        @error "The number of points must be lower than the number of proteomes" points spCnt
        exit(-5)
    end

    stepSize::Int = ceil(spCnt/points)
    @show stepSize spCnt/stepSize
    # use the floor if nexessary
    if spCnt/stepSize < points
        stepSize = floor(spCnt/points)
    end
    @show stepSize

    # Now create the stepSize
    currentStep::Int = 0
    datasetSizes::Vector{Int} = Int[]
    while length(datasetSizes) < points
        currentStep += stepSize
        push!(datasetSizes, currentStep)
    end

    
    # Check that the number of datapoints is correct
    if length(datasetSizes) != points
        @error "The dataset sizes must be same as the points" points, length(datasetSizes)
        exit(-5)
    end

    # Adjust the value of the last datapoints if required
    if datasetSizes[end] > spCnt
        @error "The The last dataset size cannot be higher than the max number of species" spCnt, datasetSizes[end]
        exit(-5)
    elseif datasetSizes[end] < spCnt
        datasetSizes[end] = spCnt
    end
    
    # println(datasetSizes)

    return datasetSizes

end



"""Load proteome sizes and sequence counts from species files"""
# function load_run_info(runDir::String)::Nothing
function load_run_info(runDir::String)::Tuple{OrderedDict{String, Int}, OrderedDict{String, Int}}
    @debug "load_run_info :: START" runDir

    spFile::String = joinpath(runDir, "aux/species.tsv")
    # @show spFile
    spId::String = ""
    flds::Vector{SubString} = []
    protCntDict::OrderedDict{String, Int} = OrderedDict()
    protSizeDict::OrderedDict{String, Int} = OrderedDict()
    # Lines have the following format
    # 1 aaeolicus 0703c85ee7f7c7ff8bdff893bdb9f1816cb01b77cd5844145b15ffcb0f359a12 1553 488987
    for l in eachline(spFile, keep=false)
        flds = split(l, "\t", limit=5)
        # @show flds
        spId = flds[1]
        # Add the values to the corresponding dictionaries
        protCntDict[spId] = parse(Int, flds[4])
        protSizeDict[spId] = parse(Int, flds[5])
        # @show spId protCntDict[spId] protSizeDict[spId]
    end

    return (protCntDict, protSizeDict)

end



"""Prepare directories and input for a single run"""
function prepare_run(chunk::Array{String, 1}, inputProteomes::String, outDir::String, prefix::String)::OrderedDict{String, Any}
    @debug "prepare_run :: START" length(chunk) inputProteomes outDir, prefix
    chunkSize::Int = length(chunk)
    pyRunPrefix::String = "$(prefix)_$(chunkSize)"

    # Create the input directory and copy the files
    pyRunDir::String = joinpath(outDir, pyRunPrefix)
    makedir(pyRunDir)
    pyRunInputDir::String = joinpath(pyRunDir, "input_$(chunkSize)")
    makedir(pyRunInputDir)
    tmpPath::String = ""
    sp::String = ""
    # @show pyRunInputDir
    for sp in chunk
        tmpPath = joinpath(inputProteomes, sp)
        cp(tmpPath, joinpath(pyRunInputDir, sp), force=true)
    end

    # set the path for the log file
    pyLogPath::String = joinpath(pyRunDir, "log.$(pyRunPrefix).txt")
    # Add elements to the output dictionary
    outDict::OrderedDict{String, Any} = OrderedDict("outdir"=>pyRunDir, "indir"=>pyRunInputDir, "log"=>pyLogPath,"size"=>chunkSize, "prefix"=>pyRunPrefix)

    return outDict
end



"""Check that all input files exists"""
function validate_input(proteomesDir::String, species::Array{String, 1})::Nothing
    @debug "validate_input :: START" runDir

    tmpPath::String = ""
 
    for sp in species
        tmpPath = joinpath(proteomesDir, sp)
        if !isfile(tmpPath)
            @error "File for species $sp was not found" tmpPath
            exit(-2)
        end
        # println(isfile(tmpPath))
    end

    return nothing

end



#####  MAIN  #####
args = get_params(ARGS)
@show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

inDir = abspath(args["input-dir"])
runDir = abspath(args["run-dir"])
pyScript = abspath(args["python-script"])
outDir = abspath(args["output-dir"])
points = args["points"]
mainPrefix = args["prefix"]
clean = args["cleanup"]
threads = args["threads"]
debug = args["debug"]
makedir(outDir)

#=
validDataTypes = ["top-int-double", "top-double-double", "bottom-int-double", "bottom-double-double"]
if dataType ∉ validDataTypes
    @error "The data type is not valid!" dataType validDataTypes
    exit(-7)
end
=#

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
@show mainLogger

@info "The benchmark runs will be perfomed with the following parameters:" inDir runDir pyScript outDir points mainPrefix threads clean

# Load info on the input files
protCntDict = OrderedDict{String, Int}()
protSizeDict = OrderedDict{String, Int}()
protCntDict, protSizeDict = load_run_info(runDir)
species::Array{String, 1} = collect(keys(protCntDict))
# Check that all files exist
validate_input(inDir, species)


# This is only for debugging
# maxSize::Int = 8
# if maxSize > 0
#     species = species[1:maxSize]
# end

# println(species)

# Create a sorted array of Ints
datasetSizes = compute_incremental_run_datasets(points, species)
# println(datasetSizes)

inputChunks::Array{Array{String, 1}, 1} = [species[1:x] for x in datasetSizes]

# serilaize the input chunks in file
chunksFile::String = joinpath(outDir, "input_chunks.txt")
# serialize(chunksFile, inputChunks)
# @show chunksFile

ofd::IO = open(chunksFile, "w")
# Write the chunks into the file
for (idx, chunk) in enumerate(inputChunks)
    write(ofd, "# $idx\t$(length(chunk))\n")
    write(ofd, join(chunk, ","))
    write(ofd, "\n")
end
close(ofd)

# Perfor the run for each chunk
for chunk in inputChunks
    
    runParams::OrderedDict{String, Any} = prepare_run(chunk, inDir, outDir, mainPrefix)
    # println(runParams)
    
    # Prepare the python command to be executed
    tmpCmd::Cmd = `python3 $(pyScript) -i $(runParams["indir"]) -o $(runParams["outdir"]) -r $(runDir) -p $(runParams["prefix"]) -t $(threads) -c`
    
    run(pipeline(tmpCmd, stdout=runParams["log"]), wait=true)
    # println(runParams["log"])
    
    if clean
        if isdir(runParams["indir"])
            rm(runParams["indir"], recursive=true, force=true)
        end
    end
    
end
