# Generate qfo results datasets
julia /home/salvocos/work_repos/sonic-manuscript/scripts/julia/create_qfo_benchmark_datapoints_sonic2_vs_all.jl ~/work_repos/sonic-manuscript/tables/qfo-benchmarks/benchmark_results_sonic2-vs-all/ --output-dir ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/ --cleanup --all-methods
wait
# Generate the plots
python3 /home/salvocos/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_results_sonic2_vs_all.py -i ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/qfo-benchmark-results.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/ --prefix qfo-bench-s2-hybrid-manuscript --mrkr-size 5 --pareto-script ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl --format svg
