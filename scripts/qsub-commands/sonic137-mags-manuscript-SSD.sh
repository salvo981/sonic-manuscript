#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic137-mags-manuscript-SSD
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date

 ### Diamond runs #####
echo $'\n@@@@ sonic137-dmnd-essentials-fast-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-dmnd-ess-fast-mags -p s137-dmnd-ess-fast-mags -ow --aln-tool diamond --threads 128 -m fast -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-dmnd-complete-fast-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-dmnd-ca-fast-mags -p s137-dmnd-ca-fast-mags -ow --aln-tool diamond --threads 128 -ca -m fast -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-dmnd-essentials-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-dmnd-ess-default-mags -p s137-dmnd-ess-default-mags -ow --aln-tool diamond --threads 128 -m default -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-dmnd-complete-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-dmnd-ca-default-mags -p s137-dmnd-ca-default-mags -ow --aln-tool diamond --threads 128 -ca -m default -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-dmnd-essentials-sensitive-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-dmnd-ess-sens-mags -p s137-dmnd-ess-sens-mags -ow --aln-tool diamond --threads 128 -m sensitive -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-dmnd-complete-sensitive-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-dmnd-ca-sens-mags -p s137-dmnd-ca-sens-mags -ow --aln-tool diamond --threads 128 -ca -m sensitive -op -noidx
wait
echo $'\n'


 #### MMseqs runs #####
echo $'\n@@@@ sonic137-mmseqs-essentials-fast-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-mmseqs-ess-fast-mags -p s137-mmseqs-ess-fast-mags -ow --threads 128 -m fast -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-mmseqs-complete-fast-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-mmseqs-ca-fast-mags -p s137-mmseqs-ca-fast-mags -ow --threads 128 -ca -m fast -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-mmseqs-essentials-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-mmseqs-ess-default-mags -p s137-mmseqs-ess-default-mags -ow --threads 128 -m default -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-mmseqs-complete-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-mmseqs-ca-default-mags -p s137-mmseqs-ca-default-mags -ow --threads 128 -ca -m default -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-mmseqs-essentials-sensitive-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-mmseqs-ess-sens-mags -p s137-mmseqs-ess-sens-mags -ow --threads 128 -m sensitive -op -noidx
wait
echo $'\n'


echo $'\n@@@@ sonic137-mmseqs-complete-sensitive-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/s137-mmseqs-ca-sens-mags -p s137-mmseqs-ca-sens-mags -ow --threads 128 -ca -m sensitive -op -noidx
wait
echo $'\n'


# All runs completed
date