'''
Generate plots to compare profile searches with pfam full and seed.
The input is a table genetated from profile search runs
'''

import os
import sys
from typing import TextIO, List, Set, Dict, Tuple, Any
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots info about the proteomes.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-ifull", "--in-tbl-full", type=str, required=True, help="TSV file from profile search results using PFamA-full\n", default=None)
    parser.add_argument("-iseed", "--in-tbl-seed", type=str, required=True, help="TSV file from profile search results using PFamA-seed\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=False, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=False, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--snapshot", type=str, required=True, help="TSV snapshot file from SonicParanoid run.", default=os.getcwd())
    parser.add_argument("--meta-data", type=str, required=True, help="TSV table with metadata on the input proteomes.", default=os.getcwd())
    parser.add_argument("--hue", type=str, required=False, help="Column containing categorical information.", default="")
    parser.add_argument("--divisor", type=int, required=False, help="Integer value by which the main dapaoint should be divided (e.g., 1000).", default=1)
    parser.add_argument("--multi-col-name", type=str, required=False, help="Column name used to create multiple plots.", default="")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("-r", "--rows", type=int, required=False, help="Number of records to load. Used for debugging. Default=0", default=0)
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def add_qfo_taxa_info(inTbl: str, mappingDict: Dict[str, str], spInfoDict: Dict[str, Tuple[int, int, str, str]]) -> pd.DataFrame:
    """Add taxonmy infomation and real names to the profile search result tables"""
    # for k, v in mappingDict.items():
    #     print(f"{k}\t{v}")

    colsTypeMap: Dict[str, Any] = {"search": str, "psearch_time": np.double, "psearch_conv_time": np.double, "arch_extraction_time": np.double, "raw_hits_cnt": np.uint32, "pct_query_w_raw_hits": np.double, "usable_hits_cnt": np.uint32, "pct_query_w_usable_hits": np.double, "arch_cnt": np.uint32, "pct_query_w_arch": np.double}
    # load the dataframe
    df = pd.read_csv(inTbl, sep="\t", dtype=colsTypeMap, usecols=colsTypeMap.keys(),  engine="c")

    # Create vectors sorted based on the species ID in the dataframe
    # For example if the DF has the entries in the following 'search' column
    # 42-pfama.mmseqs
    # 17-pfama.mmseqs
    # The the vectors must have:
    # spIdVec = [42, 2]
    # spNameVec = [mgenitalium, agambiae]
    # taxaEmpireVec = ["prokaryota", "eukaryota"]
    # taxaDomainVec = ["bacteria", "eukaryota"]
    VEC_LEN: int = df.shape[0]
    spIdVec: List[str] = []
    spNameVec: List[str] = []
    taxaEmpireVec: List[str] = []
    taxaDomainVec: List[str] = []
    proteomeSizeVec: List[int] = []
    proteinCntVec: List[int] = []

    tmpStr: str = ""
    tmpSpName: str = ""
    tmpInfo: Tuple[int, int, str, str] = (0, 0, "", "")
    for searchName in df["search"]:
        # print(searchName)
        tmpStr = searchName.split("-", 1)[0]
        # Add info to the lists
        spIdVec.append(tmpStr)
        tmpSpName = mappingDict[tmpStr]
        spNameVec.append(tmpSpName)
        # print(tmpStr, tmpSpName)
        # print(spInfoDict[tmpSpName])
        # The content would be similar to the following
        # (39461, 18009409, 'eukaryota', 'eukaryota')
        tmpInfo = spInfoDict[tmpSpName]
        proteinCntVec.append(tmpInfo[0])
        proteomeSizeVec.append(tmpInfo[1])
        taxaEmpireVec.append(tmpInfo[2])
        taxaDomainVec.append(tmpInfo[3])

    # Add the vectors as new columns to the dataframe
    # print(spIdVec)
    df.insert(loc=0, column="sp_id", value=spIdVec)
    df.insert(loc=0, column="sp_name", value=spNameVec)
    df.insert(loc=2, column="domain", value=taxaDomainVec)
    df.insert(loc=2, column="empire", value=taxaEmpireVec)
    df.insert(loc=4, column="proteome_size", value=np.array(proteomeSizeVec, dtype=np.uint32))
    df.insert(loc=5, column="protein_cnt", value=np.array(proteinCntVec, dtype=np.uint32))
    # print(df.dtypes)
    # Sort by species ID
    df.sort_values(by=["sp_name"], axis=0, ascending=[True], inplace=True, ignore_index=True)
    df.sort_values(by=["proteome_size"], axis=0, ascending=[False], inplace=True, ignore_index=True)

    return df



def concat_profile_search_dataframes(tblFull: str, tblSeed: str) -> pd.DataFrame:
    """Add column with pfam-db type to dataframea and concatenate them"""

    # Load the pfam-full DF
    dfFull = pd.read_csv(tblFull, sep="\t", dtype= {"search": str, "psearch_time": np.double, "psearch_conv_time": np.double, "arch_extraction_time": np.double, "raw_hits_cnt": np.uint32, "pct_query_w_raw_hits": np.double, "usable_hits_cnt": np.uint32, "pct_query_w_usable_hits": np.double, "arch_cnt": np.uint32, "pct_query_w_arch": np.double}, engine="c")
    # Add column related to pfam
    dfFull.insert(dfFull.shape[1], "pfam_db", ["full" for x in range(0, dfFull.shape[0])])
    # print(dfFull.describe())
    # print(dfFull.head(n=2))

    # Load the pfam-seed DF
    dfSeed = pd.read_csv(tblSeed, sep="\t", dtype= {"search": str, "psearch_time": np.double, "psearch_conv_time": np.double, "arch_extraction_time": np.double, "raw_hits_cnt": np.uint32, "pct_query_w_raw_hits": np.double, "usable_hits_cnt": np.uint32, "pct_query_w_usable_hits": np.double, "arch_cnt": np.uint32, "pct_query_w_arch": np.double}, engine="c")
    # Add column related to pfam
    dfSeed.insert(dfSeed.shape[1], "pfam_db", ["seed" for x in range(0, dfSeed.shape[0])])
    # Concatenate the 2 dataframes
    dfCatted = pd.concat([dfFull, dfSeed], axis=0, join='outer', ignore_index=False, keys=None, levels=None, names=None, verify_integrity=False, sort=False, copy=True)
    # print(dfCatted.describe())
    # Sort the values in the concatenated DF
    dfCatted.sort_values(by=["search", "pct_query_w_arch"], axis=0, ascending=[True, False], inplace=True, ignore_index=True)
    # print(dfCatted.head(n=4))

    return dfCatted



def compare_full_and_seed_boxplots(tblFull: str, tblSeed: str, plotsDir: str, prefix: str,  fmt: str="svg"):
    """Generate comparison box plots"""
    import matplotlib as mpl
    import importlib

    # myStyle = "ticks"

    # sns.set_theme(style="whitegrid")
    sns.set_style("ticks", {'axes.grid' : True})

    # Concatenate the dataframes
    dfCatted = concat_profile_search_dataframes(tblFull=tblFull, tblSeed=tblSeed)

    # Column names for which plots will be generated
    yLimits: Tuple[int, int] = (0, 100)
    yVectNames: List[str] = ["pct_query_w_raw_hits", "pct_query_w_usable_hits", "pct_query_w_arch"]
    # Create box plots based on percentages
    for name in yVectNames:
        ax = sns.boxplot(x="pfam_db", y=dfCatted[name], data=dfCatted, hue="pfam_db", dodge=False)
        sns.swarmplot(x="pfam_db", y=dfCatted[name], data=dfCatted, color=".25", ax=ax)
        # Set limits
        ax.set(ylim=yLimits)
        # ax.set(ylabel="")
        # Remove legend
        ax.legend().set_visible(False)
        # plt.grid(visible=True, which='major', axis='both')
        sns.despine(ax=ax, top=True, right=True, left=False, bottom=True, offset=None, trim=True)
        tmpOutPath = os.path.join(plotsDir, f"boxplot.{prefix}.{name}.{fmt}")
        plt.savefig(tmpOutPath, dpi=160)
        importlib.reload(mpl); importlib.reload(plt); importlib.reload(sns)
        sns.set_style("ticks", {'axes.grid' : True})

    # Plot Hits, usable hits and arch numbers
    # Compute the max for the columns
    yVectNames = ["raw_hits_cnt", "usable_hits_cnt", "arch_cnt"]
    maxY: int = np.max([np.max(dfCatted[x]) for x in yVectNames])
    minY: int = np.min([np.min(dfCatted[x]) for x in yVectNames])
    # Create box plots based on counts
    for name in yVectNames:
        ax = sns.boxplot(x="pfam_db", y=dfCatted[name], data=dfCatted, hue="pfam_db", dodge=False)
        sns.swarmplot(x="pfam_db", y=dfCatted[name], data=dfCatted, color=".25", ax=ax)
        # Set limits
        ax.set(ylim=(minY, maxY))
        # ax.set(ylabel="")
        # Remove legend
        ax.legend().set_visible(False)

        sns.despine(ax=ax, top=True, right=True, left=False, bottom=True, offset=None, trim=True)
        tmpOutPath = os.path.join(plotsDir, f"boxplot.{prefix}.{name}.{fmt}")
        # print(tmpOutPath)
        plt.savefig(tmpOutPath, dpi=160)
        importlib.reload(mpl); importlib.reload(plt); importlib.reload(sns)
        sns.set_style("ticks", {'axes.grid' : True})

    # Reset all the styles and reload plotting modules
    # to avoid boxes to write on the same plot
    importlib.reload(mpl); importlib.reload(plt); importlib.reload(sns)

    # sys.exit("DEBUG :: compare_full_and_seed_boxplots")



def compare_full_and_seed_relplots(tblFull: str, tblSeed: str, mappingDict: Dict[str, str], spInfoDict: Dict[str, Tuple[int, int, str, str]], plotsDir: str, prefix: str, mrkSize: int = 20, hideLegend: bool = False, fmt: str="svg"):
    """Generate comparison relplots which include taxonomies of proteomes"""
    logging.debug("compare_full_and_seed_relplots :: START")
    logging.info(f"PFamA-full results: {tblFull}")
    logging.info(f"PFamA-seed results: {tblSeed}")

    # Load the two dataframes adding the metadata
    dfFull: pd.DataFrame = add_qfo_taxa_info(inTbl=tblFull, mappingDict=mappingDict, spInfoDict=spInfoDict)
    dfSeed: pd.DataFrame = add_qfo_taxa_info(inTbl=tblSeed, mappingDict=mappingDict, spInfoDict=spInfoDict)

    # Available styles: "darkgrid", "whitegrid", "white", "dark", "ticks"
    myStyle = "ticks"
    sns.set_style(myStyle, {'axes.grid' : True})

    # Set the colum names
    xLab: str = "PFamA-seed"
    yLab: str = "PFamA-full"
    tmpOutPath: str = ""
    # Vector to be considered in the comparison
    colNames2PlotsLabels: Dict[str, str] = {"pct_query_w_raw_hits": "% of queries with one or more hits (unfiltered)", "pct_query_w_usable_hits": "% of queries with one or more hits (filtered)", "pct_query_w_arch": f"% of queries with an architecture."}
    
    xLimits: Tuple[int, int] = (0, 100)
    yLimits: Tuple[int, int] = (0, 100)
    yVectNames: List[str] = list(colNames2PlotsLabels.keys())
    for name in yVectNames:
        ax = sns.relplot(x=dfSeed[name], y=dfFull[name], data=dfSeed, hue="empire", s=mrkSize, legend=not(hideLegend))
        # Add diagnoal line
        ax.axes.flat[0].plot([0, 100], [0, 100], color='grey', marker=None, linestyle='dashed', linewidth=1, markersize=12)
        ax.set(xlim=xLimits)
        ax.set(ylim=yLimits)
        # Set the X and Y labels
        ax.set(xlabel=xLab)
        ax.set(ylabel=yLab)
        ax.set(title=name)
        sns.despine(top=False, right=False, left=False, bottom=False, offset=None, trim=False)
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.relplot.full-VS-seed.{name}.{fmt}")
        plt.savefig(tmpOutPath, dpi=160)

    # Generate the plots for comparing diffent columns (e.g., raw_hits_pct VS arch_pct)
    # Set the colum names
    colPairsList: List[Tuple[str, str]] = [("pct_query_w_raw_hits", "pct_query_w_usable_hits"), ("pct_query_w_raw_hits", "pct_query_w_arch"), ("pct_query_w_usable_hits", "pct_query_w_arch")]
    # Generate the plots for each pair

    for dbType, tmpDf in {"full": dfFull, "seed": dfSeed}.items():
        for xName, yName in colPairsList:
            xLab = colNames2PlotsLabels[xName]
            yLab = colNames2PlotsLabels[yName]
            # print(xName, xLab, yName, yLab)
            ax = sns.relplot(x=tmpDf[xName], y=tmpDf[yName], data=tmpDf, hue="empire", s=mrkSize, legend=not(hideLegend))
            # Add diagnoal line
            ax.axes.flat[0].plot([0, 100], [0, 100], color='grey', marker=None, linestyle='dashed', linewidth=1, markersize=12)
            ax.set(xlim=xLimits)
            ax.set(ylim=yLimits)
            # Set the X and Y labels
            ax.set(xlabel=xLab)
            ax.set(ylabel=yLab)
            # ax.set(title= "MINCHIA")
            sns.despine(top=False, right=False, left=False, bottom=False, offset=None, trim=False)
            tmpOutPath = os.path.join(plotsDir, f"{prefix}.relplot.{dbType}.{xName}.{yName}.{fmt}")
            plt.savefig(tmpOutPath, dpi=160)
            # break

    ''' # DO same thing using scatterplot
    # Try with sns.scatterplot instead
    # ax = sns.scatterplot(x=dfFull[xName], y=dfFull[yName], data=dfFull, hue="empire", s=mrkSize, legend=not(hideLegend))
    # ax = sns.scatterplot(x=dfSeed[xName], y=dfSeed[yName], data=dfSeed, hue="empire", s=mrkSize, legend=not(hideLegend), ax=ax)
    # Add diagnoal line
    # ax.plot([0, 100], [0, 100], color='grey', marker=None, linestyle='dashed', linewidth=1, markersize=12)
    '''



def load_species_info(snapPath:str, metaDataTbl:str) -> Tuple[Dict[str, str], Dict[str, Tuple[int, int, str, str]]]:
    """Load species information from the snapshot file and metadata table.
    """

    # tmp variables
    # Each entry will contain the prot count, proteome size, empire and domain
    spInfoDict: Dict[str, Tuple[int, int, str, str]] = {}
    mappingDict: Dict[str, str] = {}

    tmpTpl: Tuple[int, int, str, str] = (0, 0, "", "")
    protCnt: str = ""
    protSize: str = ""
    spId: str = ""
    spName: str = ""
    empire: str = ""
    domain: str = ""
    flds: List[str] = []

    # Load information from the snapshot file
    # Lines in the snapshot have the following format:
    # sp_id species file_hash protein_cnt proteome_size
    # 6 clupus 84a09b94943956211e53faf2df1c0b76e2459910541ff237b8faaffdd15720c9 20649 11583642
    for ln in open(snapPath, "rt"):
        # println(ln)
        flds = ln.split("\t", 4)
        # @show flds
        # Extract the required information
        spId = flds[0]
        spName = flds[1]
        protCnt = flds[3]
        protSize = flds[4]
        # @show spId spName protCnt protSize
        # Add elements to mapping file
        mappingDict[spId] = spName
        # The last 2 values will updated in later step
        # spInfoDict[spName] = (parse(Int, protCnt), parse(Int, protSize), "", "")
        spInfoDict[spName] = (int(protCnt), int(protSize), "", "")
        # print(spInfoDict[spName])

    # Open the metadafile and skip the hdr
    ifd: TextIO = open(metaDataTbl, "rt")
    ifd.readline()
    # The metadata tables has the following content
    # File Empire Domain Proteome ID Taxonomy ID Species name Proteins Proteome size
    # agambiae eukaryota eukaryota UP000007062 7165 Anopheles gambiae (African malaria mosquito) 12533 6529087
    # Load the taxonomic information
    for ln in ifd:
        # print(ln)
        flds = ln.split("\t", 3)
        # print(flds)
        spName = flds[0]
        empire = flds[1]
        domain = flds[2]
        # print(spName, empire, domain)
        tmpTpl = spInfoDict[spName]
        # print(tmpTpl)
        # update the species information
        spInfoDict[spName] = (tmpTpl[0], tmpTpl[1], empire, domain)
        # print(spInfoDict[spName])
        # break

    ifd.close()

    # sys.exit("DEBUG :: load_species_info")
    # Return the 2 Dictionaries
    return (mappingDict, spInfoDict)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTblFull = os.path.realpath(args.in_tbl_full)
    inTblSeed = os.path.realpath(args.in_tbl_seed)
    outDir = os.path.realpath(args.output_directory)
    snapPath = os.path.realpath(args.snapshot)
    metaDataTbl = os.path.realpath(args.meta_data)
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    hueCol: int = args.hue
    multicolVar: int = args.multi_col_name
    divisor: int = args.divisor
    outFmt: str = args.format
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table PFamA-full: {inTblFull}")
    logging.info(f"Input table PFamA-seed: {inTblSeed}")
    logging.info(f"Snapshot file from SOnicParanoid: {snapPath}")
    logging.info(f"File with meta-data on the input dataset: {metaDataTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Divisor:\t{divisor}")

    # Load species information
    mappingDict, spInfoDict = load_species_info(snapPath=snapPath, metaDataTbl=metaDataTbl)
    # Generate box plots for comparison
    compare_full_and_seed_boxplots(tblFull=inTblFull, tblSeed=inTblSeed, plotsDir=outDir, prefix=prefix, fmt=outFmt)
    # Generate comparison plots
    compare_full_and_seed_relplots(tblFull=inTblFull, tblSeed=inTblSeed, mappingDict=mappingDict, spInfoDict=spInfoDict, plotsDir=outDir, prefix=prefix, mrkSize=20, hideLegend=True, fmt=outFmt)


    ''' Multiplots
    mergedDf = concat_profile_search_dataframes(tblFull=inTblFull, tblSeed=inTblSeed)

    ax = sns.relplot(x="pct_query_w_raw_hits", y="pct_query_w_arch", data=mergedDf, col="pfam_db")
    xLimits: Tuple[int, int] = (0, 100)
    yLimits: Tuple[int, int] = (0, 100)
    ax.set(xlim=xLimits)
    ax.set(ylim=yLimits)
    tmpOutPath = os.path.join(outDir, f"test1.relplot.multicol.{outFmt}")
    plt.savefig(tmpOutPath, dpi=160)
    sys.exit("DEBUG")

    # Plots different columns
    ax = sns.relplot(x="pct_query_w_raw_hits", y="pct_query_w_usable_hits", data=mergedDf, col="pfam_db")
    xLimits: Tuple[int, int] = (0, 100)
    yLimits: Tuple[int, int] = (0, 100)
    ax.set(xlim=xLimits)
    ax.set(ylim=yLimits)
    tmpOutPath = os.path.join(outDir, f"test2.relplot.multicol.{outFmt}")
    plt.savefig(tmpOutPath, dpi=160)
    '''

    sys.exit("DEBUG :: plot_pfam_full_vs_seed.py")



if __name__ == "__main__":
    main()
