#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic136-qfo20-ismb-all
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date

echo $'\n@@@@ sonic136-mmseqs-qfo20-essentials-ismb21-default @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-essentials-ismb21-default -p sonic136-mmseqs-qfo20-essentials-ismb21-ml5-default -ot --threads 128 -m default -op
wait

echo $'\n@@@@ sonic136-mmseqs-qfo20-complete-ismb21-default @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-complete-ismb21-default -p sonic136-mmseqs-qfo20-complete-ismb21-ml5-default -ot --threads 128 -m default -op -ca
wait

echo $'\n@@@@ sonic136-mmseqs-qfo20-essentials-ismb21-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-essentials-ismb21-sensitive -p sonic136-mmseqs-qfo20-essentials-ismb21-ml5-sensitive -ot --threads 128 -m sensitive -op
wait

echo $'\n@@@@ sonic136-mmseqs-qfo20-complete-ismb21-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-complete-ismb21-sensitive -p sonic136-mmseqs-qfo20-complete-ismb21-ml5-sensitive -ot --threads 128 -m sensitive -op -ca
wait

echo $'@@@@ sonic136-mmseqs-qfo20-essentials-ismb21-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-essentials-ismb21-fast -p sonic136-mmseqs-qfo20-essentials-ismb21-ml5-fast -ot --threads 128 -m fast -op
wait

echo $'@@@@ sonic136-mmseqs-qfo20-complete-ismb21-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-complete-ismb21-fast -p sonic136-mmseqs-qfo20-complete-ismb21-ml5-fast -ot --threads 128 -m fast -op -ca
wait

echo $'\n@@@@ sonic136-mmseqs-qfo20-essentials-ismb21-most-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-essentials-ismb21-most-sensitive -p sonic136-mmseqs-qfo20-essentials-ismb21-ml5-most-sensitive -ot --threads 128 -m most-sensitive -op
wait

echo $'\n@@@@ sonic136-mmseqs-qfo20-complete-ismb21-most-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-mmseqs-qfo20-complete-ismb21-most-sensitive -p sonic136-mmseqs-qfo20-complete-ismb21-ml5-most-sensitive -ot --threads 128 -m most-sensitive -op -ca
wait


 #### Diamond runs #####

echo $'@@@@ sonic136-dmnd-qfo20-essentials-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-essentials-fast -p sonic136-dmnd-qfo20-essentials-ismb21-ml5-fast -ot --diamond --threads 128 -m fast -op -noidx
wait

echo $'\n@@@@ sonic136-dmnd-qfo20-essentials-default @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-essentials-default -p sonic136-dmnd-qfo20-essentials-ismb21-ml5-default -ot --diamond --threads 128 -m default -op -noidx
wait

echo $'\n@@@@ sonic136-dmnd-qfo20-essentials-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-essentials-sensitive -p sonic136-dmnd-qfo20-essentials-ismb21-ml5-sensitive -ot --diamond --threads 128 -m sensitive -op -noidx
wait

echo $'\n@@@@ sonic136-dmnd-qfo20-essentials-most-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-essentials-most-sensitive -p sonic136-dmnd-qfo20-essentials-ismb21-ml5-most-sensitive -ot --diamond --threads 128 -m most-sensitive -op -noidx
wait

echo $'@@@@ sonic136-dmnd-qfo20-complete-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-complete-fast -p sonic136-dmnd-qfo20-complete-ismb21-ml5-fast -ot --diamond --threads 128 -m fast -op -ca -noidx
wait

echo $'\n@@@@ sonic136-dmnd-qfo20-complete-default @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-complete-default -p sonic136-dmnd-qfo20-complete-ismb21-ml5-default -ot --diamond --threads 128 -m default -op -ca -noidx
wait

echo $'\n@@@@ sonic136-dmnd-qfo20-complete-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-complete-sensitive -p sonic136-dmnd-qfo20-complete-ismb21-ml5-sensitive -ot --diamond --threads 128 -m sensitive -op -ca -noidx
wait

echo $'\n@@@@ sonic136-dmnd-qfo20-complete-most-sensitive @@@@'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-ismb-all/sonic136-dmnd-qfo20-complete-most-sensitive -p sonic136-dmnd-qfo20-complete-ismb21-ml5-most-sensitive -ot --diamond --threads 128 -m most-sensitive -op -ca -noidx
wait

# All runs completed
date