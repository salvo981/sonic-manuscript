date

# generate training samples and labels from execution times
# echo $'\n@@@@ generate-raw-tr-data using the matissa @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/generate_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/models-manuscript/tables/aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv -s ~/work_repos/sonic-manuscript/binary-classifier-training/models-manuscript/tables/snapshot.250_proteomes.azur.tsv -f ~/Desktop/adaboost_training/reference250processed/ -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/ --mantissa 250

wait
date

# generate training samples and labels from execution times
echo $'\n@@@@ generate-raw-tr-data using categorical labels @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/generate_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/models-manuscript/tables/aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv -s ~/work_repos/sonic-manuscript/binary-classifier-training/models-manuscript/tables/snapshot.250_proteomes.azur.tsv -f ~/Desktop/adaboost_training/reference250processed/ -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/ --categorical

# All done
date