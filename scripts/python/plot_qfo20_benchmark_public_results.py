'''
Generate plots of QfO benchmark results.
The input is a table genetated using the script under scripts/julia/create_qfo_benchmark_datapoints.jl
'''

from __future__ import annotations

import os
import sys
# from typing import TextIO
import logging
import numpy as np
import pandas as pd
import polars as pl
import itertools
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']
rcParams['font.size'] = 9
rcParams['font.weight'] = 'normal'
rcParams['lines.markersize'] = 3
#'weight' : 'bold',
import matplotlib as plt
import matplotlib.pyplot as pyplt
# import oapackage # Use to compute the pareto set of points
import subprocess



########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots QfO benchmark results.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file with datapoints from QfO benchmark results\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=False, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--hue", type=str, required=False, help="Column containing categorical information.", default="")
    parser.add_argument("--multi-col-name", type=str, required=False, help="Column name used to create multiple plots.", default="")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("--fig-width-inches", type=float, required=False, help="Width of the plot in inches.", default=2.7)
    parser.add_argument("--fig-height-inches", type=float, required=False, help="Height of the plot in inches.", default=1.8)
    parser.add_argument("--pareto-script", type=str, required=False, help="Path to the script to compute the pareto frontier.", default="")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def compute_pareto_julia(script: str, inputPointsPath: str, outDir: str, dataType: str = "top-double-double") -> str:
    """
    Compute pareto front calling a julia program
    """
    # Example of execution of the script
    # julia ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl plots/tmp.x.y.tsv --output-dir plots/ -d
    validDataTypes: list[str] = ["top-int-double", "top-double-double", "bottom-int-double", "bottom-double-double"]
    if not dataType in validDataTypes:
        sys.exit(f"\nERROR: not valid data type {dataType}")

    # The file that should been created is named 'pareto.x.y.tsv'
    outPath: str = os.path.join(outDir, "pareto.x.y.tsv")
    # Remove the previous file if it does already exists
    if os.path.isfile(outPath):
        os.remove(outPath)
        print("\nINFO: previous pareto file removed.")

    rOut = subprocess.run(f"julia {script} {inputPointsPath} --output-dir {outDir} --data-type {dataType} -d", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    cmdOut:str = rOut.stdout.decode().rstrip()
    print(cmdOut)

    # Through an error if the file was not created
    if not os.path.isfile(outPath):
        logging.error("The pareto file was not created!")
        sys.exit(-8)
    # Return the path to the pareto front table
    return outPath



def map_method_to_plot_settings(methodName: str) -> tuple[str, str, str, str]:
    """
    Assign a label, color and marker shape to each method.
    ignore list[str]: methods which results should be ignored
    """

    # Methods with results in the QfO 2020 bnchmark web-page as of 22/02/2023
    qfo2020methods: list[str] = ["BBH", "Domainoid+", "Ensembl_Compara", "Hieranoid_2", "InParanoid_Xenfix",
        "MetaPhOrs_v.2.5",
        "OMA_GETHOGs", "OMA_Groups", "OMA_Pairs",
        "OrthoFinder_MSA_v2.5.2",
        "OrthoInspector 3",
        "OrthoMCL",
        "PANTHER_16_all",
        "PANTHER__16__LDO__only",
        "phylomedb_v5",
        "RSD",
        "sonicparanoid", "sonicparanoid-fast", "sonicparanoid-sens", "sonicparanoid-mostsensitive"
        ]

    # Because SP2 runs have the color and shape set manually, these should be part of the index
    sonic2RunNames: list[str] = ["SonicParanoid2", "SonicParanoid2-g", "SonicParanoid2-fast", "SonicParanoid2-fast-g", "SonicParanoid2-sens", "SonicParanoid2-sens-g"]

    if ((methodName in qfo2020methods) or (methodName in sonic2RunNames)) == False:
        sys.stderr.write(f"\nERROR: {methodName} is not a valid participant name for the QfO benchmark.")
        sys.exit(-7)

    #set the shape and color
    nlabel: str = ""
    color: str = ""
    shape: str = ""
    fillStyle: str = "none"
    colIdx: int = 0 # index of the color in the list colors
    # For this kind of Plot 4 Colors are abough
    # https://matplotlib.org/stable/gallery/color/named_colors.html
    # NOTE: tab:blue, tab:orange, tab:olive, black, tab:pink are reserved for SonicParanoid runs
    validColors: list[str] = ["tab:brown", "tab:gray", "tab:red", "tab:cyan", "tab:pink", "tab:orange"]
    
    colCnt: int = len(validColors)

    # TODO: use match case when using Python > 3.9
    if methodName == "BBH":
        nlabel = "BBH"
        shape = "*"
    elif methodName == "Domainoid+":
        nlabel = "Domainoid"
        shape = "d"
    elif methodName == "Ensembl_Compara":
        nlabel = "Ensembl Compara"
        shape = "<"
    elif methodName == "Hieranoid_2":
        nlabel = "Hieranoid2"
        shape = "p"
    elif methodName == "InParanoid_Xenfix":
        nlabel = "InParanoid"
        shape = ">"
    elif methodName == "MetaPhOrs_v.2.5":
        nlabel = "MetaPhOrs v2.5"
        shape = "p"
    elif methodName == "OMA_GETHOGs":
        nlabel = "OMA GETHOGs"
        shape = "^"
    elif methodName == "OMA_Groups":
        nlabel = "OMA Groups"
        shape = "^"
    elif methodName == "OMA_Pairs":
        nlabel = "OMA Pairs"
        shape = "^"
    elif methodName == "OrthoFinder_MSA_v2.5.2":
        nlabel = "OrthoFinder (MSA)"
        shape = "v"
    elif methodName == "OrthoInspector 3":
        nlabel = methodName
        shape = "p"
    elif methodName == "OrthoMCL":
        nlabel = methodName
        shape = "H"
    elif methodName == "PANTHER_16_all":
        nlabel = "PANTHER 16 (all)"
        shape = "X"
    elif methodName == "PANTHER__16__LDO__only":
        nlabel = "PANTHER 16 (LDO)"
        shape = "X"
    elif methodName == "phylomedb_v5":
        nlabel = "PhylomeDB v5"
        shape = "v"
    elif methodName == "RSD":
        nlabel = "RSD"
        shape = "*"
    elif methodName == "sonicparanoid-fast":
        nlabel = "SonicParanoid 1.3 (fast)"
        shape = "P"
    elif methodName == "sonicparanoid":
        nlabel = "SonicParanoid 1.3"
        shape = "P"
    elif methodName == "sonicparanoid-sens":
        nlabel = "SonicParanoid 1.3 (sens)"
        shape = "P"
    elif methodName == "sonicparanoid-mostsensitive":
        nlabel = "SonicParanoid 1.3 (msens)"
        shape = "P"
    elif methodName == "SonicParanoid2":
        nlabel = "SP2"
        color = "tab:olive"
        shape = "o"
    elif methodName == "SonicParanoid2-g":
        nlabel = "SP2 (g)"
        color = "tab:olive"
        shape = "s"
    elif methodName == "SonicParanoid2-fast":
        nlabel = "SP2 (fast)"
        color = "tab:blue"
        shape = "o"
    elif methodName == "SonicParanoid2-fast-g":
        nlabel = "SP2 (fast) (g)"
        color = "tab:blue"
        shape = "s"
    elif methodName == "SonicParanoid2-sens":
        nlabel = "SP2 (sens)"
        color = "black"
        shape = "o"
    elif methodName == "SonicParanoid2-sens-g":
        nlabel = "SP2 (sens) (g)"
        color = "black"
        shape = "s"
    
    # Set the color based on the position of the method in the list
    # Only in the case of Sp2 runs the color is set manually
    if not methodName.startswith("Sonic"): # a SonicParanoid2 result point
        color = validColors[qfo2020methods.index(methodName) % colCnt]

    fillStyle = "full"

    # Return the tuple with the results
    return(nlabel, color, shape, fillStyle)



def plot_qfo_ec_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")
    
    # Set temp variables
    testName: str = "EC"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    # Filter to contain only results for EC test
    tmpDf:pl.DataFrame = df.filter(pl.col("test") == "EC")

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    # The sizes used in the manuscript are
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    ecTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #ecTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    ecTest.set_xlabel("Recall\n(number of orthologous relationships)")
    ecTest.set_ylabel("Precision\n(average Schlicker similarity)")
    ecTest.set_title("Enzyme classification test")
    ecTest.spines["right"].set_visible(False)
    ecTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    ecTest.yaxis.set_ticks_position("left")
    ecTest.xaxis.set_ticks_position("bottom")

    # Iterate through the rows
    # Tuples are returned, but if named=True, a dictionary is returned
    # Using the dictionary would allow us to access the values by column name
    # but it would be more computationally expensive
    # Following are column ins
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        # Use a different mapping function depending on the competitor
        methodName = row[1]
        # print(f"Result line for:\t{methodName}")
        # print(row)
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        print(methodLabel, mycolor, mrkShape, fillingStyle)
        ecTest.errorbar(x=float(row[2]), y=float(row[3]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    # Set limits and legend
    #ecTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    ecTest.set_xlim(80000, 320001)
    start, end = ecTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 60000
    ecTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size X:\t{stepSizeX:.4f}")

    # Set Y limits
    ecTest.set_ylim(0.80, 0.9801)
    start, end = ecTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    ecTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size Y:\t{stepSizeY:.4f}")

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        # Use pandas
        # tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Use polars
        # tmpDf.write_csv(tmpDfPath, sep="\t")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")

        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-int-double")
        # print(tblPareto)
        #plot the pareto line
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        ecTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #ecTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
        print(tmpOutPath)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_ec_results")



def plot_qfo_go_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "GO"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter(pl.col("test") == "GO")

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figures
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    goTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #goTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    goTest.set_xlabel("Recall\n(number of orthologous relationships)")
    goTest.set_ylabel("Precision\n(average Schlicker similarity)")
    goTest.set_title("Gene ontology conservation test")
    goTest.spines["right"].set_visible(False)
    goTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    goTest.yaxis.set_ticks_position("left")
    goTest.xaxis.set_ticks_position("bottom")

    # Iterate through the rows
    # Tuples are returned, but if named=True, a dictionary is returned
    # Using the dictionary would allow us to access the values by column name
    # but it would be more computationally expensive
    # Following are column ins
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")        
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        goTest.errorbar(x=float(row[2]), y=float(row[3]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #goTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    goTest.set_xlim(50000, 400001)
    start, end = goTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    # stepSizeX: int = 15000
    # goTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # print(f"Step size for X:\t{stepSizeX:.4f}")
    # Set Y limits
    goTest.set_ylim(0.4000, 0.5000)
    start, end = goTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.005
    goTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        goTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #goTest.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_go_results")



def plot_qfo_treefam_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "TreeFam-A"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter(pl.col("test") == "TreeFam-A")

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    treefamTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #treefamTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    treefamTest.set_xlabel("Recall\n(true positive rate)")
    treefamTest.set_ylabel("Precision\n(positive predictive value)")
    # treefamTest.set_title("Comparison with TreeFam-A reference genes trees")
    treefamTest.set_title("TreeFam-A")
    treefamTest.spines["right"].set_visible(False)
    treefamTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    treefamTest.yaxis.set_ticks_position("left")
    treefamTest.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")        
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        treefamTest.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    treefamTest.set_xlim(0.35, 0.70)
    start, end = treefamTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeX: float = 0.025
    treefamTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''

    # Set Y limits
    treefamTest.set_ylim(0.800, 0.9751)
    start, end = treefamTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.025
    treefamTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        treefamTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_treefam_results")



def plot_qfo_swisstree_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "SwissTrees"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter(pl.col("test") == "SwissTrees")

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    swisstreeTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #swisstreeTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    swisstreeTest.set_xlabel("Recall\n(true positive rate)")
    swisstreeTest.set_ylabel("Precision\n(positive predictive value)")
    # swisstreeTest.set_title("Comparison with SwissTree reference genes trees")
    swisstreeTest.set_title("SwissTree")
    swisstreeTest.spines["right"].set_visible(False)
    swisstreeTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    swisstreeTest.yaxis.set_ticks_position("left")
    swisstreeTest.xaxis.set_ticks_position("bottom")


    # Set the colors and shapes
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        swisstreeTest.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #swisstreeTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    swisstreeTest.set_xlim(0.46, 0.8201)
    start, end = swisstreeTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 0.04
    swisstreeTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    swisstreeTest.set_ylim(0.82, 0.98)
    start, end = swisstreeTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.020
    swisstreeTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        swisstreeTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_swisstree_results")



def plot_qfo_vgnc_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "VGNC"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter(pl.col("test") == "VGNC")

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    vgncTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #vgncTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    vgncTest.set_xlabel("Recall\n(true positive rate)")
    vgncTest.set_ylabel("Precision\n(positive predictive value)")
    vgncTest.set_title("Agreement with Reference Orthologs from VGNC")
    vgncTest.set_title("VGNC")
    vgncTest.spines["right"].set_visible(False)
    vgncTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    vgncTest.yaxis.set_ticks_position("left")
    vgncTest.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        vgncTest.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    # vgncTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    vgncTest.set_xlim(0.91, 0.9901)
    start, end = vgncTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 0.01
    vgncTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    vgncTest.set_ylim(0.46, 1.01)
    start, end = vgncTest.get_ylim()
    '''
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.020
    vgncTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        vgncTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # vgncTest.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_vgnc_results")



def plot_qfo_gstd_luca_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "G_STD2_Luca"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "G_STD2_Luca") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    gstdLuca1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdLuca1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdLuca1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdLuca1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdLuca1.set_title("Generalized Species Tree Discordance (LUCA)")
    gstdLuca1.set_title("Last Universal Common Ancestor (LUCA)")
    gstdLuca1.spines["right"].set_visible(False)
    gstdLuca1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdLuca1.yaxis.set_ticks_position("left")
    gstdLuca1.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        gstdLuca1.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdLuca1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdLuca1.set_xlim(0, 3501)
    start, end = gstdLuca1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 500
    gstdLuca1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdLuca1.set_ylim(0.200, 0.4201)
    start, end = gstdLuca1.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    gstdLuca1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        gstdLuca1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_gstd_luca_complete_vs_rf")



def plot_qfo_gstd_vertebrata_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "G_STD2_Vertebrata"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "G_STD2_Vertebrata") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    gstdVertebrata1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdVertebrata1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdVertebrata1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdVertebrata1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdVertebrata1.set_title("Generalized Species Tree Discordance (Vertebrata)")
    gstdVertebrata1.set_title("Vertebrata")
    gstdVertebrata1.spines["right"].set_visible(False)
    gstdVertebrata1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdVertebrata1.yaxis.set_ticks_position("left")
    gstdVertebrata1.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        gstdVertebrata1.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdVertebrata1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdVertebrata1.set_xlim(12000, 32001)
    start, end = gstdVertebrata1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 5000
    gstdVertebrata1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdVertebrata1.set_ylim(0.20, 0.5001)
    start, end = gstdVertebrata1.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.05
    gstdVertebrata1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        gstdVertebrata1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # gstdVertebrata1.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_gstd_vertebrata_complete_vs_rf")



def plot_qfo_gstd_eukaryota_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "G_STD2_Eukaryota"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "G_STD2_Eukaryota") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    gstdEukaryota1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdEukaryota1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdEukaryota1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdEukaryota1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdEukaryota1.set_title("Generalized Species Tree Discordance (Eukaryota)")
    gstdEukaryota1.set_title("Eukaryota")
    gstdEukaryota1.spines["right"].set_visible(False)
    gstdEukaryota1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdEukaryota1.yaxis.set_ticks_position("left")
    gstdEukaryota1.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        gstdEukaryota1.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdEukaryota1.set_xlim(800, 7201)
    start, end = gstdEukaryota1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 800
    gstdEukaryota1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdEukaryota1.set_ylim(0.18, 0.3601)
    start, end = gstdEukaryota1.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    gstdEukaryota1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        gstdEukaryota1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_gstd_eukaryota_complete_vs_rf")



def plot_qfo_gstd_fungi_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "G_STD2_Fungi"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "G_STD2_Fungi") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    gstdFungi1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdFungi1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdFungi1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdFungi1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdFungi1.set_title("Generalized Species Tree Discordance (Fungi)")
    gstdFungi1.set_title("Fungi")
    gstdFungi1.spines["right"].set_visible(False)
    gstdFungi1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdFungi1.yaxis.set_ticks_position("left")
    gstdFungi1.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes, and plot points
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        gstdFungi1.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdFungi1.legend(numpoints=1, loc='lower left', frameon=False)

    gstdFungi1.set_xlim(3500, 16001)
    start, end = gstdFungi1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 2500
    gstdFungi1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdFungi1.set_ylim(0.20, 0.375)
    start, end = gstdFungi1.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.01
    gstdFungi1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        gstdFungi1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_gstd_fungi_complete_vs_rf")



def plot_qfo_std_eukaryota_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "STD_Eukaryota"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "STD_Eukaryota") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    stdEukaryota = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdEukaryota.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdEukaryota.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdEukaryota.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdEukaryota.set_title("Species Tree Discordance (Eukaryota)")
    stdEukaryota.set_title("Eukaryota")
    stdEukaryota.spines["right"].set_visible(False)
    stdEukaryota.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdEukaryota.yaxis.set_ticks_position("left")
    stdEukaryota.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes, and plot points
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        stdEukaryota.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")

    # Set limits and legend
    #stdEukaryota.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    stdEukaryota.set_xlim(7000, 15001)
    start, end = stdEukaryota.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 2000
    stdEukaryota.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    stdEukaryota.set_ylim(0.0000, 0.28001)
    start, end = stdEukaryota.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.040
    stdEukaryota.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        stdEukaryota.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # stdEukaryota.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_std_eukaryota_complete_vs_rf")



def plot_qfo_std_fungi_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "STD_Fungi"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "STD_Fungi") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    stdFungi = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdFungi.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdFungi.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdFungi.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdFungi.set_title("Species Tree Discordance (Fungi)")
    stdFungi.set_title("Fungi")
    stdFungi.spines["right"].set_visible(False)
    stdFungi.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdFungi.yaxis.set_ticks_position("left")
    stdFungi.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes, and plot points
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        stdFungi.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")

    # Set X limits
    stdFungi.set_xlim(4000, 12000)
    start, end = stdFungi.get_xlim()
    '''
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 750
    stdFungi.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''

    # Set Y limits
    stdFungi.set_ylim(0.28, 0.42)
    start, end = stdFungi.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.01
    stdFungi.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        stdFungi.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # stdFungi.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_std_fungi_complete_vs_rf")



def plot_qfo_std_bacteria_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "STD_Bacteria"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    tmpDf:pl.DataFrame = df.filter((pl.col("test") == "STD_Bacteria") & (pl.col("xlabel") == "NR_COMPLETED_TREE_SAMPLINGS") & (pl.col("ylabel") == "RF_DISTANCE"))
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    # Setup figure
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    stdBacteria = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdBacteria.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdBacteria.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdBacteria.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdBacteria.set_title("Species Tree Discordance (Bacteria)")
    stdBacteria.set_title("Bacteria")
    stdBacteria.spines["right"].set_visible(False)
    stdBacteria.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdBacteria.yaxis.set_ticks_position("left")
    stdBacteria.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes, and plot points
    for row in tmpDf.iter_rows(named=False):
        # ['test', 'method', 'x', 'y', 'xerr', 'yerr', 'xlabel', 'ylabel']
        methodName = row[1]
        logging.debug(f"Result line for:\t{methodName}")
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_to_plot_settings(methodName=methodName)
        stdBacteria.errorbar(x=float(row[2]), y=float(row[3]), xerr=float(row[4]), yerr=float(row[5]), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")

    # Set limits and legend
    #stdBacteria.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    stdBacteria.set_xlim(400, 1800)
    start, end = stdBacteria.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeX: int = 200
    stdBacteria.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''

    # Set Y limits
    stdBacteria.set_ylim(0.47, 0.7201)
    start, end = stdBacteria.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.05
    stdBacteria.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.select(pl.col(["x", "y"])).write_csv(tmpDfPath, sep="\t")        
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        dfPareto = pl.read_csv(tblPareto, sep="\t")
        # print(dfPareto)
        stdBacteria.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # stdFungi.legend(numpoints=1, loc='lower left', frameon=False)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')
    # sys.exit("DEBUG :: plot_qfo_std_bacteria_complete_vs_rf")



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



def sp2_on_top(df: pl.DataFrame) -> pl.DataFrame:
    """
    Sort the dataframe entries so that SP2 results are appear in the bottom.
    This would allow for the datapoints to be printed always on top
    """
    lenDf: int = len(df)
    print(f"Initial size for df:\t{lenDf:d}")
    print(df)
    # Extract Sp1 results
    dfSp1: pl.DataFrame = df.filter(pl.col("method").str.contains("sonicparanoid"))
    # Sort the dataframe
    dfSp1 = dfSp1.sort(["method"], descending=[False])
    lenDfSp1: int = len(dfSp1)
    print(f"Datapoints for SP1:\t{lenDfSp1:d}")
    # remove Sp1 runs from original df
    df = df.filter(pl.col("method").str.contains("sonicparanoid")==False)

    # Do the same for Sp2 results
    dfSp2: pl.DataFrame = df.filter(pl.col("method").str.contains("SonicParanoid"))
    # Sort the dataframe
    # using descending=[True] the datapoints for hybrid runs will printed last and not overlapped
    dfSp2 = dfSp2.sort(["method"], descending=[True])
    lenDfSp2: int = len(dfSp2)
    print(f"Datapoints for SP2:\t{lenDfSp2:d}")
    # remove Sp2 runs from original df
    df = df.filter(pl.col("method").str.contains("SonicParanoid")==False)
    print(f"Datapoints for DF (without SP records):\t{len(df):d}")

    # add the records to the original dataframe
    df = pl.concat([df, dfSp1, dfSp2])
    # make sure the number of record in df is correct
    # the sum of the record must be same as in the original df
    lenDfFinal: int = len(df)
    if lenDf != lenDfFinal:
        logging.error(f"The length of the modified datatframe ({lenDfFinal:d}) must be the as the original one ({lenDf:d})")
        sys.exit(-10)

    # sys.exit("DEBUG :: sp2_on_top")
    
    return df



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    paretoScript = os.path.abspath(args.pareto_script)
    # methods = args.methods
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    outFmt: str = args.format
    hueCol: int = args.hue
    multicolVar: int = args.multi_col_name
    figWidth: float = args.fig_width_inches
    figHeight: float = args.fig_height_inches
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)
    makedir(plotsDir)

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Pareto script:\t{paretoScript}")
    logging.info(f"Marker size:\t{markerSize}")
    logging.info(f"Figure width (inches):\t{figWidth}")
    logging.info(f"Figure height (inches):\t{figHeight}")

    # Read dataframe
    # The hdr of datapoint file have the following format:
    # method test run_type aligner sensitivity x y xerr yerr xlabel ylabel
    # df = pd.read_csv(inTbl, sep="\t", dtype= {"method": str, "test": str, "run_type": str, "aligner": str, "sensitivity": str, "x": np.float32, "y": np.float32, "xerr": np.float32, "yerr": np.float32, "xlabel": str, "ylabel": str}, engine="c")
    # Read using polars (instead of pandas)
    # df = pl.read_csv(inTbl, sep="\t")
    df = pl.read_csv(inTbl, sep="\t", dtypes={"test": pl.Utf8, "method": pl.Utf8, "x": pl.Float64, "y": pl.Float64, "xerr": pl.Float64, "yerr": pl.Float64, "xlabel": pl.Utf8, "ylabel": pl.Utf8})
    logging.info(f"Datapoints loaded:\t{len(df)}")
    
    # print(df.dtypes)
    
    # Show or hide X and Y labels
    hideXlab = True
    hideYlab = True

    # Ignore the results for the following methods in the list below
    # methods_to_ignore: list[str] = ["phylomedb_v5"]
    methods_to_ignore: list[str] = []
    logging.warning(f"Results for the following methods will be ignored:\n{methods_to_ignore}")

    # Remove the entried of the methods to ignore from the dataframe
    if len(methods_to_ignore) > 0:
        removedCnt: int = 0
        for m in methods_to_ignore:
            logging.warning(f"Removing datapoints for {m} from the dataframe")
            df = df.filter(pl.col("method") != m)
            removedCnt += len(df)
            logging.info(f"The number of datapoints after the update is {len(df)}.")
        logging.warning(f"{removedCnt:d} datapoints were removed from the input datapoints table.")
    
    # Sort the dataset so that SP2 results are last, and always printed on top of competitors
    df = sp2_on_top(df)
    logging.info(f"Datapoints that will be used for plotting:\t{len(df)}")

    '''
    '''
    # Plot benchmark points
    # EC Test
    plot_qfo_ec_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GO Test
    plot_qfo_go_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # TreeFam-A Test
    plot_qfo_treefam_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # SwissTree Test
    plot_qfo_swisstree_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # VGNC agreement test
    plot_qfo_vgnc_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)

    ### GSTD tests ###
    # GSTD Luca Test
    plot_qfo_gstd_luca_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Vertebrate Test
    plot_qfo_gstd_vertebrata_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Eukaryota Test
    plot_qfo_gstd_eukaryota_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Fungi Test
    plot_qfo_gstd_fungi_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)

    ### STD tests ###
    # STD Eukaryota Test
    plot_qfo_std_eukaryota_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # STD Fungi Test
    plot_qfo_std_fungi_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # STD Bacteria Test
    plot_qfo_std_bacteria_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    sys.exit("DEBUG :: plot_qfo_benchmark_results_sonic2_vs_all")



if __name__ == "__main__":
    main()
