This directory included log files fro extra runs perfomed using the 2000 MAGs dataset.  
These runs include:
- ProteinOrtho (default)
- OrthoFinder

#### TLDR
OrthoFinder failed due to "too many open files limit" (which also happened with the 200 euka dataset).  
In the case of the 200 euka dataset we were able to increase the `nfile` limit to a little more than million. This was not possible for the 2,000 MAGs dataset which requires 40M alignments. The problem is related to of2 opening too many files.  
This problem was alleviated 2.5.5 which required n files to be opened instead of n^2
https://github.com/davidemms/OrthoFinder/issues/571
We are not sure how this would affect the performance of of2...
https://github.com/davidemms/OrthoFinder/issues/384
We tested the run with of2.5.5 and we still get the same error as in the attached log file.

ProteinOrtho failed due poor parsing of input sequences. Specifically, it considers not valid protein sequences which contain the `translation stop` character `*` (see https://en.wikipedia.org/wiki/FASTA_format for details).  
A similar issue regarding sequence parsing was previously closed for ProteinOrtho.  
https://gitlab.com/paulklemm_PHD/proteinortho/-/issues/77  
It seems the check they do should be applied only to FASTA headers, nevertheless it also applies (mistakenly) to the actual sequences, which can start with '*' if the sequence includes a new line every 60aa.
We believe this is unfornate, since in our option, ProteinOrtho could have proven to be the most scalable.  
We are also proving the MAG file in which po6 failed.