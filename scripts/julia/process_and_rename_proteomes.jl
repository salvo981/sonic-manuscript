#=
Process raw proteomes downloaded from Uniprot
and rename the files based on a table with metadata
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Try loading the module
using ProcessProteomes



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "raw-prot-dir"
            nargs = 1                        # eats up one arguments; puts the result in a Vector
            help = "Directory with proteomes to be processed."
            required = true
        "meta-data"
            nargs = 1
            help = "Path to the meta-data table file. "
            required = true
        "--output-dir"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Directory with processed files"
            required = false
        "--uniprot-id-col"
            nargs = '?'
            arg_type = Int
            default = 1
            help = "Column contaning the uniprot ID of the files to be processed"
            required = false
        "--tax-col"
            nargs = '?'
            arg_type = Int
            default = 2
            help = "Column contaning the taxonomy ID of the files to be processed"
            required = false
        "--threads", "-t"
            # nargs = '?'
            arg_type = Int
            default = 4
            help = "Number of parallel processes"
            required = false
        "--output-tbl"
            nargs = '?'
            arg_type = String
            default = ""
            help = "Table with extra information, including sequence count and proteome size"
            required = false
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
                end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

rawProtsDir = realpath(args["raw-prot-dir"][1])
metaDataTbl = realpath(args["meta-data"][1])
outDir = realpath(args["output-dir"])
protIdCol = args["uniprot-id-col"]
outTbl = args["output-tbl"]
writeOutMetatbl = false
if length(outTbl) > 0
    global outTbl = abspath(outTbl)
    writeOutMetatbl = true
    ofd = open(outTbl, "w")
end

taxIdCol = args["tax-col"]
threads = args["threads"]
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Uniprot proteomes will be processed using the following parameters:" rawProtsDir metaDataTbl outDir protIdCol taxIdCol outTbl threads

# addprocs(threads)

# Set some tmp variables
newFileName = ""
rawFileName = tmpPath = unzippedPath = outPath = ""
processingDir = joinpath(outDir, "tmp_dir")
uid = ""
taxid = ""
flds = Array{String, 1}()
# tmpFlds = String[]
makedir(outDir)
makedir(processingDir)

# Create a Set to avoid that the same file is processed multiple times
rawFileSet = Set()
newFileSet = Set()

# Start reading the file with metadata
# The header of the metadata might have this format
# File Empire Domain Proteome ID Taxonomy ID Species name
for ln in eachline(open(metaDataTbl, "r"), keep=false)
    if ln[1:4] == "File"
        if writeOutMetatbl
            write(ofd, "$(ln)\tProteins\tProteome size\n")
        end
        continue
    end
    # Lines in the meta-data have the following format
    # nequitans prokaryote archaea UP000000578 228908 Nanoarchaeum equitans (strain Kin4-M)
    global flds = split(ln, "\t", limit=6)
    # @show flds
    global newFileName = flds[1]
    global uid = flds[protIdCol]
    global taxid = flds[taxIdCol]
    global rawFileName = "$(uid)_$(taxid).fasta.gz"
    # make sure that the file was not previosuly processed
    if rawFileName ∉ rawFileSet
        push!(rawFileSet, rawFileName)
    else
        @error "File processed multiple times" rawFileName
        exit(-5)
    end

    # make sure that the new file is uniq
    if newFileName ∉ newFileSet
        push!(newFileSet, newFileName)
    else
        @error "Repeated output file name!" newFileName
        exit(-5)
    end

    global tmpPath = joinpath(rawProtsDir, rawFileName)
    global unzippedPath = joinpath(processingDir, "$(uid)_$(taxid).fasta")
    global outPath = joinpath(processingDir, newFileName)
    # println("$(uid)\t$(taxid)")
    # unzip and process the file
    if !isfile(tmpPath)
        @error "The uniprot input file is missing:" tmpPath uid taxid
        exit(-2)
    end
    pigz_decompress(tmpPath, unzippedPath, keep=true, force=true, errLog=false, threads=threads)
    simplify_uniprot_hdr(unzippedPath, outPath)
    # Extract sequence stats if required
    if writeOutMetatbl
        fstats = compute_fasta_stats(outPath)
        write(ofd, "$(ln)\t$(fstats.cnt)\t$(fstats.totlen)\n")
    end

    # Remove temporary files and move processed files
    rm(unzippedPath, force=true)
    mv(outPath, joinpath(outDir, newFileName), force=true)

    # break
end


@show length(rawFileSet)

# Close the output table if required
if writeOutMetatbl
    close(ofd)
end

# Remove the processing directory
rm(processingDir, force=true)