This directory contains the raw training data generated using the script 

`~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/bash/generate_raw_training_data.sh`

Which in turns uses  
`~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/generate_raw_tr_data.py`