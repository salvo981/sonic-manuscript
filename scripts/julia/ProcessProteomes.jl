module ProcessProteomes

include("modules/seqtools.jl")
include("modules/systools.jl")



# expose the methods
export extract_file_from_tar,
    is_archive,
    makedir,
    pigz_decompress,
    simplify_uniprot_hdr,
    simplify_gem_mag_hdr,
    FastaStats,
    compute_fasta_stats



end # end module

