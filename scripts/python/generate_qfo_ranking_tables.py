'''
Parse QfO classification result tables from a raw tables obtained form OpenEBench.
The output table contains the counts (and ratios) of ohw many time the method obtain 1st, 2nd, 3rd or 4th quartile.
'''

import os
import sys
from typing import TextIO, NamedTuple
import logging
import numpy as np
import pandas as pd

class Ranks(NamedTuple):
    """Contains the counts and ratios obtaine din the classifiction"""
    counts: tuple[int, int, int, int]
    ratios: tuple[float, float, float, float]

class Names(NamedTuple):
    """Defines long and shortname for a given qfo2020 method"""
    name: str
    shortname: str



########### FUNCTIONS ############
def get_params():
    """Obtain parameters."""
    import argparse
    parser_usage = "\nProvide the RAW tables from OpenEBench.\n"
    parser = argparse.ArgumentParser(description="Generate counts for rankings.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("--square-quartile-raw", type=str, required=True, help="Raw square-quartile classification file\n", default=None)
    parser.add_argument("--diagonal-quartile-raw", type=str, required=True, help="Raw diagonal-quartile classification file\n", default=None)
    parser.add_argument("--kmeans-raw", type=str, required=True, help="Raw kmeans classification file\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=True, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)


'''
def create_method_names_mapping_qfo2020() -> dict[str, Names]:
    """Generate a mapping of method names, with short and long versions.
    """

    # Create a dictionary mapping all the method names
    # which includes the short name were possible
    # Names: namedtuple = namedtuple("Names", "name shortname")
    qfo2020methodNameMap: dict[str, Names] = {}

    # Create the named tuple for names
    qfo2020methodNameMap["BBH"] = Names("BBH", "BBH")
    qfo2020methodNameMap["Domainoid"] = Names("Domainoid", "Domainoid")
    qfo2020methodNameMap["Ensembl Compara"] = Names("Ensembl Compara", "Ensembl Compara")
    qfo2020methodNameMap["Hieranoid2"] = Names("Hieranoid2", "Hieranoid2")
    qfo2020methodNameMap["InParanoid"] = Names("InParanoid", "InParanoid")
    qfo2020methodNameMap["MetaPhOrs"] = Names("MetaPhOrs v2.5", "MetaPhOrs v2.5")
    qfo2020methodNameMap["OMA GETHOGs"] = Names("OMA GETHOGs", "OMA GETHOGs")
    qfo2020methodNameMap["OMA Groups"] = Names("OMA Groups", "OMA Groups")
    qfo2020methodNameMap["OMA Pairs"] = Names("OMA Pairs", "OMA Pairs")
    qfo2020methodNameMap["OrthoFinder"] = Names("OrthoFinder (MSA)", "OrthoFinder (MSA)")
    qfo2020methodNameMap["OrthoInspector"] = Names("OrthoInspector", "OrthoInspector")
    qfo2020methodNameMap["OrthoMCL"] = Names("OrthoMCL", "OrthoMCL")
    qfo2020methodNameMap["PANTHER All"] = Names("PANTHER 16 (all)", "PANTHER 16 (all)")
    qfo2020methodNameMap["PANTHER LDO"] = Names("PANTHER 16 (LDO)", "PANTHER 16 (LDO)")
    qfo2020methodNameMap["PhylomeDB"] = Names("PhylomeDB v5", "PhylomeDB v5")
    qfo2020methodNameMap["RDS"] = Names("RDS", "RDS")
    qfo2020methodNameMap["SonicParanoid"] = Names("SonicParanoid 1.3", "SP v1.3")
    qfo2020methodNameMap["SonicParanoid-fast"] = Names("SonicParanoid 1.3 (fast)", "SP v1.3 (fast)")
    qfo2020methodNameMap["SonicParanoid-sens"] = Names("SonicParanoid 1.3 (sens)", "SP v1.3 (sens)")
    qfo2020methodNameMap["SonicParanoid-mostsens"] = Names("SonicParanoid 1.3 (most-sens)", "SP v1.3 (most-sens)")
    qfo2020methodNameMap["SonicParanoid2"] = Names("SonicParanoid2", "SP2")
    qfo2020methodNameMap["SonicParanoid2-fast"] = Names("SonicParanoid2 (fast)", "SP2 (fast)")
    qfo2020methodNameMap["SonicParanoid2-sens"] = Names("SonicParanoid2 (sens)", "SP2 (sens)")    
    qfo2020methodNameMap["SonicParanoid2-g"] = Names("SonicParanoid2 (g)", "SP2 (g)")
    qfo2020methodNameMap["SonicParanoid2-fast-g"] = Names("SonicParanoid2 (g)(fast)", "SP2 (g)(fast)")
    qfo2020methodNameMap["SonicParanoid2-sens-g"] = Names("SonicParanoid2 (g)(sens)", "SP2 (g)(sens)")

    return qfo2020methodNameMap
'''



def create_method_names_mapping_qfo2022() -> dict[str, Names]:
    """Generate a mapping of method names, with short and long versions for QfO 2022 challenge.
    """

    # Create a dictionary mapping all the method names
    # which includes the short name were possible
    # Names: namedtuple = namedtuple("Names", "name shortname")
    qfo2022methodNameMap: dict[str, Names] = {}

    # Create the named tuple for names
    qfo2022methodNameMap["BBH_(SW_alignments)"] = Names("BBH (Smith-Waterman)", "BBH")
    qfo2022methodNameMap["Domainoid+"] = Names("Domainoid", "Domainoid+")
    qfo2022methodNameMap["Ensembl_Compara"] = Names("Ensembl Compara", "Ensembl Compara")
    qfo2022methodNameMap["FastOMA_v0.1.4_nov"] = Names("FastOMA", "FastOMA (v0.1.4)")
    qfo2022methodNameMap["Hieranoid_2"] = Names("Hieranoid2", "Hieranoid2")
    qfo2022methodNameMap["InParanoid5"] = Names("InParanoid", "InParanoid5")
    qfo2022methodNameMap["MetaPhOrs_v2.5"] = Names("MetaPhOrs v2.5", "MetaPhOrs")
    qfo2022methodNameMap["OMA_HOGs"] = Names("OMA HOGs", "OMA HOGs")
    qfo2022methodNameMap["OMA_Groups"] = Names("OMA Groups", "OMA Groups")
    qfo2022methodNameMap["OMA_Pairs"] = Names("OMA Pairs", "OMA Pairs")
    qfo2022methodNameMap["OrthoFFGC"] = Names("OrthoFFGC", "OrthoFFGC")
    qfo2022methodNameMap["OrthoInspector_3.5"] = Names("OrthoInspector", "OrthoInspector (v3.5)")
    qfo2022methodNameMap["OrthoFinder"] = Names("OrthoFinder", "OrthoFinder2 (MSA)")
    qfo2022methodNameMap["OrthoMCL"] = Names("OrthoMCL", "OrthoMCL")
    qfo2022methodNameMap["PANTHER_(v18)_all"] = Names("PANTHER 16 (all)", "PANTHER (all)")
    qfo2022methodNameMap["PANTHER_(v18)_LDO"] = Names("PANTHER 18 (LDO)", "PANTHER (LDO)")
    qfo2022methodNameMap["phylomedb_v5"] = Names("PhylomeDB v5", "PhylomeDB v5")
    qfo2022methodNameMap["RSD_(SW-Alignments)"] = Names("RDS (Smith-Waterman)", "RDS")
    qfo2022methodNameMap["SonicParanoid2-sens"] = Names("SonicParanoid2 (sens)", "SP2 (sens)")
    qfo2022methodNameMap["SonicParanoid2-sens-g"] = Names("SonicParanoid2 (g)(sens)", "SP2 (g)(sens)")


    return qfo2022methodNameMap



'''
def map_method_name(methodName: str, namesMapping: dict[str, Names], shortNames: bool) -> str:
    """Modify the method based on the what is used in the plot lables"""

    if not methodName in namesMapping:
        sys.stderr.write(f"\nERROR: {methodName} is not a valid participant name for the QfO benchmark.")
        sys.exit(-7)

    # return the method name
    if shortNames:
        return namesMapping[methodName].shortname
    else:
        return namesMapping[methodName].name
'''



def process_raw_table(inTbl: str, outdir: str, outname: str) -> dict[str, Ranks]:
    """Process a table with RAW counts from OEBench.
    Write a TSV file containing the counts and ratios for each method.
    """
    logging.debug(f"\nprocess_raw_table :: START")
    logging.debug(f"Input table: {inTbl}")
    logging.debug(f"Output dir: {outdir}")
    logging.debug(f"Output file name: {outname}")

    # Contain the ranks for each method
    rankings: dict[str, Ranks] = {}
    # number of tests for each method
    # this must be same in each line
    testsCnt: int = 0
    totq1: int = 0
    totq2: int = 0
    totq3: int = 0
    totq4: int = 0
    ratio_q1: float = 0
    ratio_q2: float = 0
    ratio_q3: float = 0
    ratio_q4: float = 0
    methodRawName: str = ""
    methodName: str = ""
    flds: list[str] = []
    validEntries: list[str] = ["Q1", "Q2", "Q3", "Q4", ]

    methodNameMapping: dict[str, Names] = create_method_names_mapping_qfo2022()

    # The raw table must contain tab-separated lines with the classification
    # BBH_(SW_alignments) Q4 Q4 Q3 Q3 Q4 Q4 Q3 Q2 Q3 Q3 Q2 Q2 Q4 Q4 Q2 Q2 Q4 Q4 Q4 Q4 Q4 Q4 Q3 Q3 Q4 Q4 Q3 Q2 Q3 Q3 Q4 Q4 Q1
    # Domainoid+ Q1 Q2 Q1 Q1 Q2 Q1 Q2 Q1 Q2 Q2 Q1 Q1 Q2 Q2 Q1 Q1 Q2 Q2 Q1 Q1 Q3 Q3 Q1 Q1 Q3 Q3 Q2 Q2 Q2 Q2 Q1 Q1 Q1

    # Verify that the table can be loaded using pandas
    # this is verify that each line has the same number of columns
    df = pd.read_csv(inTbl, sep="\t", header=None)
    methodsCnt: int = df.shape[0]
    # The fist column should be the method name
    testsCnt: int = df.shape[1] - 1

    with open(inTbl, "rt") as ifd:
        for ln in ifd:
            flds = ln.rstrip("\n").split("\t")
            methodRawName = flds[0]
            if methodRawName in validEntries:
                sys.exit(f"The method name ({methodRawName}) should not be a classification. The input raw table is probably malformed.")
            
            # update the name of the method
            methodName = methodNameMapping[methodRawName][0]
            # extract the classifications
            for q in flds[1:]:
                if q == "Q1":
                    totq1 += 1
                elif q == "Q2":
                    totq2 += 1
                elif q == "Q3":
                    totq3 += 1
                elif q == "Q4":
                    totq4 += 1
                else:
                    sys.exit(f"The classification in the table ({q}) is not a valid one.")

            if (totq1 + totq2 + totq3 + totq4) != testsCnt:
                print(flds)
                sys.exit(f"There are {testsCnt} expected but only {totq1 + totq2 + totq3 + totq4} were found in the line.")

            # Set the ratio values
            ratio_q1 = totq1 / testsCnt
            ratio_q2 = totq2 / testsCnt
            ratio_q3 = totq3 / testsCnt
            ratio_q4 = totq4 / testsCnt
            # add the method to the dictionary
            rankings[methodName] = Ranks(counts=(totq1, totq2, totq3, totq4), ratios=(ratio_q1, ratio_q2, ratio_q3, ratio_q4))
            # reset the variables
            totq1 = 0
            totq2 = 0
            totq3 = 0
            totq4 = 0
            ratio_q1 = 0
            ratio_q2 = 0
            ratio_q3 = 0
            ratio_q4 = 0

    # Print the count and ranks
    # for m, ranks in rankings.items():
    #     print(f"{m}:\t{ranks}")

    # Write the output table
    outpath: str = os.path.join(outdir, outname)
    ofd = open(outpath, "wt")

    # The hdr of datapoint file have the following format:
    # method i-cnt ii-cnt iii-cnt iv-cnt i-ratio ii-ratio iii-ratio iv-ratio tests-cnt
    ofd.write("method\ti-cnt\tii-cnt\tiii-cnt\tiv-cnt\ti-ratio\tii-ratio\tiii-ratio\tiv-ratio\ttests-cnt\n")

    for m, ranks in rankings.items():
        # print(f"{m}:\t{ranks}")
        ofd.write(f"{m}\t{ranks.counts[0]}\t{ranks.counts[1]}\t{ranks.counts[2]}\t{ranks.counts[3]}") 
        ofd.write(f"\t{ranks.ratios[0]:.2f}\t{ranks.ratios[1]:.2f}\t{ranks.ratios[2]:.2f}\t{ranks.ratios[3]:.2f}\t") 
        ofd.write(f"{sum(ranks.counts)}\n") 

    ofd.close()

    logging.debug("\nExtraction summary:")
    logging.debug(f"Methods:\t{methodsCnt}")
    logging.debug(f"Tests count:\t{testsCnt}")

    # sys.exit("process_raw_table :: DEBUG")

    return rankings



def merge_tables(sqDict: dict[str, Ranks], dqDict: dict[str, Ranks], kmeansDict: dict[str, Ranks], outdir: str, outname: str) -> dict[str, Ranks]:
    """Merge the 3 tables with ranks.
    """
    logging.debug(f"\nmerge_tables :: START")
    logging.debug(f"Square quartile ranks: {len(sqDict)}")
    logging.debug(f"Diagonal quartile ranks: {len(dqDict)}")
    logging.debug(f"Kmeans ranks: {len(kmeansDict)}")
    logging.debug(f"Output dir: {outdir}")
    logging.debug(f"Output file name: {outname}")

    if (len(kmeansDict) != len(sqDict)) or (len(kmeansDict) != len(dqDict)):
        sys.exit("The dictionaries must contain the same number of entries")

    rankings: dict[str, Ranks] = {}
    tmpCntTpl: tuple[int, int, int, int] = (0, 0, 0, 0)
    tmpRatioTpl: tuple[float, float, float, float] = (0., 0., 0., 0.)
    tmpIntArray = np.array([])
    # tmpFloatArray = np.array([])
    for m in sqDict.keys():
        tmpIntArray = np.array(sqDict[m].counts) + np.array(dqDict[m].counts) + np.array(kmeansDict[m].counts)
        tmpFloatArray = np.array(sqDict[m].ratios) + np.array(dqDict[m].ratios) + np.array(kmeansDict[m].ratios)

        tmpCntTpl = tuple(tmpIntArray)
        # print(tmpCntTpl, sum(tmpCntTpl))
        tmpRatioTpl = tuple(tmpIntArray/sum(tmpIntArray))
        # print(tmpRatioTpl, sum(tmpRatioTpl))    
        # add the values in the dictionary
        rankings[m] = Ranks(tmpCntTpl, tmpRatioTpl)

    # Write the output table
    outpath: str = os.path.join(outdir, outname)
    ofd = open(outpath, "wt")

    # The hdr of datapoint file have the following format:
    # method i-cnt ii-cnt iii-cnt iv-cnt i-ratio ii-ratio iii-ratio iv-ratio tests-cnt
    ofd.write("method\ti-cnt\tii-cnt\tiii-cnt\tiv-cnt\ti-ratio\tii-ratio\tiii-ratio\tiv-ratio\ttests-cnt\n")

    for m, ranks in rankings.items():
        # print(f"{m}:\t{ranks}")
        ofd.write(f"{m}\t{ranks.counts[0]}\t{ranks.counts[1]}\t{ranks.counts[2]}\t{ranks.counts[3]}") 
        ofd.write(f"\t{ranks.ratios[0]:.2f}\t{ranks.ratios[1]:.2f}\t{ranks.ratios[2]:.2f}\t{ranks.ratios[3]:.2f}\t") 
        ofd.write(f"{sum(ranks.counts)}\n") 

    ofd.close()

    return rankings



def merge_tables_no_square_quartiles(dqDict: dict[str, Ranks], kmeansDict: dict[str, Ranks], outdir: str, outname: str) -> dict[str, Ranks]:
    """Merge the 3 tables with ranks.
    """
    logging.debug(f"\nmerge_tables_no_square_quartiles :: START")
    logging.debug(f"Diagonal quartile ranks: {len(dqDict)}")
    logging.debug(f"Kmeans ranks: {len(kmeansDict)}")
    logging.debug(f"Output dir: {outdir}")
    logging.debug(f"Output file name: {outname}")

    if len(kmeansDict) != len(dqDict):
        sys.exit("The dictionaries must contain the same number of entries")

    rankings: dict[str, Ranks] = {}
    tmpCntTpl: tuple[int, int, int, int] = (0, 0, 0, 0)
    tmpRatioTpl: tuple[float, float, float, float] = (0., 0., 0., 0.)
    tmpIntArray = np.array([])
    # tmpFloatArray = np.array([])
    for m in dqDict.keys():
        tmpIntArray = np.array(dqDict[m].counts) + np.array(kmeansDict[m].counts)
        tmpCntTpl = tuple(tmpIntArray)
        # print(tmpCntTpl, sum(tmpCntTpl))
        tmpRatioTpl = tuple(tmpIntArray/sum(tmpIntArray))
        # print(tmpRatioTpl, sum(tmpRatioTpl))    
        # add the values in the dictionary
        rankings[m] = Ranks(tmpCntTpl, tmpRatioTpl)

    # Write the output table
    outpath: str = os.path.join(outdir, outname)
    ofd = open(outpath, "wt")

    # The hdr of datapoint file have the following format:
    # method i-cnt ii-cnt iii-cnt iv-cnt i-ratio ii-ratio iii-ratio iv-ratio tests-cnt
    ofd.write("method\ti-cnt\tii-cnt\tiii-cnt\tiv-cnt\ti-ratio\tii-ratio\tiii-ratio\tiv-ratio\ttests-cnt\n")

    for m, ranks in rankings.items():
        # print(f"{m}:\t{ranks}")
        ofd.write(f"{m}\t{ranks.counts[0]}\t{ranks.counts[1]}\t{ranks.counts[2]}\t{ranks.counts[3]}") 
        ofd.write(f"\t{ranks.ratios[0]:.2f}\t{ranks.ratios[1]:.2f}\t{ranks.ratios[2]:.2f}\t{ranks.ratios[3]:.2f}\t") 
        ofd.write(f"{sum(ranks.counts)}\n") 

    ofd.close()

    return rankings




def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    sqrawtbl = os.path.realpath(args.square_quartile_raw)
    dqrawtbl = os.path.realpath(args.diagonal_quartile_raw)
    kmeansrawtbl = os.path.realpath(args.kmeans_raw)
    outDir = os.path.realpath(args.output_directory)
    prefix: str = args.prefix
    # create output directories
    makedir(outDir)

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Square quartile classification raw table: {sqrawtbl}")
    logging.info(f"Diagonal quartile classification raw table: {dqrawtbl}")
    logging.info(f"K-means classification raw table: {kmeansrawtbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Output prefix:\t{prefix}")

    # Prepare processing
    sqTblName: str = "square-quartile-ranking.tsv"
    dqTblName: str = "diagonale-quartile-ranking.tsv"
    kmeansTblName: str = "kmeans-ranking.tsv"
    aggregateTblName: str = "aggregate-ranking.tsv"
    aggregateNoSquareQuarTblName: str = "aggregate-ranking-no-square-quartiles.tsv"

    if prefix != "":
        sqTblName = f"{prefix}-{sqTblName}"
        dqTblName = f"{prefix}-{dqTblName}"
        kmeansTblName = f"{prefix}-{kmeansTblName}"
        aggregateTblName = f"{prefix}-{aggregateTblName}"
        aggregateNoSquareQuarTblName = f"{prefix}-{aggregateNoSquareQuarTblName}"

    # Process the tables
    sqDict: dict[str, Ranks] = process_raw_table(inTbl=sqrawtbl, outdir=outDir, outname=sqTblName)
    dqDict: dict[str, Ranks] = process_raw_table(inTbl=dqrawtbl, outdir=outDir, outname=dqTblName)
    kmeansDict: dict[str, Ranks] = process_raw_table(inTbl=kmeansrawtbl, outdir=outDir, outname=kmeansTblName)
    # Merge the tables
    merge_tables(sqDict, dqDict, kmeansDict, outdir=outDir, outname=aggregateTblName)
    # Merge tables ignoring square-quartiles
    merge_tables_no_square_quartiles(dqDict, kmeansDict, outdir=outDir, outname=aggregateNoSquareQuarTblName)


    sys.exit("DEBUG :: generate_qfo_ranking_tables.py")



if __name__ == "__main__":
    main()
