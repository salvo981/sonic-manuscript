#!/bin/bash

#PBS -S /usr/bin/bash
#PBS -N sonic2-review-sp2-vs-of2-48cpus
#PBS -l ncpus=48
#PBS -V
#PBS -M salvocos@k.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /ssd_home/sp2-genome-biology-review-runs/qsub_logs/stderr
#PBS -o /ssd_home/sp2-genome-biology-review-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /ssd_home/sp2-genome-biology-review-runs/

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# of2 -> OrthoFinder2
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
of2_cmd=~/work_repos/OrthoFinder_v254/orthofinder.py
fasttree_cmd=/usr/local/bin/FastTree
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=qfo20
cpu_cnt=48

# Output directories
output_root=$root\output/
mkdir $output_root
sp_output_root=$output_root\sp2/
mkdir $sp_output_root
of2_output_root=$output_root\of2/
mkdir $of2_output_root

# SonicParanoid2 runs
sp_mode=default
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus

echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json  \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# OrthoFinder Runs
# IMPORTANT: OrthoFinder usually crashes becaus eit opens too many files
# https://github.com/davidemms/OrthoFinder/issues/384
# In order to mitigate this issue increase the limits (if possible)
# $prlimit # check the current limits for NOFILE
# Change the Hard and Soft limits for NOFILE
# This should be enough for up to 1k species
# $ulimit -Hn 1048576
# $ulimit -Sn 1048576

of2_mode=default
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus

# OrthoFinder run Default
echo $'\n@@@@'$' OrthoFinder runs '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json \"python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt \> $of2_output_root\log.$of2_run_name_prefix.txt 2\> $of2_output_root\err.$of2_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json "python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt > $of2_output_root\log.$of2_run_name_prefix.txt 2> $of2_output_root\err.$of2_run_name_prefix.txt"
wait

# All runs completed
echo $'\n'
date