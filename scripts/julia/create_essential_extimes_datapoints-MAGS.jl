using Base: iscontiguous, Float64
#=
Combine execution time files for complete and essential alignments of MAGS.
To obtain a master datapoint table to be used for plotting.
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using DataStructures: OrderedDict
using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using ProcessEssentialExtimes



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "raw-extimes-ca"
            nargs = 1
            arg_type = String
            help = "TSV file with execution times from a complete run."
            required = true
        "raw-extimes-ra"
            nargs = 1
            arg_type = String
            help = "TSV file with execution times from a run with essentials."
            required = true
        "snapshot"
            nargs = 1
            arg_type = String
            help = "TSV snapshot file from SonicParanoid run."
            required = true
        "--aai-tbl"
            nargs = 1
            arg_type = String
            help = "TSV snapshot file with AAI values for the dataset computed using Comparem. "
            required = true
        "--meta-data"
            nargs = 1
            arg_type = String
            help = "Path to the meta-data table file. "
            required = true
        "--pred-tbl"
            nargs = '?'
            arg_type = String
            help = "TSV table with the Adaboost predictions."
            required = true
        "--threads", "-t"
            # nargs = '?'
            arg_type = Int
            default = 4
            help = "Number of parallel processes"
            required = false
        "--output-tbl"
            nargs = '?'
            arg_type = String
            default = ""
            help = "Table with extra information, including sequence count and proteome size"
            required = true
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""
Check the difference in magnitude between two values.
This can be used for example to compare length difference,
or the arch sizes

Return a tuple of type (1, 1.5) where 1 indicates that the xDiffThr was surpassed.
The last value says how many times one value is smaller (or bigger) than the other (e.g., 2X)
If the first value is 0 then the threshold was not surpassed
"""
function check_magnitude_difference(v1::Float64, v2::Float64, xDiffThr::Float64)::Tuple{Int, Float64}
    # @debug "check_magnitude_difference :: START" v1 v2 xDiffThr

    # simply return that it should be kept
    if v1 == v2
        return(0, 1.)
    else
        # how many times v2 is bigger than v1
        v1VSv2::Float64 = 1.
        # this should be a negative multiplier if v1 is bigger than v2
        # consider which value is the biggest:
        if v2 < v1
            v1VSv2 = round(v1/v2, digits=2)
            # @show v1VSv2
        else # v2 is bigger
            v1VSv2 = -round(v2/v1, digits=2)
            # @show v1VSv2
        end
        # Check if the threhold has been surpassed
        # and return the output tuple accordingly
        if abs(v1VSv2) > xDiffThr # drop it
            # if v1VSv2 < 0
            #     @debug "Debug @ check_magnitude_difference" v1VSv2, abs(v1VSv2)
            #     exit(-10)
            # end
            return(1, v1VSv2)
        else # keep it
            # @show (v1VSv2, abs(v1VSv2))
            return(0, v1VSv2)
        end
    end
end



"""Generate datapoints combining execution times (complete and reduce), with species info and AAI values
"""
function generate_datapoints(extimeCaDict::OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}}, extimeRaDict::OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}}, spInfoDict::OrderedDict{String, Tuple{Int, Int, String, String, String}}, aaiDict::OrderedDict{String, Tuple{String, String, String, String}}, outTbl::String)::Nothing
    @debug "generate_datapoints :: START" length(extimeCaDict) length(extimeRaDict) length(spInfoDict) length(aaiDict) outTbl

    # The following information should be in the final table
    # pair prot-cnt-a prot-cnt-b prot-size-a prot-size-b tot-time-ca tot-time-ra speedup pctA pctB avgPct prediction fastest correct-prediction ortho-cnt mean-aai std-aai ortho-fraction-comparem empire-a empire-b empire-class domain-a domain-b domain-class
    # taxa-class should be one among eukaryote, prokaryote, mixed

    # Make sure the counts on each dictionary match the required combinations
    spCnt::Int = length(spInfoDict)
    processedCnt::Int = 0
    requiredCombinations::Int = spCnt * ((spCnt - 1) / 2.)
    # @show spCnt, requiredCombinations

    # Check that number of entries is correct for complete alignments
    if length(extimeCaDict) != 2 * requiredCombinations
        @error "The number of entries in extime for complete alignments is wrong" length(extimeCaDict) requiredCombinations
        exit(-5)
    end
    # Check that number of entries is correct for essential alignments
    if length(extimeRaDict) != 2 * requiredCombinations
        @error "The number of entries in extime for essential alignments is wrong" length(extimeRaDict) requiredCombinations
        exit(-5)
    end
    # Check that number of entries is same in both dictionaries
    if length(extimeRaDict) != length(extimeCaDict)
        @error "The number of entries in from both alignment tables must be the same" length(extimeRaDict) length(extimeCaDict)
        exit(-5)
    end
    # Check that number of entries is correct for AAI dictionary
    if length(aaiDict) != requiredCombinations
        @error "The number of entries in dict for AAI is wrong" length(aaiDict) requiredCombinations
        exit(-5)
    end
    
    # Tmp variables
    # flds::Array{String, 1} = String[]
    spA::String = spB = ""
    taxaA::String = taxaB = habitatL1A = habitatL1B = habitatL2A = habitatL2B = taxaType = habitatL1Type = habitatL2Type = ""

    protCntA::Int = protCntB = protSizeA = protSizeB = 0
    totextimeCa::Float64 = totextimeRa = speedup = pctA = pctB = 0.
    pairAai::String = pairAaiInv = ""
    # Flag that indicates that the pair was computed as the fastest (the first of the 2)
    # predictedFastest::Int = isFastest = isCorrectPrediction = 0
    isFirstPair::Bool = false
    aaiTpl::Tuple{String, String, String, String} = ("", "", "", "")
    extimeFirstCaTpl::Tuple{Float64, Float64, Float64, Int, Int, Int} = (0., 0., 0., 0, 0, 0)
    extimeLastCaTpl::Tuple{Float64, Float64, Float64, Int, Int, Int} = (0., 0., 0., 0, 0, 0)
    extimeFirstRaTpl::Tuple{Float64, Float64, Float64, Int, Int, Int} = (0., 0., 0., 0, 0, 0)
    extimeLastRaTpl::Tuple{Float64, Float64, Float64, Int, Int, Int} = (0., 0., 0., 0, 0, 0)
    # spInfoTpl::Tuple{Int, Int, String, String} = (0, 0, "", "")
    outStr::String = ""
    # Create output directory
    makedir(abspath(dirname(outTbl)))
    # Create the output file and write the hdr
    ofd::IO = open(outTbl, "w")
    hdr::String = "pair\tprot-cnt-a\tprot-cnt-b\tprot-size-a\tprot-size-b\ttot-time-ca\ttot-time-ra\tspeedup\tpctA\tpctB\tavgPct\tfastest\tprediction\tcorrect-prediction\tortho-cnt\tmean-aai\tstd-aai\tortho-fraction-comparem\ttaxa-a\ttaxa-b\ttaxa-type\thabitat-l1-a\thabitat-l1-b\thabitat-l1-type\thabitat-l2-a\thabitat-l2-b\thabitat-l2-type"
    write(ofd, "$(hdr)\n")

    # This must contain the values stored for pairAai in extimeCaDict
    isFastest::Int = predictedFastest = isCorrectPrediction = 0
    # The pairs in the AAI dictionary will be used as reference
    for (k, aaiTpl) in aaiDict
        processedCnt += 1
        spA, spB = split(k, "-", limit=2)
        pairAai = k
        pairAaiInv = "$(spB)-$(spA)"
        # Extract info regarding the species
        protCntA, protSizeA, taxaA, habitatL1A, habitatL2A = spInfoDict[spA]
        protCntB, protSizeB, taxaB, habitatL1B, habitatL2B = spInfoDict[spB]
        # Extract infor about prediction and fastest pair
        isFastest, predictedFastest, isCorrectPrediction = extimeCaDict[pairAai][end-2:end]

        # Set taxa-type
        if taxaA == taxaB
            taxaType = taxaA
        else
            taxaType = "mixed"
        end
        # Set habitatL1Type
        if habitatL1A == habitatL1B
            habitatL1Type = habitatL1A
        else
            habitatL1Type = "mixed"
        end
        # Set habitatL2Type
        if habitatL2A == habitatL2B
            habitatL2Type = habitatL2A
        else
            habitatL2Type = "mixed"
        end

        # println("\n$(pairAai)")
        # @show pairAai, aaiTpl
        # println(aaiTpl)
        # Check if it the first pair computed
        if extimeCaDict[pairAai][5] == 1
            isFirstPair = true
        else
            isFirstPair = false
        end
        
        #=
        if extimeCaDict[pairAai][5] != predictedFastest
            @error "The values for the prediction must be the same!" extimeCaDict[pairAai][5] predictedFastest
        end
        =#
        
        if isFirstPair
            # Extract the extime for the second pair
            extimeFirstCaTpl = extimeCaDict[pairAai]
            extimeLastCaTpl = extimeCaDict[pairAaiInv]
            extimeFirstRaTpl = extimeRaDict[pairAai]
            extimeLastRaTpl = extimeRaDict[pairAaiInv]
            # Extract the reduction percentages
            # NOTE: spA and spB need to be inverted as the reduction happens in the second alignment
            pctA = extimeLastRaTpl[3]
            pctB = extimeLastRaTpl[2]
            avgPct = round( (pctA + pctB)/2., digits=2)
            #=
            @show (pairAai, extimeFirstCaTpl)
            @show (pairAaiInv, extimeLastCaTpl)
            @show (pairAai, extimeFirstRaTpl)
            @show (pairAaiInv, extimeLastRaTpl)
            @show (pctA, pctB)
            =#
        else
            extimeFirstCaTpl = extimeCaDict[pairAaiInv]
            extimeLastCaTpl = extimeCaDict[pairAai]
            extimeFirstRaTpl = extimeRaDict[pairAaiInv]
            extimeLastRaTpl = extimeRaDict[pairAai]
            # Extract the reduction percentages
            pctA = extimeLastRaTpl[2]
            pctB = extimeLastRaTpl[3]
            avgPct = round( (pctA + pctB)/2., digits=2)
            
            #=
            @warn "The pair do not corresponds to the first alignment, do something" pairAai
            @show (pairAaiInv, extimeFirstCaTpl)
            @show (pairAai, extimeLastCaTpl)
            @show (pairAaiInv, extimeFirstRaTpl)
            @show (pairAai, extimeLastRaTpl)
            @show (pctA, pctB)
            # exit(-7)
            =#
        end

        # Compute the total execution times
        totextimeCa = round(extimeFirstCaTpl[1] + extimeLastCaTpl[1], digits=2)
        # Note that we use the extime of the complete alingment for the fastest pair
        totextimeRa = round(extimeFirstCaTpl[1] + extimeLastRaTpl[1], digits=2)
        speedup = check_magnitude_difference(totextimeCa, totextimeRa, 0.)[2]
        
        
        # Create the output string
        # pair prot-cnt-a prot-cnt-b prot-size-a prot-size-b tot-time-ca tot-time-ra speedup pctA pctB avgPct fastest prediction correct-prediction ortho-cnt mean-aai std-aai ortho-fraction-comparem empire-a empire-b empire-class domain-a domain-b domain-class
        outStr = "$(pairAai)\t$(protCntA)\t$(protCntB)\t$(protSizeA)\t$(protSizeB)\t$(totextimeCa)\t$(totextimeRa)\t$(speedup)\t$(pctA)\t$(pctB)\t$(avgPct)\t$(isFastest)\t$(predictedFastest)\t$(isCorrectPrediction)\t$(aaiTpl[1])\t$(aaiTpl[2])\t$(aaiTpl[3])\t$(aaiTpl[4])\t$(taxaA)\t$(taxaB)\t$(taxaType)\t$(habitatL1A)\t$(habitatL1B)\t$(habitatL1Type)\t$(habitatL2A)\t$(habitatL2B)\t$(habitatL2Type)\n"

        write(ofd, "$(outStr)")
        #=
        # if processedCnt == 4
        #     break
        # end
        =#
    end
    
    close(ofd)
    # @info "DEBUG :: generate_datapoints"
    # exit(-10)

    @debug "Pairs processed and written in output" processedCnt
    return nothing
    
end



"""Load average amino acid ideantity (AAI) for each pair.
"""
function load_aai(aaiTbl::String)::OrderedDict{String, Tuple{String, String, String, String}}
    @debug "load_aai :: START" aaiTbl

    # AII tables generate using comparem have the following content
    # #Genome A Genes in A Genome B Genes in B # orthologous genes Mean AAI Std AAI Orthologous fraction (OF)
    # aaeolicus 1553 pfalciparum 5388 78 39.17 7.96 5.02
    flds::Array{String, 1} = String[]
    spA::String = spB = orthoCnt = aai = aaiStd = orthoFraction = ""
    pairMain::String = ""
    # Counter for inter species pairs detected excluding inter species alignments
    interSpCnt::Int = 0

    # Dictionaries with AAI
    # The key will be the pair
    # The fields are the following:
    # cnt_orthologs
    # mean_aai
    # std_aai
    # ortholog_fraction: fraction of orthologs shared between the 2 proteomes
    # aaiDict::OrderedDict{String, Tuple{Int, Float64, Float64, Float64}} = OrderedDict{String, Tuple{Int, Float64, Float64, Float64}}()
    aaiDict::OrderedDict{String, Tuple{String, String, String, String}} = OrderedDict{String, Tuple{String, String, String, String}}()

    # Open the input file and skip the HDR
    ifd::IO = open(aaiTbl, "r")
    readline(ifd)
    # Start processing the execution time for complete alignments
    for ln in eachline(ifd)
        flds = split(ln, "\t", limit=8)
        spA = flds[1]
        spB = flds[3]
        interSpCnt += 1
        pairMain = "$(spA)-$(spB)"
        # println("\n$(pairMain)")
        # println(flds)
        aaiDict[pairMain] = (flds[5], flds[6], flds[7], flds[8])
        # @show pairMain, aaiDict[pairMain]
        # if interSpCnt == 3
        #     break
        # end

    end

    close(ifd)

    @debug "AAI loaded:" length(aaiDict)
    return (aaiDict)

end



"""Load execution times from both complete and essential alignments.
"""
function load_ex_times(extimeCaPath::String, extimeRaPath::String, mappingDict::OrderedDict{String, String}, predDict::Dict{String, Int8})::Tuple{OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}}, OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}}}
    @debug "load_ex_times :: START" extimeCaPath extimeRaPath length(mappingDict) length(predDict)

    # Execution time tables have the following content
    # pair extime conversion_time parsing_time waiting_time pctA pctB essential_creation_time used_cpus
    # 6-5 58.580 0.130 0.320 0.0 25.54 13.15 0.214 1
    flds::Array{String, 1} = String[]
    spA::String = spB = spIdA = spIdB = ""
    pairMain::String = pairInv = ""
    # Counter for inter species pairs detected excluding inter species alignments
    slowCnt::Int = fastCnt = 0
    # Flag that indicates that the pair was computed as the fastest (the first of the 2)
    predictedFastest::Int = isFastest = isCorrectPrediction = 0
    extime::Float64 = extimeFirstAln = pctA = pctB = 0.
    tmpTpl::Tuple{Float64, Float64, Float64, Int, Int, Int} = (0., 0., 0., 0, 0, 0, )

    # Dictionaries with execution times
    # The field are the following:
    # extime
    # pct_A used
    # pct_B used
    # fastest pair: if it is the actual fastest pair among the 2
    # predicted fastest: prediction from the module
    # right prediction: 1 if the prediction is correct
    extimeCaDict::OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}} = OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}}()
    extimeRaDict::OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}} = OrderedDict{String, Tuple{Float64, Float64, Float64, Int, Int, Int}}()
    essPairsSet::Set{String} = Set{String}()

    # Start processing the execution time for complete alignments
    for ln in eachline(extimeCaPath)
        # println(ln)
        flds = split(ln, "\t", limit=9)
        spIdA, spIdB = split(flds[1], "-", limit=2)
        # Skip intra species alignments
        if spIdA == spIdB
            # println("Skipping $(flds[1])")
            continue
        end

        spA = mappingDict[spIdA]
        spB = mappingDict[spIdB]
        pairMain = "$(spA)-$(spB)"
        pairInv = "$(spB)-$(spA)"
        # println("\n$(pairMain)")
        # println(flds)
        pctA = parse(Float64, flds[6])
        pctB = parse(Float64, flds[7])
        extime = parse(Float64, flds[2])
        # @show (pairMain, extime, pctA, pctB)

        # Extract the prediction
        if haskey(predDict, pairMain)
            predictedFastest = predDict[pairMain]
        else
            predictedFastest = abs(predDict[pairInv] - 1)
        end

        if haskey(extimeCaDict, pairInv)
            
            # @debug "Essential (slow) pair found!" pairMain
            # Check the actual fastest pair
            tmpTpl = extimeCaDict[pairInv]
            extimeFirstAln = tmpTpl[1]
            if extimeFirstAln <= extime
                isFastest = 0 # The second pair is the slowest
            else
                isFastest = 1
            end
            
            # Set the value for the right prediction
            if isFastest == predictedFastest
                isCorrectPrediction = 1
            else
                isCorrectPrediction = 0
            end

            # Add the values to the dictionary
            extimeCaDict[pairMain] = (extime, pctA, pctB, isFastest, predictedFastest, isCorrectPrediction)
            # Update the values for the first pair
            extimeCaDict[pairInv] = (tmpTpl[1], tmpTpl[2], tmpTpl[3], abs(isFastest - 1), tmpTpl[5], isCorrectPrediction)
            # @show pairInv, extimeCaDict[pairInv]
            # @show pairMain, extimeCaDict[pairMain]
            fastCnt += 1
        else
            # @debug "Fast pair found!" pairMain
            # These values will be updated once the second alignment time is processed
            extimeCaDict[pairMain] = (extime, pctA, pctB, -1, predictedFastest, -1)
            # push!(essPairsSet, pairInv)
            slowCnt += 1
        end

    end

    @debug "Counts for complete aln run" slowCnt fastCnt

    slowCnt = fastCnt = 0
    # Start processing the execution time for essential alignments
    for ln in eachline(extimeRaPath)
        # println(ln)
        flds = split(ln, "\t", limit=9)
        spIdA, spIdB = split(flds[1], "-", limit=2)
        # Skip intra species alignments
        if spIdA == spIdB
            # println("Skipping $(flds[1])")
            continue
        end
        # Increse the number of processed pairs
        spA = mappingDict[spIdA]
        spB = mappingDict[spIdB]
        pairMain = "$(spA)-$(spB)"
        pairInv = "$(spB)-$(spA)"
        pctA = parse(Float64, flds[6])
        pctB = parse(Float64, flds[7])
        extime = parse(Float64, flds[2])

        # Extract the prediction
        if haskey(predDict, pairMain)
            predictedFastest = predDict[pairMain]
        else
            predictedFastest = abs(predDict[pairInv] - 1)
        end

        if haskey(extimeRaDict, pairInv)
            # Extract info about the prediction correctness
            # From the dictionary with complete alignment times
            isCorrectPrediction = extimeCaDict[pairInv][6]
            tmpTpl = extimeRaDict[pairInv]
            # Add the values to the dictionary
            extimeRaDict[pairMain] = (extime, pctA, pctB, -1, predictedFastest, isCorrectPrediction)
            # Update the values for the first pair
            extimeRaDict[pairInv] = (tmpTpl[1], tmpTpl[2], tmpTpl[3], -1, tmpTpl[5], isCorrectPrediction)
            # @show pairInv, extimeRaDict[pairInv]
            # @show pairMain, extimeRaDict[pairMain]
            fastCnt += 1
            
        else
            # @debug "Fast pair found!" pairMain
            # These values will be updated once the second alignment time is processed
            extimeRaDict[pairMain] = (extime, pctA, pctB, -1, predictedFastest, -1)
            slowCnt += 1
            
        end

    end

    @debug "Counts for reduced (or slow) aln run" slowCnt fastCnt
    @debug "Extimes loaded for complete and essential alignments:" length(extimeCaDict) length(extimeRaDict)
    return (extimeCaDict, extimeRaDict)

end



"""
    load_predictions(predTbl::String, id2spMapping::OrderedDict{String, String})::Nothing

Load predictions made by adaboost model.

The species names in the prediction tables will be mapped using the dictionary id2spMapping.
Return a dictionary with a binary prediction for each proteome pair.
"""
function load_predictions(predTbl::String, id2spMapping::OrderedDict{String, String})::Dict{String, Int8}
    @debug "load_predictions :: START" predTbl length(id2spMapping)

    # Each entry will contain the prot count, proteome size, empire and domain
    predDict::Dict{String, Int8} = Dict{String, Int8}()
    flds::Array{String, 1} = String[]
    tmpPred::Int8 = 0
    # Open the file with prediction and skip the hdr
    ifd::IO = open(predTbl, "r")
    readline(ifd)
    spA::String = spB = remappedPair = ""

    # Load information from the prediction table
    # Lines in the table have the following format:
    # pair seq_cnt_a seq_cnt_b seq_cnt_diff_folds_b_gt_a proteome_size_a proteome_size_b prot_size_diff_folds_b_gt_a avg_seq_len_a avg_seq_len_b avg_seq_len_diff_folds_b_gt_a pred
    # 1-2 1553 12533 8.070 488987 6529087 13.352 314.866 520.952 1.655 1
    for ln in eachline(ifd)
        # println(ln)
        flds = split(ln, "\t", limit=2)
        # @show flds
        # Extract the required information
        spA, spB = split(flds[1], "-", limit=2)
        remappedPair = "$(id2spMapping[spA])-$(id2spMapping[spB])"
        # Extract the last char and convert to integer
        tmpPred = parse(Int8, flds[2][end])
        # Fill the output dictionary
        predDict[remappedPair] = tmpPred
    end
    close(ifd)

    @debug "Predictions loaded:" length(predDict)

    return predDict
end



"""Load species information from the snapshot file and metadata table.
"""
function load_species_info(snapPath::String, metaDataTbl::String)::Tuple{OrderedDict{String, String}, OrderedDict{String, Tuple{Int, Int, String, String, String}}}
    @debug "load_species_info :: START" snapPath metaDataTbl

    # tmp variables
    # Each entry will contain the prot count, proteome size, empire and domain
    spInfoDict::OrderedDict{String, Tuple{Int, Int, String, String, String}} = OrderedDict{String, Tuple{Int, Int, String, String, String}}()
    mappingDict::OrderedDict{String, String} = OrderedDict{String, String}()
    tmpTpl::Tuple{Int, Int, String, String, String} = (0, 0, "", "", "")
    protCnt::String = ""
    protSize::String = ""
    spId::String = ""
    spName::String = ""
    taxa::String = ""
    habitatL1::String = ""
    habitatL2::String = ""
    flds::Array{String, 1} = String[]

    # Load information from the snapshot file
    # Lines in the snapshot have the following format:
    # sp_id species file_hash protein_cnt proteome_size
    # 6 clupus 84a09b94943956211e53faf2df1c0b76e2459910541ff237b8faaffdd15720c9 20649 11583642
    for ln in eachline(snapPath)
        # println(ln)
        flds = split(ln, "\t", limit=5)
        # @show flds

        # Extract the required information
        spId = flds[1]
        # The metatable for MAGs does not include the "faa" extensions,
        # hence it should be removed
        spName = split(flds[2], ".", limit=2)[1]
        protCnt = flds[4]
        protSize = flds[5]

        # @show spId spName protCnt protSize
        # Add elements to mapping file
        mappingDict[spId] = spName
        # The last 2 values will updated in later step
        spInfoDict[spName] = (parse(Int, protCnt), parse(Int, protSize), "", "", "")
        # @show spInfoDict[spName]
    end

    # Open the metadafile and skip the hdr
    ifd::IO = open(metaDataTbl, "r")
    readline(ifd)
    # The metadata tables has the following content
    # genome_id	metagenome_id genome_length num_contigs n50 num_16s num_5s num_23s num_trna completeness contamination quality_score mimag_quality otu_id ecosystem ecosystem_category ecosystem_type habitat longitude latitude proteins proteome_size
    # 3300013755_4 3300013755 1646175 293 5854 0 0 0 28 72.11 1.11 66.56 MQ OTU-2226 d__Bacteria;p__Bacteroidota;c__Bacteroidia;o__Bacteroidales;f__Dysgonomonadaceae;g__UBA4179;s__GCA_002347785.1 Terrestrial Oil reservoir Crude oil 124.15 45.5 1595 497251


    # Load the taxonomic information
    for ln in eachline(ifd)
        # @show ln
        flds = split(ln, "\t", limit=19)
        spName = flds[1]
        # Extract the following info
        # ecosystem ecosystem_category ecosystem_type habitat
        # Example: d__Bacteria;p__Bacteroidota;c__Bacteroidia;o__Bacteroidales;f__Dysgonomonadaceae;g__UBA4179;s__GCA_002347785.1
        taxa = split(flds[15], ";", limit=2)[1]
        # remove the 'd__'
        taxa = split(taxa, "__", limit=2)[2]
        # @show taxa
        # Example: Terrestrial
        habitatL1 = flds[16]
        # Example: Oil reservoir
        habitatL2 = flds[17]
        # @show taxa habitatL1 habitatL2
        tmpTpl = spInfoDict[spName]
        # @show tmpTpl
        # update the species information
        spInfoDict[spName] = (tmpTpl[1], tmpTpl[2], taxa, habitatL1, habitatL2)
        # @show spInfoDict[spName]
        # break
    end
    close(ifd)

    # Return the 2 Dictionaries
    return (mappingDict, spInfoDict)

end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

extimeCaPath = realpath(args["raw-extimes-ca"][1])
extimeRaPath = realpath(args["raw-extimes-ra"][1])
snapPath = realpath(args["snapshot"][1])
metaDataTbl = realpath(args["meta-data"][1])
aaiTbl = realpath(args["aai-tbl"][1])
predTbl = realpath(args["pred-tbl"])
outTbl = abspath(args["output-tbl"])
threads = args["threads"]
debug = args["debug"]

#=
outTbl = args["output-tbl"]
writeOutMetatbl = false
if length(outTbl) > 0
    global outTbl = abspath(outTbl)
    writeOutMetatbl = true
    ofd = open(outTbl, "w")
end

taxIdCol = args["tax-col"]
=#

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Execution times will be processed using the following parameters:" extimeCaPath extimeRaPath snapPath aaiTbl metaDataTbl predTbl outTbl threads

# Load species information and snapshot
mappingDict, spInfoDict = load_species_info(snapPath, metaDataTbl)

#=
for (k, v) in mappingDict
    @show k, v, spInfoDict[v]
end
=#

# Load predictions ~2 million predictions
predDict = load_predictions(predTbl, mappingDict)
#=
for (k, v) in predDict
    @show k, v
end
=#

# Load AAI values
aaiDict = load_aai(aaiTbl)
#=
for (k, v) in aaiDict
    @show k, v
end
=#

# Load execution times
extimeCaDict, extimeRaDict = load_ex_times(extimeCaPath, extimeRaPath, mappingDict, predDict)

# Merge the information to generate the final table with datapoints
generate_datapoints(extimeCaDict, extimeRaDict, spInfoDict, aaiDict, outTbl)
#=
=#


