'''
Train adaboost model optimized using GridSearch.
It requires in input a table with the final training samples,
and a file with the training labels.
'''

import os
import sys
import numpy as np
import pandas as pd
import pickle
from typing import TextIO, List, Set, Dict, Tuple
import logging
from itertools import combinations

from sklearn.preprocessing import normalize
from sklearn.preprocessing import LabelBinarizer
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_validate



def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    import argparse
    parser = argparse.ArgumentParser(description="adaboost_grid_search",  usage="%(prog)s --tr-set <TRAINING_SET> --labels <LABELS> -o <OUTPUT_DIRECTORY> --cv-steps <K-FOLDS> --threads <CPUS>", prog="train_adaboost_grid_search.py")

    # Mandatory arguments
    parser.add_argument("--tr-set", type=str, required=True, help="TSV file containing the training samples.")
    parser.add_argument("--labels", type=str, required=True, help="Text file with a single colum containing the training labels.")
    parser.add_argument("-o", "--output-dir", type=str, required=True, help="The directory in which the generated models, and results will be stored.", default=os.getcwd())

    # General options
    parser.add_argument("-cv", "--cv-steps", type=int, required=False, help="Number of CV steps to be performd. Default=10", default=10)
    parser.add_argument("--max-depth", type=int, required=False, help="Max depth for the decision trees used as stomp for adaboost", default=1)
    parser.add_argument("-t", "--threads", type=int, required=False, help="Number of parallel 1-CPU jobs to be used. Default=4", default=4)
    parser.add_argument("-d", "--debug", required=False, help="Output debug information. (WARNING: extremely verbose)", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



def main():
    #Get the parameters
    args = get_params()[0]
    # set main directories and file paths
    trPath: str = os.path.realpath(args.tr_set)
    labelsPath: str = os.path.realpath(args.labels)
    # Set the other output paths
    outDir: str = os.path.realpath(args.output_dir)
    modelsDir: str = os.path.join(outDir, "models")
    logsDir: str = os.path.join(outDir, "logs")

    # obtain the rest of the parameters for the training
    cvSteps = args.cv_steps
    dtMaxDepth: int = args.max_depth
    threads = args.threads
    debug = args.debug

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    makedir(outDir)
    makedir(modelsDir)
    makedir(logsDir)

    # Show some info
    logging.info("Training Adaptive boosting model with the following parameters:")
    logging.info(f"Training table path: {trPath}")
    logging.info(f"Labels file: {labelsPath}")
    logging.info(f"Output directory: {modelsDir}")
    logging.info(f"Logs directory: {logsDir}")
    logging.info(f"CV-folds:\t{cvSteps}")
    logging.info(f"Max depth for decision tree:\t{dtMaxDepth}")
    logging.info(f"Threads:\t{threads}")

    # check that the training set exists
    if not (os.path.isfile(trPath)):
        logging.error("The file with training samples is not valid.\n")
        sys.exit(-2)

    # check that the training labels
    if not (os.path.isfile(labelsPath)):
        sys.stderr.write("\nERROR: The file with the labels is not a valid.\n")
        sys.exit(-2)
    # Output directory
    # check that the training set exists
    if not (os.path.isdir(modelsDir)):
        # create the output directory
        makedir(modelsDir)
        logging.info(f"The trained models will be stored in\n{modelsDir}\n")


    # Prepare samples and labels
    df = pd.read_csv(trPath, sep="\t")
    features = df.values
    # load labels
    df = pd.read_csv(labelsPath, sep="\t")
    labels = df.values
    labels = labels.ravel()
    del df

    # Print some info on the training data
    print(f"\nTraining samples:\t{features.shape[0]}")
    print(f"Training features:\t{features.shape[1]}")
    print(f"Training labels:\t{labels.shape[0]}")

    print("\nNormalizing training samples...")
    normFeat = normalize(features, norm="l2", axis=0, copy=True, return_norm=False)
    print("Normalization DONE!")

    #Perform grid search
    # parameters to be tuned
    param_grid = {"learning_rate": [0.8, 0.9, 1., 1.1, 1.2, 1.3], "n_estimators": range(50, 255, 5), "algorithm": ["SAMME.R"]}
    # param_grid = {"learning_rate": [1.1], "n_estimators": range(50, 150, 5), "algorithm": ["SAMME.R"]}

    # HACK: restore this if not working
    # model = AdaBoostClassifier(random_state=None)
    model = AdaBoostClassifier(DecisionTreeClassifier(max_depth=dtMaxDepth))
    model.fit(normFeat, labels)
    bestAda = GridSearchCV(model, param_grid, cv=cvSteps, n_jobs=threads, return_train_score=True, error_score=np.nan)
    bestAda.fit(normFeat, labels)

    # write a file with the results
    gridSearchPath = os.path.join(logsDir, f"grid_search_adaboost_depth{dtMaxDepth}_{len(param_grid['n_estimators'])}x{len(param_grid['learning_rate'])}x{len(param_grid['algorithm'])}_{normFeat.shape[1]}features_CV{cvSteps}.tsv")

    ofd = open(gridSearchPath, "w")
    ofd.write("model_rank\tmax_depth_stomp\testimators\talgorithm\tlearning_rate\tmean_train_accuracy\tstd_train_accuracy\tmean_test_accuracy\tstd_test_accuracy\n")

    # extract the required info
    paramList = bestAda.cv_results_["params"]
    meanTestAccList = bestAda.cv_results_["mean_test_score"]
    stdTestAccList = bestAda.cv_results_["std_test_score"]
    meanTrAccList = bestAda.cv_results_["mean_train_score"]
    stdTrAccList = bestAda.cv_results_["std_train_score"]
    rankList = bestAda.cv_results_["rank_test_score"]

    for i, params in enumerate(paramList):
        r = rankList[i]
        l_rate = params["learning_rate"]
        estimators = params["n_estimators"]
        algorithm = params["algorithm"]
        avgTestAcc = meanTestAccList[i]
        stdTestAcc = stdTestAccList[i]
        avgTrAcc = meanTrAccList[i]
        stdTrAcc = stdTrAccList[i]
        # write the output line
        ofd.write(f"{r}\t{dtMaxDepth}\t{estimators}\t{algorithm}\t{l_rate:.6f}\t{avgTrAcc:.6f}\t{stdTrAcc:.6f}\t{avgTestAcc:.6f}\t{stdTestAcc:.6f}\n")

    ofd.close()

    # get the best model
    bestParams = bestAda.best_params_
    print(bestParams)

    # train the model using the best parameters
    estimators = bestParams["n_estimators"]
    l_rate = bestParams["learning_rate"]
    algorithm = bestParams["algorithm"]
    # HACK: restore this if not working
    # model = AdaBoostClassifier(n_estimators=bestParams["n_estimators"], learning_rate=l_rate, algorithm=algorithm)
    model = AdaBoostClassifier(DecisionTreeClassifier(max_depth=dtMaxDepth), n_estimators=bestParams["n_estimators"], learning_rate=l_rate, algorithm=algorithm)
    model.fit(normFeat, labels)
    print(model.feature_importances_)

    evaluation = cross_val_score(model, normFeat, labels, cv=cvSteps, n_jobs=threads)
    print(f"Mean accuracy cv-{cvSteps}:\t{sum(evaluation)/len(evaluation):.6f}")

    # Set vars
    l_rateStr: str = str(l_rate).replace(".", "")
    algoStr: str = algorithm.lower().replace(".", "")

    # Dump model using pickle
    modelPath = os.path.join(modelsDir, f"adaboost_mdepth{dtMaxDepth}_{estimators}_{l_rateStr}_{algoStr}_{normFeat.shape[1]}features_{normFeat.shape[0]}samples.pckl")
    # Note the if the minimum supported version of Python is 3.8
    # Then protocol=5 should be used, which is faster at loading big objects
    pfd = open(modelPath, "wb")
    pickle.dump(model, pfd, protocol=5)
    pfd.close()

    # sys.exit("DEBUG")


if __name__ == "__main__":
    main()
