#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic138-qfo20-ess-fake
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date


 ### Diamond runs #####

echo $'\n@@@@ sonic138-dmnd-essentials-fast fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-dmnd-essentials-fast -p s138-dmnd-ess-fast-fake --aln-tool diamond --threads 128 -m fast -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic138-dmnd-essentials-default fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-dmnd-essentials-default -p s138-dmnd-ess-default-fake --aln-tool diamond --threads 128 -m default -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic138-dmnd-essentials-sensitive fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-dmnd-essentials-sensitive -p s138-dmnd-ess-sens-fake --aln-tool diamond --threads 128 -m sensitive -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic138-dmnd-essentials-most-sensitive fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-dmnd-essentials-msens -p s138-dmnd-ess-msens-fake --aln-tool diamond --threads 128 -m most-sensitive -op -noidx
wait
echo $'\n'

 #### MMseqs runs #####

echo $'\n@@@@ sonic138-mmseqs-essentials-fast fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-mmseqs-essentials-fast -p s138-mmseqs-ess-fast-fake --threads 128 -m fast -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic138-mmseqs-essentials-default fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-mmseqs-essentials-default -p s138-mmseqs-ess-default-fake --threads 128 -m default -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic138-mmseqs-essentials-sensitive fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-mmseqs-essentials-sensitive -p s138-mmseqs-ess-sens-fake --threads 128 -m sensitive -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic138-mmseqs-essentials-most-sensitive fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-mmseqs-essentials-msens -p s138-mmseqs-ess-msens-fake --threads 128 -m most-sensitive -op -noidx
wait
echo $'\n'


 ### BLAST runs #####

# echo $'\n@@@@ sonic138-blast-essentials-default fake @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic138-qfo20-ess-FAKE/sonic138-blast-essentials-default -p s138-blast-ess-default-fake --aln-tool blast --threads 128 -m default -op -noidx
wait
echo $'\n'

# All runs completed
date