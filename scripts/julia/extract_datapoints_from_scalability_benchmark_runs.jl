using Base: iscontiguous, Float64
#=
Generate datapoints file extracting information from DBO scalability benchmark runs.
These runs were perfomed using the program perfom_domain_orthology_scalability_test.jl
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using DataStructures: OrderedDict
using Printf:@printf,@sprintf
using DataFrames
using CSV
using Serialization
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using DomainOrthologyScalability



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath

    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings()
    @add_arg_table! s begin
    "input-dir"
        arg_type = String
        help = "Directory containing the scalability benchmark."
        required = true
    "output-dir"
        arg_type = String
        help = "Output directory."
        required = true
    "prefix"
        arg_type = String
        help = "General prefix for the run"
        required = true
    "--debug", "-d"
        action = :store_true   # this makes it a flag
        help = "Show debug information."
    end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""Load info about chunks for each run and associate the number of proteomes used to each run"""
function load_chunks_info(chunksFilePath::String, prefix::String)::OrderedDict{String, Int}
    @debug "load_chunks_info :: START" chunksFilePath, prefix

    # Chunks are written using the following forma:
    #  # 1 3 -> the # indicates the header for a chunk; the values are the run ID, and the number of proteomes used (3)
    # 1,2,3 -> proteomes used in the run

    chunkId::String = ""
    flds::Array{String, 1} = String[]
    outDict::OrderedDict{String, Int} = OrderedDict()
    # Lines have the following format
    #  # 1 3 -> the # indicates the header for a chunk; the values are the run ID, and the number of proteomes used (3)
    # 1,2,3 -> proteomes used in the run
    for l in eachline(chunksFilePath, keep=false)
        if l[1] == '#'
            flds = split(l, "\t", limit=2)
            chunkId = "$(prefix)_$(flds[2])"
            outDict[chunkId] = parse(Int, flds[2])
            # @show chunkId outDict[chunkId]
        end
    end

    return outDict

end



"""Extract execution time information from a log file of a run"""
function process_log_file(logFilePath::String)::OrderedDict{String, Float64}
    @debug "process_log_file :: START" logFilePath

    tmpExtime::Float64 = 0.0
    flds::Array{String, 1} = String[]
    outDict::OrderedDict{String, Float64} = OrderedDict()
    # Lines have the following format
    for l in eachline(logFilePath, keep=false)
        # DB creation time
        if startswith(l, "MMs")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["db_creation"] = tmpExtime
        # Profile search time
        elseif startswith(l, "Elapsed time for profile")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            outDict["profile_search"] = tmpExtime
            # @show l tmpExtime
        # Documents creation time
        elseif startswith(l, "Elapsed time for documents")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["documents"] = tmpExtime
        # Training time
        elseif startswith(l, "Elapsed time for training")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["training"] = tmpExtime
        # Embedding assignment time
        elseif startswith(l, "Elapsed time for assign")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["embedding"] = tmpExtime
        # Arch extraction, d2v training, embedding assignment time
        elseif startswith(l, "Elapsed time (Arch")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["tot_model"] = tmpExtime
        # Arch comparison time
        elseif startswith(l, "Elapsed time for comparing")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["arch_comparison"] = tmpExtime
        # Total time
        elseif startswith(l, "Total elapsed")
            flds = split(l, "\t", limit=2)
            tmpExtime = parse(Float64, flds[2])
            # @show l tmpExtime
            outDict["total_domain_orthology"] = tmpExtime - outDict["db_creation"]
        end

    end

    return outDict

end



"""Extract execution time information from a log file of a run"""
function process_model_stats(statsFilePath::String)::OrderedDict{String, Int64}
    @debug "process_model_stats :: START" statsFilePath

    tmpStr::String = ""
    tmpCnt::Float64 = 0.0
    flds::Array{String, 1} = String[]
    outDict::OrderedDict{String, Int64} = OrderedDict()
    # Stat files have the following 4 lines:
    # Architectures: 134520
    # Vocabulary size: 10948
    # Total words: 485769
    # Training time (seconds): 175.59
    for l in eachline(statsFilePath, keep=false)
        # Archs count
        if l[1] == 'A'
            flds = split(l, "\t", limit=2)
            tmpCnt = parse(Int64, flds[2])
            outDict["architectures"] = tmpCnt
        # Vocabulary size
        elseif l[1] == 'V'
            flds = split(l, "\t", limit=2)
            tmpCnt = parse(Int64, flds[2])
            outDict["vocabulary_size"] = tmpCnt
        # Total words
        # This count includes uncovered inter-regions
        elseif startswith(l, "Tot")
            flds = split(l, "\t", limit=2)
            tmpCnt = parse(Int64, flds[2])
            outDict["words"] = tmpCnt
        end
    end

    return outDict

end


"""Process the files of a given run and extract the required information."""
function process_run(runDir::String, runId::String, protCnt::Int64)::String
    @debug "process_run :: START" runDir runId protCnt

    # Extract execution times
    tmpPath::String = joinpath(runDir, "log.$(runId).txt")
    # Process log file
    exTimeDict::OrderedDict{String, Float64} = process_log_file(tmpPath)
    exTimeStr::String = "$(exTimeDict["db_creation"])\t$(exTimeDict["profile_search"])\t$(exTimeDict["documents"])\t$(exTimeDict["training"])\t$(exTimeDict["embedding"])\t$(exTimeDict["tot_model"])\t$(exTimeDict["arch_comparison"])\t$(exTimeDict["total_domain_orthology"])"
    # println(exTimeDict)
    # process stats file
    tmpPath = joinpath(runDir, "arch_orthology/models/stats.$(runId).tsv")
    modelStatsDict::OrderedDict{String, Int64} = process_model_stats(tmpPath)
    modelStatsStr::String = "$(modelStatsDict["architectures"])\t$(modelStatsDict["vocabulary_size"])\t$(modelStatsDict["words"])"
    outStr::String = "$runId\t$(protCnt)"
    # Add the part related to execution time
    outStr = "$(outStr)\t$(exTimeStr)\t$(modelStatsStr)"

    return outStr

end



"""Check that all input files exists"""
function validate_input(proteomesDir::String, species::Array{String, 1})::Nothing
    @debug "validate_input :: START" runDir

    tmpPath::String = ""
 
    for sp in species
        tmpPath = joinpath(proteomesDir, sp)
        if !isfile(tmpPath)
            @error "File for species $sp was not found" tmpPath
            exit(-2)
        end
        # println(isfile(tmpPath))
    end

    return nothing

end



#####  MAIN  #####
args = get_params(ARGS)
@show(args)


inDir = abspath(args["input-dir"])
outDir = abspath(args["output-dir"])
prefix = args["prefix"]
debug = args["debug"]
makedir(outDir)

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
@show mainLogger

@info "The datapoints extraction from benchmark runs will be perfomed with the following parameters:" inDir outDir prefix 

# Load info about the runs and their sizes
chunksFilePath::String = joinpath(inDir, "input_chunks.txt")
# Associates a run id to the number of proteomes used
chunkSizes::OrderedDict{String, Int} = load_chunks_info(chunksFilePath, prefix)

# Open the output tables file
# and write the HDR
tblFile::String = joinpath(outDir, "s2-domain-orthology-scalability-$(prefix)_$(length(chunkSizes)).tsv")
@show tblFile

ofd::IO = open(tblFile, "w")
hdr::String = "id\trun\tproteomes\tdb_creation\tprofile_search\tdocuments\ttraining\tembedding\ttot_model\tarch_comparison\ttot_dbo\tarchitectures\tvocabulary_size\twords\n"
# Write the chunks into the file
write(ofd, hdr)


# Validate that run directories exist
runId::String = ""
runDir::String = ""
protCnt::Int64 = 0
benchRunStr::String = ""
sampleIdx::UInt32 = 0
for (runId, protCnt) in chunkSizes
    global runDir = joinpath(inDir, runId)
    if !isdir(runDir)
        @error "The run directory for run $(runId) was not found." runDir
        exit(-2)
    end

    global sampleIdx += 1
    global benchRunStr = process_run(runDir, runId, protCnt)
    # println(benchRunStr)
    write(ofd, "$(sampleIdx)\t$(benchRunStr)\n")

    # break
    
end

close(ofd)
