Different scripts and materials used in the manuscript regarding SonicParanoid2.  

It contains the following:
- Scripts for generating plots
- Scripts for generating tables
- Scripts runnning tests (e.g. SonicParanoid2 trials)
- Scripts for training and evaluating AdaBoost or Doc2Vec
- Scripts created during the revision of SP2 in Genome Biology

If you cannot find any of the material you are looking for, please contact the owner of this repository.