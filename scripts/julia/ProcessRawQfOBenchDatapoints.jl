module ProcessRawQfOBenchDatapoints

include("modules/systools.jl")



# expose the methods
export extract_tar_archive,
    is_archive,
    makedir,
    pigz_decompress,
    xz_decompress


end # end module

