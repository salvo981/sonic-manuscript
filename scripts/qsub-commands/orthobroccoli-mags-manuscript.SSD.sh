#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N orthobroccoli-mags-manuscript-SSD
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /ssd_home/orthobroccoli-mags-manuscript-SSD/

date

echo $'\n@@@@ orthofinder-mags-128cpus @@@@\n'
# python3 /home/salvocos/work_repos/OrthoFinder/orthofinder.py -f /ssd_home/orthobroccoli-mags-manuscript-SSD/small_input/ -o orthofinder_output_3mags -n small_input_3mags -t 128 -og

# Stop after the creation fo orthogroups using MCL
# python3 /home/salvocos/work_repos/OrthoFinder/orthofinder.py -f /ssd_home/reference_2000_mags/ -o /ssd_home/orthobroccoli-mags-manuscript-SSD/orthofinder-mags-128cpus-ogs -n orthofinder-mags-128cpus-ogs -t 128 -og

# Perform MSAs and stop after the creation of Phylogenetically inferred orthogroups
python3 /home/salvocos/work_repos/OrthoFinder/orthofinder.py -f /ssd_home/reference_2000_mags/ -o /ssd_home/orthobroccoli-mags-manuscript-SSD/orthofinder-mags-128cpus-phogs -n orthofinder-mags-128cpus-phogs -t 128 -ot -M msa


wait
echo $'\n'
date
echo $'\n'


echo $'\n@@@@ broccoli-mags-128cpus @@@@\n'
# test run
# python3 /home/salvocos/work_repos/Broccoli/broccoli.py -dir /ssd_home/orthobroccoli-mags-manuscript-SSD/small_input/ -path_fasttree /ssd_home/orthobroccoli-mags-manuscript-SSD/bin/FastTree -ext faa -threads 9

# Complete run
# python3 /home/salvocos/work_repos/Broccoli/broccoli.py -dir /ssd_home/reference_2000_mags/ -path_fasttree /ssd_home/orthobroccoli-mags-manuscript-SSD/bin/FastTree -ext faa -threads 128

wait
echo $'\n'


# All runs completed
date