# This script will generate the datapoints from the inter-proteomes
# Complete all-vs-all execution times fpor the 250 proteomes used
# for the AdaBoost training
# The datapoints are use to plot the execution tinme differences between interproteome alignments
# using the script in sonic-manuscript/scripts/julia/create_adaboost_training_extimes_datapoints.jl

# MMseqs
julia ~/work_repos/sonic-manuscript/scripts/julia/create_adaboost_training_extimes_datapoints.jl ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/tables/aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/tables/snapshot.250_proteomes.azur.tsv --aai-tbl ~/work_repos/sonic-manuscript/tables/aai_computation_comparem/aai_reference250processed.tsv --meta-data ~/work_repos/sonic-manuscript/tables/main250processed.table.tsv --output-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/adaboost-training/adaboost250prot.mmseqs.msens.noidx.extime.datapoints.tsv

wait

# Diamond
julia ~/work_repos/sonic-manuscript/scripts/julia/create_adaboost_training_extimes_datapoints.jl ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/tables/aln_ex_times_250_proteomes_dmnd_ca_msens-noidx.128cpus.azur.tsv ~/work_repos/sonic-manuscript/adaboost-training/models-manuscript/tables/snapshot.250_proteomes.azur.tsv --aai-tbl ~/work_repos/sonic-manuscript/tables/aai_computation_comparem/aai_reference250processed.tsv --meta-data ~/work_repos/sonic-manuscript/tables/main250processed.table.tsv --output-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/adaboost-training/adaboost250prot.dmnd.msens.noidx.extime.datapoints.tsv

