# This will generate single figures with 3 facets (plots)
# 1 for each type of domain: prokaryote, eukaryote, mixed

# MMseqs msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-mmseqs-ess-msens.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-msens-MULTIPLOT-extimes

# MMseqs sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-mmseqs-ess-sens.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-sens-MULTIPLOT-extimes

# MMseqs default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-mmseqs-ess-default.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-default-MULTIPLOT-extimes

# MMseqs fast
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-mmseqs-ess-fast.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-fast-MULTIPLOT-extimes

# Diamond msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-dmnd-ess-msens.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-msens-MULTIPLOT-extimes

# Diamond sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-dmnd-ess-sens.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-sens-MULTIPLOT-extimes

# Diamond default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-dmnd-ess-default.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-default-MULTIPLOT-extimes

# Diamond fast
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-dmnd-ess-fast.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-fast-MULTIPLOT-extimes

# BLAST default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s136-qfo20-blast-ess-default.extime.datapoint.tsv -o ./plots_multi --multi-col-name empire-class --mrkr-size 10 --format svg --prefix qfo20-blast-default-MULTIPLOT-extimes