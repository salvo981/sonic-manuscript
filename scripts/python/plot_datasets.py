'''
Given Proteome names, taxonomy and sequence stats, generate plots of the table.
'''

import os
import sys
from typing import TextIO, List, Set, Dict, Tuple
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots info about the proteomes.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file with proteome information\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=True, help="Output directory.")
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("-c", "--col", type=int, required=True, help="Column containing the datapoint to plots.")
    parser.add_argument("--category-col", type=int, required=True, help="Column containing the categories that will be on the x-axis of box-plots.")
    parser.add_argument("--hue", type=int, required=False, help="Column containing categorical information.", default=0)
    parser.add_argument("--divisor", type=int, required=False, help="Integer value by which the main dapaoint should be divided (e.g., 1000).", default=1)
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("-r", "--rows", type=int, required=False, help="Number of records to load. Used for debugging. Default=0", default=0)
    parser.add_argument("--xlab", type=str, required=True, help="X label.", default="x label")
    parser.add_argument("--ylab", type=str, required=True, help="Y label.", default="y label")

    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def plot_cosine_values(df, plotsDir:str, prefix:str):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")

    # Set temp variables
    minCos:float = min(df.cosine)
    bins = int(abs(minCos)*10) + 10
    negBins = int(abs(minCos)*10)
    fmt:str = "png"

    # Set plot paramaters
    colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    fig = plt.figure()
    # plt.title(plotTitle)

    print(f"Min cosine:\t{minCos:.3f}")
    print(f"bins:\t{bins}")
    print(f"Negative bins:\t{negBins}")

    '''
    sns.displot(df, x="cosine", col="methods", kind="hist", bins=bins)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.all.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig

    # Single plot with categories
    fig = plt.figure()
    sns.histplot(data=df, x="cosine", hue="methods", multiple="dodge", shrink=.9)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.all.hue.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig
    '''

    ###### PLOTS POSITIVE COSINE VALUES
    # subset the df
    tmpDf = df.loc[df.cosine >= 0 ]
    print(tmpDf.shape)

    fig = plt.figure()
    sns.displot(tmpDf, x="cosine", col="methods", kind="hist", bins=10)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.positive.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig

    # Single multibar plot
    fig = plt.figure()
    sns.histplot(data=tmpDf, x="cosine", hue="methods", multiple="dodge", bins=10)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.positive.hue.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig

    # Densitites
    '''
    fig = plt.figure()
    sns.histplot(data=tmpDf, x="cosine", hue="methods", element="step", stat="density", common_norm=False)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.positive.densities.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig
    '''

    ###### PLOTS NEGATIVE COSINE VALUES
    # subset the df
    tmpDf = df.loc[df.cosine < 0 ]
    print(tmpDf.shape)

    # '''
    fig = plt.figure()
    sns.displot(tmpDf, x="cosine", col="methods", kind="hist", bins=negBins)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.negative.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig
    # '''

    print(tmpDf.describe())
    # Single plot with categories
    fig = plt.figure()
    # sns.histplot(data=tmpDf, x=tmpDf.cosine, hue=tmpDf.methods, multiple="dodge", shrink=.8, bins=negBins)
    # sns.histplot(data=tmpDf, x=tmpDf.cosine, hue=tmpDf.methods, shrink=.8, bins=negBins)
    sns.histplot(data=tmpDf, x=tmpDf.cosine, hue=tmpDf.methods, multiple="dodge", bins=negBins*2)

    #element="step"

    tmpOutPath = os.path.join(plotsDir, f"{prefix}.hist.negative.hue.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig
    # sys.exit("DEBUG")



def plot_cosine_kde(df, plotsDir:str, prefix:str):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")

    # Set temp variables
    minCos:float = min(df.cosine)
    fmt:str = "png"

    print(f"Min cosine:\t{minCos:.3f}")

    # Set plot paramaters
    fig = plt.figure()
    # plt.title(plotTitle)

    sns.kdeplot(data=df, x="cosine", hue="methods", fill=True, common_norm=False, palette="crest", alpha=.5, linewidth=0,)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.kde.all.{fmt}")
    print(tmpOutPath)
    plt.savefig(tmpOutPath, dpi=160)
    del fig

    ###### PLOTS POSITIVE COSINE VALUES
    # subset the df
    tmpDf = df.loc[df.cosine >= 0 ]

    fig = plt.figure()
    sns.kdeplot(data=tmpDf, x="cosine", hue="methods", fill=True, common_norm=False, palette="crest", alpha=.5, linewidth=0,)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.kde.positive.{fmt}")
    plt.savefig(tmpOutPath, dpi=160)
    del tmpDf
    del fig

    ###### PLOTS NEGATIVE COSINE VALUES
    # subset the df
    tmpDf = df.loc[df.cosine < 0 ]

    fig = plt.figure()
    sns.kdeplot(data=tmpDf, x=tmpDf.cosine, hue="methods", fill=True, common_norm=False, palette="crest", alpha=.5, linewidth=0,)
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.kde.negative.{fmt}")
    plt.savefig(tmpOutPath, dpi=160)
    del fig



def plot_box(df, plotsDir: str, prefix: str, dataColIdx: int, categoryColIdx: int, hueColIdx: int, divisor: int = 0, mrkSize: int = 3, fmt: str="svg", xlab: str = "", ylab: str = "",):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    logging.info(f"Img format:\t{fmt}")
    logging.info(f"Output directory: {plotsDir}")
    logging.info(f"X label:\t{xlab}")
    logging.info(f"Y label:\t{ylab}")

    sns.set_theme(style="whitegrid")

    # Set output file name
    dataColName: str = df.columns[dataColIdx]
    categoryColName: str = df.columns[categoryColIdx]
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.box.{categoryColName}.{dataColName}")

    # Check if categories should be used
    hasHue: bool = False
    if hueColIdx > 0:
        hasHue = True
        tmpOutPath = f"{tmpOutPath}.{df.columns[hueColIdx]}.{fmt}"
    else:
        tmpOutPath = f"{tmpOutPath}.{fmt}"

    # Set temp variables
    print(f"Column with data:\t{dataColName}")
    print(f"Column categories:\t{categoryColName}")
    print(f"Categorical info will be used:\t{hasHue}")
    print(f"Output format:\t{fmt}")
    print(f"Output plot: {tmpOutPath}")

    outlierSize: int = 0

    # Set plot paramaters
    # colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    # fig = plt.figure()
    # plt.title(plotTitle)

    # Initialize the figure with a logarithmic x axis
    # f, ax = plt.subplots(figsize=(7, 6))
    # ax.set_xscale("log")

    # Plot box plots
    if hasHue:
        if divisor > 0:
            # Divide the value if a divisor was provided
            ax = sns.boxplot(x=categoryColName, y=df[dataColName]/divisor, data=df, fliersize=outlierSize, dodge=True, hue=df.columns[hueColIdx])
            ax = sns.swarmplot(x=categoryColName, y=df[dataColName]/divisor, data=df, color=".2", dodge=True, hue=df.columns[hueColIdx], size=mrkSize)
        else:
            ax = sns.boxplot(x=categoryColName, y=dataColName, data=df, fliersize=outlierSize, dodge=True, hue=df.columns[hueColIdx])
            ax = sns.swarmplot(x=categoryColName, y=dataColName, data=df, color=".2", dodge=True, hue=df.columns[hueColIdx], size=mrkSize)
    else:
        if divisor > 0:
            # Divide the value if a divisor was provided
            ax = sns.boxplot(x=categoryColName, y=df[dataColName]/divisor, data=df, fliersize=outlierSize, dodge=True)
            ax = sns.swarmplot(x=categoryColName, y=df[dataColName]/divisor, data=df, color=".2", size=mrkSize)
        else:
            ax = sns.boxplot(x=categoryColName, y=dataColName, data=df, fliersize=outlierSize, dodge=True)
            ax = sns.swarmplot(x=categoryColName, y=df[dataColName], data=df, color=".2", size=mrkSize)

    # Add in points to show each observation
    # sns.stripplot(x="methods", y="cosine", data=df, size=4, color=".3", linewidth=0)

    # Tweak the visual presentation
    ax.set(xlabel=xlab)
    ax.set(ylabel=ylab)
    # ecTest.set_xlabel("Recall\n(number of orthologous relationship)")
    # ecTest.set_ylabel("Precision\n(average Schlicker similarity)")
    sns.despine(trim=True, left=True)

    plt.savefig(tmpOutPath, dpi=160)
    # del f
    sys.exit("DEBUG ::plot_box")





def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    prefix: str = args.prefix
    dataCol: int = args.col
    categoryCol: int = args.category_col
    hueCol: int = args.hue
    divisor: int = args.divisor
    markerSize: int = args.mrkr_size
    imgFmt: str = args.format
    # create output directories
    makedir(outDir)


    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Column with categoricak data: {categoryCol}")
    logging.info(f"Column with data points: {dataCol}")
    logging.info(f"Image output format:\t{imgFmt}")
    logging.info(f"X label:\t{args.xlab}")
    logging.info(f"Y label:\t{args.ylab}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Divisor:\t{divisor}")

    # Read dataframe
    # The hdr of datapoint file have the following format
    # File Empire Domain Proteome ID Taxonomy ID Species name Proteins Proteome size Dataset
    df = pd.read_csv(inTbl, sep="\t", dtype= {"File": str, "Empire": str, "Domain": str, "Species name": "str", "Proteins": np.uint32, "Proteome size": np.uint32, "Dataset": str}, engine="c")

    # print(inTbl)
    # plot_cosine_box(df, outDir, prefix=prefix, colIdx=dataCol, divisor=divisor)
    plot_box(df, outDir, prefix=prefix, dataColIdx=dataCol, categoryColIdx=categoryCol, hueColIdx=hueCol, divisor=divisor, mrkSize=markerSize, fmt=imgFmt, xlab=args.xlab, ylab=args.ylab)
    sys.exit("DEBUG :: MAIN")

    '''
    if totRows > 0:
        df = pd.read_csv(inTbl, sep="\t", dtype= {"o1": str, "o2": "str", "agreement": np.float32, "cosine": np.float32}, engine="c", nrows=totRows)
    else:
        df = pd.read_csv(inTbl, sep="\t", dtype= {"o1": str, "o2": "str", "agreement": np.float32, "cosine": np.float32}, engine="c")
    # add the column with method counts
    df.insert(2, "methods", (df.agreement.to_numpy() * 8).astype(np.uint8))
    # remove the agreement column
    df.drop(columns=["agreement"], axis=0, inplace=True)
    # print(df.dtypes)

    # Start plotting
    plot_cosine_values(df, plotsDir, prefix=prefix)
    # plot KDE
    plot_cosine_kde(df, plotsDir, prefix=prefix)
    # plot Box plots
    plot_cosine_box(df, plotsDir, prefix=prefix)
    '''



if __name__ == "__main__":
    main()
