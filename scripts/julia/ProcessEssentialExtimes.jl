module ProcessEssentialExtimes

# include("modules/seqtools.jl")
include("modules/systools.jl")



# expose the methods
export extract_file_from_tar,
    is_archive,
    makedir,
    pigz_decompress


end # end module

