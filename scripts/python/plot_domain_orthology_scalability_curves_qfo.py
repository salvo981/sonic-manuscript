'''
Plots curves to show the scalability of the domain orthology based pipeline.
The input table is generated using the script under
scripts/julia/extract_datapoints_from_scalability_benchmark_runs.jl
'''

from __future__ import annotations

import os
import sys
# from typing import TextIO
import logging
import numpy as np
import pandas as pd
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']
rcParams['font.size'] = 9
rcParams['font.weight'] = 'normal'
rcParams['lines.markersize'] = 3
#'weight' : 'bold',
import matplotlib as plt
import matplotlib.pyplot as pyplt



########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots domain-based scalability curves.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file with datapoints from QfO benchmark results\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=True, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("--fig-width-inches", type=float, required=False, help="Width of the plot in inches.", default=2.7)
    parser.add_argument("--fig-height-inches", type=float, required=False, help="Height of the plot in inches.", default=1.8)
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def plot_profile_search_vs_training_times(df, plotsDir: str, prefix: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "prof_search_vs_training_time"
    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')

    # Set output file name
    tmpOutPath: str = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)

    # load datapoints
    # The sizes used in the manuscript are
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    timeCurves = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #timeCurves.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    timeCurves.set_xlabel("Proteomes from QfO")
    timeCurves.set_ylabel("Ex. time (minutes)")
    timeCurves.set_title("Profile search time VS training time")
    timeCurves.spines["right"].set_visible(False)
    timeCurves.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    timeCurves.yaxis.set_ticks_position("left")
    timeCurves.xaxis.set_ticks_position("bottom")

    # print max and min for x and y
    print(f"min/max X:\t{min(df['proteomes']):.4f}\t{max(df['proteomes']):.4f}")
    print(f"min/max Y (Training time):\t{min(df['training']/60):.4f}\t{max(df['training']/60):.4f}")
    print(f"min/max Y (Profile Search):\t{min(df['profile_search']/60):.4f}\t{max(df['profile_search']/60):.4f}")
    # Set limits and legend

    # Set X limits
    timeCurves.set_xlim(0, 80)
    start, end = timeCurves.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 6
    timeCurves.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    # The time must be plotted in minutes
    timeCurves.set_ylim(0.0, 3.0)
    start, end = timeCurves.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.02
    timeCurves.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size Y:\t{stepSizeY:.4f}")
    '''

    # Diagonal
    timeCurves.plot([0, 78], [0, 3], color='tab:grey', linestyle='dashed', ms=2, label='Diagonal')
    # Training time
    timeCurves.plot(df["proteomes"], df["training"]/60., color='tab:blue', linestyle='solid', ms=2, label='Training time')
    # Profile search
    timeCurves.plot(df["proteomes"], df["profile_search"]/60., color='tab:orange', linestyle='solid', ms=2, label='Training time')

    # print(tmpOutPath)

    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_samples_vs_training_time(df, plotsDir: str, prefix: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "corpus_vs_training_time"

    # Set output file name
    tmpOutPath: str = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)

    # load datapoints
    # The sizes used in the manuscript are
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    curves = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #curves.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    curves.set_xlabel("Proteomes from QfO")
    curves.set_ylabel("Count (thousands)")
    curves.set_title("Corpus VS training time")
    curves.spines["right"].set_visible(False)
    curves.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    curves.yaxis.set_ticks_position("left")
    curves.xaxis.set_ticks_position("bottom")

    # print max and min for x and y
    print(f"min/max X:\t{min(df['proteomes']):.4f}\t{max(df['proteomes']):.4f}")
    print(f"min/max Y (Architecutes):\t{min(df['architectures']/1000):.4f}\t{max(df['architectures']/1000):.4f}")
    print(f"min/max Y (vocabulary size):\t{min(df['vocabulary_size']/1000):.4f}\t{max(df['vocabulary_size']/1000):.4f}")
    # Set limits and legend

    # Set X limits
    curves.set_xlim(0, 80)
    start, end = curves.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 6
    curves.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size X:\t{stepSizeX:.4f}")

    # Set Y limits
    # The time must be plotted in minutes
    curves.set_ylim(0.0, 140.0)
    '''
    start, end = curves.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    curves.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size Y:\t{stepSizeY:.4f}")
    '''

    # Diagonal
    curves.plot([0, 78], [0, 140], color='tab:grey', linestyle='dashed', ms=2, label='Diagonal')
    # Training time
    curves.plot(df["proteomes"], df["architectures"]/1000., color='tab:orange', linestyle='solid', ms=2, label='Architectures')
    # Profile search
    curves.plot(df["proteomes"], df["vocabulary_size"]/1000., color='tab:pink', linestyle='solid', ms=2, label='Vocabulary size')

    # Create the second Y-axis
    curves2 = curves.twinx()
    curves2.spines["left"].set_visible(False)
    curves2.spines["top"].set_visible(False)
    curves2.spines["bottom"].set_visible(False)
    # Set limits for the second Y axis
    curves2.set_ylim(0.0, 3.0)

    curves2.set_ylabel('Training (minutes)', color = 'tab:blue')
    curves2.plot(df["proteomes"], df["training"]/60., color = 'tab:blue', linestyle='solid', ms=2, label='Training time')
    curves2.tick_params(axis ='y', labelcolor = 'tab:blue')

    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_vocab_vs_training_time(df, plotsDir: str, prefix: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "vocab_vs_training_time"

    # Set output file name
    tmpOutPath: str = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)

    # load datapoints
    # The sizes used in the manuscript are
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    curves = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #curves.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    curves.set_xlabel("Proteomes from QfO")
    curves.set_ylabel("Words (thousand)", color = "tab:orange")
    curves.tick_params(axis ='y', labelcolor = "tab:orange")
    curves.set_title("Vocabulary size VS training time")
    curves.spines["right"].set_visible(False)
    curves.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    curves.yaxis.set_ticks_position("left")
    curves.xaxis.set_ticks_position("bottom")

    # print max and min for x and y
    print(f"min/max X:\t{min(df['proteomes']):.4f}\t{max(df['proteomes']):.4f}")
    print(f"min/max Y (vocabulary size):\t{min(df['vocabulary_size']/1000):.4f}\t{max(df['vocabulary_size']/1000):.4f}")
    # Set limits and legend

    # Set X limits
    curves.set_xlim(0, 80)
    start, end = curves.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 6
    curves.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size X:\t{stepSizeX:.4f}")

    # Set Y limits
    # The time must be plotted in minutes
    curves.set_ylim(0.0, 12.0)
    '''
    start, end = curves.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    curves.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size Y:\t{stepSizeY:.4f}")
    '''

    # Diagonal
    curves.plot([0, 78], [0, 12], color='tab:grey', linestyle='dashed', ms=2, label='Diagonal')
    # Profile search
    curves.plot(df["proteomes"], df["vocabulary_size"]/1000., color='tab:orange', linestyle='solid', ms=2, label='Vocabulary size')

    # Create the second Y-axis
    curves2 = curves.twinx()
    curves2.spines["left"].set_visible(False)
    curves2.spines["top"].set_visible(False)
    curves2.spines["bottom"].set_visible(False)
    # Set limits for the second Y axis
    curves2.set_ylim(0.0, 3.0)

    curves2.set_ylabel('Training (minutes)', color = 'tab:blue')
    curves2.plot(df["proteomes"], df["training"]/60., color = 'tab:blue', linestyle='solid', ms=2, label='Training time')
    curves2.tick_params(axis ='y', labelcolor = 'tab:blue')

    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_archs_vs_training_time(df, plotsDir: str, prefix: str, figWidth: float = 2.7, figHeight: float = 1.8, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Set temp variables
    testName: str = "arch_vs_training_time"

    # Set output file name
    tmpOutPath: str = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)

    # load datapoints
    # The sizes used in the manuscript are
    # figWidth = 2.7
    # figHeight = 1.8
    fig = pyplt.figure(figsize=(figWidth, figHeight))
    curves = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #curves.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    curves.set_xlabel("Proteomes from QfO")
    curves.set_ylabel("Architectures (thousand)", color = "tab:orange")
    curves.tick_params(axis ='y', labelcolor = "tab:orange")
    curves.set_title("Corpus size VS training time")
    curves.spines["right"].set_visible(False)
    curves.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    curves.yaxis.set_ticks_position("left")
    curves.xaxis.set_ticks_position("bottom")

    # print max and min for x and y
    print(f"min/max X:\t{min(df['proteomes']):.4f}\t{max(df['proteomes']):.4f}")
    print(f"min/max Y (Architectures):\t{min(df['architectures']/1000):.4f}\t{max(df['architectures']/1000):.4f}")
    print(f"min/max Y (training):\t{min(df['training']/60):.4f}\t{max(df['training']/60):.4f}")

    # Set limits and legend
    # Set X limits
    curves.set_xlim(0, 80)
    start, end = curves.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 6
    curves.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size X:\t{stepSizeX:.4f}")

    # Set Y limits
    # The time must be plotted in minutes
    curves.set_ylim(0.0, 140.0)
    start, end = curves.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.02
    curves.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size Y:\t{stepSizeY:.4f}")
    '''

    # Diagonal
    curves.plot([0, 78], [0, 140], color='tab:grey', linestyle='dashed', ms=2, label='Diagonal')
    # Profile search
    curves.plot(df["proteomes"], df["architectures"]/1000., color='tab:orange', linestyle='solid', ms=2, label='Architectures')

    # Create the second Y-axis
    curves2 = curves.twinx()
    curves2.set_ylabel('Training (minutes)', color = 'tab:blue')
    curves2.spines["left"].set_visible(False)
    curves2.spines["top"].set_visible(False)
    curves2.spines["bottom"].set_visible(False)
    # Set limits for the second Y axis
    curves2.set_ylim(0.0, 3.0)

    curves2.plot(df["proteomes"], df["training"]/60., color = 'tab:blue', linestyle='solid', ms=2, label='Training time')
    curves2.tick_params(axis ='y', labelcolor = 'tab:blue')

    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    outFmt: str = args.format
    figWidth: float = args.fig_width_inches
    figHeight: float = args.fig_height_inches
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)
    makedir(plotsDir)

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Marker size:\t{markerSize}")
    logging.info(f"Figure width (inches):\t{figWidth}")
    logging.info(f"Figure height (inches):\t{figHeight}")

    # Read dataframe
    # The hdr of datapoint file have the following format:
    # method test run_type aligner sensitivity x y xerr yerr xlabel ylabel
    df = pd.read_csv(inTbl, sep="\t", dtype= {"id": np.uint32, "run": str, "db_creation": np.float32, "profile_search": np.float32, "documents": np.float32, "training": np.float32, "embedding": np.float32, "tot_model": np.float32, "arch_comparison": np.float32, "tot_dbo": np.float32, "architectures": np.uint64, "vocabulary_size": np.uint64, "words": np.uint64}, engine="c")
    # print(df.dtypes)
    # Set a custom color palette
    # Use this when showing the prediction errors
    # myPalette = ["#FF0B04", "#4374B3", "#000000"]

    # Show or hide X and Y labels
    hideXlab = True
    hideYlab = True

    '''
    '''
    # Plot training and profile search times
    plot_profile_search_vs_training_times(df, plotsDir, prefix, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)

    # Archs and vocabulary vs Training time
    plot_samples_vs_training_time(df, plotsDir, prefix, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)

    # Vocabulary vs Training time
    plot_vocab_vs_training_time(df, plotsDir, prefix, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)

    # Architectures vs Training time
    plot_archs_vs_training_time(df, plotsDir, prefix, figWidth=figWidth, figHeight=figHeight, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)



if __name__ == "__main__":
    main()
