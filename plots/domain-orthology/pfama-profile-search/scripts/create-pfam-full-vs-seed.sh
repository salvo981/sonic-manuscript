# Generate different plots that compare
# profile search results with pfam full and seed


# minimum target coverage = 0.50
python3 ~/work_repos/sonic-manuscript/scripts/python/domain-orthology/plot_pfam_full_vs_seed_comparisons.py -ifull ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/profile-searches/prof_search_ex_times_30bits_tcov0.5_totqcov0.1.full.tsv -iseed ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/profile-searches/prof_search_ex_times_30bits_tcov0.5_totqcov0.1.seed.tsv -o ~/work_repos/sonic-manuscript/plots/domain-orthology/pfama-profile-search/pfam-full-vs-seed --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data ~/work_repos/sonic-manuscript/tables/qfo2020processed.table.tsv --format svg --prefix pfam_res_tcov050_minqcov010
wait

# minimum target coverage = 0.75
python3 ~/work_repos/sonic-manuscript/scripts/python/domain-orthology/plot_pfam_full_vs_seed_comparisons.py -ifull ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/profile-searches/prof_search_ex_times_30bits_tcov0.75_totqcov0.1.full.tsv -iseed ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/profile-searches/prof_search_ex_times_30bits_tcov0.75_totqcov0.1.seed.tsv -o ~/work_repos/sonic-manuscript/plots/domain-orthology/pfama-profile-search/pfam-full-vs-seed --snapshot ~/work_repos/sonic-manuscript/tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data ~/work_repos/sonic-manuscript/tables/qfo2020processed.table.tsv --format svg --prefix pfam_res_tcov075_minqcov010