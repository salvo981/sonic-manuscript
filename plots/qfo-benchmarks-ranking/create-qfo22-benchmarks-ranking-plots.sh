# At first the raw-rankings need to be processed and the final tables must be generated
python3 ~/work_repos/sonic-manuscript/scripts/python/generate_qfo_ranking_tables.py --square-quartile ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-square-quartiles-raw.tsv --diagonal-quartile-raw ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-diagonal-quartiles-raw.tsv --kmeans-raw ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-kmeans-ranking-raw.tsv -o ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking --prefix qfo22 -d

# Aggregrate
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-aggregate-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo22-ranking-aggregate --mrkr-size 8 --short-names  --seaborn-height 4.5 --seaborn-aspect 0.42 --qfo-competition 2022 --format svg
wait

# Aggregrate (NO SQUARE-QUARTILES)
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-aggregate-ranking-no-square-quartiles.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo22-ranking-aggregate-no-square-quartile --mrkr-size 8 --short-names  --seaborn-height 4.5 --seaborn-aspect 0.42 --qfo-competition 2022 --format svg
wait

# k-means
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-kmeans-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo22-ranking-kmeans --mrkr-size 8 --short-names  --seaborn-height 4.5 --seaborn-aspect 0.42 --qfo-competition 2022 --format svg
wait

# square-quartile
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-square-quartile-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo22-ranking-square-quartile --mrkr-size 8 --short-names  --seaborn-height 4.5 --seaborn-aspect 0.42 --qfo-competition 2022 --format svg
wait

# diagonal-quartile
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo22-webpage-method-ranking/qfo22-diagonale-quartile-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo22-ranking-diagonal-quartile --mrkr-size 8 --short-names  --seaborn-height 4.5 --seaborn-aspect 0.42 --qfo-competition 2022 --format svg
wait