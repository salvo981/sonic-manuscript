'''
Generate plots of QfO benchmark results.
The input is a table genetated using the script under scripts/julia/create_qfo_benchmark_datapoints.jl
'''

import os
import sys
from typing import TextIO, List, Set, Dict, Tuple
import logging
import numpy as np
import pandas as pd
import itertools
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']
rcParams['font.size'] = 9
rcParams['font.weight'] = 'normal'
rcParams['lines.markersize'] = 3
#'weight' : 'bold',
import matplotlib as plt
import matplotlib.pyplot as pyplt
# import oapackage # Use to compute the pareto set of points
import subprocess



########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots QfO benchmark results.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file with datapoints from QfO benchmark results\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=False, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--hue", type=str, required=False, help="Column containing categorical information.", default="")
    parser.add_argument("--multi-col-name", type=str, required=False, help="Column name used to create multiple plots.", default="")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("--pareto-script", type=str, required=False, help="Path to the script to compute the pareto frontier.", default="")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def compute_pareto_julia(script: str, inputPointsPath: str, outDir: str, dataType: str = "top-double-double") -> str:
    '''
    Compute pareto front calling a julia program
    '''
    # Example of execution of the script
    # julia ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl plots/tmp.x.y.tsv --output-dir plots/ -d
    validDataTypes: List[str] = ["top-int-double", "top-double-double", "bottom-int-double", "bottom-double-double"]
    if not dataType in validDataTypes:
        sys.exit(f"\nERROR: not valid data type {dataType}")

    # The file that should been created is named 'pareto.x.y.tsv'
    outPath: str = os.path.join(outDir, "pareto.x.y.tsv")
    # Remove the previous file if it does already exists
    if os.path.isfile(outPath):
        os.remove(outPath)
        print("\nINFO: previous pareto file removed.")

    rOut = subprocess.run(f"julia {script} {inputPointsPath} --output-dir {outDir} --data-type {dataType} -d", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    cmdOut:str = rOut.stdout.decode().rstrip()
    print(cmdOut)

    # Through an error if the file was not created
    if not os.path.isfile(outPath):
        logging.error("The pareto file was not created!")
        sys.exit(-8)
    # Return the path to the pareto front table
    return outPath



def map_method_labels_sonicparanoid_only(run_type: str, aligner: str, sensitivity: str) -> Tuple[str, str, str, str]:
    """Assign a label, color and marker shape to each method"""
    # Partecipant IDs for sonicpranoid have the following format
    # s136-dmnd-ca-default
    # run_type: Complete | Essential 
    # aligner: MMseqs | Diamond 
    # sensitivity: fast | default | sens | msens

    #set the shape and color
    nlabel: str = ""
    color: str = ""
    shape: str = ""
    fillStyle: str = "none"
    # For this kind of Plot 4 Colors are abough
    # https://matplotlib.org/stable/gallery/color/named_colors.html
    validColors: List[str] = ["tab:blue", "tab:orange", "tab:olive", "black", "tab:pink"]

    # Colors should be expressed in exadecimal
    if aligner == "Diamond":
        shape = "D" # Diamond shape
    elif aligner == "MMseqs":
        shape = "o"
    elif aligner == "BLAST":
        shape = "s"
    else:
        sys.stderr.write(f"\nERROR: unknonw alignment tool ({aligner})!")
        sys.exit(-5)
    # set the color based on the sensitivity
    if sensitivity == "msens":
        # color = "#003300"
        color = "black"
    elif sensitivity == "sens":
        # color = "#99cc00"
        color = "tab:olive"
    if sensitivity == "default":
        color = "orange"
        # color = "#ffcc00"
    elif sensitivity == "fast":
        color = "tab:blue"
        # color = "#66ccff"

    # Set filling style
    if run_type == "Complete":
        fillStyle = "full"
    else:
        fillStyle = "none"

    # Return the tuple with the results
    return(nlabel, color, shape, fillStyle)



def plot_qfo_ec_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "EC"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "EC"')
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    ecTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #ecTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    ecTest.set_xlabel("Recall\n(number of orthologous relationships)")
    ecTest.set_ylabel("Precision\n(average Schlicker similarity)")
    ecTest.set_title("Enzyme classification test")
    ecTest.spines["right"].set_visible(False)
    ecTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    ecTest.yaxis.set_ticks_position("left")
    ecTest.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        ecTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    # Set limits and legend
    #ecTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    ecTest.set_xlim(174000, 214001)
    start, end = ecTest.get_xlim()
    stepSizeX: int = 10000
    print(start, end)
    ecTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    ecTest.set_ylim(0.90, 0.92501)
    start, end = ecTest.get_ylim()
    print(start, end)
    stepSizeY: float = 0.005
    ecTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        ecTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #ecTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
        print(tmpOutPath)
        # plt.savefig(tmpOutPath, dpi=160)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_go_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "GO"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "GO"')
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    goTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #goTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    goTest.set_xlabel("Recall\n(number of orthologous relationships)")
    goTest.set_ylabel("Precision\n(average Schlicker similarity)")
    goTest.set_title("Gene ontology conservation test")
    goTest.spines["right"].set_visible(False)
    goTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    goTest.yaxis.set_ticks_position("left")
    goTest.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        goTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #goTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    goTest.set_xlim(155000, 185001)
    start, end = goTest.get_xlim()
    stepSizeX: int = 7500
    print(start, end)
    goTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    goTest.set_ylim(0.456, 0.466)
    '''
    start, end = goTest.get_ylim()
    print(start, end)
    stepSizeY: float = 0.005
    goTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        goTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #goTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_treefam_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "TreeFam-A"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "TreeFam-A"')

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    treefamTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #treefamTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    treefamTest.set_xlabel("Recall\n(true positive rate)")
    treefamTest.set_ylabel("Precision\n(positive predictive value)")
    # treefamTest.set_title("Comparison with TreeFam-A reference genes trees")
    treefamTest.set_title("TreeFam-A")
    treefamTest.spines["right"].set_visible(False)
    treefamTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    treefamTest.yaxis.set_ticks_position("left")
    treefamTest.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        treefamTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    treefamTest.set_xlim(0.61, 0.67)
    start, end = treefamTest.get_xlim()
    stepSizeX: float = 0.01
    print(start, end)
    treefamTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    treefamTest.set_ylim(0.930, 0.945)
    '''
    start, end = treefamTest.get_ylim()
    print(start, end)
    stepSizeY: float = 0.005
    treefamTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        treefamTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_swisstree_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "SwissTrees"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "SwissTrees"')

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    swisstreeTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #swisstreeTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    swisstreeTest.set_xlabel("Recall\n(true positive rate)")
    swisstreeTest.set_ylabel("Precision\n(positive predictive value)")
    # swisstreeTest.set_title("Comparison with SwissTree reference genes trees")
    swisstreeTest.set_title("SwissTree")
    swisstreeTest.spines["right"].set_visible(False)
    swisstreeTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    swisstreeTest.yaxis.set_ticks_position("left")
    swisstreeTest.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        swisstreeTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #swisstreeTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    swisstreeTest.set_xlim(0.62, 0.72)
    '''
    start, end = swisstreeTest.get_xlim()
    stepSizeX: int = 0.02
    swisstreeTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    '''
    # 0.9384432435035706 0.9425380825996399
    # Set Y limits
    swisstreeTest.set_ylim(0.90, 0.98)
    start, end = swisstreeTest.get_ylim()
    '''
    print(start, end)
    stepSizeY: float = 0.002
    swisstreeTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        swisstreeTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # swisstreeTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_luca_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")

    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Luca"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Luca" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdLuca1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdLuca1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdLuca1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdLuca1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdLuca1.set_title("Generalized Species Tree Discordance (LUCA)")
    gstdLuca1.set_title("Last Universal Common Ancestor (LUCA)")
    gstdLuca1.spines["right"].set_visible(False)
    gstdLuca1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdLuca1.yaxis.set_ticks_position("left")
    gstdLuca1.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)

        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        gstdLuca1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #gstdLuca1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdLuca1.set_xlim(800, 2000)
    '''
    start, end = gstdLuca1.get_xlim()
    stepSizeX: int = 200
    print(start, end)
    gstdLuca1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    '''
    # Set Y limits
    gstdLuca1.set_ylim(0.22, 0.2901)
    start, end = gstdLuca1.get_ylim()
    stepSizeY: float = 0.01
    gstdLuca1.yaxis.set_ticks(np.arange(start, end, stepSizeY))

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdLuca1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdLuca1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_vertebrata_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")

    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Vertebrata"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Vertebrata" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdVertebrata1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdVertebrata1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdVertebrata1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdVertebrata1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdVertebrata1.set_title("Generalized Species Tree Discordance (Vertebrata)")
    gstdVertebrata1.set_title("Vertebrata")
    gstdVertebrata1.spines["right"].set_visible(False)
    gstdVertebrata1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdVertebrata1.yaxis.set_ticks_position("left")
    gstdVertebrata1.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        gstdVertebrata1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #gstdVertebrata1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    # 21518.0 22263.0
    # 0.23733332753181458 0.2473333328962326

    gstdVertebrata1.set_xlim(21250, 22251)
    start, end = gstdVertebrata1.get_xlim()
    stepSizeX: int = 250
    # print(start, end)
    gstdVertebrata1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    gstdVertebrata1.set_ylim(0.23, 0.26)
    '''
    start, end = gstdVertebrata1.get_ylim()
    stepSizeY: float = 0.01
    gstdVertebrata1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdVertebrata1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdVertebrata1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_eukaryota_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")

    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Eukaryota"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Eukaryota" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdEukaryota1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdEukaryota1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdEukaryota1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdEukaryota1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdEukaryota1.set_title("Generalized Species Tree Discordance (Eukaryota)")
    gstdEukaryota1.set_title("Eukaryota")
    gstdEukaryota1.spines["right"].set_visible(False)
    gstdEukaryota1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdEukaryota1.yaxis.set_ticks_position("left")
    gstdEukaryota1.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        gstdEukaryota1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdEukaryota1.set_xlim(3000, 5001)
    start, end = gstdEukaryota1.get_xlim()
    stepSizeX: int = 250
    # print(start, end)
    gstdEukaryota1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    gstdEukaryota1.set_ylim(0.21, 0.245)
    start, end = gstdEukaryota1.get_ylim()
    '''
    stepSizeY: float = 0.01
    gstdEukaryota1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdEukaryota1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_fungi_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Fungi"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Fungi" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdFungi1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdFungi1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdFungi1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdFungi1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdFungi1.set_title("Generalized Species Tree Discordance (Fungi)")
    gstdFungi1.set_title("Fungi")
    gstdFungi1.spines["right"].set_visible(False)
    gstdFungi1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdFungi1.yaxis.set_ticks_position("left")
    gstdFungi1.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        gstdFungi1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #gstdFungi1.legend(numpoints=1, loc='lower left', frameon=False)

    gstdFungi1.set_xlim(8000, 12501)
    start, end = gstdFungi1.get_xlim()
    stepSizeX: int = 750
    # print(start, end)
    gstdFungi1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    gstdFungi1.set_ylim(0.24, 0.29)
    '''
    start, end = gstdFungi1.get_ylim()
    stepSizeY: float = 0.01
    gstdFungi1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdFungi1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdFungi1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_std_eukaryota_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "STD_Eukaryota"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "STD_Eukaryota" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    stdEukaryota = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdEukaryota.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdEukaryota.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdEukaryota.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdEukaryota.set_title("Species Tree Discordance (Eukaryota)")
    stdEukaryota.set_title("Eukaryota")
    stdEukaryota.spines["right"].set_visible(False)
    stdEukaryota.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdEukaryota.yaxis.set_ticks_position("left")
    stdEukaryota.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        stdEukaryota.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #stdEukaryota.legend(numpoints=1, loc='lower left', frameon=False)
    stdEukaryota.set_xlim(11600, 13101)
    start, end = stdEukaryota.get_xlim()
    stepSizeX: int = 250
    stdEukaryota.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # Set Y limits
    stdEukaryota.set_ylim(0.034, 0.044)
    '''
    start, end = stdEukaryota.get_ylim()
    stepSizeY: float = 0.002
    stdEukaryota.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        stdEukaryota.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #stdEukaryota.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_std_fungi_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "STD_Fungi"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "STD_Fungi" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    stdFungi = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdFungi.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdFungi.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdFungi.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdFungi.set_title("Species Tree Discordance (Fungi)")
    stdFungi.set_title("Fungi")
    stdFungi.spines["right"].set_visible(False)
    stdFungi.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdFungi.yaxis.set_ticks_position("left")
    stdFungi.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        stdFungi.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #stdFungi.legend(numpoints=1, loc='lower left', frameon=False)
    stdFungi.set_xlim(7500, 10000)
    start, end = stdFungi.get_xlim()
    '''
    stepSizeX: int = 200
    stdFungi.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    '''
    # Set Y limits
    stdFungi.set_ylim(0.30, 0.34)
    start, end = stdFungi.get_ylim()
    # stepSizeY: float = 0.002
    # stdFungi.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        stdFungi.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #stdFungi.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_std_bacteria_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "STD_Bacteria"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "STD_Bacteria" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    stdBacteria = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdBacteria.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdBacteria.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdBacteria.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdBacteria.set_title("Species Tree Discordance (Bacteria)")
    stdBacteria.set_title("Bacteria")
    stdBacteria.spines["right"].set_visible(False)
    stdBacteria.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdBacteria.yaxis.set_ticks_position("left")
    stdBacteria.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        stdBacteria.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)
    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(min(tmpDf["x"]), max(tmpDf["x"]))
    print(min(tmpDf["y"]), max(tmpDf["y"]))
    print("####################\n")
    # Set limits and legend
    #stdBacteria.legend(numpoints=1, loc='lower left', frameon=False)
    stdBacteria.set_xlim(600, 1200)
    start, end = stdBacteria.get_xlim()
    '''
    stepSizeX: int = 200
    stdBacteria.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    '''
    # Set Y limits
    stdBacteria.set_ylim(0.52, 0.62)
    start, end = stdBacteria.get_ylim()
    '''
    stepSizeY: float = 0.002
    stdBacteria.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    '''
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        stdBacteria.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #stdBacteria.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    paretoScript = os.path.abspath(args.pareto_script)
    # methods = args.methods
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    outFmt: str = args.format
    hueCol: int = args.hue
    multicolVar: int = args.multi_col_name
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)
    makedir(plotsDir)


    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Pareto script:\t{paretoScript}")

    # Read dataframe
    # The hdr of datapoint file have the following format:
    # method test run_type aligner sensitivity x y xerr yerr xlabel ylabel
    df = pd.read_csv(inTbl, sep="\t", dtype= {"method": str, "test": str, "run_type": str, "aligner": str, "sensitivity": str, "x": np.float32, "y": np.float32, "xerr": np.float32, "yerr": np.float32, "xlabel": str, "ylabel": str}, engine="c")

    # print(df.dtypes)
    # Set a custom color palette
    # Use this when showing the prediction errors
    # myPalette = ["#FF0B04", "#4374B3", "#000000"]

    # Show or hide X and Y labels
    hideXlab = True
    hideYlab = True

    # Plot benchmark points
    # EC Test
    plot_qfo_ec_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GO Test
    plot_qfo_go_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # TreeFam Test
    plot_qfo_treefam_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # SwissTree Test
    plot_qfo_swisstree_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    ### GSTD tests ###
    # GSTD Luca Test
    plot_qfo_gstd_luca_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Vertebrate Test
    plot_qfo_gstd_vertebrata_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Eukaryota Test
    plot_qfo_gstd_eukaryota_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Fungi Test
    plot_qfo_gstd_fungi_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    ### STD tests ###
    # STD Eukaryota Test
    plot_qfo_std_eukaryota_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # STD Fungi Test
    plot_qfo_std_fungi_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # STD Bacteria Test
    plot_qfo_std_bacteria_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)

    '''
    # Suggested X and Y limits
    xlims = (30, 100)
    ylims = (-5, 30)
    xCol: str = "speedup"
    yCol: str = "mean-aai"
    '''


if __name__ == "__main__":
    main()
