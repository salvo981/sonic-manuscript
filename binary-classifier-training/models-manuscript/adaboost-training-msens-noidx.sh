date


# echo $'\n@@@@ adaboost-grid-maxdepth5-search-mmseqs-msens-noidx no-weights @@@@'
# python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --no-mol-weights --max-depth 5 --threads 120 --out-prefix mmseqs_msens.no-weights &> log.no-weights.training_gridsearch_maxdepth5_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth4-search-mmseqs-msens-noidx no-weights @@@@'
# python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --no-mol-weights --max-depth 4 --threads 120 --out-prefix mmseqs_msens.no-weights &> log.no-weights.training_gridsearch_maxdepth4_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth3-search-mmseqs-msens-noidx no-weights @@@@'
# python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --no-mol-weights --max-depth 3 --threads 120 --out-prefix mmseqs_msens.no-weights &> log.no-weights.training_gridsearch_maxdepth3_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth2-search-mmseqs-msens-noidx no-weights @@@@'
# python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --no-mol-weights --max-depth 2 --threads 120 --out-prefix mmseqs_msens.no-weights &> log.no-weights.training_gridsearch_maxdepth2_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth1-search-mmseqs-msens-noidx no-weights @@@@'
# python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --no-mol-weights --max-depth 1 --threads 120 --out-prefix mmseqs_msens.no-weights &> log.no-weights.training_gridsearch_maxdepth1_mmseqs_msens_noidx_trdata.txt

# wait


# Use 12 features, including the weights

echo $'\n@@@@ adaboost-grid-maxdepth5-search-mmseqs-msens-noidx @@@@'
python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --max-depth 5 --threads 120 --out-prefix mmseqs_msens &> log.training_gridsearch_maxdepth5_mmseqs_msens_noidx_trdata.txt

wait

echo $'\n@@@@ adaboost-grid-maxdepth4-search-mmseqs-msens-noidx @@@@'
python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --max-depth 4 --threads 120 --out-prefix mmseqs_msens &> log.training_gridsearch_maxdepth4_mmseqs_msens_noidx_trdata.txt

wait

echo $'\n@@@@ adaboost-grid-maxdepth3-search-mmseqs-msens-noidx @@@@'
python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --max-depth 3 --threads 120 --out-prefix mmseqs_msens &> log.training_gridsearch_maxdepth3_mmseqs_msens_noidx_trdata.txt

wait

echo $'\n@@@@ adaboost-grid-maxdepth2-search-mmseqs-msens-noidx @@@@'
python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --max-depth 2 --threads 120 --out-prefix mmseqs_msens &> log.training_gridsearch_maxdepth2_mmseqs_msens_noidx_trdata.txt

wait

echo $'\n@@@@ adaboost-grid-maxdepth1-search-mmseqs-msens-noidx @@@@'
python3 ~/work_repos/sonic-manuscript/adaboost-training/adaboost_grid_search_from_raw_tr_data.py -r tables/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o . --max-depth 1 --threads 120 --out-prefix mmseqs_msens &> log.training_gridsearch_maxdepth1_mmseqs_msens_noidx_trdata.txt

wait

# All runs completed
date