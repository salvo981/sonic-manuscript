#!/bin/bash
# Perfom the memory estimation memory_profiler (aka mprof), create a plot and print memory peak
# https://pypi.org/project/memory-profiler/

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# of2 -> OrthoFinder2
# po6 -> ProteinOrtho6
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
of2_cmd=~/work_repos/OrthoFinder_v254/orthofinder.py
po6_cmd=~/work_repos/proteinortho/proteinortho6.pl
broccoli_cmd=~/work_repos/Broccoli/broccoli.py
fasttree_cmd=/usr/local/bin/FastTree
root=/ssd_home/sp2-genome-biology-review-runs/
# root=~/Desktop/sp2-genome-biology-review-runs/
img_fmt=".mprof.png" # tested with svg and png

# Input and cpu counts
input_root=$root\input/
input_set=qfo20
cpu_cnt=128
# --backend {psutil,psutil_pss,psutil_uss,posix}
backend=psutil_pss
# This should be used only for testing purposes
# It might use too much memory and cause the system to crash
of2_parallel_algo_threads=128 # by default it is set to 1

# Output directories
output_root=$root\mprof_output/
mkdir $output_root
sp_output_root=$output_root\sp2/
mkdir $sp_output_root
of2_output_root=$output_root\of2/
mkdir $of2_output_root
po6_output_root=$output_root\po6/
mkdir $po6_output_root
broccoli_output_root=$output_root\broccoli/
mkdir $broccoli_output_root

# SonicParanoid2 (default)
sp_mode=default-mprof-$backend
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
sp_mprof_dat_file=$sp_output_root$sp_run_name_prefix\.mprof.dat
sp_mprof_log_file=$sp_output_root\log.$sp_run_name_prefix.txt
sp_mprof_err_file=$sp_output_root\err.$sp_run_name_prefix.txt
sp_mprof_memory_peak_file=$sp_output_root\peak.$sp_run_name_prefix.txt
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'

# SP2-default [used in manuscript]
echo mprof run --nopython --backend $backend --output $sp_mprof_dat_file  sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt \> $sp_mprof_log_file 2\> $sp_mprof_err_file

# Profile the memory
mprof run --nopython --backend $backend --output $sp_mprof_dat_file sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt > $sp_mprof_log_file 2> $sp_mprof_err_file
wait

# Plot memory usage
mprof plot $sp_mprof_dat_file --title $sp_run_name_prefix --output $sp_output_root$sp_run_name_prefix$img_fmt
wait

# Extract memory peak
mprof peak $sp_mprof_dat_file > $sp_mprof_memory_peak_file
wait

echo $'\n'
date


# SonicParanoid2 (default graph-only)
sp_mode=default-mprof-go-$backend
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
sp_mprof_dat_file=$sp_output_root$sp_run_name_prefix\.mprof.dat
sp_mprof_log_file=$sp_output_root\log.$sp_run_name_prefix.txt
sp_mprof_err_file=$sp_output_root\err.$sp_run_name_prefix.txt
sp_mprof_memory_peak_file=$sp_output_root\peak.$sp_run_name_prefix.txt
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'

# SP2-default [used in manuscript]
echo mprof run --nopython --backend $backend --output $sp_mprof_dat_file  sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt -go \> $sp_mprof_log_file 2\> $sp_mprof_err_file

# Profile the memory
mprof run --nopython --backend $backend --output $sp_mprof_dat_file sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt -go > $sp_mprof_log_file 2> $sp_mprof_err_file
wait

# Plot memory usage
mprof plot $sp_mprof_dat_file --title $sp_run_name_prefix --output $sp_output_root$sp_run_name_prefix$img_fmt
wait

# Extract memory peak
mprof peak $sp_mprof_dat_file > $sp_mprof_memory_peak_file
wait

echo $'\n'
date


# OrthoFinder Runs #

# OrthoFinder run Default
of2_mode=default-mprof-$backend
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus
of2_mprof_dat_file=$of2_output_root$of2_run_name_prefix\.mprof.dat
of2_mprof_log_file=$of2_output_root\log.$of2_run_name_prefix.txt
of2_mprof_err_file=$of2_output_root\err.$of2_run_name_prefix.txt
of2_mprof_memory_peak_file=$of2_output_root\peak.$of2_run_name_prefix.txt
echo $'\n@@@@'$' OrthoFinder run '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'

echo mprof run --nopython --backend $backend --output $of2_mprof_dat_file python3 $of2_cmd -f $input_root$input_set/ -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt \> $of2_mprof_log_file 2\> $of2_mprof_err_file

mprof run --nopython --backend $backend --output $of2_mprof_dat_file python3 $of2_cmd -f $input_root$input_set/ -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt > $of2_mprof_log_file 2> $of2_mprof_err_file
wait

# Plot memory usage
mprof plot $of2_mprof_dat_file --title $of2_run_name_prefix --output $of2_output_root$of2_run_name_prefix$img_fmt
wait

# Extract memory peak
mprof peak $of2_mprof_dat_file > $of2_mprof_memory_peak_file
wait

# OrthoFinder run Reviewer 1
of2_mode=a$of2_parallel_algo_threads-mprof-$backend
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus
of2_mprof_dat_file=$of2_output_root$of2_run_name_prefix\.mprof.dat
of2_mprof_log_file=$of2_output_root\log.$of2_run_name_prefix.txt
of2_mprof_err_file=$of2_output_root\err.$of2_run_name_prefix.txt
of2_mprof_memory_peak_file=$of2_output_root\peak.$of2_run_name_prefix.txt
# echo $'\n@@@@'$' OrthoFinder run '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'

# echo mprof run --nopython --backend $backend -C --output $of2_mprof_dat_file python3 $of2_cmd -f $input_root$input_set/ -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt -a $of2_parallel_algo_threads \> $of2_mprof_log_file 2\> $of2_mprof_err_file

# mprof run --nopython --backend $backend -C --output $of2_mprof_dat_file python3 $of2_cmd -f $input_root$input_set/ -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt  -a $of2_parallel_algo_threads > $of2_mprof_log_file 2> $of2_mprof_err_file
# wait

# Plot memory usage
# mprof plot $of2_mprof_dat_file --title $of2_run_name_prefix --output $of2_output_root$of2_run_name_prefix$img_fmt
# wait

# Extract memory peak
# mprof peak $of2_mprof_dat_file > $of2_mprof_memory_peak_file
# wait

# echo $'\n'

# OrthoFinder (MSA)
of2_mode=Mmsa-mprof-$backend
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus
of2_mprof_dat_file=$of2_output_root$of2_run_name_prefix\.mprof.dat
of2_mprof_log_file=$of2_output_root\log.$of2_run_name_prefix.txt
of2_mprof_err_file=$of2_output_root\err.$of2_run_name_prefix.txt
of2_mprof_memory_peak_file=$of2_output_root\peak.$of2_run_name_prefix.txt
echo $'\n@@@@'$' OrthoFinder run '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'

echo mprof run --nopython --backend $backend --output $of2_mprof_dat_file python3 $of2_cmd -f $input_root$input_set/ -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt -M msa \> $of2_mprof_log_file 2\> $of2_mprof_err_file

mprof run --nopython --backend $backend --output $of2_mprof_dat_file python3 $of2_cmd -f $input_root$input_set/ -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt -M msa > $of2_mprof_log_file 2> $of2_mprof_err_file
wait

# Plot memory usage
mprof plot $of2_mprof_dat_file --title $of2_run_name_prefix --output $of2_output_root$of2_run_name_prefix$img_fmt
wait

# Compute peak memory
mprof peak $of2_mprof_dat_file > $of2_mprof_memory_peak_file
wait

echo $'\n'

# ProteinOrtho6 run [default]
po6_mode=default-mprof-$backend
po6_run_name_prefix=po6-$po6_mode-$input_set-$cpu_cnt\cpus
po6_mprof_dat_file=$po6_output_root$po6_run_name_prefix\.mprof.dat
po6_mprof_log_file=$po6_output_root\log.$po6_run_name_prefix.txt
po6_mprof_err_file=$po6_output_root\err.$po6_run_name_prefix.txt
po6_mprof_memory_peak_file=$po6_output_root\peak.$po6_run_name_prefix.txt
echo $'\n@@@@'$' ProteinOrtho run '$cpu_cnt$' cpus' $input_set $po6_run_name_prefix $'@@@@\n'

# Create and enter the po6 output directory
mkdir $po6_output_root$po6_run_name_prefix
wait
cd $po6_output_root$po6_run_name_prefix
wait

echo mprof run --nopython --backend $backend --output $po6_mprof_dat_file $po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* \> $po6_mprof_log_file 2\> $po6_mprof_log_file

mprof run --nopython --backend $backend --output $po6_mprof_dat_file $po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* > $po6_mprof_log_file 2> $po6_mprof_log_file
wait

# Plot memory usage
mprof plot $po6_mprof_dat_file --title $po6_run_name_prefix --output $po6_output_root$po6_run_name_prefix$img_fmt
wait

# Compute peak memory
mprof peak $po6_mprof_dat_file > $po6_mprof_memory_peak_file
wait

echo $'\n'
date

# Broccoli run [default]
# broccoli_mode=default-mprof-$backend
# broccoli_run_name_prefix=broccoli-$broccoli_mode-$input_set-$cpu_cnt\cpus
# broccoli_mprof_dat_file=$broccoli_output_root$broccoli_run_name_prefix\.mprof.dat
# broccoli_mprof_log_file=$broccoli_output_root\log.$broccoli_run_name_prefix.txt
# broccoli_mprof_err_file=$broccoli_output_root\err.$broccoli_run_name_prefix.txt
# broccoli_mprof_memory_peak_file=$broccoli_output_root\peak.$broccoli_run_name_prefix.txt
# echo $'\n@@@@'$' Broccoli run '$cpu_cnt$' cpus' $input_set $broccoli_run_name_prefix $'@@@@\n'

# Create and enter the broccoli output directory
# mkdir $broccoli_output_root$broccoli_run_name_prefix
# wait
# cd $broccoli_output_root$broccoli_run_name_prefix
# wait

# echo mprof run --nopython --backend $backend --output $broccoli_mprof_dat_file python3 $broccoli_cmd -dir $input_root$input_set -path_fasttree $fasttree_cmd -ext faa -threads $cpu_cnt \> $broccoli_mprof_log_file 2\> $broccoli_mprof_err_file

# mprof run --nopython --backend $backend --output $broccoli_mprof_dat_file python3 $broccoli_cmd -dir $input_root$input_set -path_fasttree $fasttree_cmd -ext faa -threads $cpu_cnt > $broccoli_mprof_log_file 2> $broccoli_mprof_err_file
# wait

# Plot memory usage
# mprof plot $broccoli_mprof_dat_file --title $broccoli_run_name_prefix --output $broccoli_output_root$broccoli_run_name_prefix$img_fmt
# wait

# Compute peak memory
# mprof peak $broccoli_mprof_dat_file > $broccoli_mprof_memory_peak_file
# wait

# All runs completed
# date