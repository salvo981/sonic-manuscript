#!/bin/bash

# This script performs runs in which sp2 randomly selects the interproteomes instead of using the adaboost predictions.

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=qfo20
cpu_cnt=128
# Controls the number of RUNs in each benchmark
hyperfine_runs=10

# Output directories
output_root=$root\output_random_interproteome_selection/
mkdir $output_root
sp_output_root=$output_root
mkdir $sp_output_root

# SonicParanoid2 runs
# The sensivity set through the following command
sp_mode=default
# Because this test only regards the adaboost predictions, domain orthology and groups creation are skipped
sp_extra_params=$'-go -sm -ow'
sp_run_name_prefix=sp2-random-interproteome-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs

# SonicParanoid2 (default)
echo $'\n@@@@'$' SonicParanoid2 '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 (fast)
sp_mode=fast
sp_extra_params=$'-go -sm -ow'
sp_run_name_prefix=sp2-random-interproteome-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 (sensitive)
sp_mode=sensitive
sp_extra_params=$'-go -sm -ow'
sp_run_name_prefix=sp2-random-interproteome-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

# All runs completed
echo $'\n'
date

echo $'Make sure to load the environment in which the standand sp2 (which uses adaboost) is installed, before proceeding\n'

# exit -5

### Runs using AdaBoost

# Controls the number of RUNs in each benchmark
hyperfine_runs=1
# The sensivity set through the following command
sp_mode=default
# Because this test only regards the adaboost predictions, domain orthology and groups creation are skipped
sp_extra_params=$'-go -sm -ow'
sp_run_name_prefix=sp2-adaboost-no-domain-no-ogs-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs

# SonicParanoid2 AdaBoost (default)
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date


# SonicParanoid2 (fast)
sp_mode=fast
sp_extra_params=$'-go -sm -ow'
sp_run_name_prefix=sp2-adaboost-no-domain-no-ogs-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 (sensitive)
sp_mode=sensitive
sp_extra_params=$'-go -sm -ow'
sp_run_name_prefix=sp2-adaboost-no-domain-no-ogs-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date
