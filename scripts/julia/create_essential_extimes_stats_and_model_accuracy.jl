using Base: iscontiguous, Float64
#=
Procees archives with datapoints from qfo20 benchmark and generate datapoints for plotting.
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using DataFrames
using CSV
using MLJ
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using ProcessEssentialExtimes



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "data-points"
            nargs = 1
            arg_type = String
            help = "A TSV table datapoints regarding the essential execution times."
            required = true
        "--output-dir"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Directory that will contain the output files."
            required = true
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""
Compute simple stats and accuracy measures based on the input datapoints.
"""
function compute_stats_and_accuracy(essentialsDatapoints::String, outDir::String)::String
    @debug "compute_stats_and_accuracy :: START" essentialsDatapoints outDir cleanup
    
    # tmp variables
    outname::String = "stats.$(basename(essentialsDatapoints))"
    outPath::String = joinpath(outDir, outname)

    #=
    =#
    # Essential extime datapoint files have the following format
    # pair prot-cnt-a prot-cnt-b prot-size-a prot-size-b tot-time-ca tot-time-ra speedup pctA pctB avgPct fastest prediction correct-prediction ortho-cnt mean-aai std-aai ortho-fraction-comparem empire-a empire-b empire-class domain-a domain-b domain-class
    # Load only columns that are required for the stats
    df = CSV.File(essentialsDatapoints, delim="\t", select=["tot-time-ca", "tot-time-ra", "speedup", "pctA", "pctB", "avgPct", "correct-prediction", "mean-aai", "ortho-fraction-comparem"]) |> DataFrame
    statsDf = describe(df, :all)
    # Drop unwanted columns
    statsDf = select!(statsDf, Not(:nmissing))
    statsDf = select!(statsDf, Not(:nunique))
    statsDf = select!(statsDf, Not(:eltype))
    statsDf = select!(statsDf, Not(:first))
    statsDf = select!(statsDf, Not(:last))

    # println(names(statsDf))
    CSV.write(outPath, statsDf, delim="\t")
    # Write the pareto points in the output file
    # return the file with datapoints

    # Reload the df extracting only the columns
    df = CSV.File(essentialsDatapoints, delim="\t", select=["fastest", "prediction", "correct-prediction"]) |> DataFrame

    # Define some variables
    correctCnt::Int = sum(df[!, "correct-prediction"])
    smplCnt::Int = size(df)[1]

    # Extract the prediction vectors for evaluation
    # Convert the gold standard to strings
    catArray::Array{String, 1} = [x == 0 ? "slow" : "fast" for x in df[!, "prediction"]]
    pred = categorical(catArray, ordered = true, levels=["slow", "fast"])
    catArray = [x == 0 ? "slow" : "fast" for x in df[!, "fastest"]]
    # ground truth
    gt = categorical(catArray, ordered = true, levels=["slow", "fast"])
    # @show scitype(pred) length(pred) scitype(gt) length(gt)

    # Compute confusion matrix
    ofd::IOStream = open(outPath, "a")
    # cmtx = confusion_matrix(pred, gt)
    # tp, fp, fn, tn =cmtx.mat
    # Compute other measures
    #=
    fp::Int = false_positive(pred, gt)
    fn::Int = false_negative(pred, gt)
    fpr::Float64 = true_positive_rate(pred, gt)
    tp::Int = true_positive(pred, gt)
    tn::Int = true_negative(pred, gt)
    tpr::Float64 = true_positive_rate(pred, gt) # sensitivity, recall
    acc::Float64 = accuracy(pred, gt)
    mcc::Float64 = matthews_correlation(pred, gt)
    fscore::Float64 = f1score(pred, gt)
    =#

    write(ofd, "\nPerformance measures:")
    write(ofd, "\nTP:\t$(true_positive(pred, gt))")
    write(ofd, "\nFP:\t$(false_positive(pred, gt))")
    write(ofd, "\nTN:\t$(true_negative(pred, gt))")
    write(ofd, "\nFN:\t$(false_negative(pred, gt))")
    write(ofd, "\nTPR/sensitivity/recall:\t$(true_positive_rate(pred, gt))")
    write(ofd, "\nTNR/specificity:\t$(true_negative_rate(pred, gt))")
    write(ofd, "\nFPR:\t$(false_positive_rate(pred, gt))")
    write(ofd, "\nFNR/miss rate:\t$(false_negative_rate(pred, gt))")
    write(ofd, "\nPPV/Precision:\t$(positive_predictive_value(pred, gt))")
    write(ofd, "\nNPV:\t$(negative_predictive_value(pred, gt))")
    write(ofd, "\nAccuracy:\t$(accuracy(pred, gt))")
    write(ofd, "\nMCC:\t$(matthews_correlation(pred, gt))")
    write(ofd, "\nF1score:\t$(f1score(pred, gt))\n")


    # write(ofd, cmtx)
    # write(ofd, display(cmtx))

    close(ofd)

    # related to predictions
    return outPath

end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

essentialsDatapoints = realpath(args["data-points"][1])
outDir = abspath(args["output-dir"])
makedir(outDir)
# outTbl = abspath(args["output-tbl"])
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Stats on ex times will be computed with the following parameters:" essentialsDatapoints outDir debug

# Compute stats
compute_stats_and_accuracy(essentialsDatapoints, outDir)