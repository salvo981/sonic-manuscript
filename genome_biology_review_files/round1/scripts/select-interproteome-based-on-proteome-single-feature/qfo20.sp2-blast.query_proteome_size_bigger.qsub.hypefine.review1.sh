#!/bin/bash

#PBS -S /usr/bin/bash
#PBS -N sp2-blast-select-interprot-query-proteome-size-bigger
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@k.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

# This script performs runs in which sp2 using a single feature for the interproteomes.

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=qfo20
cpu_cnt=128
# Controls the number of RUNs in each benchmark
hyperfine_runs=1

# Output directories
output_root=$root\output_select_interproteome_using_single_feature/
mkdir $output_root
sp_output_root=$output_root
mkdir $sp_output_root

# SonicParanoid2 runs (BLAST)
# The sensivity set through the following command
sp_mode=blast
# Because this test only regards the adaboost predictions, domain orthology and groups creation are skipped
sp_extra_params=$'--blast -go -sm -ow'
sp_run_name_prefix=sp2-query-proteome-size-bigger-$sp_mode-$input_set-$cpu_cnt\cpus-$hyperfine_runs\runs

# SonicParanoid2 (default)
echo $'\n@@@@'$' SonicParanoid2 '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs $hyperfine_runs --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -t $cpu_cnt $sp_extra_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# All runs completed
echo $'\n'
date