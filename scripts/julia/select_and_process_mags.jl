#=
Process the table with metadata for MAGS obtained from the study from Nayfach et al.
https://doi.org/10.1038/s41587-020-0718-6
The original MAGs and metadata were obtained from https://portal.nersc.gov/GEM/
Metadata file: /genomes/genome_metadata.tsv
MAGs containing protein sequences: /genomes/faa.tar
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using Distributed
using Logging
using DataFrames
using CSV
using DataStructures:counter, Accumulator, OrderedDict


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Try loading the module
using ProcessProteomes



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Randomly select and process MAGs ",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "mag-tar"
            nargs = 1
            help = "TAR archive containing all the MAGs."
            required = true
        "meta-data"
            nargs = 1
            help = "Path to the meta-data table file. "
            required = true
        "--output-dir"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Directory with processed files"
            required = true
        "--gem-id-col"
            nargs = '?'
            arg_type = Int
            default = 1
            help = "Column contaning the GEM ID for each MAG"
            required = false
        "--out-mags"
            arg_type = Int
            default = 2000
            help = "Number of MAGs to extract"
            required = true
        "--threads", "-t"
            # nargs = '?'
            arg_type = Int
            default = 4
            help = "Number of parallel processes"
            required = false
        "--output-tbl"
            nargs = '?'
            arg_type = String
            default = ""
            help = "Table with extra information, including sequence count and proteome size"
            required = true
        "--include-archaea"
            action = :store_true
            help = "Also include Archaeal MAGs."
        "--debug", "-d"
            action = :store_true
            help = "Show debug information."
                end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""Extract and process MAGs listed in the input meta-data table."""
function extract_and_process_mags(tblPath::String, magTarPath::String, outDir::String, outTbl::String)::String
    @debug "extract_and_process_mags :: START" tblPath magTarPath outDir outTbl

    if !ispath(dirname(outTbl))
        @error "You must provide a valid output path for the output table"
        exit(-2)
    end

    # Create a Set to avoid that the same file is processed multiple times
    rawFileSet::Set{String} = Set{String}()
    newFileSet::Set{String} = Set{String}()

    # Set some tmp variables
    newFileName::String = ""
    rawFileName::String = tmpPath = unzippedPath = outPath = ""
    rawProtsDir::String = joinpath(outDir, "faa")
    archivedMagPath::String = ""
    gemId::String = ""

    # Create output table
    ofd::IOStream = open(outTbl, "w")

    # Start reading the file with metadata
    # The original metadata contains following columns
    # genome_id metagenome_id genome_length num_contigs n50 num_16s num_5s num_23s num_trna completeness contaminationquality_score mimag_quality otu_id ecosystem ecosystem_category ecosystem_type habitat longitude latitude
    for ln in eachline(open(tblPath, "r"), keep=false)
        if ln[1:4] == "geno"
            write(ofd, "$(ln)\tproteins\tproteome_size\n")
            continue
        end

        # Lines in the meta-data have the following format
        # nequitans prokaryote archaea UP000000578 228908 Nanoarchaeum equitans (strain Kin4-M)
        gemId = split(ln, "\t", limit=2)[1]
        # @show gemId
        # Files are stored in the TAR archive in paths like the following
        # faa/3300029549_26.faa.gz
        # In the above example 3300029549_26 is the GEM ID
        rawFileName = "$(gemId).faa.gz"
        newFileName = "$(gemId).faa"
        archivedMagPath = "faa/$(gemId).faa.gz"

        # Command example to extract a file from the TAR archive
        # tar --extract --file=faa.tar faa/3300029549_26.faa.gz
        extract_file_from_tar(magTarPath, archivedMagPath, outDir)

        # make sure that the file was not previosuly processed
        if rawFileName ∉ rawFileSet
            push!(rawFileSet, rawFileName)
        else
            @error "File processed multiple times" rawFileName
            exit(-5)
        end
        
        # make sure that the new file is uniq
        if newFileName ∉ newFileSet
            push!(newFileSet, newFileName)
        else
            @error "Repeated output file name!" newFileName
            exit(-5)
        end

        tmpPath = joinpath(rawProtsDir, rawFileName)
        unzippedPath = joinpath(processingDir, "faa/$(gemId).raw.faa")
        outPath = joinpath(processingDir, newFileName)
        # unzip and process the file
        if !isfile(tmpPath)
            @error "The MAG extracted from the TAR archive is missing:" tmpPath uid taxid
            exit(-2)
        end
        pigz_decompress(tmpPath, unzippedPath, keep=true, force=true, errLog=false, threads=threads)
        # println(outPath)
        simplify_gem_mag_hdr(unzippedPath, outPath)
        # Extract sequence stats if required
        fstats = compute_fasta_stats(outPath)
        write(ofd, "$(ln)\t$(fstats.cnt)\t$(fstats.totlen)\n")

        # Remove temporary files and move processed files
        rm(unzippedPath, force=true)
        rm(tmpPath, force=true)

        # break
    end

    close(ofd)

    # Remove the processing directory
    rm(joinpath(processingDir, "faa"), force=true)

    @debug "MAG extraction completed:" length(rawFileSet) length(newFileSet)

    return tblPath
end



"""Randomly select a number of MAG a DataFrame"""
function randomly_select_mags(df::DataFrame, toSelect::Int)::DataFrame
    @debug "randomly_select_mags :: START" size(df)[1] toSelect
    # Count and sort entries by Ecosystem type
    ecoTypeCounter::OrderedDict{String, Int} = OrderedDict{String, Int}(counter(df[!, :ecosystem_type]))
    ecoTypeCounter = sort(ecoTypeCounter, byvalue=true, rev=false)
    # Tmp variables
    availableMags::Int = size(df)[1]
    selectedCnt::Int = 0
    toSelectInEco::Int = 0
    selectedInEco::Int = 0
    availInEco::Int = 0
    ecoPct::Float64 = 0.
    ecoTypeDf::DataFrame = DataFrame()
    tmpSelectedVec::Array{Int, 1} = Int[]
    extractedMagIdx::Int = 0
    extractedDf::DataFrame = DataFrame()
    # Make sure there are available MAGs
    if toSelect > size(df)[1]
        @warn "Not enough MAGs available!" availableMags toSelect
        toSelect = availableMags
        @info "The number of MAGs to be selected has been adjusted accordingly" toSelect
    end
    # Random selection for each eco type
    for eco in keys(ecoTypeCounter)
        # Subset the data frame
        ecoTypeDf = filter(row -> row.ecosystem_type == eco, df)
        availInEco = size(ecoTypeDf)[1]
        ecoPct = availInEco / availableMags
        toSelectInEco = round(ecoPct * toSelect, digits=0)
        if toSelectInEco == 0
            toSelectInEco = 1
        end
        # Perform Random selection
        # Until the number of samples to select has been reached
        # Considering also the total number of samples already selected (selectedCnt)
        while (selectedInEco < toSelectInEco) && (selectedCnt < toSelect)
            extractedMagIdx = rand(1:toSelectInEco)
            if extractedMagIdx ∉ tmpSelectedVec
                push!(tmpSelectedVec, extractedMagIdx)
                selectedCnt += 1
                selectedInEco += 1
            end
        end
        # Sort the vector and extract the df subset
        extractedDf = vcat(extractedDf, ecoTypeDf[sort(tmpSelectedVec), :])
        # Reset variables
        selectedInEco = 0
        empty!(tmpSelectedVec)
    end
    @debug "MAGs selection done!" size(extractedDf)[1] toSelect
    return extractedDf
end



"""Filter meta-data table and select MAGs based on different criteria."""
function select_mags(tblPath::String, toSelect::Int, outDir::String; onlyBacteria::Bool = true, corruptedMags::Array{String, 1} = String[])::String
    @debug "select_mags :: START" tblPath toSelect outDir onlyBacteria length(corruptedMags)
    # The original metadata contains following columns
    # genome_id metagenome_id genome_length num_contigs n50 num_16s num_5s num_23s num_trna completeness contaminationquality_score mimag_quality otu_id ecosystem ecosystem_category ecosystem_type habitat longitude latitude
    # Load the metadata
    df::DataFrame = CSV.read(tblPath, delim="\t", DataFrame)
    rawCnt::Int = size(df)[1]
    corruptedMagsIdxs::Array{Int, 1} = Int[]

    # Remove any corrupted MAGs
    if length(corruptedMags) > 0
        @info "Excluding corrupted MAGS:" corruptedMags
        # Obtain the indexes for the corrupted MAGs
        for (i, v) in enumerate(df[!, :genome_id])
            if v ∈ corruptedMags
                push!(corruptedMagsIdxs, i)
            end
        end

        # Remove all the corrupted indexes from the DF
        if length(corruptedMagsIdxs) > 0
            delete!(df, corruptedMagsIdxs)
            # update the row counts
            rawCnt = size(df)[1]
            println(corruptedMagsIdxs)
            @info "IDs of corrupted MAGs were remove:" length(corruptedMagsIdxs) rawCnt
        end
    end

    # Only keep Bacterial MAGs
    if onlyBacteria
        df = filter!(row -> startswith(row.ecosystem, r"d__Bact"), df)
    end
    # Remove entries with Unclassified ecosystem type
    df = filter!(row -> row.ecosystem_type != "Unclassified", df)
    # Remove entries with less than 25 contigs
    df = filter!(row -> row.ecosystem_type != "Unclassified", df)
    # First select terrestrial samples
    dfSoil::DataFrame = filter(row -> row.ecosystem_category == "Terrestrial", df)
    soilCnt::Int = size(dfSoil)[1]
    dfWater::DataFrame = filter(row -> row.ecosystem_category == "Aquatic", df)
    aquaCnt::Int = size(dfWater)[1]
    totRemaining::Int = aquaCnt + soilCnt
    aquaPct::Float64 = aquaCnt / totRemaining
    soilPct::Float64 = soilCnt / totRemaining
    @info "Meta-data filtering results:" rawCnt soilCnt aquaCnt totRemaining soilPct aquaPct
    # Select and extract Soil MAGs
    toSelectSoil::Int = round(soilPct * toSelect, digits=0)
    selectedDf::DataFrame = randomly_select_mags(dfSoil, toSelectSoil)
    # Select and extract Water MAGs
    toSelectWater::Int = round(aquaPct * toSelect, digits=0)
    selectedDf = vcat(selectedDf, randomly_select_mags(dfWater, toSelectWater))
    outTbl::String = joinpath(outDir, "selected.$(toSelect).$(basename(tblPath))")
    CSV.write(outTbl, selectedDf, delim="\t")
    return outTbl
end




#####  MAIN  #####
args = get_params(ARGS)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

# rawProtsDir = realpath(args["raw-prot-dir"][1])
magTarPath = realpath(args["mag-tar"][1])
metaDataTbl = realpath(args["meta-data"][1])
outDir = abspath(args["output-dir"])
makedir(outDir)
protIdCol = args["gem-id-col"]
outTbl = args["output-tbl"]
magsToExtract = args["out-mags"]
writeOutMetatbl = false
if length(outTbl) > 0
    global outTbl = abspath(outTbl)
    writeOutMetatbl = true
    # ofd = open(outTbl, "w")
end

onlyBacteria = !args["include-archaea"]
threads = args["threads"]
debug = args["debug"]

# IDs of corrupted MAGs (they show an encoding problem when processed using FASTX)
corruptedMags = String["3300027791_6", "3300012108_2"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "GEM MAGs will be processed using the following parameters:" magTarPath metaDataTbl outDir protIdCol outTbl onlyBacteria magsToExtract threads

# addprocs(threads)

processingDir = joinpath(outDir, "processing_dir")
makedir(outDir)
makedir(processingDir)

filteredTblPath = select_mags(metaDataTbl, magsToExtract, outDir, onlyBacteria=onlyBacteria, corruptedMags=corruptedMags)

extract_and_process_mags(filteredTblPath, magTarPath, processingDir, outTbl)

# println("DEBUG!")
# exit(-10)

#=
@show length(rawFileSet)

# Remove the processing directory
rm(processingDir, force=true)

=#