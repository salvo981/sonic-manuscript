import os
import sys
import numpy as np
import pandas as pd
from joblib import dump, load
import pickle
from typing import TextIO, List, Set, Dict, Tuple
import logging

from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_validate
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import normalize
from sklearn.feature_selection import VarianceThreshold, SelectFpr, f_classif, chi2, SelectKBest, RFE, RFECV


def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    import argparse
    parser = argparse.ArgumentParser(description="Feature selection for Adaboost",  usage="%(prog)s --tr-set <TRAINING_SET> --labels <LABELS> -o <OUTPUT_DIRECTORY> --cv-steps <K-FOLDS> --threads <CPUS>", prog="train_adaboost_grid_search.py")

    # Mandatory arguments
    parser.add_argument("-m", "--model", type=str, required=True, help="Path to the training model to be inspeected for feature selection.", default=None)
    parser.add_argument("-tr", "--tr-set", type=str, required=True, help="TSV file with training samples, labels.", default=None)
    parser.add_argument("-lbs", "--labels", type=str, required=True, help="Text file containing the training labels.")
    parser.add_argument("-o", "--output-dir", type=str, required=True, help="The directory in which the generated models, training samples and results will be stored.", default=os.getcwd())

    # General options
    parser.add_argument("-cv", "--cv-steps", type=int, required=False, help="Number of CV steps to be performd. Default=10", default=10)
    parser.add_argument("-t", "--threads", type=int, required=False, help="Number of parallel 1-CPU jobs to be used. Default=4", default=4)
    parser.add_argument("-d", "--debug", required=False, help="Output debug information. (WARNING: extremely verbose)", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def create_reduced_tr_set(srcTrPath: str, srcColNames: List[str], selectedColsBool: List[bool], outTrTbl: str):
    """
    Create a training set containing only the columns for the selected features
    """
    logging.debug(f"create_reduced_tr_set :: START")
    logging.debug(f"Src training data: {srcTrPath}")
    logging.debug(f"Column names of all src features:\t{len(srcColNames)}")
    logging.debug(f"Selected features:\t{len(selectedColsBool)}")
    logging.debug(f"Reduced training set:\t{outTrTbl}")

    # Extract the column names

    # Output the training table with the reduced feature set
    selectedColIdxs: List[int] = [idx for idx, x in enumerate(selectedColsBool) if x]
    selectedColNames: List[str]  = [srcColNames[idx] for idx in selectedColIdxs]

    # Load the required columns
    trDf = pd.read_csv(srcTrPath, sep="\t", usecols=selectedColNames)
    # print(trDf.dtypes)
    # Write the table with the trainig data
    trDf.to_csv(outTrTbl, sep="\t", index=False)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



def main():

    #Get the parameters
    args = get_params()[0]
    oldModelPath: str = os.path.realpath(args.model)
    trPath: str = os.path.realpath(args.tr_set)
    labelsPath: str = os.path.realpath(args.labels)
    outDir: str = os.path.realpath(args.output_dir)

    # obtain the rest of the parameters for the training
    cvSteps: int = args.cv_steps
    threads: int = args.threads
    debug: bool = args.debug

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # makedir(outDir)
    # makedir(modelsDir)
    # makedir(logsDir)

    reducedTrDir: str = os.path.join(outDir, "reduced_train_datasets")
    makedir(reducedTrDir)

    # Show some info
    logging.info("Feature selection for the best model:")
    logging.info(f"Trained model to inspect: {oldModelPath}")
    logging.info(f"Training samples path: {trPath}")
    logging.info(f"Labels file: {labelsPath}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"CV-folds:\t{cvSteps}")
    logging.info(f"Threads:\t{threads}")
    # logging.info(f"Logs directory: {logsDir}")


    # Load model and exatrct features
    refModel = pickle.load(open(oldModelPath, "rb"))
    l_rate: float = refModel.learning_rate
    estimators: int = refModel.n_estimators
    oldFeatCnt: int = refModel.n_features_in_

    print(f"\nLoaded model:\t{os.path.basename(oldModelPath)}")
    print(f"Learning rate:\t{l_rate}")
    print(f"Estimators:\t{estimators}")
    print(f"oldFeatCnt:\t{oldFeatCnt}")


    # Set the other output paths
    '''
    trDir: str = os.path.join(outDir, "training-data")
    modelsDir: str = os.path.join(outDir, "models")
    logsDir = os.path.join(outDir, "logs")

    rootDir = "/Users/salvocos/Google_Drive/Work/utokyo/presentations/progress_reports/27092019/prediction_model"
    trDir = os.path.join(rootDir, "training")
    modelDir = os.path.join(rootDir, "models")
    logsDir = os.path.join(rootDir, "logs")
    '''

    # load training sets
    trDf = pd.read_csv(trPath, sep="\t")
    print(trDf.dtypes)
    trBname: str = os.path.basename(trPath)
    colNames: List[str] = trDf.columns
    # print(trDf.describe())
    features = trDf.values

    # load labels
    # labelsPath = os.path.join(trDir, "labels_final.txt")
    labelsDf = pd.read_csv(labelsPath, sep="\t")
    # print(labelsDf.describe())
    labels = labelsDf.values
    labels = labels.ravel()
    del labelsDf, trDf
    print(features.shape)
    print(labels.shape)

    print("Normalizing...")
    normFeat = normalize(features, norm="l2", axis=0, copy=True, return_norm=False)
    print(normFeat.shape)
    print("Normalization DONE!\n")

    # remove features with low variance
    selector = VarianceThreshold()
    highVarFeat = selector.fit_transform(normFeat)
    print(f"Features with high variance:\t{highVarFeat.shape[1]}")

    # use select K-best
    print("\nFeature selection using SelectKBest:")
    #bestKfeat = SelectKBest(f_classif, k=4).fit_transform(features, labels)
    bestKfeat = SelectKBest(f_classif, k=4)
    bestKfeat.fit(normFeat, labels)
    mask = bestKfeat.get_support()
    print(mask)
    del mask, bestKfeat

    # Recursive feature elimination
    '''
    print("\nFeature selection using RFE:")
    # model = AdaBoostClassifier(base_estimator=None, n_estimators=estimators, learning_rate=l_rate)
    # rfeSelect = RFE(model, n_features_to_select=None, step=1, verbose=1)

    rfeSelect = RFE(refModel, n_features_to_select=None, step=1, verbose=1)

    rfeSelect = rfeSelect.fit(normFeat, labels)
    print(rfeSelect.n_features_)
    print(rfeSelect.get_support())
    print(rfeSelect.ranking_)
    '''

    '''
    print("\nFeature selection using RFECV:")
    # rfeSelect = RFECV(model, step=cvSteps, verbose=1, n_jobs=threads)
    rfeSelect = RFECV(refModel, step=cvSteps, verbose=1, n_jobs=threads)
    rfeSelect = rfeSelect.fit(normFeat, labels)
    print(rfeSelect.n_features_)
    print(rfeSelect.get_support())
    print(rfeSelect.ranking_)
    '''

    # ITERATIVE FEATURE SELECTION FROM MODEL
    # Will contains the idxs and names of the columns for selected features
    newFeatTrName: str = ""
    newFeatTrPath: str = ""
    modelDir: str = os.path.dirname(oldModelPath)

    # Select the most important features
    # print("\nFeature selection from Model:")
    # model.fit(normFeat, labels)
    # featSelector = SelectFromModel(model, prefit=True)

    print("\nFeature selection from Model with threhosld=0.05")
    featSelector = SelectFromModel(refModel, prefit=True, threshold=0.05)
    # featSelector = SelectFromModel(refModel, prefit=True)
    newFeatures = featSelector.transform(normFeat)
    # print(newFeatures.shape)
    print(featSelector.get_support())
    # print(featSelector.get_params())

    # Extract training table with the reduced feature set
    newFeatTrName = trBname.split(".", 1)[1] # remove 'training_smpls.'
    newFeatTrName = f"tr_smpls.{newFeatures.shape[1]}feat_thr_005.{newFeatTrName}"
    newFeatTrPath = os.path.join(reducedTrDir, newFeatTrName)
    create_reduced_tr_set(trPath, colNames, featSelector.get_support(), newFeatTrPath)

    # Train adaboost model using the new features
    model = AdaBoostClassifier(base_estimator=None, n_estimators=estimators, learning_rate=l_rate, algorithm='SAMME.R', random_state=None)
    model.fit(newFeatures, labels)
    evaluationNew = cross_val_score(model, newFeatures, labels, cv=cvSteps, n_jobs=threads)
    print(f"Mean accuracy CV-{cvSteps} (New model: thr=0.05):\t{sum(evaluationNew)/len(evaluationNew):.5f}")
    print(model.feature_importances_)

    # Set vars
    l_rateStr: str = str(l_rate).replace(".", "")
    # store the model in a pickle file
    outModelName: str = f"predict_fast_pair_adaboost_est{estimators}_lr{l_rateStr}_sammer_{newFeatures.shape[1]}feat.selection_thr_005.pckl"
    modelPath = os.path.join(modelDir, outModelName)
    with open(modelPath, "wb") as fd:
        pickle.dump(model, fd)
        print(model.n_features_in_)


    print("\nFeature selection from Model with threhosld=0.10")
    featSelector = SelectFromModel(refModel, prefit=True, threshold=0.10)
    newFeatures = featSelector.transform(normFeat)
    print(featSelector.get_support())

    # Extract training table with the reduced feature set
    newFeatTrName = trBname.split(".", 1)[1] # remove 'training_smpls.'
    newFeatTrName = f"tr_smpls.{newFeatures.shape[1]}feat_thr_010.{newFeatTrName}"
    newFeatTrPath = os.path.join(reducedTrDir, newFeatTrName)
    create_reduced_tr_set(trPath, colNames, featSelector.get_support(), newFeatTrPath)

    # Train adaboost model using the new features
    model = AdaBoostClassifier(base_estimator=None, n_estimators=estimators, learning_rate=l_rate, algorithm='SAMME.R', random_state=None)
    model.fit(newFeatures, labels)
    evaluationNew = cross_val_score(model, newFeatures, labels, cv=cvSteps, n_jobs=threads)
    print(f"Mean accuracy CV-{cvSteps} (New model: thr=0.10):\t{sum(evaluationNew)/len(evaluationNew):.5f}")
    print(model.feature_importances_)

    # store the model in a pickle file
    outModelName: str = f"predict_fast_pair_adaboost_est{estimators}_lr{l_rateStr}_sammer_{newFeatures.shape[1]}feat.selection_thr_010.pckl"
    modelPath = os.path.join(modelDir, outModelName)
    with open(modelPath, "wb") as fd:
        pickle.dump(model, fd)


    print("\nFeature selection from Model with threhosld=mean")
    featSelector = SelectFromModel(refModel, prefit=True, threshold="mean")
    newFeatures = featSelector.transform(normFeat)
    print(featSelector.get_support())

    # Extract training table with the reduced feature set
    newFeatTrName = trBname.split(".", 1)[1] # remove 'training_smpls.'
    newFeatTrName = f"tr_smpls.{newFeatures.shape[1]}feat_thr_mean.{newFeatTrName}"
    newFeatTrPath = os.path.join(reducedTrDir, newFeatTrName)
    create_reduced_tr_set(trPath, colNames, featSelector.get_support(), newFeatTrPath)

    # Train adaboost model using the new features
    model = AdaBoostClassifier(base_estimator=None, n_estimators=estimators, learning_rate=l_rate, algorithm='SAMME.R', random_state=None)
    model.fit(newFeatures, labels)
    evaluationNew = cross_val_score(model, newFeatures, labels, cv=cvSteps, n_jobs=threads)
    print(f"Mean accuracy CV-{cvSteps} (New model: thr=mean):\t{sum(evaluationNew)/len(evaluationNew):.5f}")
    print(model.feature_importances_)

    # store the model in a pickle file
    outModelName: str = f"predict_fast_pair_adaboost_est{estimators}_lr{l_rateStr}_sammer_{newFeatures.shape[1]}feat.selection_thr_mean.pckl"
    modelPath = os.path.join(modelDir, outModelName)
    with open(modelPath, "wb") as fd:
        pickle.dump(model, fd)

    print("\nFeature selection from Model with threhosld=median")
    featSelector = SelectFromModel(refModel, prefit=True, threshold="median")
    newFeatures = featSelector.transform(normFeat)
    print(featSelector.get_support())

    # Extract training table with the reduced feature set
    newFeatTrName = trBname.split(".", 1)[1] # remove 'training_smpls.'
    newFeatTrName = f"tr_smpls.{newFeatures.shape[1]}feat_thr_median.{newFeatTrName}"
    newFeatTrPath = os.path.join(reducedTrDir, newFeatTrName)
    create_reduced_tr_set(trPath, colNames, featSelector.get_support(), newFeatTrPath)

    # Train adaboost model using the new features
    model = AdaBoostClassifier(base_estimator=None, n_estimators=estimators, learning_rate=l_rate, algorithm='SAMME.R', random_state=None)
    model.fit(newFeatures, labels)
    evaluationNew = cross_val_score(model, newFeatures, labels, cv=cvSteps, n_jobs=threads)
    print(f"Mean accuracy CV-{cvSteps} (New model: thr=median):\t{sum(evaluationNew)/len(evaluationNew):.5f}")
    print(model.feature_importances_)

    # store the model in a pickle file
    outModelName: str = f"predict_fast_pair_adaboost_est{estimators}_lr{l_rateStr}_sammer_{newFeatures.shape[1]}feat.selection_thr_median.pckl"
    modelPath = os.path.join(modelDir, outModelName)
    with open(modelPath, "wb") as fd:
        pickle.dump(model, fd)

    # Evaluate reference model
    evaluation = cross_val_score(refModel, normFeat, labels, cv=cvSteps, n_jobs=threads)
    #print(evaluation)
    print(f"\nMean accuracy CV-{cvSteps}:\t{sum(evaluation)/len(evaluation):.5f}")
    print(refModel.feature_importances_)

    # sys.exit("DEBUG")


    # load the model
    # and test on two samples
    loadedModel = pickle.load(open(modelPath, "rb"))
    # this model predict samples with 6 informations as follows
    # seq_diff_folds_b_gt_a size_diff_folds_b_gt_a size_a size_b avg_len_a avg_len_b
    # 3.215 2.762 177065 488987 366.594 314.866
    # print(loadedModel.predict([[3.215, 2.762, 177065, 488987, 366.594, 314.866]]))
    # print(loadedModel.predict([[-3.215, -2.778, 491826, 177065, 316.694, 366.594]]))

if __name__ == "__main__":
    main()
