This directory will contail files with QfO result archive files for SonicParanoid 2 in 3 preset modes (in graph and hybrid mode).  
The resuls files are stored in:  
`/home/salvocos/work_repos/sonic-manuscript/tables/qfo-benchmarks/benchmark_results_s2_hybrid_qfo20-raw-datapoints`

### Benchmark result archived that must be copied inside this directory  
- s2-dm-fast-graph_20220719T101446.tar.gz
- s2-dm-fast-thr75_20220715T161131.tar.gz
- s2-dm-sens-graph_20220719T155831.tar.gz
- s2-dm-sens-thr75_20220715T222831.tar.gz
- s2-mm-msens-graph_20220719T202801.tar.gz
- s2-mm-msens-thr75_20220716T040516.tar.gz