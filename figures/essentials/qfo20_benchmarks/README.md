This directory will contain `illustrations` regarding qfo benchmark results.  
Using and without using essential alignments  
Results BLAST, Diamond, and MMseqs should be included  

The reference illuastration in previous SonicParanoid paper (`Figure_S11`) had the following dimensions:  
 - x: 518.74 px
 - y: 624.51 px

#### Reference plots used for version 1.3.7  
>Archive: plots.qfo-bench-s137-manuscript.tar.xz
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.ec.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.g_std2_eukaryota.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.g_std2_fungi.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.g_std2_luca.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.g_std2_vertebrata.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.go.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.std_bacteria.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.std_eukaryota.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.std_fungi.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.swisstrees.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-s137-manuscript.treefam-a.pareto.svg



#### Reference plots used for version 1.3.6 
>Archive: plots.qfo-bench-s136-manuscript.tar.xz
- /plots/qfo-benchmarks/qfo-bench-manuscript.ec.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.g_std2_eukaryota.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.g_std2_fungi.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.g_std2_luca.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.g_std2_vertebrata.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.go.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.std_bacteria.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.std_eukaryota.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.std_fungi.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.swisstrees.pareto.svg
- /plots/qfo-benchmarks/qfo-bench-manuscript.treefam-a.pareto.svg