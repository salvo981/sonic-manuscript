This directory contains training scripts used to retrain the binary classifiers.  
Retraining was required because from sklearn v1.3.0 negative values were not allowed anymore.  

### New training data 
The new script to generate the training data (``)

### Adaboost
Regards the best model 

### Tests model for single sample prediction
Test samples:  
LINES: 38792~39793
Pairs: 97-149, 149-97

#### Original model
- 97-149 [55855, 536, -104.21, 21729948, 151454, -143.48, 389.042, 282.563, -1.38] label=0
- 149-97 [536, 55855, 104.21, 151454, 21729948, 143.48, 282.56, 389.04, 1.38] label=1
- Best model: /home/salvocos/work_repos/sonic-manuscript/binary-classifier-training/models-manuscript/models/mmseqs_msens.no-weights.adaboost_mdepth2_225_11_sammer_9features_62250samples.pckl

This is working properly with sklearn 1.2.2  
modelManuscript.predict([[55855, 536, -104.21, 21729948, 151454, -143.48, 389.042, 282.563, -1.38]]) -> 0 `OK`  
modelManuscript.predict([[55855, 536, -104.21, 21729948, 151454, -143.48, 389.042, 282.563, -1.38]]) -> [0.79727274, 0.20272726]  
modelManuscript.predict([[536, 55855, 104.21, 151454, 21729948, 143.48, 282.56, 389.04, 1.38]]) -> 1 `OK`  
modelManuscript.predict([[536, 55855, 104.21, 151454, 21729948, 143.48, 282.56, 389.04, 1.38]]) -> [0.32340754, 0.67659246]  


#### Retrained (mantissa)
- 97-149 [55855, 536, 145.793, 21729948, 151454, 106.524, 389.042, 282.563, 248.623] label=0
- 149-97 [536, 55855, 354.21, 151454, 21729948, 393.48, 282.56, 389.04, 251.38] label=1
- Best model: /home/salvocos/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/models/adaboost.mmseqs_msens.no-weights.adaboost_mdepth2_210_11_sammer_9features_62250samples.pckl

#### Retrained (categorical) [NOT WORKING]
- 97-149 [55855, 536, 0, 21729948, 151454, 0, 389.042, 282.563, 0] label=0
- 149-97 [536, 55855, 1, 151454, 21729948, 1, 282.56, 389.04, 1] label=1
- Best model: 
/home/salvocos/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/models/adaboost.mmseqs_msens.no-weights.cat.adaboost_mdepth2_220_10_sammer_9features_62250samples.pckl

modelCat.predict([[55855, 536, 0, 21729948, 151454, 0, 389.042, 282.563, 0]]) -> 1 `IT SHOULD BE 0!`
modelCat.predict_proba([[55855, 536, 0, 21729948, 151454, 0, 389.042, 282.563, 0]]) -> [0.46930746, 0.53069254]
modelCat.predict([[536, 55855, 1, 151454, 21729948, 1, 282.56, 389.04, 1]]) -> 1
modelCat.predict_proba([[536, 55855, 1, 151454, 21729948, 1, 282.56, 389.04, 1]]) -> [0.45448436, 0.54551564]

#### Retrained (lightBgm, categorical data)
- 97-149 [55855, 536, 0, 21729948, 151454, 0, 389.042, 282.563, 0] label=0
- 149-97 [536, 55855, 1, 151454, 21729948, 1, 282.56, 389.04, 1] label=1
- Best model: /home/salvocos/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/lightgbm/models/lightgbm.pckl

modelLbg.predict([[55855, 536, 0, 21729948, 151454, 0, 389.042, 282.563, 0]]) -> 0 `OK`
modelCat.predict_proba([[55855, 536, 0, 21729948, 151454, 0, 389.042, 282.563, 0]]) -> [0.96415408, 0.03584592]
modelCat.predict([[536, 55855, 1, 151454, 21729948, 1, 282.56, 389.04, 1]]) -> 1 `OK`
modelCat.predict_proba([[536, 55855, 1, 151454, 21729948, 1, 282.56, 389.04, 1]]) -> [0.09075095, 0.90924905]

