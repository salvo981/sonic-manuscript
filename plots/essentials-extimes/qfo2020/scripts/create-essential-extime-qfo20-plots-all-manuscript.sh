# Generate different plots regarding the execution times for essentials
# Using diiferent colors for each category

# MMseqs msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-msens.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-msens-extimes

# MMseqs sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-sens.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-sens-extimes

# MMseqs default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-default.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-default-extimes

# MMseqs fast
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-fast.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-fast-extimes

# DIAMOND RELATED TABLES
# Diamond msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-msens.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-msens-extimes

# Diamond sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-sens.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-sens-extimes

# Diamond default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-default.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-default-extimes

# Diamond fast
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-fast.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-fast-extimes

# BLAST default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-blast-ess-default.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-blast-default-extimes