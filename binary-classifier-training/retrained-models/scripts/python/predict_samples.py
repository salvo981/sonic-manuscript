'''
Predict a set of samples using a user specified prediction model
'''

import os
import sys
import pickle
import time
import logging
from itertools import combinations
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, f1_score, matthews_corrcoef, confusion_matrix


def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    import argparse
    parser = argparse.ArgumentParser(description="predict_samples",  usage="", prog="predict_samples.py")

    # Mandatory arguments
    parser.add_argument("-s", "--snapshot", type=str, required=True, help="Snapshot file from SonicParanoid which contains proteins count and proteomes size. This is used to construct the table with samples", default=None)
    parser.add_argument("-m", "--model", type=str, required=True, help="Path to the prediction model to be used.", default=None)
    parser.add_argument("--mantissa", type=int, required=False, help="A positive value that is added by default when computing the folds. This value is used to avoid that negative values are added to the training data.\nFor example if the fold is -45x than the value in the sample will be 500-45=445; if folds is 45x then it is 500+45=545.", default=250)
    parser.add_argument("--true-values", type=str, required=False, help="File containing a list of 1-0 values representing the reference predictions. These are used to evaluate the predictions from the model.", default="")

    # parser.add_argument("-o", "--output-dir", type=str, required=True, help="The directory in which the generated models, training samples and results will be stored.", default=os.getcwd())

    parser.add_argument("-t", "--threads", type=int, required=False, help="Number of parallel 1-CPU jobs to be used. Default=4", default=4)

    parser.add_argument("--categorical", required=False, help="Ignore mantissa, and use categorical labels for samples instead.", default=False, action="store_true")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information. (WARNING: extremely verbose)", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def compute_true_values(extimePath: str, pair2sample: dict[tuple[int, int], np.ndarray]) -> dict[tuple[int, int], int]:
    """
    Compute reference labels from execution time file from SonicParanoid.
    Given a pair a-b and b-a, will set the label 1 or 0 if fastest or not.
    """

    logging.debug(f"compute_true_values :: START")
    logging.debug(f"Execution time file: {extimePath}")
    logging.debug(f"Total pairs/samples:\t{len(pair2sample)}")

    # Entries in the execution time files have the folowing format
    # pair aln_time conversion_time parsing_time ... and other columns related
    # to the % of sequences used (for complete alignments these are always 100%)
    # 149-235 6.670 0.010 0.010 100.00 100.00 0.000
    # The fields we will use are the first 4
    flds: list[str] = []
    sp_left: int = 0
    sp_right: int = 0
    pair2extime: dict[tuple[int, int], float] = {}
    # This is used in special cases in which ex times are the same
    pair2RawExtime: dict[tuple[int, int], list[str]] = {}
    spList: list[int] = []
    with open(extimePath, "rt") as fd:
        for ln in fd:
            flds = ln.rstrip("\n").split("\t" , maxsplit=4)
            sp_left, sp_right = [int(x) for x in flds[0].split("-", maxsplit=1)]
            # skip computation for intra-proteome execution times
            if sp_left == sp_right:
                continue
            if not sp_left in spList:
                spList.append(sp_left)
            if not sp_right in spList:
                spList.append(sp_right)
            # FIXME: the training data is generated using only
            # the actual alignment execution time
            # without the conversion time
            # pair2extime[(sp_left, sp_right)] = sum([float(x) for x in flds[1:4]])
            pair2extime[(sp_left, sp_right)] = float(flds[1])
            pair2RawExtime[(sp_left, sp_right)] = flds[1:4]
            # print(flds, pair2extime[flds[0]])

    # associate a label to each pair
    tmpPair: tuple[int, int] = (0, 0)
    tmpPairInv: tuple[int, int] = (0, 0)
    pair2label: dict[tuple[int, int], int] = {}
    timePair: float = 0.
    timePairInv: float = 0.
    tmpSmpl: np.ndarray = np.zeros((1, 1), dtype=np.int32)
    # sum of the assigned categories
    catSum: int = 0
    # Perform te labelings
    combinationCnt: int = 0
    for sp_left, sp_right in combinations(spList, r=2):
        combinationCnt += 1
        tmpPair = (sp_left, sp_right)
        tmpPairInv = (sp_right, sp_left)
        # Make sure the extime was loaded
        if tmpPair not in pair2extime:
            logging.error(f"The execution time for pair {tmpPair} is missing!\nCheck the execution time file.")
            sys.exit(-10)
        if tmpPairInv not in pair2extime:
            logging.error(f"The execution time for pair {tmpPairInv} is missing!\nCheck the execution time file.")
            sys.exit(-10)

        # set the labels
        # tmpPairInv is the fastest
        if pair2extime[tmpPair] > pair2extime[tmpPairInv]:
            pair2label[tmpPair] = 0
            pair2label[tmpPairInv] = 1
        elif pair2extime[tmpPair] < pair2extime[tmpPairInv]:
            pair2label[tmpPair] = 1
            pair2label[tmpPairInv] = 0
        else:
            # print(tmpPair, tmpPairInv, pair2extime[tmpPair], pair2extime[tmpPairInv])
            # logging.warning("Execution times for both pairs are the same. This is very unlikely!")
            # Print the line with the pair in context
            # HACK: in this case let's also consider
            # the execution time for the file conversion
            # and parsing
            ''' FIXME: this is too computationally expensive when processing MAG datapoints
            with open(extimePath, "rt") as fd:
                for ln in fd:
                    flds = ln.rstrip("\n").split("\t" , maxsplit=4)
                    if flds[0] == f"{tmpPair[0]}-{tmpPair[1]}":
                        # recompute the time
                        timePair = sum([float(x) for x in flds[1:4]])
                        print(ln, timePair)
                        # sys.exit("FOUND!")
            with open(extimePath, "rt") as fd:
                for ln in fd:
                    flds = ln.rstrip("\n").split("\t" , maxsplit=4)
                    if flds[0] == f"{tmpPair[1]}-{tmpPair[0]}":
                        # recompute the time
                        timePairInv = sum([float(x) for x in flds[1:4]])
                        print(ln, timePairInv)
            '''

            timePair = sum([float(x) for x in pair2RawExtime[tmpPair]])
            timePairInv = sum([float(x) for x in pair2RawExtime[tmpPairInv]])

            # Update the extimes and set the labels
            if timePair != timePairInv:
                # Update extime
                pair2extime[tmpPair] = timePair
                pair2extime[tmpPairInv] = timePairInv
                # Set the labels
                if pair2extime[tmpPair] > pair2extime[tmpPairInv]:
                    pair2label[tmpPair] = 0
                    pair2label[tmpPairInv] = 1
                elif pair2extime[tmpPair] < pair2extime[tmpPairInv]:
                    pair2label[tmpPair] = 1
                    pair2label[tmpPairInv] = 0
                # logging.warning("Execution times for both pairs were updated including conversion times.")
            else:
                # print(tmpPair, pair2sample[tmpPair])
                # print("\n")
                # logging.error("Execution times for both pairs are (still) the same. This is very unlikely!\nFind another criteria to set the label")
                # HACK: use the sum of the assigned categories to decide
                # if sum <= 1 -> label = 0
                # if sum >= 2 -> label = 1
                tmpSmpl = pair2sample[tmpPair]
                catSum = tmpSmpl[2] + tmpSmpl[5] + tmpSmpl[8]
                # print(f"Sum of categories:\t{catSum}")
                if catSum >= 2:
                    pair2label[tmpPair] = 1
                    pair2label[tmpPairInv] = 0
                else:
                    pair2label[tmpPair] = 0
                    pair2label[tmpPairInv] = 1
                logging.warning("The reference label was assigned using the sum of the categorical features.")
                # sys.exit(-10)

    refLabelsCnt: int = len(pair2label)
    # At this stage the number true labels should be
    # 2 times the number of combinations
    if (combinationCnt * 2) != refLabelsCnt:
        logging.error(f"The number of computed labels ({refLabelsCnt}) must be 2x the number of species combination ({combinationCnt})")
        sys.exit(-10)

    positiveLabelsCnt: int = sum(pair2label.values())
    positiveLabelsRatio: float = positiveLabelsCnt/refLabelsCnt
    logging.info(f"Loaded {refLabelsCnt} pairs and labels")
    logging.info(f"Positive labels: {positiveLabelsCnt}/{refLabelsCnt}")
    logging.info(f"Positive labels (%): {positiveLabelsRatio * 100.}")

    return pair2label



def generate_samples(snapshotPath: str, mantissa: int) -> tuple[list[int], dict[int, str], dict[tuple[int, int], np.ndarray]]:
    """
    Given a snapshot file generate the samples for which the prediction is required.
    return (spList, spId2Name, pair2sample)

    """
    logging.debug(f"generate_samples :: START")
    logging.debug(f"Snapshot path: {snapshotPath}")
    logging.debug(f"Mantissa:\t{mantissa}")

    # Define constants for maximum and minimum differences
    # these are used to set diff variables in cases of extreme diff value (e.g. negative values)
    MAX_DIFF: int = 2 * mantissa
    MIN_DIFF: int = 0

    timer_start: float = time.perf_counter()

    # read the file and load the information
    # load proteomes sizes and protein lengths
    spId2Name: dict[int, str] = {}
    tmpSp: int = 0
    spSizeDict: dict[int, int] = {}
    protCntDict: dict[int, int] = {}
    flds: list[str] = []
    with open(snapshotPath, "rt", encoding="utf8") as fd:
        for ln in fd:
            flds = ln.rstrip("\n").split("\t", maxsplit=4)
            # print(flds)
            tmpSp = int(flds[0])
            protCntDict[tmpSp] = int(flds[3])
            spSizeDict[tmpSp] = int(flds[4])
            spId2Name[tmpSp] = flds[1]

    spList: list[int] = list(spId2Name.keys())
    spList.sort()
    print("Some debug info.")
    print(f"Species count:\t{len(spList)}")
    print(f"Protein counts:\t{len(protCntDict)}")
    print(f"Proteome sizes:\t{len(spSizeDict)}")

    outDir: str = os.path.dirname(snapshotPath)
    # path to the file with the samples
    samplesTbl: str = os.path.join(outDir, "samples.tsv")

    # write the starting dataset
    tmpA: str = ""
    tmpB: str = ""
    seqCntA: int = 0
    seqCntB: int = 0
    proteomeSizeA: int = 0
    proteomeSizeB: int = 0
    avgLenA: float = 0.0
    avgLenB: float = 0.0
    cntDiff: float = 0.0
    sizeDiff: float = 0.0
    avgLenDiff: float = 0.0

    # Compute species combinations
    spPairs: list[tuple[int, int]] = list(combinations(spList, r=2))

    pairsCnt: int = len(spPairs)
    # Associate a sample vector to each pair
    pair2sample: dict[tuple[int, int], np.ndarray] = {}
    print(f"\nTotal pairs:\t{pairsCnt}")

    # Columns in the dataframe
    sampleFileCols: list[str] = ["seq_cnt_a", "seq_cnt_b", "seq_cnt_diff_folds_b_gt_a", "proteome_size_a", "proteome_size_b", "prot_size_diff_folds_b_gt_a", "avg_seq_len_a", "avg_seq_len_b", "avg_seq_len_diff_folds_b_gt_a"]
    # NOTE: The part below uses the model trained for ISMB2021
    # generate samples to be predicted
    with open(samplesTbl, "wt") as ofd:
        hdr: str = "\t".join(sampleFileCols)
        hdr = f"pair\t{hdr}\n"
        # write the header
        ofd.write(hdr)
        del hdr
        for idx, p in enumerate(spPairs):
            tmpA, tmpB = p
            # print(tmpA, tmpB)
            # compute values for A
            seqCntA = protCntDict[tmpA]
            proteomeSizeA = spSizeDict[tmpA]
            avgLenA = proteomeSizeA / seqCntA
            # compute values for B
            seqCntB = protCntDict[tmpB]
            proteomeSizeB = spSizeDict[tmpB]
            avgLenB = proteomeSizeB / seqCntB

            # print(f"{tmpA}-{tmpB}", tmpA, tmpB, seqCntA, seqCntB, proteomeSizeA, proteomeSizeB, avgLenA, avgLenB)
            # compute protein counts difference folds considering B > A
            if seqCntA > seqCntB:
                cntDiff = -(seqCntA / seqCntB) + mantissa
            else:
                cntDiff = (seqCntB / seqCntA) + mantissa

            # Check diff boundaries
            if cntDiff < MIN_DIFF:
                logging.warning(f"The difference in sequence counts is way smaller than expected ({cntDiff})\nand will be set to {MIN_DIFF}. Try to increase the mantissa (now is {mantissa}).")
                cntDiff = MIN_DIFF
            elif cntDiff > MAX_DIFF:
                logging.warning(f"The difference in sequence counts is way bigger than expected ({cntDiff})\nand will be set to {MAX_DIFF} (2 x {mantissa}). Try to increase the mantissa (now is {mantissa}).")
                cntDiff = MAX_DIFF

            # compute proteome size difference folds considering B > A
            if proteomeSizeA > proteomeSizeB:
                sizeDiff = -(proteomeSizeA / proteomeSizeB) + mantissa
            else:
                sizeDiff = (proteomeSizeB / proteomeSizeA) + mantissa

            # Check diff boundaries
            if sizeDiff < MIN_DIFF:
                logging.warning(f"The difference in proteome size is way smaller than expected ({sizeDiff})\nand will be set to {MIN_DIFF}. Try to increase the mantissa (now is {mantissa}).")
                sizeDiff = MIN_DIFF
            elif sizeDiff > MAX_DIFF:
                logging.warning(f"The difference in proteome size is way bigger than expected ({sizeDiff})\nand will be set to {MAX_DIFF} (2 x {mantissa}). Try to increase the mantissa (now is {mantissa}).")
                sizeDiff = MAX_DIFF

            # compute avg protein length difference folds considering B > A
            if avgLenA > avgLenB:
                avgLenDiff = -(avgLenA / avgLenB) + mantissa
            else:
                avgLenDiff = (avgLenB / avgLenA)+ mantissa

            # Check diff boundaries
            if avgLenDiff < MIN_DIFF:
                logging.warning(f"The difference in sequence average lengths is way smaller than expected ({avgLenDiff})\nand will be set to {MIN_DIFF}. Try to increase the mantissa (now is {mantissa}).")
                avgLenDiff = MIN_DIFF
            elif avgLenDiff > MAX_DIFF:
                logging.warning(f"The difference in sequence average lengths is way bigger than expected ({avgLenDiff})\nand will be set to {MAX_DIFF} (2 x {mantissa}). Try to increase the mantissa (now is {mantissa}).")
                avgLenDiff = MAX_DIFF

            # add the array to the samples matrix
            # print(f"idx:\t{idx}")
            pair2sample[p] = np.array([seqCntA, seqCntB, cntDiff, proteomeSizeA, proteomeSizeB, sizeDiff, avgLenA, avgLenB, avgLenDiff], dtype=np.float64)
            # print(seqCntA, seqCntB, cntDiff, proteomeSizeA, proteomeSizeB, sizeDiff, avgLenA, avgLenB, avgLenDiff)
            # write the output file
            ofd.write(f"{p[0]}-{p[1]}\t{seqCntA}\t{seqCntB}\t{cntDiff:.3f}\t{proteomeSizeA}\t{proteomeSizeB}\t{sizeDiff:.3f}\t{avgLenA:.3f}\t{avgLenB:.3f}\t{avgLenDiff:.3f}\n")

            # if idx == 5:
            #     break
    del tmpA, tmpB

    # Output execution time
    sys.stdout.write(f"\nSamples creation time (seconds):\t{(time.perf_counter() - timer_start):.3f}\n")

    # Return tuple with
    # list of species IDs
    # list of tuples of pairs as list[tuple[int, int]]
    # 2D array (matrix) with the samples
    return (spList, spId2Name, pair2sample)



def generate_samples_categorical(snapshotPath: str, include_inverse: bool = False) -> tuple[list[int], dict[int, str], dict[tuple[int, int], np.ndarray]]:
    """
    Given a snapshot file generate the samples for which the prediction is required.
    Diff values will be categorical (1|0)
    include_inverse will also generate the sample for inverted pair. for a total n^2 samples
    return (spList, spId2Name, pair2sample)

    """
    logging.debug(f"generate_samples :: START")
    logging.debug(f"Snapshot path: {snapshotPath}")

    timer_start: float = time.perf_counter()

    # read the file and load the information
    # load proteomes sizes and protein lengths
    spId2Name: dict[int, str] = {}
    tmpSp: int = 0
    spSizeDict: dict[int, int] = {}
    protCntDict: dict[int, int] = {}
    flds: list[str] = []
    with open(snapshotPath, "rt", encoding="utf8") as fd:
        for ln in fd:
            flds = ln.rstrip("\n").split("\t", maxsplit=4)
            # print(flds)
            tmpSp = int(flds[0])
            protCntDict[tmpSp] = int(flds[3])
            spSizeDict[tmpSp] = int(flds[4])
            spId2Name[tmpSp] = flds[1]

    spList: list[int] = list(spId2Name.keys())
    spList.sort()
    print("Some debug info.")
    print(f"Species count:\t{len(spList)}")
    print(f"Protein counts:\t{len(protCntDict)}")
    print(f"Proteome sizes:\t{len(spSizeDict)}")

    outDir: str = os.path.dirname(snapshotPath)
    # path to the file with the samples
    samplesTbl: str = os.path.join(outDir, "samples.tsv")

    # write the starting dataset
    tmpA: int = 0
    tmpB: int = 0
    seqCntA: int = 0
    seqCntB: int = 0
    proteomeSizeA: int = 0
    proteomeSizeB: int = 0
    avgLenA: float = 0.0
    avgLenB: float = 0.0
    cntDiff: int = 0
    sizeDiff: int = 0
    avgLenDiff: int = 0
    # variables used for inverse pairs
    pInv: tuple[int, int] = (0, 0)
    cntDiffInv: int = 0
    sizeDiffInv: int = 0
    avgLenDiffInv: int = 0

    # Compute species combinations
    spPairs: list[tuple[int, int]] = list(combinations(spList, r=2))

    pairsCnt: int = len(spPairs)
    # Associate a sample vector to each pair
    pair2sample: dict[tuple[int, int], np.ndarray] = {}
    print(f"\nTotal combinations:\t{pairsCnt}")

    # Columns in the dataframe
    sampleFileCols: list[str] = ["seq_cnt_a", "seq_cnt_b", "seq_cnt_diff_folds_b_gt_a", "proteome_size_a", "proteome_size_b", "prot_size_diff_folds_b_gt_a", "avg_seq_len_a", "avg_seq_len_b", "avg_seq_len_diff_folds_b_gt_a"]
    # NOTE: The part below uses the model trained for ISMB2021
    # generate samples to be predicted
    with open(samplesTbl, "wt") as ofd:
        hdr: str = "\t".join(sampleFileCols)
        hdr = f"pair\t{hdr}\n"
        # write the header
        ofd.write(hdr)
        del hdr
        for idx, p in enumerate(spPairs):
            tmpA, tmpB = p
            # print(tmpA, tmpB)
            # compute values for A
            seqCntA = protCntDict[tmpA]
            proteomeSizeA = spSizeDict[tmpA]
            avgLenA = proteomeSizeA / seqCntA
            # compute values for B
            seqCntB = protCntDict[tmpB]
            proteomeSizeB = spSizeDict[tmpB]
            avgLenB = proteomeSizeB / seqCntB

            # print(f"{tmpA}-{tmpB}", tmpA, tmpB, seqCntA, seqCntB, proteomeSizeA, proteomeSizeB, avgLenA, avgLenB)
            # compute protein counts difference folds considering B > A
            if seqCntA > seqCntB:
                cntDiff = 0
                cntDiffInv = 1
            else:
                cntDiff = 1
                cntDiffInv = 0

            # compute proteome size difference folds considering B > A
            if proteomeSizeA > proteomeSizeB:
                sizeDiff = 0
                sizeDiffInv = 1
            else:
                sizeDiff = 1
                sizeDiffInv = 0

            # compute avg protein length difference folds considering B > A
            if avgLenA > avgLenB:
                avgLenDiff = 0
                avgLenDiffInv = 1
            else:
                avgLenDiff = 1
                avgLenDiffInv = 0

            # add the array to the samples matrix
            # print(f"idx:\t{idx}")
            pair2sample[p] = np.array([seqCntA, seqCntB, cntDiff, proteomeSizeA, proteomeSizeB, sizeDiff, avgLenA, avgLenB, avgLenDiff], dtype=np.float64)
            # print(seqCntA, seqCntB, cntDiff, proteomeSizeA, proteomeSizeB, sizeDiff, avgLenA, avgLenB, avgLenDiff)
            # write the output file
            ofd.write(f"{p[0]}-{p[1]}\t{seqCntA}\t{seqCntB}\t{cntDiff:.0f}\t{proteomeSizeA}\t{proteomeSizeB}\t{sizeDiff:.0f}\t{avgLenA:.3f}\t{avgLenB:.3f}\t{avgLenDiff:.0f}\n")

            if include_inverse:
                pInv = (tmpB, tmpA)
                # print(p, pInv)
                # Add the sample
                pair2sample[pInv] = np.array([seqCntB, seqCntA, cntDiffInv, proteomeSizeB, proteomeSizeA, sizeDiffInv, avgLenB, avgLenA, avgLenDiffInv], dtype=np.float64)
                # Write the sample in the files
                ofd.write(f"{pInv[0]}-{pInv[1]}\t{seqCntB}\t{seqCntA}\t{cntDiffInv:d}\t{proteomeSizeB}\t{proteomeSizeA}\t{sizeDiffInv:d}\t{avgLenB:.3f}\t{avgLenA:.3f}\t{avgLenDiffInv:d}\n")

            # if idx == 5:
            #     break

    del tmpA, tmpB

    # Output execution time
    sys.stdout.write(f"\nSamples creation time (seconds):\t{(time.perf_counter() - timer_start):.3f}\n")

    print(f"\nTotal samples:\t{len(pair2sample)}")

    # Return tuple with
    # list of species IDs
    # list of tuples of pairs as list[tuple[int, int]]
    # 2D array (matrix) with the samples
    return (spList, spId2Name, pair2sample)



def evaluate_model(pair2pred: dict[tuple[int, int], int], pair2refLabel: dict[tuple[int, int], int]) -> None:
    """
    Compute multiple metrics for binary classifier based on vector of predicted and true refrence values.
    """
    logging.debug(f"evaluate_model :: START")
    logging.debug(f"Pair to reference label:\t{len(pair2refLabel)}")
    logging.debug(f"Pair to prediction label:\t{len(pair2pred)}")

    pairsCnt: int = len(pair2pred)
    if len(pair2refLabel) != pairsCnt:
        logging.error(f"The dictionaries with predictions and reference labels must be of the same length.\n{len(pair2refLabel)}\t{len(pair2pred)}")
        sys.exit(-10)

    # Extract the reference and prediction vectors
    predVec: np.ndarray = np.array(list(pair2pred.values()), dtype=np.int8)
    refVec: np.ndarray = np.array(list(pair2refLabel.values()), dtype=np.int8)
    # print(refVec.shape, predVec.shape)

    # Accuracy
    accuracy: float = accuracy_score(y_true=refVec, y_pred=predVec, normalize=True, sample_weight=None)
    print(accuracy_score(y_true=refVec, y_pred=predVec))
    # MCC
    mcc: float = matthews_corrcoef(y_true=refVec, y_pred=predVec)
    # F1-score
    f1: float = f1_score(y_true=refVec, y_pred=predVec)

    # print results regarding accuracy
    print("\nModel performance:")
    print(f"MCC:\t{mcc:.4f}")
    print(f"Accuracy:\t{accuracy:.4f}")
    print(f"F1-score:\t{f1:.4f}")

    # initialize confusion matrix
    cm = confusion_matrix(y_true=refVec, y_pred=predVec, labels=[0, 1])
    tn, fp, fn, tp = cm.ravel()
    print("\nConfusion matrix:")
    print(cm)
    print(f"tn:\t{tn}\nfp:\t{fp}\nfn:\t{fn}\ntp:\t{tp}\n")



def load_true_values(trueValsPath: str) -> dict[str, int]:
    """
    Load reference labels (e.g., from training data) true refrence values.
    """
    logging.debug(f"load_true_values :: START")
    logging.debug(f"Reference labels file: {trueValsPath}")

    # load the reference labels
    # The reference ;ables are those stored in the original raw-training data
    # for the binary classifier
    # pair extime extime_diff_folds seq_cnt_a seq_cnt_b seq_cnt_diff_folds_b_gt_a proteome_size_a proteome_size_b prot_size_diff_folds_b_gt_a avg_seq_len_a avg_seq_len_b avg_seq_len_diff_folds_b_gt_a mol_mass_a mol_mass_b mol_mass_diff_folds_b_gt_a folds_combination_class fastest_pair
    # In this case pair and fastest pair columns should be loaded
    # Nevertheless the file should have only two columns (pair, fastest_pair)
    # Examples:
    # pair fastest_pair
    # aanophagefferens-abisporus 1
    # abisporus-aanophagefferens 0
    # ...
    flds: list[str] = []
    pair2labelDict: dict[str, int] = {}
    with open(trueValsPath, "rt") as fd:
        for ln in fd:
            if ln.startswith("pair"):
                continue
            flds = ln.rstrip("\n").split("\t" , maxsplit=1)
            pair2labelDict[flds[0]] = int(flds[1])

    labelsCnt: int = len(pair2labelDict)
    positiveLabelsCnt: int = sum(pair2labelDict.values())
    positiveLabelsRatio: float = positiveLabelsCnt/labelsCnt
    logging.info(f"Loaded {labelsCnt} pairs and labels")
    logging.info(f"Positive labels: {positiveLabelsCnt}/{labelsCnt}")
    logging.info(f"Positive labels (%): {positiveLabelsRatio * 100.}")

    return pair2labelDict



def predict_fastest_pairs(pair2sample: dict[tuple[int, int], np.ndarray], modelPath: str) -> dict[tuple[int, int], int]:
    """
    Predict fastest pairs.
    """
    logging.debug(f"predict_fastest_pairs :: START")
    logging.debug(f"Samples:\t{len(pair2sample)}")
    logging.debug(f"Path to prediction model: {modelPath}")

    timer_start: float = time.perf_counter()
    # load the model
    model = pickle.load(open(modelPath, "rb"))
    tmp_timer: float = time.perf_counter()
    sys.stdout.write(f"\nModel loading time (seconds):\t{(tmp_timer - timer_start):.3f}\n")

    # Initialize the matrix with all samples
    samples: np.ndarray = np.zeros((len(pair2sample), 9), dtype=np.float64)
    for idx, smpl in enumerate(pair2sample.values()):
        samples[idx] = smpl
    # Perform predictions
    tmp_timer: float = time.perf_counter()
    predictions = model.predict(samples)
    # pair2pred: dict[tuple[int, int], int] = {}
    pair2pred: dict[tuple[int, int], int] = dict(zip(pair2sample.keys(), predictions))
    # myDict = { k:v for (k,v) in zip(pair2sample.keys(), prediction)}
    print(f"sum(predictions):\t{sum(predictions)}")

    # Translated vector
    sys.stdout.write(f"\nPrediction time (seconds):\t{(time.perf_counter() - tmp_timer):.3f}\n")

    return pair2pred



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



def main():
    """
    Predict input samples using a user-specified prediction model
    """
    #Get the parameters
    args = get_params()[0]
    debug = args.debug
    # set main directories and file paths
    snapshotPath: str = os.path.realpath(args.snapshot)
    modelPath: str = os.path.realpath(args.model)
    extimePath: str = os.path.realpath(args.true_values)
    mantissa: int = args.mantissa
    categorical: bool = args.categorical
    threads = args.threads

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # trPath, labelsPath = extract_samples_and_labels(snapshotPath, args.labels, outDir=trDir, noMolWeights=skipMolWeights, useDummyFeatures=args.dummy)

    # Show some info
    logging.info("Prediction of fastest pairs will be peromed using the following parameters:")
    logging.info(f"Snapshot file from SonicParanoid: {snapshotPath}")
    logging.info(f"Mantissa:\t{mantissa}")
    logging.info(f"Categorical:\t{categorical}")
    logging.info(f"Model file: {modelPath}")
    logging.info(f"Reference predictions: {extimePath}")
    logging.info(f"Threads:\t{threads}")

    # check that the file with samples exists
    if not os.path.isfile(snapshotPath):
        logging.error("The snapshot file is not valid.\n")
        sys.exit(-2)

    # check that the file with samples exists
    if not os.path.isfile(modelPath):
        logging.error("The file with the model is not valid.\n")
        sys.exit(-2)

    # List with species IDs
    spListInt: list[int] = []
    spId2Name: dict[int, str] = {}
    pair2sample: dict[tuple[int, int], np.ndarray] = {}
    # Generate disctionaries with samples to be predicted
    if categorical:
        spListInt, spId2Name, pair2sample = generate_samples_categorical(snapshotPath=snapshotPath, include_inverse=True)
    else:
        spListInt, spId2Name, pair2sample = generate_samples(snapshotPath=snapshotPath, mantissa=mantissa)

    # list species combinations
    spPairs: list[tuple[int, int]] = list(pair2sample.keys())
    pairsCnt: int = len(spPairs)

    # Perform the predictions
    pair2pred: dict[tuple[int, int], int] = predict_fastest_pairs(pair2sample, modelPath)

    '''
    np.set_printoptions(suppress=True)
    for p, pred in pair2pred.items():
        print(p, pair2sample[p], pred)
    '''

    if os.path.isfile(extimePath):
        # Compute reference labels from table with execution times
        pair2refLabel: dict[tuple[int, int], int] = compute_true_values(extimePath=extimePath, pair2sample=pair2sample)
        # Contains only the reference labels matching
        # pairs for which a prediction is avaliable
        pair2refToUse: dict[tuple[int, int], int] = {p:pair2refLabel[p] for p in pair2pred.keys()}

        print(len(pair2pred), len(pair2refLabel), len(pair2refToUse))
        # Convert reference lables to array
        evaluate_model(pair2pred=pair2pred, pair2refLabel=pair2refToUse)
    else:
        logging.warning("No execution time table was provided, hence the model will not be evaluated.")

    # sys.exit("DEBUG")





if __name__ == "__main__":
    main()
