#### Relevant updates and changes  

  > `(28/02/2023) bbh_fail-no-swap.svg` is the same as `bbh_fail.svg, but without the `swap` scenario.
  >The swap was removed to simplify the figure description.