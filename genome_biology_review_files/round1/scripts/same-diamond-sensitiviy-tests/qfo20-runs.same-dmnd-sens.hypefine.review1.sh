#!/bin/bash

# This script contains runs of SP2 on the qfo dataset using the same diamond sensitivity as the competitors
# OrthoFinder and Broccoli: diamond sensitivity -> more-sensitive
# ProteinOrtho6: diamond sensitivity -> sensitive

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=qfo20
cpu_cnt=128

# Output directories
output_root=$root\output_same_dmnd_sens/
sp_output_root=$output_root\sp2/
mkdir $sp_output_root

# SonicParanoid2 runs
# The sensivity set through the following command
sp_dmnd_params=sensitive
sp_mode=dmdn-$sp_dmnd_params
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus

# SonicParanoid2 (same diamond sensitivity as proteinortho6)
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --diamond $sp_dmnd_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --diamond $sp_dmnd_params > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

sp_dmnd_params=sensitive
sp_mode=dmdn-$sp_dmnd_params-no-domain-orthology
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
# SonicParanoid2 (same diamond sensitivity as proteinortho6 bu skip domain orthology)
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --diamond $sp_dmnd_params \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --diamond $sp_dmnd_params -go > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"

echo $'\n'
date


# All runs completed
echo $'\n'
date
