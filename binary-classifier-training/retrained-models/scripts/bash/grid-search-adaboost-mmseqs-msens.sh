date

echo $'\n@@@@ adaboost-grid-maxdepth5-search-mmseqs-msens-noidx no-weights @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --no-mol-weights --max-depth 5 --threads 8 --out-prefix adaboost.mmseqs_msens.no-weights &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.no-weights.training_adaboost_gsearch_maxdepth5_mmseqs_msens_noidx_trdata.txt
wait

echo $'\n@@@@ adaboost-grid-maxdepth4-search-mmseqs-msens-noidx no-weights @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --no-mol-weights --max-depth 4 --threads 8 --out-prefix adaboost.mmseqs_msens.no-weights &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.no-weights.training_adaboost_gsearch_maxdepth4_mmseqs_msens_noidx_trdata.txt
wait

echo $'\n@@@@ adaboost-grid-maxdepth3-search-mmseqs-msens-noidx no-weights @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --no-mol-weights --max-depth 3 --threads 8 --out-prefix adaboost.mmseqs_msens.no-weights &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.no-weights.training_adaboost_gsearch_maxdepth3_mmseqs_msens_noidx_trdata.txt
wait

echo $'\n@@@@ adaboost-grid-maxdepth2-search-mmseqs-msens-noidx no-weights @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --no-mol-weights --max-depth 2 --threads 8 --out-prefix adaboost.mmseqs_msens.no-weights &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.no-weights.training_adaboost_gsearch_maxdepth2_mmseqs_msens_noidx_trdata.txt
wait

echo $'\n@@@@ adaboost-grid-maxdepth1-search-mmseqs-msens-noidx no-weights @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --no-mol-weights --max-depth 1 --threads 8 --out-prefix adaboost.mmseqs_msens.no-weights &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.no-weights.training_adaboost_gsearch_maxdepth1_mmseqs_msens_noidx_trdata.txt
wait


# Use 12 features, including the weights

# echo $'\n@@@@ adaboost-grid-maxdepth5-search-mmseqs-msens-noidx @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --max-depth 5 --threads 8 --out-prefix adaboost.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.training_adaboost_gsearch_maxdepth5_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth4-search-mmseqs-msens-noidx @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --max-depth 4 --threads 8 --out-prefix adaboost.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.training_adaboost_gsearch_maxdepth4_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth3-search-mmseqs-msens-noidx @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --max-depth 3 --threads 8 --out-prefix adaboost.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.training_adaboost_gsearch_maxdepth3_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth2-search-mmseqs-msens-noidx @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --max-depth 2 --threads 8 --out-prefix adaboost.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.training_adaboost_gsearch_maxdepth2_mmseqs_msens_noidx_trdata.txt

# wait

# echo $'\n@@@@ adaboost-grid-maxdepth1-search-mmseqs-msens-noidx @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/adaboost_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/ --max-depth 1 --threads 8 --out-prefix adaboost.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/adaboost/logs/log.training_adaboost_gsearch_maxdepth1_mmseqs_msens_noidx_trdata.txt

# wait

# All runs completed
date