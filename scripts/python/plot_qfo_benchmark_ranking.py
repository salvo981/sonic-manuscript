'''
Generate plots with rankigs from QfO benchmark web-page.
The input is a table genetated using the script under scripts/julia/create_qfo_benchmark_datapoints.jl
'''

import os
import sys
# from typing import TextIO
import logging
import numpy as np
import pandas as pd
# from collections import namedtuple
from typing import NamedTuple
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']
rcParams['font.size'] = 9
rcParams['font.weight'] = 'normal'
rcParams['lines.markersize'] = 3
#'weight' : 'bold',
# import matplotlib as plt
# import matplotlib.pyplot as pyplt

class Names(NamedTuple):
    """Defines long and shortname for a given qfo2020 method"""
    name: str
    shortname: str

# Names: namedtuple = namedtuple("Names", "name shortname")



########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots QfO benchmark results.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file with datapoints from QfO benchmark results\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=False, help="Output directory.", default=os.getcwd())
    parser.add_argument("--qfo-competition", required=True, help="Format of output file.", choices=["2020", "2022"], default="2020")
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--hue", type=str, required=False, help="Column containing categorical information.", default="")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("--fig-width-inches", type=float, required=False, help="Width of the plot in inches.", default=2.7)
    parser.add_argument("--fig-height-inches", type=float, required=False, help="Height of the plot in inches.", default=1.8)
    # Seaborn height and aspect are described in
    # https://seaborn.pydata.org/generated/seaborn.PairGrid.html#seaborn.PairGrid
    parser.add_argument("--seaborn-height", type=float, required=False, help="Height of the plot in inches.", default=2.7)
    parser.add_argument("--seaborn-aspect", type=float, required=False, help="Aspect * height gives the width (in inches) of each facet", default=1.8)

    parser.add_argument("--best-n", type=int, required=False, help="Integer to limit the number of methods to plot.", default=0)
    parser.add_argument("-s", "--short-names", required=False, help="Use short method names in plots.\n", default=False, action="store_true")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)


def compute_width_inches(height: float, aspect: float):
    """Compute the seaborn figure width in inches"""
    print(f"height:\t{height}")
    print(f"aspect:\t{aspect}")
    width: float = height*aspect
    print(f"width:\t{width}")

    return width



def create_method_names_mapping2020() -> dict[str, Names]:
    """Generate a mapping of method names, with short and long versions.
    """

    # Create a dictionary mapping all the method names
    # which includes the short name were possible
    # Names: namedtuple = namedtuple("Names", "name shortname")
    qfo2020methodNameMap: dict[str, Names] = {}

    # Create the named tuple for names
    qfo2020methodNameMap["BBH"] = Names("BBH", "BBH")
    qfo2020methodNameMap["Domainoid"] = Names("Domainoid", "Domainoid")
    qfo2020methodNameMap["Ensembl Compara"] = Names("Ensembl Compara", "Ensembl Compara")
    qfo2020methodNameMap["Hieranoid2"] = Names("Hieranoid2", "Hieranoid2")
    qfo2020methodNameMap["InParanoid"] = Names("InParanoid", "InParanoid")
    qfo2020methodNameMap["MetaPhOrs"] = Names("MetaPhOrs v2.5", "MetaPhOrs v2.5")
    qfo2020methodNameMap["OMA GETHOGs"] = Names("OMA GETHOGs", "OMA GETHOGs")
    qfo2020methodNameMap["OMA Groups"] = Names("OMA Groups", "OMA Groups")
    qfo2020methodNameMap["OMA Pairs"] = Names("OMA Pairs", "OMA Pairs")
    qfo2020methodNameMap["OrthoFinder"] = Names("OrthoFinder (MSA)", "OrthoFinder (MSA)")
    qfo2020methodNameMap["OrthoInspector"] = Names("OrthoInspector", "OrthoInspector")
    qfo2020methodNameMap["OrthoMCL"] = Names("OrthoMCL", "OrthoMCL")
    qfo2020methodNameMap["PANTHER All"] = Names("PANTHER 16 (all)", "PANTHER 16 (all)")
    qfo2020methodNameMap["PANTHER LDO"] = Names("PANTHER 16 (LDO)", "PANTHER 16 (LDO)")
    qfo2020methodNameMap["PhylomeDB"] = Names("PhylomeDB v5", "PhylomeDB v5")
    qfo2020methodNameMap["RDS"] = Names("RDS", "RDS")
    qfo2020methodNameMap["SonicParanoid"] = Names("SonicParanoid 1.3", "SP v1.3")
    qfo2020methodNameMap["SonicParanoid-fast"] = Names("SonicParanoid 1.3 (fast)", "SP v1.3 (fast)")
    qfo2020methodNameMap["SonicParanoid-sens"] = Names("SonicParanoid 1.3 (sens)", "SP v1.3 (sens)")
    qfo2020methodNameMap["SonicParanoid-mostsens"] = Names("SonicParanoid 1.3 (most-sens)", "SP v1.3 (most-sens)")
    qfo2020methodNameMap["SonicParanoid2"] = Names("SonicParanoid2", "SP2")
    qfo2020methodNameMap["SonicParanoid2-fast"] = Names("SonicParanoid2 (fast)", "SP2 (fast)")
    qfo2020methodNameMap["SonicParanoid2-sens"] = Names("SonicParanoid2 (sens)", "SP2 (sens)")    
    qfo2020methodNameMap["SonicParanoid2-g"] = Names("SonicParanoid2 (g)", "SP2 (g)")
    qfo2020methodNameMap["SonicParanoid2-fast-g"] = Names("SonicParanoid2 (g)(fast)", "SP2 (g)(fast)")
    qfo2020methodNameMap["SonicParanoid2-sens-g"] = Names("SonicParanoid2 (g)(sens)", "SP2 (g)(sens)")

    return qfo2020methodNameMap



def create_method_names_mapping2022() -> dict[str, Names]:
    """Generate a mapping of method names, with short and long versions for QfO 2022 challenge.
    """

    # Create a dictionary mapping all the method names
    # which includes the short name were possible
    # Names: namedtuple = namedtuple("Names", "name shortname")
    qfo2022methodNameMap: dict[str, Names] = {}

    # Create the named tuple for names
    qfo2022methodNameMap["BBH (Smith-Waterman)"] = Names("BBH (Smith-Waterman)", "BBH")
    qfo2022methodNameMap["Domainoid"] = Names("Domainoid+", "Domainoid")
    qfo2022methodNameMap["Ensembl Compara"] = Names("Ensembl Compara", "Ensembl Compara")
    qfo2022methodNameMap["FastOMA"] = Names("FastOMA", "FastOMA (v0.1.4)")
    qfo2022methodNameMap["Hieranoid2"] = Names("Hieranoid2", "Hieranoid")
    qfo2022methodNameMap["InParanoid"] = Names("InParanoid5", "InParanoid")
    qfo2022methodNameMap["MetaPhOrs v2.5"] = Names("MetaPhOrs v2.5", "MetaPhOrs")
    qfo2022methodNameMap["OMA Groups"] = Names("OMA Groups", "OMA Groups")
    qfo2022methodNameMap["OMA HOGs"] = Names("OMA HOGs", "OMA HOGs")
    qfo2022methodNameMap["OMA Pairs"] = Names("OMA Pairs", "OMA Pairs")
    qfo2022methodNameMap["OrthoFFGC"] = Names("OrthoFFGC", "OrthoFFGC")
    qfo2022methodNameMap["OrthoInspector"] = Names("OrthoInspector (v3.5)", "OrthoInspector")
    qfo2022methodNameMap["OrthoFinder"] = Names("OrthoFinder", "OrthoFinder2 (MSA)")
    qfo2022methodNameMap["PANTHER 18 (LDO)"] = Names("PANTHER 18 (LDO)", "PANTHER (LDO)")
    qfo2022methodNameMap["PANTHER 16 (all)"] = Names("PANTHER 16 (all)", "PANTHER (all)")
    qfo2022methodNameMap["RDS (Smith-Waterman)"] = Names("RDS (Smith-Waterman)", "RDS")
    qfo2022methodNameMap["SonicParanoid2 (sens)"] = Names("SonicParanoid2 (sens)", "SP2 (sens)")
    qfo2022methodNameMap["SonicParanoid2 (g)(sens)"] = Names("SonicParanoid2 (g)(sens)", "SP2 (g)(sens)")
    qfo2022methodNameMap["PhylomeDB v5"] = Names("PhylomeDB v5", "PhylomeDB v5")

    return qfo2022methodNameMap





def map_method_name(methodName: str, namesMapping: dict[str, Names], shortNames: bool) -> str:
    """Modify the method based on the what is used in the plot lables"""

    if not methodName in namesMapping:
        sys.stderr.write(f"\nERROR: {methodName} is not a valid participant name for the QfO benchmark.")
        sys.exit(-7)

    # return the method name
    if shortNames:
        return namesMapping[methodName].shortname
    else:
        return namesMapping[methodName].name



def plot_qfo_rankings(df, plotsDir: str, prefix: str, bestN: int, namesMapping: dict[str, Names], shortNames: bool, hueColName: str, seabornHeight: float = 1.8, seabornAspect: float = 2.7, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """Plot rankings obtained form the QfO 2020 public results.

    The plots are generated using the PairGrid object in SeaBorn as described in
    https://seaborn.pydata.org/examples/pairgrid_dotplot.html
    """

    import seaborn as sns
    sns.set_theme(style="whitegrid")

    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"bestN:\t{bestN}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    # Check for marker shapes in
    # https://matplotlib.org/stable/api/markers_api.html
    # These are the best options for markers
    # mrkShape = "D" # (diamond) NOTE: good, easy to distinguish wh ois in front
    mrkShape = "s" # NOTE: Better and more understandable then cicles
    # mrkShape = "o" # DEFAULT

    # Set output file name
    tmpOutPath: str = os.path.join(plotsDir, prefix)

    # Sort the daframes
    tmpDf: pd.DataFrame = df.sort_values(by=["i-ratio", "ii-ratio", "iii-ratio"], ascending=[False, False, False])
    # select the best n if required
    if bestN > 0:
        tmpDf = tmpDf.iloc[0:bestN, :]
        tmpOutPath = f"{tmpOutPath}-best-{bestN:d}.{fmt}"
    else:
        tmpOutPath = f"{tmpOutPath}.{fmt}"

    myLegend = True

    # Names to be used on top of each plot
    # titles = ["I", "II", "III", "IV"]
    # titles = ["1st", "2nd", "3rd", "4th"]
    titles = ["Group 1 (best)", "Group 2", "Group 3", "Group 4"]

    # Columns for whic plots wil be created
    # xValsToUse: list[str] = tmpDf.columns[1:5]
    xValsToUse: list[str] = tmpDf.columns[5:9]

    # print(newMethodNames)

    # methodName: str, namesMapping: dict[str, namedtuple[str, str]], shortNames: bool
    tmpDf["method"] = tmpDf["method"].apply(lambda x: map_method_name(methodName=x, namesMapping=namesMapping, shortNames=shortNames))

    # Convert ratios to percentages
    for colName in xValsToUse:
        tmpDf[colName] = tmpDf[colName].apply(lambda x: x*100)
    # Make the PairGrid
    # https://seaborn.pydata.org/generated/seaborn.PairGrid.html#seaborn.PairGrid
    # heightscalar: Height (in inches) of each facet.
    # aspectscalar: Aspect * height gives the width (in inches) of each facet.
        # g = sns.PairGrid(data=tmpDf, x_vars=xValsToUse, y_vars=["method"], height=10, aspect=.25)
    # g = sns.PairGrid(data=tmpDf, x_vars=xValsToUse, y_vars=["method"], height=4.8, aspect=.4)
    g = sns.PairGrid(data=tmpDf, x_vars=xValsToUse, y_vars=["method"], height=seabornHeight, aspect=seabornAspect)

    # Draw a dot plot using the stripplot function
    # g.map(sns.stripplot, size=6, orient="h", jitter=False, palette="flare_r", linewidth=1, edgecolor="w")

    g.map(sns.stripplot, size=markerSize, orient="h", jitter=False, palette="flare_r", linewidth=1, edgecolor="w", marker=mrkShape)

    # sys.exit("DEBUG :: plot_qfo_rankings")
    # Use the same x axis limits on all columns and add better labels
    # g.set(xlim=(-5, 100), xlabel="Medals", ylabel="")
    g.set(xlim=(-5, 105), xlabel=None, ylabel="")

    # For each axis set the title set in titles
    for ax, title in zip(g.axes.flat, titles):
        # Set a different title for each axes
        ax.set(title=title)
        # Make the grid horizontal instead of vertical
        ax.xaxis.grid(False)
        ax.yaxis.grid(True)

    sns.despine(left=True, bottom=True)

    g.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300)

    # Save the plot provided
    # fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    bestN: int = args.best_n
    qfoCompetition: str = args.qfo_competition
    shortNames: bool = args.short_names
    outFmt: str = args.format
    hueCol: int = args.hue
    figWidth: float = args.fig_width_inches
    figHeight: float = args.fig_height_inches
    seabornHeight: float = args.seaborn_height
    seabornAspect: float = args.seaborn_aspect
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)
    makedir(plotsDir)

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # compute the seaborn figure width
    seabornWidth: float = compute_width_inches(height=seabornHeight, aspect=seabornAspect)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"QfO competition:\t{qfoCompetition}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Marker size:\t{markerSize}")
    logging.info(f"Figure width (inches):\t{figWidth}")
    logging.info(f"Figure height (inches):\t{figHeight}")
    logging.info(f"Seaborn height (inches):\t{seabornHeight}")
    logging.info(f"Seaborn aspect (inches):\t{seabornAspect}")
    logging.info(f"Seaborn width (inches):\t{seabornWidth}")
    logging.info(f"Use short names:\t{shortNames}")


    # Obtain mapping of method names
    methodNameMap: dict[str, Names] = {}
    if qfoCompetition == "2020":
        methodNameMap: dict[str, Names] = create_method_names_mapping2020()
    elif qfoCompetition == "2022":
        methodNameMap: dict[str, Names] = create_method_names_mapping2022()

    # for m, v in methodNameMap.items():
    #     print(f"{m}:\t{v}")

    # Read dataframe
    # The hdr of datapoint file have the following format:
    # method i-cnt ii-cnt iii-cnt iv-cnt i-ratio ii-ratio iii-ratio iv-ratio tests-cnt

    df = pd.read_csv(inTbl, sep="\t", dtype= {"method": str, "i-cnt": np.int32, "ii-cnt": np.int32, "iii-cnt": np.int32, "iv-cnt": np.int32, "i-ratio": np.float64, "ii-ratio": np.float64, "iii-ratio": np.float64, "iv-ratio": np.float64, "tests-cnt": np.int32}, engine="c")

    # print(df.describe())

    if bestN > 0:
        if bestN > len(df):
            bestN = len(df)
        logging.info(f"Number of best methods to show:\t{bestN}")

    # Show or hide X and Y labels
    hideXlab = True
    hideYlab = True

    tmpNames: Names = Names("long", "short")
    for k in methodNameMap:
        tmpNames = methodNameMap[k]
        print(f"{k}:\t{tmpNames.name}\t{tmpNames.shortname}")

    # Plot benchmark rankings
    plot_qfo_rankings(df, plotsDir, prefix, bestN, namesMapping=methodNameMap, shortNames=shortNames, hueColName="", seabornHeight=seabornHeight, seabornAspect=seabornAspect, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    sys.exit("DEBUG :: plot_qfo20_benchmark_ranking")



if __name__ == "__main__":
    main()
