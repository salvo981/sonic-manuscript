using Base: iscontiguous, Float64
#=
Procees archives with datapoints from qfo20 benchmark and generate datapoints for plotting.
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using DataFrames
using CSV
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using GeneratePareto



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "data-points"
            nargs = 1
            arg_type = String
            help = "A TSV table with X and Y values for which the Pareto front must be computed."
            required = true
        "--output-dir"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Directory that will contain the output files."
            required = true
        "--data-type"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Type datapoints plot, for example int and double, double and double, etc."
            required = true
        "--cleanup"
            action = :store_true   # this makes it a flag
            help = "Remove temporary files."
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



function compute_pareto_front_bottom_int_double(qfoDatapoints::String, outDir::String; cleanup::Bool=false)::String
    @debug "compute_pareto_front_bottom_int_double :: START" qfoDatapoints outDir cleanup
    
    # tmp variables
    tmpDir::String = tmpPath = ""
    # These directories and files must be in the results directory
    bname::String = string(rsplit(basename(qfoDatapoints), ".", limit=2)[1])
    paretoDatapoints::String = joinpath(outDir, "pareto.x.y.tsv")
    @show paretoDatapoints

    # df = DataFrame(CSV.File(qfoDatapoints))
    # df = CSV.File(qfoDatapoints) |> DataFrame
    df = CSV.read(qfoDatapoints, DataFrame)
    println(describe(df))

    # Compute the pareto front
    sort!(df, [order(:x, rev=true), order(:y, rev=true)]) # Sort X in descending order
    pareto = df[1:1, :]; # Add the first extreme to the pareto front
    # Search for other frontier points and add
    foreach(row -> row.y < pareto.y[end] && push!(pareto, row), eachrow(df));

    # Add the first point for the pareto extreme line
    paretoFinal = DataFrame(x = Int(maximum(pareto.x)), y = 1.0)
    # Add the previously computed points
    append!(paretoFinal, pareto)
    # Add the second extreme point of the pareto frontier
    push!(paretoFinal, [0, minimum(pareto.y)])
    # Write the pareto points in the output file
    CSV.write(paretoDatapoints, paretoFinal, delim="\t")

    # return the file with datapoints
    return paretoDatapoints
end



"""Create the pareto frontier.
"""
function compute_pareto_front_top_double_double(qfoDatapoints::String, outDir::String; cleanup::Bool=false)::String
    @debug "compute_pareto_front_top_double_double :: START" qfoDatapoints outDir cleanup
    
    # tmp variables
    tmpDir::String = tmpPath = ""
    # These directories and files must be in the results directory
    bname::String = string(rsplit(basename(qfoDatapoints), ".", limit=2)[1])
    paretoDatapoints::String = joinpath(outDir, "pareto.x.y.tsv")
    @show paretoDatapoints

    df = CSV.read(qfoDatapoints, DataFrame)
    println(describe(df))

    # Compute the pareto front
    sort!(df, rev=true) # Sort X in descending order
    pareto = df[1:1, :]; # Add the first extreme to the pareto front
    # Search for other frontier points and add
    foreach(row -> row.y > pareto.y[end] && push!(pareto, row), eachrow(df));

    # Add the first point for the pareto extreme line
    paretoFinal = DataFrame(x = maximum(pareto.x), y = 0.0)
    # Add the previously computed points
    append!(paretoFinal, pareto)
    # Add the second extreme point of the pareto frontier
    push!(paretoFinal, [0.0, maximum(pareto.y)])
    # Write the pareto points in the output file
    CSV.write(paretoDatapoints, paretoFinal, delim="\t")

    # return the file with datapoints
    return paretoDatapoints
end



"""Create the pareto frontier.
"""
function compute_pareto_front_top_int_double(qfoDatapoints::String, outDir::String; cleanup::Bool=false)::String
    @debug "compute_pareto_front_top_int_double :: START" qfoDatapoints outDir cleanup
    
    # tmp variables
    tmpDir::String = tmpPath = ""
    # These directories and files must be in the results directory
    bname::String = string(rsplit(basename(qfoDatapoints), ".", limit=2)[1])
    paretoDatapoints::String = joinpath(outDir, "pareto.x.y.tsv")
    @show paretoDatapoints

    # df = DataFrame(CSV.File(qfoDatapoints))
    # df = CSV.File(qfoDatapoints) |> DataFrame
    df = CSV.read(qfoDatapoints, DataFrame)
    println(describe(df))

    # Compute the pareto front
    sort!(df, rev=true) # Sort X in descending order
    pareto = df[1:1, :]; # Add the first extreme to the pareto front
    # Search for other frontier points and add
    foreach(row -> row.y > pareto.y[end] && push!(pareto, row), eachrow(df));

    # Add the first point for the pareto extreme line
    paretoFinal = DataFrame(x = Int(maximum(pareto.x)), y = 0.0)
    # Add the previously computed points
    append!(paretoFinal, pareto)
    # Add the second extreme point of the pareto frontier
    push!(paretoFinal, [0, maximum(pareto.y)])
    # Write the pareto points in the output file
    CSV.write(paretoDatapoints, paretoFinal, delim="\t")

    # return the file with datapoints
    return paretoDatapoints
end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)

#=
println("Parsed args:")
for (key,val) in args
    println("  $key  =>  $(repr(val))")
end
=#

qfoDatapoints = realpath(args["data-points"][1])
outDir = abspath(args["output-dir"])
dataType = args["data-type"]
validDataTypes = ["top-int-double", "top-double-double", "bottom-int-double", "bottom-double-double"]
if dataType ∉ validDataTypes
    @error "The data type is not valid!" dataType validDataTypes
    exit(-7)
end

makedir(outDir)
# outTbl = abspath(args["output-tbl"])
cleanup = args["cleanup"]
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "Parteto frontier will be computed with the following parameters:" qfoDatapoints outDir debug

# Compute pareto
if dataType == "top-int-double"
    compute_pareto_front_top_int_double(qfoDatapoints, outDir, cleanup=cleanup)
elseif dataType == "top-double-double"
    compute_pareto_front_top_double_double(qfoDatapoints, outDir, cleanup=cleanup)
elseif dataType == "bottom-int-double"
    compute_pareto_front_bottom_int_double(qfoDatapoints, outDir, cleanup=cleanup)

end