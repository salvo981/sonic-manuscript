# This will generate single figures with 3 facets (plots)
# 1 for each type of domain: prokaryote, eukaryote, mixed


# MMseqs default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ./datapoints/s138-mags-mmseqs-ess-default.extime.datapoint.tsv -o ./plots_multi --multi-col-name habitat-l1-type --mrkr-size 10 --prefix mags-mmseqs-default-MULTIPLOT-extimes --format svg

# MMseqs fast
# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/s138-qfo20-mmseqs-ess-fast.extime.datapoint.tsv -o ./plots_multi --multi-col-name habitat-l1-type --mrkr-size 10 --prefix qfo20-mmseqs-fast-MULTIPLOT-extimes --format svg


# Diamond default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ./datapoints/s138-mags-dmnd-ess-default.extime.datapoint.tsv -o ./plots_multi --multi-col-name habitat-l1-type --mrkr-size 10 --prefix mags-dmnd-default-MULTIPLOT-extimes --format svg

# Diamond fast
# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/s138-qfo20-dmnd-ess-fast.extime.datapoint.tsv -o ./plots_multi --multi-col-name habitat-l1-type --mrkr-size 10 --prefix qfo20-dmnd-fast-MULTIPLOT-extimes --format svg