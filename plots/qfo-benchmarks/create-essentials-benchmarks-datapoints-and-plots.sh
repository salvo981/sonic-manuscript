# Generate qfo results datasets
julia ~/work_repos/sonic-manuscript/scripts/julia/create_qfo_benchmark_datapoints.jl ~/work_repos/sonic-manuscript/tables/qfo-benchmarks/benchmark_results_s138_qfo20-raw-datapoints/ --output-dir ~/work_repos/sonic-manuscript/plots/qfo-benchmarks/datapoints/ --cleanup
wait
# Generate the plots
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_results_essentials_vs_complete.py -i ~/work_repos/sonic-manuscript/plots/qfo-benchmarks/datapoints/qfo-benchmark-results.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks/ --prefix qfo-bench-s138-manuscript --mrkr-size 5 --pareto-script ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl --format svg