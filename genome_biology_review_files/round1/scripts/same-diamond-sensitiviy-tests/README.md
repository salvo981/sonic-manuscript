This test will runs each of the compared methods using the same diamond sensitivity.  
Specifically, the diamond sensitivity of SP2 will be set as the same as those used by the competitors by default.

#### Motivation
This tests are necessary to show that SP2 is faster when using the diamond sensitivity, which is a more fair comparison.  
Runs with the Diamond settings should also give more similar CPU times for each of the comared methods.
