#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic136-mags-manuscript
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date

 #### MMseqs runs #####

echo $'\n@@@@ sonic136-mmseqs-complete-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/mags-orthology-inference -p s136-mmseqs-ca-default-mags -ow --threads 128 -ca -m default -op -noidx

wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-essentials-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/mags-orthology-inference/ -p s136-mmseqs-ess-default-mags -ow --threads 128 -m default -op -noidx


wait
echo $'\n'


 ### Diamond runs #####

echo $'\n@@@@ sonic136-dmnd-complete-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/mags-orthology-inference -p s136-dmnd-ca-default-mags -ow --aln-tool diamond --threads 128 -ca -m default -op -noidx


wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-essentials-default-mags @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/mags-orthology-inference/ -p s136-dmnd-ess-default-mags -ow --aln-tool diamond --threads 128 -m default -op -noidx

wait
echo $'\n'

# All runs completed
date