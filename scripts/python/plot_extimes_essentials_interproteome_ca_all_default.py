'''
Generate plots related to execution times for essential mode.
The input is a table genetated using the script under /scripts/julia/create_essential_extimes_datapoints.jl
'''

import os
import sys
from typing import TextIO, List, Set, Dict, Tuple
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots info about the proteomes.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file datapoints for execution times and AAI\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=False, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--hue", type=str, required=False, help="Column containing categorical information.", default="")
    parser.add_argument("--divisor", type=int, required=False, help="Integer value by which the main dapaoint should be divided (e.g., 1000).", default=1)
    parser.add_argument("--multi-col-name", type=str, required=False, help="Column name used to create multiple plots.", default="")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("-r", "--rows", type=int, required=False, help="Number of records to load. Used for debugging. Default=0", default=0)

    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def plot_box(df, plotsDir: str, prefix: str, dataColIdx: int, categoryColIdx: int, hueColIdx: int, divisor: int = 0, mrkSize: int = 3):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")

    # sns.set_theme(style="whitegrid")

    # Set output file name
    fmt:str = "png"
    dataColName: str = df.columns[dataColIdx]
    categoryColName: str = df.columns[categoryColIdx]
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.box.{categoryColName}.{dataColName}")

    # Check if categories should be used
    hasHue: bool = False
    if hueColIdx > 0:
        hasHue = True
        tmpOutPath = f"{tmpOutPath}.{df.columns[hueColIdx]}.{fmt}"
    else:
        tmpOutPath = f"{tmpOutPath}.{fmt}"

    # Set temp variables
    print(f"Column with data:\t{dataColName}")
    print(f"Column categories:\t{categoryColName}")
    print(f"Categorical info will be used:\t{hasHue}")
    print(f"Output format:\t{fmt}")
    print(f"Output plot: {tmpOutPath}")

    outlierSize: int = 0

    # Set plot paramaters
    # colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    # fig = plt.figure()
    # plt.title(plotTitle)

    # Initialize the figure with a logarithmic x axis
    # f, ax = plt.subplots(figsize=(7, 6))
    # ax.set_xscale("log")

    # Plot box plots
    if hasHue:
        if divisor > 0:
            # Divide the value if a divisor was provided
            ax = sns.boxplot(x=categoryColName, y=df[dataColName]/divisor, data=df, fliersize=outlierSize, dodge=True, hue=df.columns[hueColIdx])
            ax = sns.swarmplot(x=categoryColName, y=df[dataColName]/divisor, data=df, color=".2", dodge=True, hue=df.columns[hueColIdx], size=mrkSize)
        else:
            ax = sns.boxplot(x=categoryColName, y=dataColName, data=df, fliersize=outlierSize, dodge=True, hue=df.columns[hueColIdx])
            ax = sns.swarmplot(x=categoryColName, y=dataColName, data=df, color=".2", dodge=True, hue=df.columns[hueColIdx], size=mrkSize)
    else:
        if divisor > 0:
            # Divide the value if a divisor was provided
            ax = sns.boxplot(x=categoryColName, y=df[dataColName]/divisor, data=df, fliersize=outlierSize, dodge=True)
            ax = sns.swarmplot(x=categoryColName, y=df[dataColName]/divisor, data=df, color=".2", size=mrkSize)
        else:
            ax = sns.boxplot(x=categoryColName, y=dataColName, data=df, fliersize=outlierSize, dodge=True)
            ax = sns.swarmplot(x=categoryColName, y=df[dataColName], data=df, color=".2", size=mrkSize)

    # Add in points to show each observation
    # sns.stripplot(x="methods", y="cosine", data=df, size=4, color=".3", linewidth=0)

    # Tweak the visual presentation
    # ax.set(ylabel="")
    # sns.despine(trim=True, left=True)

    plt.savefig(tmpOutPath, dpi=160)
    # del f




def plot_relplot(df, plotsDir: str, prefix: str, xColName: str, yColName: int, hueColName: str, multiplotColumn: str = "", divisor: int = 0, mrkSize: int = 3, xLimits: Tuple[int, int] = (0, 100), yLimits: Tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.debug("plot_relplot :: START")
    logging.info(f"Records: {df.shape[0]}")
    # sns.set_theme(style="whitegrid")
    # sns.set_theme(style="white")

    # myStyle = "darkgrid"
    # myStyle = "whitegrid"
    # myStyle = "white"
    # myStyle = "dark"
    myStyle = "ticks"
    sns.set_style(myStyle,{'axes.grid' : True})

    # Set output file name
    tmpOutPath = os.path.join(plotsDir, f"{prefix}.relplot.{yColName}.{xColName}")

    # Check if categories should be used
    hasHue: bool = False
    if len(hueColName) > 0:
        hasHue = True
        tmpOutPath = f"{tmpOutPath}.{hueColName}"
    else:
        tmpOutPath = f"{tmpOutPath}"

    if len(multiplotColumn) == 0:
        multiplotColumn = ""
        tmpOutPath = f"{tmpOutPath}.{fmt}"
    else:
        tmpOutPath = f"{tmpOutPath}.{multiplotColumn}.{fmt}"

    # Set temp variables
    print(f"Column X values:\t{xColName}")
    print(f"Column Y values:\t{yColName}")
    print(f"Categorical info will be used:\t{hasHue}")
    print(f"Multiplot column:\t{multiplotColumn}")
    print(f"Output format:\t{fmt}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Output plot: {tmpOutPath}")

    # Set plot paramaters
    # colors = ["red" , "green", "blue", "black", "orange", "cyan"]
    # fig = plt.figure()
    # plt.title(plotTitle)

    # Initialize the figure with a logarithmic x axis
    # f, ax = plt.subplots(figsize=(7, 6))
    # ax.set_xscale("log")

    # Show the legend or not
    myLegend: bool = False

    # Plot box plots
    if hasHue:
        if divisor > 0:
            # Divide the value if a divisor was provided
            if len(multiplotColumn) > 0:
                ax = sns.relplot(x=df[xColName], y=df[yColName]/divisor, data=df, hue=hueColName, s=mrkSize, col=multiplotColumn, legend=myLegend)
            else:
                # generate single column plot
                ax = sns.relplot(x=df[xColName], y=df[yColName]/divisor, data=df, hue=hueColName, s=mrkSize, col=None, legend=myLegend)
        else:
            if len(multiplotColumn) > 0:
                ax = sns.relplot(x=df[xColName], y=df[yColName], data=df, hue=hueColName, s=mrkSize, col=multiplotColumn, legend=myLegend)
            else:
                ax = sns.relplot(x=df[xColName], y=df[yColName], data=df, hue=hueColName, s=mrkSize, col=None, legend=myLegend)
    else:
        if divisor > 0:
            # Divide the value if a divisor was provided
            if len(multiplotColumn) > 0:
                ax = sns.relplot(x=df[xColName], y=df[yColName]/divisor, data=df, s=mrkSize, col=multiplotColumn, legend=myLegend)
            else:
                ax = sns.relplot(x=df[xColName], y=df[yColName]/divisor, data=df, s=mrkSize, col=None, legend=myLegend)
        else:
            if len(multiplotColumn) > 0:
                ax = sns.relplot(x=df[xColName], y=df[yColName], data=df, s=mrkSize, col=multiplotColumn, legend=myLegend)
            else:
                ax = sns.relplot(x=df[xColName], y=df[yColName], data=df, s=mrkSize, col=None, legend=myLegend)

    # Add in points to show each observation
    # Tweak the visual presentation

    # ax.set(ylabel="")
    # sns.despine(trim=True, left=True)

    # Controls wether the borders are shown or not

    sns.despine(top=False, right=False, left=False, bottom=False, offset=None, trim=False)
    # sns.axes_style(myStyle)

    ax.set(xlim=xLimits)
    ax.set(ylim=yLimits)
    if hideXlab:
        ax.set(xlabel=None)
    if hideYlab:
        ax.set(ylabel=None)

    # This can be used for a finer control
    # Directly modifying matplotlib style
    # rc = {'figure.figsize':(10,5),
    #     'axes.facecolor':'white',
    #     'axes.grid' : True,
    #     'grid.color': '.8',
    #     'font.family':'Times New Roman',
    #     'font.size' : 15}

    # sns.despine(bottom = False, left = True)

    plt.savefig(tmpOutPath, dpi=160)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    # methods = args.methods
    # totRows = args.rows
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    # dataCol: int = args.col
    # categoryCol: int = args.category_col
    hueCol: int = args.hue
    multicolVar: int = args.multi_col_name
    divisor: int = args.divisor
    outFmt: str = args.format
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)
    # makedir(plotsDir)


    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Divisor:\t{divisor}")

    # Read dataframe
    # The hdr of datapoint file have the following format
    # pair prot-cnt-a prot-cnt-b prot-size-a prot-size-b tot-time-ca tot-time-ra speedup pctA pctB avgPct fastest prediction correct-prediction ortho-cnt mean-aai std-aai ortho-fraction-comparem empire-a empire-b empire-class domain-a domain-b domain-class
    df = pd.read_csv(inTbl, sep="\t", dtype= {"pair": str, "prot-cnt-a": np.uint32, "prot-cnt-b": np.uint32, "prot-size-a": np.uint32, "prot-size-b": np.uint32, "tot-time-ca": np.float32, "tot-time-ra": np.float32, "speedup": np.float32, "pctA": np.float32, "pctB": np.float32, "avgPct": np.float32, "fastest": np.uint32, "prediction": np.uint32, "correct-prediction": np.uint32, "ortho-cnt": np.uint32, "mean-aai": np.float32, "std-aai": np.float32, "ortho-fraction-comparem": np.float32,  "empire-a": str, "empire-b": str, "empire-class": str, "domain-a": str, "domain-b": str, "domain-class": str, "extime-diff-folds-ca": np.float32, "extime-diff-folds-ra": np.float32, "prot-count-diff-folds": np.float32, "prot-size-diff-folds": np.float32}, engine="c")

    # Set a custom color palette
    # Use this when showing the prediction errors
    # myPalette = ["#FF0B04", "#4374B3", "#000000"]
    # sns.set_palette(sns.color_palette(myPalette))

    # Show or hide X and Y labels
    hideXlab = True
    hideYlab = True

    # Create plots regarding the extime difference between 2 pairs
    # and plots also relating to the protein size difference

    # Apply ABS to proteins size and count folds
    print(min(df["prot-count-diff-folds"]), max(df["prot-count-diff-folds"]))
    df["prot-count-diff-folds"] = df["prot-count-diff-folds"].transform(abs)
    print(min(df["prot-count-diff-folds"]), max(df["prot-count-diff-folds"]))
    print(min(df["prot-size-diff-folds"]), max(df["prot-size-diff-folds"]))
    df["prot-size-diff-folds"] = df["prot-size-diff-folds"].transform(abs)
    print(min(df["prot-size-diff-folds"]), max(df["prot-size-diff-folds"]))

    minProtCntFoldsDiff: float32 = 1.0
    maxProtCntFoldsDiff: float32 = 105.0
    minProtSizeFoldsDiff: float32 = 1.0
    maxProtSizeFoldsDiff: float32 = 105.0
    minExtimeDiffFoldsCa: float32 = 1.
    maxExtimeDiffFoldsCa: float32 = 38.83

    # Interproteome ex time difference folds VS Mean-AAI
    # Suggested X and Y limits
    xlims = (30, 100)
    ylims = (0, 10)
    xCol = "mean-aai"
    yCol = "extime-diff-folds-ca"
    plot_relplot(df, outDir, prefix=prefix, xColName=xCol, yColName=yCol, hueColName=hueCol, divisor=divisor, multiplotColumn=multicolVar, mrkSize=markerSize, xLimits=xlims, yLimits=ylims, hideXlab=hideXlab, hideYlab=hideYlab, fmt=outFmt)


    # Interproteome ex time difference folds VS prot-size-diff-folds
    # Suggested X and Y limits
    xlims = (0, 105)
    ylims = (0, 10)
    xCol = "prot-size-diff-folds"
    yCol = "extime-diff-folds-ca"
    plot_relplot(df, outDir, prefix=prefix, xColName=xCol, yColName=yCol, hueColName=hueCol, divisor=divisor, multiplotColumn=multicolVar, mrkSize=markerSize, xLimits=xlims, yLimits=ylims, hideXlab=hideXlab, hideYlab=hideYlab, fmt=outFmt)

    # Interproteome ex time difference folds VS prot-counts-diff-folds
    # Suggested X and Y limits
    xlims = (0, 105)
    ylims = (0, 10)
    xCol = "prot-count-diff-folds"
    yCol = "extime-diff-folds-ca"
    plot_relplot(df, outDir, prefix=prefix, xColName=xCol, yColName=yCol, hueColName=hueCol, divisor=divisor, multiplotColumn=multicolVar, mrkSize=markerSize, xLimits=xlims, yLimits=ylims, hideXlab=hideXlab, hideYlab=hideYlab, fmt=outFmt)





if __name__ == "__main__":
    main()
