#### Fast mode

# echo $'\n@@@@ sonic2-dm-hybrid-fast-thr75 @@@@\n'
# sonicparanoid -i /home/salvocos/Desktop/sonicparanoid-arch-analysis/reference-datasets/input-qfo2020 -o /home/salvocos/Desktop/sonicparanoid_test/qfo20-premerge-tests/sonic2-dm-hybrid-fast -p s2-dm-fast-thr75 -ot --aln-tool diamond --threads 8 -m fast -op -noidx --min-arch-merging-cov 0.75
# wait
# echo $'\n'

# echo $'\n@@@@ sonic2-dm-graph-fast @@@@\n'
# sonicparanoid -i /home/salvocos/Desktop/sonicparanoid-arch-analysis/reference-datasets/input-qfo2020 -o /home/salvocos/Desktop/sonicparanoid_test/qfo20-premerge-tests/sonic2-dm-hybrid-fast-g -p s2-dm-fast-graph -ot --aln-tool diamond --threads 8 -m fast -op -noidx -sa
# wait
# echo $'\n'

#### Default mode

echo $'\n@@@@ sonic2-dm-hybrid-sens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/Desktop/sonicparanoid-arch-analysis/reference-datasets/input-qfo2020 -o /home/salvocos/Desktop/sonicparanoid_test/qfo20-premerge-tests/sonic2-dm-hybrid-sens -p s2-dm-sens-thr75 -ot --aln-tool diamond --threads 8 -m sensitive -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-graph-sens @@@@\n'
sonicparanoid -i /home/salvocos/Desktop/sonicparanoid-arch-analysis/reference-datasets/input-qfo2020 -o /home/salvocos/Desktop/sonicparanoid_test/qfo20-premerge-tests/sonic2-dm-hybrid-sens-g -p s2-dm-sens-graph -ot --aln-tool diamond --threads 8 -m sensitive -op -noidx -sa
wait
echo $'\n'

#### Sensitive mode

# echo $'\n@@@@ sonic2-mm-hybrid-msens-thr75 @@@@\n'
# sonicparanoid -i //home/salvocos/Desktop/sonicparanoid-arch-analysis/reference-datasets/input-qfo2020 -o /home/salvocos/Desktop/sonicparanoid_test/qfo20-premerge-tests/sonic2-mm-hybrid-msens -p s2-mm-msens-thr75 -ot --threads 8 -m most-sensitive -op -noidx --min-arch-merging-cov 0.75
# wait
# echo $'\n'

# echo $'\n@@@@ sonic2-mm-graph-msens @@@@\n'
# sonicparanoid -i /home/salvocos/Desktop/sonicparanoid-arch-analysis/reference-datasets/input-qfo2020 -o /home/salvocos/Desktop/sonicparanoid_test/qfo20-premerge-tests/sonic2-mm-hybrid-msens-g -p s2-mm-msens-graph -ot --threads 8 -m most-sensitive -op -noidx -sa
# wait
# echo $'\n'
