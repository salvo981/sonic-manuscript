Script and media files generated during the revisions for Genome Biology.  

The directories have the following content:
- scripts: main scripts used to perform the requested tests  
- logs: logs, error, and execution files
- tables: sources tables from which extra tables in the manuscript were generated

If you cannot find any of the material you are looking for, please contact the owner of this repository.