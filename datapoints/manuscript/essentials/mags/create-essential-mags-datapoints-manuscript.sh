# This script will generate the data for essential executiont times
# using the script in sonic-manuscript/scripts/julia/create_essential_extimes_datapoints-MAGS.jl

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints-MAGS.jl tables/aln-time-files/mags/aln_ex_times_mmseqs_ca_s137-mmseqs-ca-default-mags.tsv tables/aln-time-files/mags/aln_ex_times_mmseqs_ra_s137-mmseqs-ess-default-mags.tsv tables/snapshot-2000mags-bacteria-sonic136-azur.tsv --aai-tbl tables/aai_computation_comparem/aai_reference2000mags_bacteria.tsv --meta-data tables/reference_2000_mags_bacteria-tbl.tsv --pred-tbl tables/fastest_pairs-2000mags-bacteria-sonic136-azur.tsv --output-tbl datapoints/s138-mags-mmseqs-ess-default.extime.datapoint.tsv
wait

# Generate stats
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl datapoints/s138-mags-mmseqs-ess-default.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/mags/essential-stats-mags



##### DIAMOND RUNS #####
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints-MAGS.jl tables/aln-time-files/mags/aln_ex_times_diamond_ca_s137-dmnd-ca-default-mags.tsv tables/aln-time-files/mags/aln_ex_times_diamond_ra_s137-dmnd-ess-default-mags.tsv tables/snapshot-2000mags-bacteria-sonic136-azur.tsv --aai-tbl tables/aai_computation_comparem/aai_reference2000mags_bacteria.tsv --meta-data tables/reference_2000_mags_bacteria-tbl.tsv --pred-tbl tables/fastest_pairs-2000mags-bacteria-sonic136-azur.tsv --output-tbl datapoints/s138-mags-dmnd-ess-default.extime.datapoint.tsv -d

# Generate stats
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl datapoints/s138-mags-dmnd-ess-default.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/mags/essential-stats-mags