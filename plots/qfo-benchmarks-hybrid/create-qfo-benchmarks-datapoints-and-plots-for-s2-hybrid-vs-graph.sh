# Generate qfo results datasets
julia /home/salvocos/work_repos/sonic-manuscript/scripts/julia/create_qfo_benchmark_datapoints_s2_hybrid_vs_graph.jl ~/work_repos/sonic-manuscript/tables/qfo-benchmarks/benchmark_results_s2_hybrid_qfo20-raw-datapoints/ --output-dir ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/ --cleanup
wait
# Generate the plots
python3 /home/salvocos/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_results_s2_hybrid_vs_graph.py -i ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/qfo-benchmark-results.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/ --prefix qfo-bench-s2-hybrid-manuscript --mrkr-size 5 --pareto-script ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl --format svg
