# MMseqs msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-msens-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-mmseqs-msens-extimes

# MMseqs sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-sens-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-mmseqs-sens-extimes

# MMseqs default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-default-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-mmseqs-default-extimes

# MMseqs fast
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-fast-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-mmseqs-fast-extimes

# DIAMOND RELATED TABLES
# Diamond msens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-msens-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-dmnd-msens-extimes

# Diamond sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-sens-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-dmnd-sens-extimes

# Diamond default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-default-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-dmnd-default-extimes

# Diamond fast
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials.py -i ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-fast-ismb.extime.datapoint.tsv --hue empire-class --mrkr-size 10 --prefix qfo-ismb-dmnd-fast-extimes