#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e ${HOME}/tmp/sonicparanoid_runs/training-data-creation/logs/stderr
#PBS -o ${HOME}/tmp/sonicparanoid_runs/training-data-creation/logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/training-data-creation
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/training-data-creation/reference250processed -o /home/salvocos/tmp/sonicparanoid_runs/training-data-creation/training-runs-mmseqs-250prots-sens -ot --threads 128 -p mmseqs-sens -noidx -m sensitive -op -ca