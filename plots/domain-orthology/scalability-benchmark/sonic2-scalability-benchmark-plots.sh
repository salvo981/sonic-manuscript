# This script create the plots

# The datapoints to be plotted, if not available shoudl be created
# using the script in
# /home/salvocos/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark/sonic2-scalability-bench-datapoints.sh

date

# QfO2020
echo $'\n@@@@ PLOT :: sonic2-domain-orhtology-scalability-benchmark-qfo20 @@@@\n'
python3 /home/salvocos/work_repos/sonic-manuscript/scripts/python/plot_domain_orthology_scalability_curves_qfo.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark/s2-domain-orthology-scalability-qfo20_39.tsv -o ~/work_repos/sonic-manuscript/plots/domain-orthology/scalability-benchmark/ --prefix qfo-scalability-s2-manuscript --mrkr-size 5 --format svg

wait
echo $'\n'

date

######### QfO END ############

# Plot curves for MAGs
echo $'\n@@@@ PLOT :: sonic2-domain-orhtology-scalability-benchmark-2000mags @@@@\n'
python3 /home/salvocos/work_repos/sonic-manuscript/scripts/python/plot_domain_orthology_scalability_curves_mags.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark/s2-domain-orthology-scalability-mags_20.tsv -o ~/work_repos/sonic-manuscript/plots/domain-orthology/scalability-benchmark/ --prefix mags-scalability-s2-manuscript --mrkr-size 5 --format svg

wait
echo $'\n'

# All runs completed
date