# Generate SVG files for equations using tex2svg
date
echo "Create SVG  files from Latex equations."
# Equation 1: BBH
cat equation1.tex | xargs -0 -t -I % tex2svg '%' > equation1.svg
wait

# Equation 2: Essential subsets
cat equation2.tex | xargs -0 -t -I % tex2svg '%' > equation2.svg
wait

# Equation 3: Essential-BBH
cat equation3.tex | xargs -0 -t -I % tex2svg '%' > equation3.svg
date
