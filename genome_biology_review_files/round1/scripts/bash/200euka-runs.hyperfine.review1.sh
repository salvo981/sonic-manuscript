#!/bin/bash

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# of2 -> OrthoFinder2
# po6 -> ProteinOrtho6
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
of2_cmd=~/work_repos/OrthoFinder_v254/orthofinder.py
po6_cmd=~/work_repos/proteinortho/proteinortho6.pl
broccoli_cmd=~/work_repos/Broccoli/broccoli.py
fasttree_cmd=/usr/local/bin/FastTree
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=reference200proteomes-euka
cpu_cnt=128

# Output directories
output_root=$root\output_200euka/
sp_output_root=$output_root\sp2/
mkdir $sp_output_root
of2_output_root=$output_root\of2/
mkdir $of2_output_root
po6_output_root=$output_root\po6/
mkdir $po6_output_root
broccoli_output_root=$output_root\broccoli/
mkdir $broccoli_output_root

# SonicParanoid2 runs
sp_mode=default
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus

echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json  \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 (fast)
sp_mode=fast
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 (sensitive)
sp_mode=sensitive
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 runs (graph-only)
sp_mode=default
sp_run_name_prefix=sp2-$sp_mode-graph-only-$input_set-$cpu_cnt\cpus

echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json  \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --graph-only \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix -t $cpu_cnt --graph-only > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# SonicParanoid2 (fast graph-only)
sp_mode=fast
sp_run_name_prefix=sp2-$sp_mode-graph-only-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt --graph-only \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt --graph-only > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date

# OrthoFinder Runs
# IMPORTANT: OrthoFinder usually crashes becaus eit opens too many files
# https://github.com/davidemms/OrthoFinder/issues/384
# In order to mitigate this issue increase the limits (if possible)
# $prlimit # check the current limits for NOFILE
# Change the Hard and Soft limits for NOFILE
# This should be enough for up to 1k species
# $ulimit -Hn 1048576
# $ulimit -Sn 1048576

of2_mode=default
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus

# OrthoFinder run Default
echo $'\n@@@@'$' OrthoFinder runs '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json \"python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt \> $of2_output_root\log.$of2_run_name_prefix.txt 2\> $of2_output_root\err.$of2_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json "python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt > $of2_output_root\log.$of2_run_name_prefix.txt 2> $of2_output_root\err.$of2_run_name_prefix.txt"
wait

# ProteinOrtho6 run [default]
po6_mode=default # This should use the pseudo bitscores
po6_run_name_prefix=po6-$po6_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' ProteinOrtho runs '$cpu_cnt$' cpus' $input_set $po6_run_name_prefix $'@@@@\n'

echo $po6_cmd
echo $po6_output_root
echo $po6_run_name_prefix
# Create and enter the po6 output directory
mkdir $po6_output_root$po6_run_name_prefix
wait
cd $po6_output_root$po6_run_name_prefix
wait

echo hyperfine --style none --runs 1 --export-json $po6_output_root$po6_run_name_prefix.hypefine.json \"$po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* \> $po6_output_root\log.$po6_run_name_prefix.txt 2\&\>1\"
# hyperfine --style none --runs 1 --export-json $po6_output_root$po6_run_name_prefix.hypefine.json "$po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* > $po6_output_root\log.$po6_run_name_prefix.txt 2>&1"
wait

echo $'\n'
date

#### Broccoli used the 2TB of memory and should be tested again ####
# Broccoli run [default]
broccoli_mode=default # This should use the pseudo bitscores
broccoli_run_name_prefix=broccoli-$broccoli_mode-$input_set-$cpu_cnt\cpus
# echo $'\n@@@@'$' Broccoli runs '$cpu_cnt$' cpus' $input_set $broccoli_run_name_prefix $'@@@@\n'

# Create and enter the broccoli output directory
# mkdir $broccoli_output_root$broccoli_run_name_prefix
# wait
# cd $broccoli_output_root$broccoli_run_name_prefix
# wait

# echo hyperfine --style none --runs 1 --export-json $broccoli_output_root$broccoli_run_name_prefix.hypefine.json \"python3 $broccoli_cmd -dir $input_root$input_set -path_fasttree $fasttree_cmd -ext faa -threads $cpu_cnt \> $broccoli_output_root\log.$broccoli_run_name_prefix.txt 2\> $broccoli_output_root\err.$broccoli_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $broccoli_output_root$broccoli_run_name_prefix.hypefine.json "python3 $broccoli_cmd -dir $input_root$input_set -path_fasttree $fasttree_cmd -ext faa -threads $cpu_cnt > $broccoli_output_root\log.$broccoli_run_name_prefix.txt 2> $broccoli_output_root\err.$broccoli_run_name_prefix.txt"
# wait

# echo $'\n'
# date

# echo $'\n Start the slow runs separately, since Broccoli probably crashed the server...'

# exit -5

# WE WILL NOW START THE SLOWEST RUNS #

# SonicParanoid2 (sensitive graph-only)
sp_mode=sensitive
sp_run_name_prefix=sp2-$sp_mode-graph-only-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json \"sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt --graph-only \> $sp_output_root\log.$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_run_name_prefix.txt\"
# hyperfine --style none --runs 1 --export-json $sp_output_root$sp_run_name_prefix\.hyperfine.json "sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_run_name_prefix --mode $sp_mode -t $cpu_cnt --graph-only > $sp_output_root\log.$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_run_name_prefix.txt"
wait

echo $'\n'
date


# OrthoFinder run [-M msa only]
of2_mode=Mmsa
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' OrthoFinder runs '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json \"python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt -M msa \> $of2_output_root\log.$of2_run_name_prefix.txt 2\> $of2_output_root\err.$of2_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json "python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt  -M msa > $of2_output_root\log.$of2_run_name_prefix.txt 2> $of2_output_root\err.$of2_run_name_prefix.txt"
wait

# All runs completed
echo $'\n'
date
