This directory will contain `illustrations` regarding qfo benchmark results for sonicparanoid2 (in hybrid mode).  
To types of Figures will be in this directory
1) Benchmark comparison for sonicparanoid in graph and hybrid mode
2) Benchmark comparison between sonicparanoid (hybrid mode) and all other methods


### Reference plots used for graph vs hybrid benchmark  
>Archive: plots.qfo-bench-s2-hybrid-vs-graph.tar.xz
- qfo-bench-s2-hybrid-manuscript.ec.pareto.svg
- qfo-bench-s2-hybrid-manuscript.g_std2_eukaryota.pareto.svg
- qfo-bench-s2-hybrid-manuscript.g_std2_fungi.pareto.svg
- qfo-bench-s2-hybrid-manuscript.g_std2_luca.pareto.svg
- qfo-bench-s2-hybrid-manuscript.g_std2_vertebrata.pareto.svg
- qfo-bench-s2-hybrid-manuscript.go.pareto.svg
- qfo-bench-s2-hybrid-manuscript.std_bacteria.pareto.svg
- qfo-bench-s2-hybrid-manuscript.std_eukaryota.pareto.svg
- qfo-bench-s2-hybrid-manuscript.std_fungi.pareto.svg
- qfo-bench-s2-hybrid-manuscript.swisstrees.pareto.svg
- qfo-bench-s2-hybrid-manuscript.treefam-a.pareto.svg


### QfO20 ranking best-N
The plot showing the best N ranked in the QfO benchmark are generated through the bash script  
- /sonic-manuscript/plots/qfo-benchmarks-ranking/create-qfo-benchmarks-ranking-plots.sh
- /sonic-manuscript/plots/qfo-benchmarks-ranking/create-qfo-benchmarks-ranking-plots-best5.sh  
- /sonic-manuscript/plots/qfo-benchmarks-ranking/create-qfo-benchmarks-ranking-plots-best7.sh
- /sonic-manuscript/plots/qfo-benchmarks-ranking/create-qfo-benchmarks-ranking-plots-best8.sh

The size (height and scale) of the seaborn plot are adjusted to fit in the main figure as an extra panel.  
NOTE: not all the plots generated are used.  
The script create-qfo-benchmarks-ranking-plots.sh generates the plots with all the participants, and used as supplementary figures.