#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N orthobroccoli-qfo20-manuscript-all-SSD
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /ssd_home/extime-comparison/

date

echo $'\n@@@@ orthofinder-qfo20-128cpus @@@@\n'
# python3 /home/salvocos/work_repos/OrthoFinder/orthofinder.py -f /ssd_home/extime-comparison/small_input/ -o orthofinder_output_7orgs -n small_input_7orgs -t 128 -og

# Stop after the creation fo orthogroups using MCL
# python3 /home/salvocos/work_repos/OrthoFinder/orthofinder.py -f /ssd_home/extime-comparison/qfo2020processed_fixed_xtropicalis_renamed/ -o /ssd_home/extime-comparison/orthofinder-qfo20-128cpus -n orthofinder-qfo20-128cpus -t 128 -og

# Perform MSAs and stop after the creation of Phylogenetically inferred orthogroups
python3 /home/salvocos/work_repos/OrthoFinder/orthofinder.py -f /ssd_home/extime-comparison/qfo2020processed_fixed_xtropicalis_renamed/ -o /ssd_home/extime-comparison/orthofinder-qfo20-128cpus-phogs -n orthofinder-qfo20-128cpus-phogs -t 128 -ot -M msa


wait
echo $'\n'
date
echo $'\n'


echo $'\n@@@@ broccoli-qfo20-128cpus @@@@\n'
# python3 /home/salvocos/work_repos/Broccoli/broccoli.py -dir /ssd_home/extime-comparison/small_input/ -path_fasttree /ssd_home/extime-comparison/bin/FastTree -threads 128
# python3 /home/salvocos/work_repos/Broccoli/broccoli.py -dir /ssd_home/extime-comparison/qfo2020processed_fixed_xtropicalis_renamed/ -path_fasttree /ssd_home/extime-comparison/bin/FastTree -threads 128

wait
echo $'\n'


# All runs completed
date