# Reference 2000 MAGS
# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/reference_2000_mags_bacteria.table.tsv -c 20 --category-col 15 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 2 --prefix reference2000mags --format svg --xlab Domain --ylab Proteins

python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/reference_2000_mags_bacteria.table.tsv -c 21 --category-col 15 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 2 --divisor 1000000 --prefix reference2000mags --format svg --xlab Domain --ylab "Proteome size"

# QfO2020
# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/qfo2020processed.table.tsv -c 6 --category-col 2 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 3 --prefix qfo20 --format svg --xlab Domain --ylab Proteins

python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/qfo2020processed.table.tsv -c 7 --category-col 2 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 3 --divisor 1000000 --prefix qfo20 --format svg --format svg --xlab Domain --ylab "Proteome size"

# Reference 200 Proteomes
# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/200eukaryotes.table.tsv -c 6 --category-col 2 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 3 --prefix 200eukaryotes --format svg --xlab Domain --ylab Proteins

python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/200eukaryotes.table.tsv -c 7 --category-col 2 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 3 --divisor 1000000 --prefix 200eukaryotes --format svg --format svg --xlab Domain --ylab "Proteome size"

# Reference 250 Proteomes
# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/main250processed.table.tsv -c 6 --category-col 2 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 3 --prefix main250proteomes --format svg --xlab Domain --ylab Proteins

# python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/main250processed.table.tsv -c 7 --category-col 2 -o /home/salvocos/work_repos/sonic-manuscript/genome_biology_review_files/round1/plots --mrkr-size 3 --divisor 1000000 --prefix main250proteomes --format svg --format svg --xlab Domain --ylab "Proteome size"

