# This script will generate the data for essential executiont times
# using the script in sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl


##### MMSEQS RUNS #####
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-msens.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-sens.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-default.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-fast.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait


##### DIAMOND RUNS #####
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-msens.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-sens.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-default.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-fast.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
wait


##### BLAST RUNS #####
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-blast-ess-default.extime.datapoint.tsv --output-dir ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/essential-stats
