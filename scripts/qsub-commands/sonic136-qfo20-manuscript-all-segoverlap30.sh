#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic136-qfo20-all-ovrlp30
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@bs.s.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date

 #### MMseqs runs #####

echo $'\n@@@@ sonic136-mmseqs-essentials-most-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-essentials-most-sensitive -p s136-ovrlp30-mmseqs-ess-msens -ot --threads 128 -m most-sensitive -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-complete-most-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-complete-most-sensitive -p s136-ovrlp30-mmseqs-ca-msens -ot --threads 128 -m most-sensitive -op -ca -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-essentials-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-essentials-sensitive -p s136-ovrlp30-mmseqs-ess-sens -ot --threads 128 -m sensitive -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-complete-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-complete-sensitive -p s136-ovrlp30-mmseqs-ca-sens -ot --threads 128 -m sensitive -op -ca -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-essentials-default @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-essentials-default -p s136-ovrlp30-mmseqs-ess-default -ot --threads 128 -m default -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-complete-default @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-complete-default -p s136-ovrlp30-mmseqs-ca-default -ot --threads 128 -m default -op -ca -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-essentials-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-essentials-fast -p s136-ovrlp30-mmseqs-ess-fast -ot --threads 128 -m fast -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-mmseqs-complete-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-mmseqs-complete-fast -p s136-ovrlp30-mmseqs-ca-fast -ot --threads 128 -m fast -op -ca -noidx
wait
echo $'\n'


 ### Diamond runs #####

echo $'\n@@@@ sonic136-dmnd-essentials-most-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-essentials-most-sensitive -p s136-ovrlp30-dmnd-ess-msens -ot --aln-tool diamond --threads 128 -m most-sensitive -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-complete-most-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-complete-most-sensitive -p s136-ovrlp30-dmnd-ca-msens -ot --aln-tool diamond --threads 128 -m most-sensitive -op -ca -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-essentials-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-essentials-sensitive -p s136-ovrlp30-dmnd-ess-sens -ot --aln-tool diamond --threads 128 -m sensitive -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-complete-sensitive @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-complete-sensitive -p s136-ovrlp30-dmnd-ca-sens -ot --aln-tool diamond --threads 128 -m sensitive -op -ca -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-essentials-default @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-essentials-default -p s136-ovrlp30-dmnd-ess-default -ot --aln-tool diamond --threads 128 -m default -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-complete-default @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-complete-default -p s136-ovrlp30-dmnd-ca-default -ot --aln-tool diamond --threads 128 -m default -op -ca -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-essentials-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-essentials-fast -p s136-ovrlp30-dmnd-ess-fast -ot --aln-tool diamond --threads 128 -m fast -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-dmnd-complete-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-dmnd-complete-fast -p s136-ovrlp30-dmnd-ca-fast -ot --aln-tool diamond --threads 128 -m fast -op -ca -noidx
wait
echo $'\n'


 ### BLAST runs #####

echo $'\n@@@@ sonic136-blast-essentials-default @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-blast-essentials-default -p s136-ovrlp30-blast-ess-default -ot --aln-tool blast --threads 128 -m default -op -noidx
wait
echo $'\n'

echo $'\n@@@@ sonic136-blast-complete-default @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/sonic136-qfo20-all/sonic136-blast-complete-default -p s136-ovrlp30-blast-ca-default -ot --aln-tool blast --threads 128 -m default -op -ca -noidx
wait
echo $'\n'

# All runs completed
date