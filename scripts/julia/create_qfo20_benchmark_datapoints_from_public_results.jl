using Base: iscontiguous, Float64
#=
Process JSON result files obtained from the QfO benchmark web-page
https://orthology.benchmarkservice.org/proxy/results/2020/#tab_species-tree-discordance-benchmark
on 20/02/2023

This program processes a single archive (tar.xz) containing all the JSON files singularly download from the web-page above.
=#

using ArgParse: @add_arg_table!, ArgParseSettings, parse_args
using Printf:@printf,@sprintf
using DataStructures: OrderedDict
using JSON
using DataFrames
using CSV
# using Distributed
using Logging


# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Load the module
using ProcessRawQfOBenchDatapoints



"""Parse command line arguments"""
function get_params(args::Vector{String})::Dict{String, Any}
    @debug "get_params :: START" fpath
    # initialize the settings (the description is for the help screen)
    s = ArgParseSettings("Example 3 for argparse.jl: " *
                         "version info, default values, " *
                         "options with types, variable " *
                         "number of arguments.",
                         version = "Version 1.0", # version info
                         add_version = true)      # audo-add version option

    @add_arg_table! s begin
        "raw-datapoints-archive"
            nargs = 1
            arg_type = String
            help = "Archive containing the JSON files publicly available on the QfO benchmark web-site."
            required = true
        "--output-name"
            nargs = '?'
            arg_type = String
            default = "qfo20-public-results.tsv"
            help = "Name of the output datapoint file. "
            required = false
        "--output-dir"
            nargs = '?'
            arg_type = String
            default = "."
            help = "Directory that will contain the output files."
            required = true
        # "--output-tbl"
        #     nargs = '?'
        #     arg_type = String
        #     default = ""
        #     help = "Table with extra information, including sequence count and proteome size"
        #     required = true
        "--cleanup"
            action = :store_true   # this makes it a flag
            help = "Remove temporary files."
        "--debug", "-d"
            action = :store_true   # this makes it a flag
            help = "Show debug information."
        end

    parsed_args = parse_args(args, s) # the result is a Dict{String,Any}end
    return parsed_args
end



"""
Identify the type of test and return a tuple with the required info
"""
function extract_datapoints(jsonPath::String)::Tuple{String, String, String, Dict{String, Tuple{Float64, Float64, Float64, Float64}}}
    @debug "extract_datapoints" jsonPath

    #=
    The return Tuple contain teh following fields
    1::String : Name of the test (eg. STD_Fungi)
    2::String : Metric used in the x-axis (eg. NR_COMPLETED_TREE_SAMPLINGS)
    3::String : Metric used in the y-axis (eg. FRAC_INCORRECT_TREES)
    4::Dict{String, Tuple{Float64, Float64, Float64, Float64}}} : Dictionary with datapoints for each participant
    =#
    
    resInfo::Dict{String, Any} = JSON.parsefile(jsonPath)
    testName::String = ""
    metrics::String = ""
    testName, metrics = split(resInfo["orig_id"], "_agg_", limit=2)
    testName = testName[5:end]
    metricX::String = ""
    metricY::String = ""
    metricX, metricY = split(metrics, "+", limit=2)
    # @show resInfo["orig_id"] testName metricX metricY

    # Contains the datapoints (4 floats) for each partecipant
    # the tuple contains x, y, x_error. and y_error
    datapoints::Dict{String, Tuple{Float64, Float64, Float64, Float64}} = Dict()
    # Extract the datapoints
    resultsDict::Vector{Dict{String, Any}} = resInfo["datalink"]["inline_data"]["challenge_participants"]
    for res in resultsDict
        datapoints[res["tool_id"]] = (res["metric_x"], res["metric_y"], res["stderr_x"], res["stderr_y"])
    end

    return (testName, metricX, metricY, datapoints)

end



"""
Unarchive and process benchmark results files for all participants.
"""
function process_raw_datapoints_for_all_methods(qfoResultsArchivePath::String, outDir::String; cleanup::Bool=false)::String
    @debug "process_raw_datapoints_for_all_methods :: START" qfoResultsArchivePath outDir cleanup
    
    # tmp variables
    tmpDir::String = tmpPath = resLine = ""
    # These directories and files must be in the results directory
    resultDirs::Array{String, 1} = ["EC", "GO", "G_STD2_Eukaryota", "G_STD2_Fungi", "G_STD2_Luca", "G_STD2_Vertebrata", "STD_Bacteria", "STD_Eukaryota", "STD_Fungi", "SwissTrees", "TreeFam-A", "Manifest.json"]
    # resultFiles::Dict{String}
    bname::String = string(rsplit(basename(qfoResultsArchivePath), ".", limit=2)[1])
    tarPath::String = joinpath(outDir, bname)
    resLine::String = ""
    jsonCnt::Int = 0
    # Directory contaoinig the result files in JSON
    jsonDir::String = joinpath(outDir, rsplit(bname, ".", limit=2)[1])
    expectedJsonCnt::Int = 33
    
    # Check of the file is an archive and extract it in the output directory
    if is_archive(qfoResultsArchivePath)[1]
        pigz_decompress(qfoResultsArchivePath, tarPath, keep=true, force=true, errLog=false, threads=1)
        xz_decompress(qfoResultsArchivePath, tarPath; keep=true, force=true, errLog=false, threads=1)
        extract_tar_archive(tarPath, outDir)
        # Remove temporary files
        rm(tarPath, force=true)
    else
        @error "The provided file is not a valid archive:" qfoResultsArchivePath
        exit(-5)
    end

    # Create the output file and write the hdr
    flatDataPointsFile::String = joinpath(outDir, "$(basename(jsonDir)).tsv")
    @show flatDataPointsFile
    ofd::IO = open(flatDataPointsFile, "w")
    write(ofd, "test\tmethod\tx\ty\txerr\tyerr\txlabel\tylabel\n")

    testName::String = ""
    metricX::String = ""
    metricY::String = ""
    datapoints::Dict{String, Tuple{Float64, Float64, Float64, Float64}} = Dict()
    
    # walkdir could be used
    # or just list the json files in the directory using readir
    for f in readdir(jsonDir, sort=true)
        if endswith(f, ".json")
            jsonCnt += 1
            # @show joinpath(jsonDir, f)
            testName, metricX, metricY, datapoints = extract_datapoints(joinpath(jsonDir, f))
            # Extract the results and write in the output table
            @debug testName, metricX, metricY
            for (k, v) in datapoints
                write(ofd, "$(testName)\t$(k)\t$(v[1])\t$(v[2])\t$(v[3])\t$(v[4])\t$(metricX)\t$(metricY)\n")
            end
        end
    end

    close(ofd)

    if jsonCnt != expectedJsonCnt
        @error "The number of JSON files found ($(jsonCnt)) is different than the expected $(expectedJsonCnt)"
        exit(-10)
    end

    # Remove directory with raw files
    if cleanup
        rm(jsonDir, recursive=true, force=true)
    end

    # return the file with datapoints
    return flatDataPointsFile
end



#####  MAIN  #####
args = get_params(ARGS)
# @show(args)


rawJsonResArchive = realpath(args["raw-datapoints-archive"][1])
# metaDataTbl = realpath(args["meta-data"][1])
outDir = abspath(args["output-dir"])
outTblName = args["output-name"]
if length(outTblName) < 1
    @error "The name of the output table must be at least one character!" outTblName
    exit(-5)
end
makedir(outDir)
# outTbl = abspath(args["output-tbl"])
cleanup = args["cleanup"]
debug = args["debug"]

logLevel = Logging.Info
if debug
    logLevel = Logging.Debug
end

mainLogger = Logging.ConsoleLogger(stdout, logLevel)
global_logger(mainLogger)
# @show mainLogger

@info "QfO benchmark datapoints will be processed using the following parameters:" rawJsonResArchive outDir outTblName debug

# Process all the files in the input directory
tmpPath = ""
hasHdr = false
outTbl = joinpath(outDir, outTblName)
# Check that the archive file exists
if !isfile(rawJsonResArchive)
    @error "File not found!" rawJsonResArchive 
end

dataPointsPath::String = process_raw_datapoints_for_all_methods(rawJsonResArchive, outDir, cleanup=cleanup)

# Rename the table file if required
if dataPointsPath != outTbl
    mv(dataPointsPath, outTbl, force=true)
end

@info "The table with the datapoints was successfully created!" outTbl
