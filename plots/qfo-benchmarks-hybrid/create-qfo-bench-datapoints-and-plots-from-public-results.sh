# Generate qfo results datasets
julia /home/salvocos/work_repos/sonic-manuscript/scripts/julia/create_qfo20_benchmark_datapoints_from_public_results.jl ~/work_repos/sonic-manuscript/tables/qfo-benchmarks/sp2-qfo20-webpage-json-files-200223.tar.xz --output-dir ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/ --cleanup
wait
# Generate the plots
python3 /home/salvocos/work_repos/sonic-manuscript/scripts/python/plot_qfo20_benchmark_public_results.py -i ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/datapoints/qfo20-public-results.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-hybrid/ --prefix qfo20-bench-public-results-manuscript --mrkr-size 5 --pareto-script ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl --format svg
