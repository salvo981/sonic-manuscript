# Generate different plots regarding the execution times for essentials
# Using diiferent colors for each category

# MMseqs sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/s136-qfo20-mmseqs-ess-sens.extime.datapoint.tsv -o ./plots_single --hue empire-class --mrkr-size 10 --prefix qfo20-mmseqs-sens-extimes

# MMseqs default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/s136-qfo20-mmseqs-ess-default.extime.datapoint.tsv -o ./plots_single --hue empire-class --mrkr-size 10 --prefix qfo20-mmseqs-default-extimes

# DIAMOND RELATED TABLES

# Diamond sens
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/s136-qfo20-dmnd-ess-sens.extime.datapoint.tsv -o ./plots_single --hue empire-class --mrkr-size 10 --prefix qfo20-dmnd-sens-extimes

# Diamond default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_MAGS.py -i ./datapoints/s136-mags-dmnd-ess-default.extime.datapoint.tsv -o ./plots_single --hue habitat-l1-type --mrkr-size 10 --prefix mags-dmnd-default-extimes
