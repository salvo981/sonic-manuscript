#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic2-qfo20-manuscript-all-SSD
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@k.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date


 ### Diamond runs #####

echo $'\n@@@@ sonic2-dm-hybrid-fast-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-fast -p s2-dm-fast-thr75 -ot --aln-tool diamond --threads 128 -m fast -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-hybrid-fast-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-fast -p s2-dm-fast-thr80 -ot --aln-tool diamond --threads 128 -m fast -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-graph-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-fast -p s2-dm-fast-graph -ot --aln-tool diamond --threads 128 -m fast -op -noidx -sa
wait
echo $'\n'

#### Default ####

echo $'\n@@@@ sonic2-dm-hybrid-def-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-def -p s2-dm-def-thr75 -ot --aln-tool diamond --threads 128 -m default -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-hybrid-def-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-def -p s2-dm-def-thr80 -ot --aln-tool diamond --threads 128 -m default -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-graph-def @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-def -p s2-dm-def-graph -ot --aln-tool diamond --threads 128 -m default -op -noidx -sa
wait
echo $'\n'

#### Sensitive ####

echo $'\n@@@@ sonic2-dm-hybrid-sens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-sens -p s2-dm-sens-thr75 -ot --aln-tool diamond --threads 128 -m sensitive -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-hybrid-sens-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-sens -p s2-dm-sens-thr80 -ot --aln-tool diamond --threads 128 -m sensitive -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-graph-sens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-sens -p s2-dm-sens-graph -ot --aln-tool diamond --threads 128 -m sensitive -op -noidx -sa
wait
echo $'\n'

#### Most-sensitive ####

echo $'\n@@@@ sonic2-dm-hybrid-msens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-msens -p s2-dm-msens-thr75 -ot --aln-tool diamond --threads 128 -m most-sensitive -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-hybrid-msens-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-msens -p s2-dm-msens-thr80 -ot --aln-tool diamond --threads 128 -m most-sensitive -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-dm-graph-msens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-dm-hybrid-msens -p s2-dm-msens-graph -ot --aln-tool diamond --threads 128 -m most-sensitive -op -noidx -sa
wait
echo $'\n'



 #### MMseqs runs #####

#### Fast ####
echo $'\n@@@@ sonic2-mm-hybrid-fast-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-fast -p s2-mm-fast-thr75 -ot --threads 128 -m fast -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-hybrid-fast-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-fast -p s2-mm-fast-thr80 -ot --threads 128 -m fast -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-graph-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-fast -p s2-mm-fast-graph -ot --threads 128 -m fast -op -noidx -sa
wait
echo $'\n'

#### Default ####

echo $'\n@@@@ sonic2-mm-hybrid-def-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-def -p s2-mm-def-thr75 -ot --threads 128 -m default -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-hybrid-def-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-def -p s2-mm-def-thr80 -ot --threads 128 -m default -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-graph-def @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-def -p s2-mm-def-graph -ot --threads 128 -m default -op -noidx -sa
wait
echo $'\n'

#### Sensitive ####

echo $'\n@@@@ sonic2-mm-hybrid-sens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-sens -p s2-mm-sens-thr75 -ot --threads 128 -m sensitive -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-hybrid-sens-thr80 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-sens -p s2-mm-sens-thr80 -ot --threads 128 -m sensitive -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-graph-sens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-sens -p s2-mm-sens-graph -ot --threads 128 -m sensitive -op -noidx -sa
wait
echo $'\n'

#### Most-sensitive ####

echo $'\n@@@@ sonic2-mm-hybrid-msens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-msens -p s2-mm-msens-thr75 -ot --threads 128 -m most-sensitive -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-hybrid-msens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-msens -p s2-mm-msens-thr80 -ot --threads 128 -m most-sensitive -op -noidx --min-arch-merging-cov 0.80
wait
echo $'\n'

echo $'\n@@@@ sonic2-mm-graph-msens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-qfo20-manuscript-all/sonic2-mm-hybrid-msens -p s2-mm-msens-graph -ot --threads 128 -m most-sensitive -op -noidx -sa
wait
echo $'\n'



# All runs completed
date