# Reference 2000 MAGS
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/reference_2000_mags_bacteria-tbl.tsv -c 20 --category-col 15 -o ~/work_repos/sonic-manuscript/plots/datasets/plots --mrkr-size 2 --prefix reference2000mags --format svg --xlab Domain --ylab Proteins

python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/reference_2000_mags_bacteria-tbl.tsv -c 21 --category-col 15 -o ~/work_repos/sonic-manuscript/plots/datasets/plots --mrkr-size 2 --divisor 1000000 --prefix reference2000mags --format svg --xlab Domain --ylab "Proteome size"

# QfO2020
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/qfo2020processed-tbl.tsv -c 6 --category-col 2 -o ~/work_repos/sonic-manuscript/plots/datasets/plots --mrkr-size 3 --prefix qfo20 --format svg --xlab Domain --ylab Proteins

python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/qfo2020processed-tbl.tsv -c 7 --category-col 2 -o ~/work_repos/sonic-manuscript/plots/datasets/plots --mrkr-size 3 --divisor 1000000 --prefix qfo20 --format svg --format svg --xlab Domain --ylab "Proteome size"

# Reference 250 Proteomes
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/main250processed-tbl.tsv -c 6 --category-col 2 -o ~/work_repos/sonic-manuscript/plots/datasets/plots --mrkr-size 3 --prefix main250proteomes --format svg --xlab Domain --ylab Proteins

python3 ~/work_repos/sonic-manuscript/scripts/python/plot_datasets.py -i ~/work_repos/sonic-manuscript/tables/main250processed-tbl.tsv -c 7 --category-col 2 -o ~/work_repos/sonic-manuscript/plots/datasets/plots --mrkr-size 3 --divisor 1000000 --prefix main250proteomes --format svg --format svg --xlab Domain --ylab "Proteome size"

