#!/bin/bash

date

# Paths to tools
# Tool name abbreviatiions:
# sp2 -> SonicParanoid2
# NOTE: SonicParanoid was installed using a python venv, which is loaded when running this bash script
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=reference200proteomes-euka
cpu_cnt=128

# Output directories
output_root=$root\output_200euka/
sp_output_root=$output_root\sp2/
mkdir $sp_output_root
of2_output_root=$output_root\of2/
mkdir $of2_output_root
po6_output_root=$output_root\po6/
mkdir $po6_output_root
broccoli_output_root=$output_root\broccoli/
mkdir $broccoli_output_root

# SonicParanoid2 runs
sp_mode=default
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
sp_extra_prefix=pairs-
sp_extra_params=$'-ot -sm -op'

echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_extra_prefix$sp_run_name_prefix -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_extra_prefix$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_extra_prefix$sp_run_name_prefix.txt

sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_extra_prefix$sp_run_name_prefix -t 6 $sp_extra_params > $sp_output_root\log.$sp_extra_prefix$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_extra_prefix$sp_run_name_prefix.txt
wait

echo $'\n'
date

# SonicParanoid2 (fast)
sp_mode=fast
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_extra_prefix$sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_extra_prefix$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_extra_prefix$sp_run_name_prefix.txt
sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_extra_prefix$sp_run_name_prefix --mode $sp_mode -t 6 $sp_extra_params > $sp_output_root\log.$sp_extra_prefix$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_extra_prefix$sp_run_name_prefix.txt
wait

echo $'\n'
date

# SonicParanoid2 (sensitive)
sp_mode=sensitive
sp_run_name_prefix=sp2-$sp_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' SonicParanoid2 runs '$cpu_cnt$' cpus' $input_set $sp_run_name_prefix $'@@@@\n'
echo sonicparanoid -i $input_root$input_set/ -o $sp_output_root$sp_run_name_prefix -p $sp_extra_prefix$sp_run_name_prefix --mode $sp_mode -t $cpu_cnt $sp_extra_params \> $sp_output_root\log.$sp_extra_prefix$sp_run_name_prefix.txt 2\> $sp_output_root\err.$sp_extra_prefix$sp_run_name_prefix.txt
sonicparanoid -i $input_root$input_set -o $sp_output_root$sp_run_name_prefix -p $sp_extra_prefix$sp_run_name_prefix --mode $sp_mode -t 6 $sp_extra_params > $sp_output_root\log.$sp_extra_prefix$sp_run_name_prefix.txt 2> $sp_output_root\err.$sp_extra_prefix$sp_run_name_prefix.txt
wait

# All runs completed
echo $'\n'
date
