# This script will generate the data for essential executiont times
# using the script in sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-msens-ismb.extime.datapoint.tsv --output-dir essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-sens-ismb.extime.datapoint.tsv --output-dir essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-default-ismb.extime.datapoint.tsv --output-dir essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-mmseqs-essentials-fast-ismb.extime.datapoint.tsv --output-dir essential-stats
wait


##### DIAMOND RUNS #####
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-msens-ismb.extime.datapoint.tsv --output-dir essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-sens-ismb.extime.datapoint.tsv --output-dir essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-default-ismb.extime.datapoint.tsv --output-dir essential-stats
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_stats_and_model_accuracy.jl ~/work_repos/sonic-manuscript/datapoints/ismb2021/sonic136-dmnd-essentials-fast-ismb.extime.datapoint.tsv --output-dir essential-stats
wait
