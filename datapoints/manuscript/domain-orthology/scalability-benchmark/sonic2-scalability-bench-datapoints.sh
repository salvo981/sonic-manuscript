date

# QfO2020
# DBO increamental runs
echo $'\n@@@@ sonic2-domain-orhtology-scalability-benchmark-qfo20 @@@@\n'
julia ~/work_repos/sonic-manuscript/scripts/julia/perform_domain_orthology_scalability_tests.jl /ssd_home/sonic2-scalability-benchmark/sonic138-qfo20-reference-mapped-info/runs/sonic_mmseqs_default_ml075/aux/mapped_input /ssd_home/sonic2-scalability-benchmark/sonic138-qfo20-reference-mapped-info/runs/sonic_mmseqs_default_ml075 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-infer-domain-orthologs.py /ssd_home/sonic2-scalability-benchmark/sonic2-domain-orthology-scalability-qfo20 --points 26 --prefix qfo20 --threads=128 --cleanup
wait
echo $'\n'

date

# Datapoints creation
echo $'\n@@@@ qfo20 DBO-scalability datapoints creation @@@@\n'
julia /home/salvocos/work_repos/sonic-manuscript/scripts/julia/extract_datapoints_from_scalability_benchmark_runs.jl /home/salvocos/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark/sonic2-domain-orthology-scalability-qfo20-clean /home/salvocos/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark qfo20
wait
echo $'\n'

######### QfO END ############

# MAGs
# DBO increamental runs
echo $'\n@@@@ sonic2-domain-orhtology-scalability-benchmark-2000mags @@@@\n'
julia ~/work_repos/sonic-manuscript/scripts/julia/perform_domain_orthology_scalability_tests.jl /ssd_home/sonic2-scalability-benchmark/sonic2-mags-reference-mapping-info/runs/s2-mags-sample-run/aux/mapped_input /ssd_home/sonic2-scalability-benchmark/sonic2-mags-reference-mapping-info/runs/s2-mags-sample-run /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-infer-domain-orthologs.py /ssd_home/sonic2-scalability-benchmark/sonic2-domain-orthology-scalability-mags --points 20 --prefix mags --threads=128 --cleanup
wait
echo $'\n'

# Datapoints creation
echo $'\n@@@@ MAGs DBO-scalability datapoints creation @@@@\n'
julia /home/salvocos/work_repos/sonic-manuscript/scripts/julia/extract_datapoints_from_scalability_benchmark_runs.jl /home/salvocos/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark/sonic2-domain-orthology-scalability-mags-clean /home/salvocos/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/scalability-benchmark mags
wait
echo $'\n'

# All runs completed
date