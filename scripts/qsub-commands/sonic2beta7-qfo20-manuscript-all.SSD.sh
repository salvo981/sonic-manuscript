#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic2-beta7-qfo20-manuscript-all-SSD
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@k.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date


 ### Fast (Diamond) #####

echo $'\n@@@@ sonic2-fast-hybrid @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-beta7-qfo20-manuscript-all/sonic2-fast-hybrid -p s2-fast -m fast -op --threads 128
wait
echo $'\n'

echo $'\n@@@@ sonic2-fast-graph-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-beta7-qfo20-manuscript-all/sonic2-fast-graph -p s2-fast-graph -m fast -op --graph-only --threads 128
wait
echo $'\n'

 ### Default (Diamond) #####

echo $'\n@@@@ sonic2-default-hybrid @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-beta7-qfo20-manuscript-all/sonic2-default-hybrid -p s2-default -m default -op --threads 128
wait
echo $'\n'

echo $'\n@@@@ sonic2-graph @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-beta7-qfo20-manuscript-all/sonic2-default-graph -p s2-default-graph -m default -op --graph-only --threads 128
wait
echo $'\n'

 ### Sensitive (MMseqs) #####

echo $'\n@@@@ sonic2-hybrid-sens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-beta7-qfo20-manuscript-all/sonic2-sens-hybrid -p s2-sens -m sensitive -op --threads 128
wait
echo $'\n'

echo $'\n@@@@ sonic2-graph-sens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qfo2020processed_fixed_xtropicalis -o /ssd_home/sonic2-beta7-qfo20-manuscript-all/sonic2-sens-graph -p s2-sens-graph -m sensitive -op --graph-only --threads 128
wait
echo $'\n'

# All runs completed
date