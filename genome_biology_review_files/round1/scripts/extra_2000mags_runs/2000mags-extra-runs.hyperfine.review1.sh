#!/bin/bash

# This script performs additional runs using the 2,000 MAGs dataset
# The extra runs are the following:
# - OrthoFinder with default settings
# - ProteinOrtho6 with default settings

date

# Tool name abbreviatiions:
# of2 -> OrthoFinder2
# po6 -> ProteinOrtho6
of2_cmd=~/work_repos/OrthoFinder_v254/orthofinder.py
po6_cmd=~/work_repos/proteinortho/proteinortho6.pl
fasttree_cmd=/usr/local/bin/FastTree
root=/ssd_home/sp2-genome-biology-review-runs/

# Input and cpu counts
input_root=$root\input/
input_set=reference_2000_mags
cpu_cnt=128

# Output directories
output_root=$root\output_extra_2000mags_runs/
mkdir $output_root
of2_output_root=$output_root\of2/
mkdir $of2_output_root
po6_output_root=$output_root\po6/
mkdir $po6_output_root

# ProteinOrtho6 run [default]
po6_mode=default # This should use the pseudo bitscores
po6_run_name_prefix=po6-$po6_mode-$input_set-$cpu_cnt\cpus
echo $'\n@@@@'$' ProteinOrtho runs '$cpu_cnt$' cpus' $input_set $po6_run_name_prefix $'@@@@\n'

echo $po6_cmd
echo $po6_output_root
echo $po6_run_name_prefix
# Create and enter the po6 output directory
mkdir $po6_output_root$po6_run_name_prefix
wait
cd $po6_output_root$po6_run_name_prefix
wait

echo hyperfine --style none --runs 1 --export-json $po6_output_root$po6_run_name_prefix.hypefine.json \"$po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* \> $po6_output_root\log.$po6_run_name_prefix.txt 2\&\>1\"
hyperfine --style none --runs 1 --export-json $po6_output_root$po6_run_name_prefix.hypefine.json "$po6_cmd -project=$po6_run_name_prefix -p=diamond -cpus=$cpu_cnt -clean $input_root$input_set/* > $po6_output_root\log.$po6_run_name_prefix.txt 2>&1"
wait


echo $'\n'
date

echo $'of2 requires modification into the system before it can. It seem it does not clode files, and would overcome the maximum number of open files.'

exit -5

# OrthoFinder Runs
# IMPORTANT: OrthoFinder usually crashes becaus eit opens too many files
# https://github.com/davidemms/OrthoFinder/issues/384
# In order to mitigate this issue increase the limits (if possible)
# $prlimit # check the current limits for NOFILE
# Change the Hard and Soft limits for NOFILE
# This should be enough for up to 1k species
# $ulimit -Hn 40000000
# $ulimit -Sn 40000000

of2_mode=default
of2_run_name_prefix=of2-$of2_mode-$input_set-$cpu_cnt\cpus
# OrthoFinder run Default
echo $'\n@@@@'$' OrthoFinder runs '$cpu_cnt$' cpus' $input_set $of2_run_name_prefix $'@@@@\n'
echo hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json \"python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt \> $of2_output_root\log.$of2_run_name_prefix.txt 2\> $of2_output_root\err.$of2_run_name_prefix.txt\"
hyperfine --style none --runs 1 --export-json $of2_output_root$of2_run_name_prefix.hypefine.json "python3 $of2_cmd -f $input_root$input_set -o $of2_output_root$of2_run_name_prefix -n $of2_run_name_prefix -t $cpu_cnt > $of2_output_root\log.$of2_run_name_prefix.txt 2> $of2_output_root\err.$of2_run_name_prefix.txt"
wait

# All runs completed
echo $'\n'
date
