# Generate different plots regarding the execution times for essentials
# Only Default mode is considered for the alignment tools

# MMseqs default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_interproteome_ca_all_default.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-mmseqs-ess-default.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-mmseqs-default-interproteome-extimes

# DIAMOND RELATED TABLES
# Diamond default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_interproteome_ca_all_default.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-dmnd-ess-default.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-dmnd-default-interproteome-extimes

# BLAST default
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_extimes_essentials_interproteome_ca_all_default.py -i ~/work_repos/sonic-manuscript/datapoints/manuscript/essentials/qfo2020/s138-qfo20-blast-ess-default.extime.datapoint.tsv -o ~/work_repos/sonic-manuscript/plots/essentials-extimes/qfo2020/plots_single --hue empire-class --mrkr-size 10 --format svg --prefix qfo20-blast-default-interproteome-extimes