date

# Use 9 features
echo $'\n@@@@ Grid search for HistogramGradientBoosting on 9 features using tr-data from mmseqs-msens @@@@'
python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/hgb_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/histogram-gradient-boosting/ --no-mol-weights --threads 8 --out-prefix hgb.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/histogram-gradient-boosting/logs/log.no-weights.hgb_grid_search_from_raw_tr_data.txt
wait

# Use 12 features, including the weights
# echo $'\n@@@@ Grid search for HistogramGradientBoosting on 9 features using tr-data from mmseqs-msens @@@@'
# python3 ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/scripts/python/hgb_grid_search_from_raw_tr_data.py -r ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/raw-training-data/adaboost_training_data_raw.aln_ex_times_250_proteomes_mmseqs_ca_msens-noidx.128cpus.azur.tsv --labels fastest_pair -o ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/histogram-gradient-boosting/ --threads 8 --out-prefix hgb.12feat.mmseqs_msens &> ~/work_repos/sonic-manuscript/binary-classifier-training/retrained-models/histogram-gradient-boosting/logs/log.hgb_grid_search_12feat_from_raw_tr_data.txt
# wait

# All runs completed
date
