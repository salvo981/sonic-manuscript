date

##### PFamA-seed #####

##### tcov=0.50 #####
# minbits=30; tcov=0.50; minqcov=0.10; threads=128

# 2000 MAGs
echo $'\n@@@@ sonic2-profile-search-seed-2000mags-tcov050-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/2000mags/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/2000mags/runs/2000mags/ --output-dir /ssd_home/sonic2-profile-search-all/2000mags/seed-2000mags-30bits-tcov050-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-seed/pfama.mmseqs --min-bitscore 30 --min-tcov 0.50 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

# qfo20
echo $'\n@@@@ sonic2-profile-search-seed-qfo20-tcov050-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/qfo20/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/qfo20/runs/qfo20/ --output-dir /ssd_home/sonic2-profile-search-all/qfo20/seed-qfo20-30bits-tcov050-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-seed/pfama.mmseqs --min-bitscore 30 --min-tcov 0.50 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

##### tcov=0.75 #####
# minbits=30; tcov=0.75; minqcov=0.10; threads=128

# 2000 MAGs
echo $'\n@@@@ sonic2-profile-search-seed-2000mags-tcov075-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/2000mags/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/2000mags/runs/2000mags/ --output-dir /ssd_home/sonic2-profile-search-all/2000mags/seed-2000mags-30bits-tcov075-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-seed/pfama.mmseqs --min-bitscore 30 --min-tcov 0.75 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

# qfo20
echo $'\n@@@@ sonic2-profile-search-seed-qfo20-tcov075-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/qfo20/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/qfo20/runs/qfo20/ --output-dir /ssd_home/sonic2-profile-search-all/qfo20/seed-qfo20-30bits-tcov075-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-seed/pfama.mmseqs --min-bitscore 30 --min-tcov 0.75 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'


##### PFamA-ful #####

##### tcov=0.50 #####
# minbits=30; tcov=0.50; minqcov=0.00; threads=128

# 2000 MAGs
echo $'\n@@@@ sonic2-profile-search-full-2000mags-tcov050-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/2000mags/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/2000mags/runs/2000mags/ --output-dir /ssd_home/sonic2-profile-search-all/2000mags/full-2000mags-30bits-tcov050-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-full/pfama.mmseqs --min-bitscore 30 --min-tcov 0.50 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

# qfo20
echo $'\n@@@@ sonic2-profile-search-full-qfo20-tcov050-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/qfo20/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/qfo20/runs/qfo20/ --output-dir /ssd_home/sonic2-profile-search-all/qfo20/full-qfo20-30bits-tcov050-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-full/pfama.mmseqs --min-bitscore 30 --min-tcov 0.50 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

##### tcov=0.75 #####
# minbits=30; tcov=0.75; minqcov=0.10; threads=128

# 2000 MAGs
echo $'\n@@@@ sonic2-profile-search-full-2000mags-tcov075-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/2000mags/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/2000mags/runs/2000mags/ --output-dir /ssd_home/sonic2-profile-search-all/2000mags/full-2000mags-30bits-tcov075-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-full/pfama.mmseqs --min-bitscore 30 --min-tcov 0.75 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

# qfo20
echo $'\n@@@@ sonic2-profile-search-full-qfo20-tcov075-minqcov0 @@@@\n'
python3 /home/salvocos/work_repos/sonicparanoid2/sonicparanoid/test-dev-profile_search.py --input-dir /ssd_home/sonic2-profile-search-input/mapped-input-datasets/qfo20/ --run-dir /ssd_home/sonic2-profile-search-input/run-dirs/qfo20/runs/qfo20/ --output-dir /ssd_home/sonic2-profile-search-all/qfo20/full-qfo20-30bits-tcov075-minqcov0 -p /ssd_home/sonic2-profile-search-input/pfam-dbs/pfam-profile-db-full/pfama.mmseqs --min-bitscore 30 --min-tcov 0.75 --min-total-query-cov 0.0 --threads 128
wait
echo $'\n'

# All runs completed
date