'''
Generate plots of QfO benchmark results.
The input is a table genetated using the script under scripts/julia/create_qfo_benchmark_datapoints.jl
'''

from __future__ import annotations

import os
import sys
# from typing import TextIO
import logging
import numpy as np
import pandas as pd
import itertools
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Arial']
rcParams['font.size'] = 9
rcParams['font.weight'] = 'normal'
rcParams['lines.markersize'] = 3
#'weight' : 'bold',
import matplotlib as plt
import matplotlib.pyplot as pyplt
# import oapackage # Use to compute the pareto set of points
import subprocess



########### FUNCTIONS ############
def get_params():
    """General test script."""
    import argparse
    parser_usage = "\nProvide an input test file.\n"
    parser = argparse.ArgumentParser(description="plots QfO benchmark results.", usage=parser_usage)
    #start adding the command line options
    parser.add_argument("-i", "--in-tbl", type=str, required=True, help="TSV file with datapoints from QfO benchmark results\n", default=None)
    parser.add_argument("-o", "--output-directory", type=str, required=False, help="Output directory.", default=os.getcwd())
    parser.add_argument("-p", "--prefix", type=str, required=False, help="Prefix for output plots.", default="my_plot")
    parser.add_argument("-f", "--format", required=True, help="Format of output file.", choices=["png", "jpg", "pdf", "svg"], default="svg")
    parser.add_argument("--hue", type=str, required=False, help="Column containing categorical information.", default="")
    parser.add_argument("--multi-col-name", type=str, required=False, help="Column name used to create multiple plots.", default="")
    parser.add_argument("--mrkr-size", type=int, required=False, help="Integer value for the marker size in plots.", default=3)
    parser.add_argument("--pareto-script", type=str, required=False, help="Path to the script to compute the pareto frontier.", default="")
    parser.add_argument("-d", "--debug", required=False, help="Output debug information.\n", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



def compute_pareto_julia(script: str, inputPointsPath: str, outDir: str, dataType: str = "top-double-double") -> str:
    '''
    Compute pareto front calling a julia program
    '''
    # Example of execution of the script
    # julia ~/work_repos/sonic-manuscript/scripts/julia/create_pareto_frontier.jl plots/tmp.x.y.tsv --output-dir plots/ -d
    validDataTypes: list[str] = ["top-int-double", "top-double-double", "bottom-int-double", "bottom-double-double"]
    if not dataType in validDataTypes:
        sys.exit(f"\nERROR: not valid data type {dataType}")

    # The file that should been created is named 'pareto.x.y.tsv'
    outPath: str = os.path.join(outDir, "pareto.x.y.tsv")
    # Remove the previous file if it does already exists
    if os.path.isfile(outPath):
        os.remove(outPath)
        print("\nINFO: previous pareto file removed.")

    rOut = subprocess.run(f"julia {script} {inputPointsPath} --output-dir {outDir} --data-type {dataType} -d", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    cmdOut:str = rOut.stdout.decode().rstrip()
    print(cmdOut)

    # Through an error if the file was not created
    if not os.path.isfile(outPath):
        logging.error("The pareto file was not created!")
        sys.exit(-8)
    # Return the path to the pareto front table
    return outPath



def map_competitor_method(methodName: str) -> tuple[str, str, str, str]:
    """Assign a label, color and marker shape to each method"""

    # Participants in the QfO2020-extended benchmark
    qfo2020methods: list[str] = ["Domainoid+", "Ensembl_Compara", "Hieranoid_2", "InParanoid_Xenfix", "OMA_GETHOGs", "OMA_Groups", "OMA_Pairs", "OrthoFinder_MSA_v2.5.2", "OrthoInspector 3", "OrthoMCL", "PANTHER_16_all", "PANTHER__16__LDO__only", "sonicparanoid", "sonicparanoid-fast", "sonicparanoid-sens", "sonicparanoid-mostsensitive"]

    if not methodName in qfo2020methods:
        sys.stderr.write(f"\nERROR: {methodName} is not a valid participant name for the QfO benchmark.")
        sys.exit(-7)

    #set the shape and color
    nlabel: str = ""
    color: str = ""
    shape: str = ""
    fillStyle: str = "none"
    colIdx: int = 0 # index of the color in the list colors
    # For this kind of Plot 4 Colors are abough
    # https://matplotlib.org/stable/gallery/color/named_colors.html
    # NOTE: tab:blue, tab:orange, tab:olive, black, tab:pink are reserved for SonicParanoid runs
    # validColors: list[str] = ["tab:blue", "tab:orange", "tab:olive", "black", "tab:pink"]
    validColors: list[str] = ["tab:brown", "tab:gray", "tab:red", "cyan"]
    colCnt: int = len(validColors)

    # TODO: use match case when using Python > 3.9
    if methodName == "Domainoid+":
        nlabel = "Domainoid+"
        shape = "d"
    elif methodName == "Ensembl_Compara":
        nlabel = "Ensembl Compara"
        shape = "<"
    elif methodName == "Hieranoid_2":
        nlabel = "Hieranoid 2"
        shape = "*"
    elif methodName == "InParanoid_Xenfix":
        nlabel = "InParanoid"
        shape = ">"
    elif methodName == "OMA_GETHOGs":
        nlabel = "OMA GETHOGs"
        shape = "^"
    elif methodName == "OMA_Groups":
        nlabel = "OMA Groups"
        shape = "^"
    elif methodName == "OMA_Pairs":
        nlabel = "OMA Pairs"
        shape = "^"
    elif methodName == "OrthoFinder_MSA_v2.5.2":
        nlabel = "OrthoFinder (MSA)"
        shape = "v"
    elif methodName == "OrthoInspector 3":
        nlabel = methodName
        shape = "p"
    elif methodName == "OrthoMCL":
        nlabel = methodName
        shape = "H"
    elif methodName == "PANTHER_16_all":
        nlabel = "PANTHER 16 (all)"
        shape = "X"
    elif methodName == "PANTHER__16__LDO__only":
        nlabel = "PANTHER 16 (LDO)"
        shape = "X"
    elif methodName == "sonicparanoid-fast":
        nlabel = "SonicParanoid 1.3 (fast)"
        shape = "P"
    elif methodName == "sonicparanoid":
        nlabel = "SonicParanoid 1.3"
        shape = "P"
    elif methodName == "sonicparanoid-sens":
        nlabel = "SonicParanoid 1.3 (sens)"
        shape = "P"
    elif methodName == "sonicparanoid-mostsensitive":
        nlabel = "SonicParanoid 1.3 (msens)"
        shape = "P"

    # Set the color based on the position of the method in the list
    color = validColors[qfo2020methods.index(methodName) % colCnt]
    fillStyle = "full"

    # Return the tuple with the results
    return(nlabel, color, shape, fillStyle)



def map_method_labels_sonicparanoid_only(run_type: str, aligner: str, sensitivity: str) -> tuple[str, str, str, str]:
    """Assign a label, color and marker shape to each method"""
    # Partecipant IDs for sonicpranoid have the following format
    # s136-dmnd-ca-default
    # run_type: Complete | Essential 
    # aligner: MMseqs | Diamond 
    # sensitivity: fast | default | sens | msens

    #set the shape and color
    nlabel: str = ""
    color: str = ""
    shape: str = ""
    fillStyle: str = "none"
    # For this kind of Plot 4 Colors are abough
    # https://matplotlib.org/stable/gallery/color/named_colors.html
    validColors: list[str] = ["tab:blue", "tab:orange", "tab:olive", "black", "tab:pink"]

    # Colors should be expressed in exadecimal
    if aligner == "Diamond":
        shape = "D" # Diamond shape
    elif aligner == "MMseqs":
        shape = "o"
    elif aligner == "BLAST":
        shape = "s"
    else:
        sys.stderr.write(f"\nERROR: unknonw alignment tool ({aligner})!")
        sys.exit(-5)
    # set the color based on the sensitivity
    if sensitivity == "msens":
        # color = "#003300"
        color = "black"
    elif sensitivity == "sens":
        # color = "#99cc00"
        color = "tab:olive"
    if sensitivity == "def":
        color = "orange"
        # color = "#ffcc00"
    elif sensitivity == "fast":
        color = "tab:blue"
        # color = "#66ccff"

    # Set filling style
    if run_type == "Hybrid":
        fillStyle = "full"
    else:
        fillStyle = "none"

    # Return the tuple with the results
    return(nlabel, color, shape, fillStyle)



def plot_qfo_ec_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    """
    Load df and generate plots.
    """
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "EC"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "EC"')
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    ecTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #ecTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    ecTest.set_xlabel("Recall\n(number of orthologous relationships)")
    ecTest.set_ylabel("Precision\n(average Schlicker similarity)")
    ecTest.set_title("Enzyme classification test")
    ecTest.spines["right"].set_visible(False)
    ecTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    ecTest.yaxis.set_ticks_position("left")
    ecTest.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        # Use a different mapping function depending on the competitor
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
            # methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)

        print(row)
        print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        print(methodLabel, mycolor, mrkShape, fillingStyle)
        ecTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    # Set limits and legend
    #ecTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    ecTest.set_xlim(80000, 320001)
    start, end = ecTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    # stepSizeX: int = 40000
    # ecTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # print(f"Step size X:\t{stepSizeX:.4f}")

    # Set Y limits
    ecTest.set_ylim(0.80, 0.9801)
    start, end = ecTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    ecTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size Y:\t{stepSizeY:.4f}")

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        ecTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #ecTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
        print(tmpOutPath)
        # plt.savefig(tmpOutPath, dpi=160)
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_go_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "GO"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "GO"')
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    goTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #goTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    goTest.set_xlabel("Recall\n(number of orthologous relationships)")
    goTest.set_ylabel("Precision\n(average Schlicker similarity)")
    goTest.set_title("Gene ontology conservation test")
    goTest.spines["right"].set_visible(False)
    goTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    goTest.yaxis.set_ticks_position("left")
    goTest.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        # Use a different mapping function depending on the competitor
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
            # methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)

        # print(row.sensitivity, row.run_type, row.aligner, fillingStyle)
        goTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #goTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    goTest.set_xlim(50000, 400001)
    start, end = goTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    # stepSizeX: int = 15000
    # goTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    # print(f"Step size for X:\t{stepSizeX:.4f}")
    # Set Y limits
    goTest.set_ylim(0.4000, 0.5000)
    start, end = goTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.005
    goTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        goTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #goTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_treefam_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "TreeFam-A"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "TreeFam-A"')

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    treefamTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #treefamTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    treefamTest.set_xlabel("Recall\n(true positive rate)")
    treefamTest.set_ylabel("Precision\n(positive predictive value)")
    # treefamTest.set_title("Comparison with TreeFam-A reference genes trees")
    treefamTest.set_title("TreeFam-A")
    treefamTest.spines["right"].set_visible(False)
    treefamTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    treefamTest.yaxis.set_ticks_position("left")
    treefamTest.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        treefamTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    treefamTest.set_xlim(0.35, 0.70)
    start, end = treefamTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeX: float = 0.025
    treefamTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''

    # Set Y limits
    treefamTest.set_ylim(0.800, 0.9751)
    start, end = treefamTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.025
    treefamTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        treefamTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #treefamTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_swisstree_results(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "SwissTrees"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"
    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "SwissTrees"')

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    swisstreeTest = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #swisstreeTest.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    swisstreeTest.set_xlabel("Recall\n(true positive rate)")
    swisstreeTest.set_ylabel("Precision\n(positive predictive value)")
    # swisstreeTest.set_title("Comparison with SwissTree reference genes trees")
    swisstreeTest.set_title("SwissTree")
    swisstreeTest.spines["right"].set_visible(False)
    swisstreeTest.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    swisstreeTest.yaxis.set_ticks_position("left")
    swisstreeTest.xaxis.set_ticks_position("bottom")

    # Set the colors and shapes
    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        swisstreeTest.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #swisstreeTest.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    swisstreeTest.set_xlim(0.46, 0.8201)
    start, end = swisstreeTest.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 0.04
    swisstreeTest.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    swisstreeTest.set_ylim(0.82, 0.98)
    start, end = swisstreeTest.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.020
    swisstreeTest.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="top-double-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        swisstreeTest.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        # swisstreeTest.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_luca_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")

    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Luca"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Luca" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdLuca1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdLuca1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdLuca1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdLuca1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdLuca1.set_title("Generalized Species Tree Discordance (LUCA)")
    gstdLuca1.set_title("Last Universal Common Ancestor (LUCA)")
    gstdLuca1.spines["right"].set_visible(False)
    gstdLuca1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdLuca1.yaxis.set_ticks_position("left")
    gstdLuca1.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        gstdLuca1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdLuca1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdLuca1.set_xlim(0, 3501)
    start, end = gstdLuca1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 500
    gstdLuca1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdLuca1.set_ylim(0.200, 0.4201)
    start, end = gstdLuca1.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    gstdLuca1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdLuca1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdLuca1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_vertebrata_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")

    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Vertebrata"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Vertebrata" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True

    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdVertebrata1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdVertebrata1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdVertebrata1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdVertebrata1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdVertebrata1.set_title("Generalized Species Tree Discordance (Vertebrata)")
    gstdVertebrata1.set_title("Vertebrata")
    gstdVertebrata1.spines["right"].set_visible(False)
    gstdVertebrata1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdVertebrata1.yaxis.set_ticks_position("left")
    gstdVertebrata1.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        gstdVertebrata1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdVertebrata1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdVertebrata1.set_xlim(12000, 32001)
    start, end = gstdVertebrata1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 5000
    gstdVertebrata1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdVertebrata1.set_ylim(0.20, 0.5001)
    start, end = gstdVertebrata1.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.05
    gstdVertebrata1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdVertebrata1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdVertebrata1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_eukaryota_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")

    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Eukaryota"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Eukaryota" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)

    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdEukaryota1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdEukaryota1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdEukaryota1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdEukaryota1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdEukaryota1.set_title("Generalized Species Tree Discordance (Eukaryota)")
    gstdEukaryota1.set_title("Eukaryota")
    gstdEukaryota1.spines["right"].set_visible(False)
    gstdEukaryota1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdEukaryota1.yaxis.set_ticks_position("left")
    gstdEukaryota1.xaxis.set_ticks_position("bottom")

    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        gstdEukaryota1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    gstdEukaryota1.set_xlim(800, 7201)
    start, end = gstdEukaryota1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 800
    gstdEukaryota1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdEukaryota1.set_ylim(0.18, 0.3601)
    start, end = gstdEukaryota1.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.02
    gstdEukaryota1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''
    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdEukaryota1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdEukaryota1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_gstd_fungi_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "G_STD2_Fungi"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "G_STD2_Fungi" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    gstdFungi1 = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #gstdFungi1.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    gstdFungi1.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    gstdFungi1.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # gstdFungi1.set_title("Generalized Species Tree Discordance (Fungi)")
    gstdFungi1.set_title("Fungi")
    gstdFungi1.spines["right"].set_visible(False)
    gstdFungi1.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    gstdFungi1.yaxis.set_ticks_position("left")
    gstdFungi1.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        gstdFungi1.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")
    # Set limits and legend
    #gstdFungi1.legend(numpoints=1, loc='lower left', frameon=False)

    gstdFungi1.set_xlim(2500, 15001)
    start, end = gstdFungi1.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 2500
    gstdFungi1.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    gstdFungi1.set_ylim(0.20, 0.32)
    start, end = gstdFungi1.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.01
    gstdFungi1.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        gstdFungi1.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #gstdFungi1.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_std_eukaryota_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "STD_Eukaryota"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "STD_Eukaryota" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    stdEukaryota = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdEukaryota.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdEukaryota.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdEukaryota.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdEukaryota.set_title("Species Tree Discordance (Eukaryota)")
    stdEukaryota.set_title("Eukaryota")
    stdEukaryota.spines["right"].set_visible(False)
    stdEukaryota.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdEukaryota.yaxis.set_ticks_position("left")
    stdEukaryota.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        stdEukaryota.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")

    # Set limits and legend
    #stdEukaryota.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    stdEukaryota.set_xlim(7000, 15001)
    start, end = stdEukaryota.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 2000
    stdEukaryota.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''
    '''

    # Set Y limits
    stdEukaryota.set_ylim(0.0200, 0.28001)
    start, end = stdEukaryota.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.020
    stdEukaryota.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        stdEukaryota.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #stdEukaryota.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_std_fungi_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "STD_Fungi"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "STD_Fungi" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    stdFungi = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdFungi.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdFungi.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdFungi.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdFungi.set_title("Species Tree Discordance (Fungi)")
    stdFungi.set_title("Fungi")
    stdFungi.spines["right"].set_visible(False)
    stdFungi.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdFungi.yaxis.set_ticks_position("left")
    stdFungi.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        stdFungi.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")

    # Set X limits
    stdFungi.set_xlim(4000, 12000)
    start, end = stdFungi.get_xlim()
    '''
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeX: int = 750
    stdFungi.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''

    # Set Y limits
    stdFungi.set_ylim(0.28, 0.42)
    start, end = stdFungi.get_ylim()
    print(f"Manually set Y limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeY: float = 0.01
    stdFungi.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        stdFungi.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #stdFungi.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def plot_qfo_std_bacteria_complete_vs_rf(df, plotsDir: str, prefix: str, hueColName: str, paretoScript: str, markerSize: int = 3, xLimits: tuple[int, int] = (0, 100), yLimits: tuple[int, int]=(0, 100), hideXlab: bool=False, hideYlab: bool=False, fmt: str="svg"):
    logging.info(f"Records: {df.shape[0]}")
    print(f"Plots dir:\t{plotsDir}")
    print(f"Prefix:\t{prefix}")
    print(f"Pareto script:\t{paretoScript}")
    print(f"markerSize:\t{markerSize}")
    print(f"X limits:\t{xLimits}")
    print(f"Y limits:\t{yLimits}")
    print(f"Format:\t{fmt}")

    lncnt: int = 0
    # Set temp variables
    testName: str = "STD_Bacteria"
    xMetric: str = "NR_COMPLETED_TREE_SAMPLINGS"
    yMetric: str = "RF_DISTANCE"
    methodName: str = ""
    methodLabel: str = ""
    mycolor: str = ""
    mrkShape: str = ""
    # ACCEPTS [‘full’ | ‘left’ | ‘right’ | ‘bottom’ | ‘top’ | ‘none’]
    fillingStyle: str = "none"

    # Filter daframes based on the QfO test
    # tmpDf:pd.DataFrame = df.query('test == "EC" & Age < 40 & JOB.str.startswith("C").values')
    tmpDf:pd.DataFrame = df.query('test == "STD_Bacteria" & xlabel == "NR_COMPLETED_TREE_SAMPLINGS" & ylabel == "RF_DISTANCE"')
    print(tmpDf)
    # Set output file name
    tmpOutPath: str = ""
    if os.path.isfile(paretoScript):
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.pareto.{fmt}")
    else:
        tmpOutPath = os.path.join(plotsDir, f"{prefix}.{testName.lower()}.{fmt}")
    print(tmpOutPath)
    myLegend = True
    #load datapoints Enzyme Conservation
    fig = pyplt.figure(figsize=(2.7, 1.8))
    stdBacteria = fig.add_axes([0.1, 0.1, 1.0, 1.0], adjustable="box") # main axes
    #set the font size
    #stdBacteria.tick_params(axis="both", which="both", labelsize=10)
    #axes lables and title
    stdBacteria.set_xlabel("Recall\n(number of completed tree samples out of 50,000 trials)")
    stdBacteria.set_ylabel("Average tree error\n(mean Robinson-Foulds\ndistance from reference tree)")
    # stdBacteria.set_title("Species Tree Discordance (Bacteria)")
    stdBacteria.set_title("Bacteria")
    stdBacteria.spines["right"].set_visible(False)
    stdBacteria.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    stdBacteria.yaxis.set_ticks_position("left")
    stdBacteria.xaxis.set_ticks_position("bottom")
    # Plot points
    for row in tmpDf.itertuples():
        lncnt += 1
        methodName = row.method
        if methodName.startswith("s2-"): # SonicParanoid2 run
            methodLabel, mycolor, mrkShape, fillingStyle = map_method_labels_sonicparanoid_only(row.run_type, row.aligner, row.sensitivity)
        elif methodName.startswith("sonic"): # SonicParanoid run
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        else:
            methodLabel, mycolor, mrkShape, fillingStyle = map_competitor_method(methodName=methodName)
        stdBacteria.errorbar(x=float(row.x), y=float(row.y), yerr=float(row.yerr), fmt=mrkShape, fillstyle=fillingStyle, elinewidth=0.75, mew=0.4, ecolor=mycolor, markerfacecolor=mycolor, markeredgecolor=mycolor, markersize=markerSize, label=methodLabel, capsize=2, capthick=1)

    # print max and min for x and y
    print("\nMIN and MAX values:")
    print(f"min/max X:\t{min(tmpDf['x']):.4f}\t{max(tmpDf['x']):.4f}")
    print(f"min/max Y:\t{min(tmpDf['y']):.4f}\t{max(tmpDf['y']):.4f}")
    print("####################\n")

    # Set limits and legend
    #stdBacteria.legend(numpoints=1, loc='lower left', frameon=False)

    # Set X limits
    stdBacteria.set_xlim(400, 1800)
    start, end = stdBacteria.get_xlim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    '''
    stepSizeX: int = 200
    stdBacteria.xaxis.set_ticks(np.arange(start, end, stepSizeX))
    print(f"Step size for X:\t{stepSizeX:.4f}")
    '''

    # Set Y limits
    stdBacteria.set_ylim(0.47, 0.7201)
    start, end = stdBacteria.get_ylim()
    print(f"Manually set X limits [Start/End]:\t{start:.4f}\t{end:.4f}")
    stepSizeY: float = 0.05
    stdBacteria.yaxis.set_ticks(np.arange(start, end, stepSizeY))
    print(f"Step size for Y:\t{stepSizeY:.4f}")
    '''
    '''

    # Compute and plot the pareto frontier if the script is provided
    if os.path.isfile(paretoScript):
        # Store x, y columns in a temporary file
        tmpDfPath: str = os.path.join(plotsDir, f"tmp.x.y.tsv")
        tmpDf.to_csv(tmpDfPath, index=False, sep="\t", columns=["x", "y"])
        # Execute the Julia script to compute the pareto frontier
        tblPareto: str = compute_pareto_julia(paretoScript, tmpDfPath, plotsDir, dataType="bottom-int-double")
        print(tblPareto)
        #plot the pareto line
        # tblPareto = '%sec.pareto.dat'%dataPointsDir
        dfPareto = pd.read_csv(tblPareto, sep="\t")
        print(dfPareto)
        stdBacteria.plot(dfPareto["x"], dfPareto["y"], color='black', linestyle=':', ms=2, label='Pareto frontier')
        #stdBacteria.legend(numpoints=1, loc='lower left', frameon=False)
        #set limits
    fig.savefig(tmpOutPath, transparent=False, format=fmt, dpi=300, bbox_inches='tight')



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



########### MAIN ############
def main():
    #Get the parameters
    args = get_params()[0]
    debug: bool = args.debug
    inTbl = os.path.realpath(args.in_tbl)
    outDir = os.path.realpath(args.output_directory)
    paretoScript = os.path.abspath(args.pareto_script)
    # methods = args.methods
    prefix: str = args.prefix
    plotsDir: str = os.path.join(outDir, "plots")
    outFmt: str = args.format
    hueCol: int = args.hue
    multicolVar: int = args.multi_col_name
    markerSize: int = args.mrkr_size
    # create output directories
    makedir(outDir)
    makedir(plotsDir)

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    # Show some info
    logging.info(f"Input table: {inTbl}")
    logging.info(f"Output directory: {outDir}")
    logging.info(f"Plots directory: {plotsDir}")
    logging.info(f"Output prefix:\t{prefix}")
    logging.info(f"Output format:\t{outFmt}")
    logging.info(f"Pareto script:\t{paretoScript}")

    # Read dataframe
    # The hdr of datapoint file have the following format:
    # method test run_type aligner sensitivity x y xerr yerr xlabel ylabel
    df = pd.read_csv(inTbl, sep="\t", dtype= {"method": str, "test": str, "run_type": str, "aligner": str, "sensitivity": str, "x": np.float32, "y": np.float32, "xerr": np.float32, "yerr": np.float32, "xlabel": str, "ylabel": str}, engine="c")

    # print(df.dtypes)
    # Set a custom color palette
    # Use this when showing the prediction errors
    # myPalette = ["#FF0B04", "#4374B3", "#000000"]

    # Show or hide X and Y labels
    hideXlab = True
    hideYlab = True

    '''
    '''
    # sys.exit("DEBUG :: plot_qfo_benchmark_results_sonic2_vs_all")
    # Plot benchmark points
    # EC Test
    plot_qfo_ec_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GO Test
    plot_qfo_go_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # TreeFam Test
    plot_qfo_treefam_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # SwissTree Test
    plot_qfo_swisstree_results(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    ### GSTD tests ###
    # GSTD Luca Test
    plot_qfo_gstd_luca_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Vertebrate Test
    plot_qfo_gstd_vertebrata_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Eukaryota Test
    plot_qfo_gstd_eukaryota_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # GSTD Fungi Test
    plot_qfo_gstd_fungi_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    ### STD tests ###
    # STD Eukaryota Test
    plot_qfo_std_eukaryota_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # STD Fungi Test
    plot_qfo_std_fungi_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)
    # STD Bacteria Test
    plot_qfo_std_bacteria_complete_vs_rf(df, plotsDir, prefix, hueColName="", paretoScript=paretoScript, markerSize=markerSize, xLimits=(0, 100), yLimits=(0, 100), hideXlab=False, hideYlab=False, fmt=outFmt)


if __name__ == "__main__":
    main()
