# Aggregrate
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo20-webpage-method-ranking/qfo20-26-methods-aggregate-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo20-ranking-aggregate --mrkr-size 8 --short-names --best-n 8 --seaborn-height 1.90 --seaborn-aspect 1.00 --qfo-competition 2020 --format svg
wait

# k-means
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo20-webpage-method-ranking/qfo20-26-methods-kmeans-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo20-ranking-kmeans --mrkr-size 8 --short-names --best-n 8 --seaborn-height 1.90 --seaborn-aspect 1.00 --qfo-competition 2020 --format svg
wait

# square-quartile
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo20-webpage-method-ranking/qfo20-26-methods-square-quartile-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo20-ranking-square-quartile --mrkr-size 8 --short-names --best-n 8 --seaborn-height 1.90 --seaborn-aspect 1.00 --qfo-competition 2020 --format svg
wait

# diagonal-quartile
python3 ~/work_repos/sonic-manuscript/scripts/python/plot_qfo_benchmark_ranking.py --in-tbl ~/work_repos/sonic-manuscript/datapoints/manuscript/domain-orthology/qfo20-webpage-method-ranking/qfo20-26-methods-diagonale-quartile-ranking.tsv -o ~/work_repos/sonic-manuscript/plots/qfo-benchmarks-ranking/ --prefix qfo20-ranking-diagonal-quartile --mrkr-size 8 --short-names --best-n 8 --seaborn-height 1.90 --seaborn-aspect 1.00 --qfo-competition 2020 --format svg
wait