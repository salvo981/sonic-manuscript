'''
Train HistGradientBoostingClassifier model optimized using GridSearch.
The files with training samples, and labels will be generated
from a table with all the training samples.
'''

import os
import sys
import pickle
import logging
import numpy as np
import pandas as pd

from sklearn.preprocessing import normalize
from sklearn.preprocessing import LabelBinarizer
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
# Use this instead of feature importance
from sklearn.inspection import permutation_importance



def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    import argparse
    parser = argparse.ArgumentParser(description="Train_adaboost",  usage="%(prog)s --raw-tr-set <TRAINING_SET> --labels <LABELS> -o <OUTPUT_DIRECTORY> --cv-steps <K-FOLDS> --threads <CPUS>", prog="train_adaboost_grid_search.py")

    # Mandatory arguments
    parser.add_argument("-rawtr", "--raw-tr-set", type=str, required=True, help="Text file containing the training samples, labels. This tables files are generated from the script generate_raw_tr_data_with_negative_values.py.", default=None)
    parser.add_argument("-lbs", "--labels", type=str, required=True, help="Column containing the labels.", default="fastest_pair")
    parser.add_argument("-o", "--output-dir", type=str, required=True, help="The directory in which the generated models, training samples and results will be stored.", default=os.getcwd())

    # General options
    parser.add_argument("-cv", "--cv-steps", type=int, required=False, help="Number of CV steps to be performd. Default=10", default=10)
    parser.add_argument("--dummy", required=False, help="Generate dummy features from categorical columns using one-hot encoding.", default=False, action="store_true")
    parser.add_argument("--no-mol-weights", required=False, help="Do not include molecular weights among the training features.", default=False, action="store_true")
    parser.add_argument("--out-prefix", type=str, required=False, help="Prefix used in the output files.", default="")
    parser.add_argument("-t", "--threads", type=int, required=False, help="Number of parallel 1-CPU jobs to be used. Default=4", default=4)
    parser.add_argument("-d", "--debug", required=False, help="Output debug information. (WARNING: extremely verbose)", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def extract_samples_and_labels(rawTrPath: str, labelsCol: str, outDir: str, noMolWeights: bool = False, useDummyFeatures: bool = False) -> tuple[str, str]:
    """
    Given a raw table with training samples generated using the script generate_raw_tr_data_with_negative_values.py.
    Generate the files with the final training set, and labels.
    """
    logging.debug(f"extract_samples_and_labels :: START")
    logging.debug(f"Raw training data: {rawTrPath}")
    logging.debug(f"Column with labels:\t{labelsCol}")
    logging.debug(f"Directory with trainning data: {outDir}")
    logging.debug(f"Exclude molecular weights from training set:\t{noMolWeights}")
    logging.debug(f"Create dummy features using one-hot encoding:\t{useDummyFeatures}")

    bnameTrRaw: str = os.path.basename(rawTrPath)

    # The file with samples contains the following columns
    # pair extime extime_diff_folds seq_cnt_a seq_cnt_b seq_cnt_diff_folds_b_gt_a proteome_size_a proteome_size_b prot_size_diff_folds_b_gt_a avg_seq_len_a avg_seq_len_b avg_seq_len_diff_folds_b_gt_a mol_mass_a mol_mass_b mol_mass_diff_folds_b_gt_a folds_combination_class fastest_pair

    # Columns to be used for training
    trCols: List[str] = ["seq_cnt_a", "seq_cnt_b", "seq_cnt_diff_folds_b_gt_a", "proteome_size_a", "proteome_size_b", "prot_size_diff_folds_b_gt_a", "avg_seq_len_a", "avg_seq_len_b", "avg_seq_len_diff_folds_b_gt_a", "mol_mass_a", "mol_mass_b", "mol_mass_diff_folds_b_gt_a", "folds_combination_class"]

    # load training sets
    trDf = pd.read_csv(rawTrPath, sep="\t", usecols=trCols)
    # print(trDf.columns)
    # Print data types
    # print(trDf.dtypes)
    # print(trDf.describe())
    
    if useDummyFeatures:
        # creating an object of the LabelBinarizer
        # Column with folds_combination_class to columns with binary values use one-hot encoding
        label_binarizer = LabelBinarizer()
        
        # fitting the column 
        # folds_combination_class to LabelBinarizer
        label_binarizer_output = label_binarizer.fit_transform( trDf['folds_combination_class'])
        
        # creating a data frame from the object
        trDfDummy = pd.DataFrame(label_binarizer_output, columns = label_binarizer.classes_)

        print(trDfDummy.describe())
        print(trDfDummy.dtypes)

        # add the new labels to the original dataframe
        # trDf = pd.concat([trDf, trDfDummy], ignore_index=True, axis=1)
        trDf = pd.concat([trDf, trDfDummy], ignore_index=False, axis=1)
        del trDfDummy

    # remove the column with the combination class
    del trDf["folds_combination_class"]

    # Set the output table name
    outTblName: str = f"training_smpls.{bnameTrRaw}"
    # Remove molecular weights if required
    if noMolWeights:
        outTblName = f"training_smpls.no_mol_weights.{bnameTrRaw}"
        del trDf["mol_mass_a"]
        del trDf["mol_mass_b"]
        del trDf["mol_mass_diff_folds_b_gt_a"]

    print(trDf.dtypes)
    print(trDf.columns)
    print(trDf.describe())

    outTrTbl: str = os.path.join(outDir, outTblName)
    # Write the table with the trainig data
    trDf.to_csv(outTrTbl, sep="\t", index=False)

    # Load the labels
    labelsDf = pd.read_csv(rawTrPath, sep="\t", usecols=[labelsCol])
    outLabels: str = os.path.join(outDir, f"labels.{bnameTrRaw}")
    labelsDf.to_csv(outLabels, sep="\t", index=False)

    # Return the paths of the training data and label tables
    return (outTrTbl, outLabels)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    # check the file or dir does not already exist
    if os.path.isfile(path):
        sys.stderr.write(f"\nWARNING: {path}\nalready exists as a file, and the directory cannot be created.\n")
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



def main():
    """
    Perform grid-search to generate the HistGradientBoostingClassifier (HGB) model
    """
    #Get the parameters
    args = get_params()[0]
    # set main directories and file paths
    rawTbl: str = os.path.realpath(args.raw_tr_set)
    # Set the other output paths
    outDir: str = os.path.realpath(args.output_dir)
    trDir: str = os.path.join(outDir, "training-data")
    modelsDir: str = os.path.join(outDir, "models")
    logsDir: str = os.path.join(outDir, "logs")

    # obtain the rest of the parameters for the training
    skipMolWeights: bool = args.no_mol_weights
    outPrefix: str = args.out_prefix
    cvSteps = args.cv_steps
    threads = args.threads
    debug = args.debug

    # Set logging level
    if debug:
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)
    else:
        # logging.basicConfig(level=logging.INFO)
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

    makedir(outDir)
    makedir(modelsDir)
    makedir(trDir)
    makedir(logsDir)

    trPath, labelsPath = extract_samples_and_labels(rawTbl, args.labels, outDir=trDir, noMolWeights=skipMolWeights, useDummyFeatures=args.dummy)

    # Show some info
    logging.info("Training Adaptive boosting model with the following parameters:")
    logging.info(f"Raw training data table: {rawTbl}")
    logging.info(f"Labels file: {labelsPath}")
    logging.info(f"Training table path: {trPath}")
    logging.info(f"Output directory: {modelsDir}")
    logging.info(f"Logs directory: {logsDir}")
    logging.info(f"Output prefix:\t{outPrefix}")
    logging.info(f"CV-folds:\t{cvSteps}")
    logging.info(f"Threads:\t{threads}")

    # check that the training set exists
    if not os.path.isfile(trPath):
        logging.error("The file with training samples is not valid.\n")
        sys.exit(-2)

    # check that the training labels
    if not os.path.isfile(labelsPath):
        sys.stderr.write("\nERROR: The file with the labels is not a valid.\n")
        sys.exit(-2)
    # Output directory
    # check that the training set exists
    if not (os.path.isdir(modelsDir)):
        # create the output directory
        makedir(modelsDir)
        logging.info(f"The trained models will be stored in\n{modelsDir}\n")

    # Prepare samples and labels
    df = pd.read_csv(trPath, sep="\t")
    features = df.values
    # load labels
    df = pd.read_csv(labelsPath, sep="\t")
    labels = df.values
    labels = labels.ravel()
    del df

    # Print some info on the training data
    print(f"\nTraining samples:\t{features.shape[0]}")
    print(f"Training features:\t{features.shape[1]}")
    print(f"Training labels:\t{labels.shape[0]}")

    print("\nNormalizing training samples...")
    normFeat = normalize(features, norm="l2", axis=0, copy=True, return_norm=False)
    print("Normalization DONE!")

    # Initialize and fit the model
    model = HistGradientBoostingClassifier()
    model.fit(normFeat, labels)

    # Perform grid search
    # Most complete set of parameters
    # This would train 25,000 models!
    param_grid = {"early_stopping": [True, False], "learning_rate": [x/10. for x in range(1, 11, 1)], "max_bins": range(10, 260, 10), "max_iter": range(10, 510, 10)}
    # NOTE: testing only
    # param_grid = {"early_stopping": [True, False], "learning_rate": [x/10. for x in range(1, 11, 1)], "max_bins": range(10, 260, 100), "max_iter": range(10, 310, 100)} # REMOE LATER

    # add max_iter as parameter
    # print(param_grid)
    bestHgb = GridSearchCV(model, param_grid, cv=cvSteps, n_jobs=threads, return_train_score=True, error_score=np.nan)
    bestHgb.fit(normFeat, labels)
    # print(bestHgb)
    print(f"\nbestHgb:\n{bestHgb}")

    # write a file with the results
    outName: str = f"gsearch_histgradientboosting_{len(param_grid['early_stopping'])}x{len(param_grid['learning_rate'])}x{len(param_grid['max_bins'])}x{len(param_grid['max_iter'])}_{normFeat.shape[1]}feat_cv{cvSteps}.tsv"
    print(f"outName:\t{outName}")
    if len(outPrefix) > 0:
        outName = f"{outPrefix}.{outName}"
    gridSearchPath = os.path.join(logsDir, outName)

    ofd = open(gridSearchPath, "w")
    ofd.write("model_rank\tearly_stopping\tlearning_rate\tmax_bins\tmax_iter\tmean_train_accuracy\tstd_train_accuracy\tmean_test_accuracy\tstd_test_accuracy\n")

    # extract the required info
    paramList = bestHgb.cv_results_["params"]
    meanTestAccList = bestHgb.cv_results_["mean_test_score"]
    stdTestAccList = bestHgb.cv_results_["std_test_score"]
    meanTrAccList = bestHgb.cv_results_["mean_train_score"]
    stdTrAccList = bestHgb.cv_results_["std_train_score"]
    rankList = bestHgb.cv_results_["rank_test_score"]

    earlystop: bool = True
    for i, params in enumerate(paramList):
        r = rankList[i]
        l_rate = params["learning_rate"]
        maxbins = params["max_bins"]
        maxiter = params["max_iter"]
        earlystop = params["early_stopping"]
        avgTestAcc = meanTestAccList[i]
        stdTestAcc = stdTestAccList[i]
        avgTrAcc = meanTrAccList[i]
        stdTrAcc = stdTrAccList[i]
        # write the output line
        ofd.write(f"{r}\t{earlystop}\t{l_rate:.2f}\t{maxbins}\t{maxiter}\t{avgTrAcc:.6f}\t{stdTrAcc:.6f}\t{avgTestAcc:.6f}\t{stdTestAcc:.6f}\n")

    ofd.close()

    # Get the best model
    # and train the final best one
    bestParams = bestHgb.best_params_
    print(f"bestParams:\t{bestParams}")
    # train the model using the best parameters
    l_rate = bestParams["learning_rate"]
    maxbins = bestParams["max_bins"]
    maxiter = bestParams["max_iter"]
    earlystop = bestParams["early_stopping"]
    # model = HistGradientBoostingClassifier(learning_rate=l_rate, max_bins=maxbins)
    model = HistGradientBoostingClassifier(learning_rate=l_rate, max_bins=maxbins, max_iter=maxiter, early_stopping=earlystop)
    model.fit(normFeat, labels)

    # Compute the permutation importance
    # which an alternative to feature_importance
    permutationImportance = permutation_importance(model, normFeat, labels, n_repeats=10, random_state=0)
    print(f"permutationImportance.importances_mean:\t{permutationImportance.importances_mean}")

    # Because the HGB is stochastic the avg accuracy might change at each iterarion
    # To mitigate this problem we take the mean accuracy multiple 10-folds cross validation
    cvEvalIters: int = 10
    cvResults = np.zeros(cvEvalIters, dtype=np.float64)
    meanAcc: float = 0.
    for i in range(cvEvalIters):
        evaluation = cross_val_score(model, normFeat, labels, cv=cvSteps, n_jobs=threads)
        meanAcc = sum(evaluation)/len(evaluation)
        print(f"Mean accuracy cv-{cvSteps} [iter {i+1}]:\t{meanAcc:.6f}")
        # add the value in the array
        cvResults[i] = meanAcc
    
    print(f"\nMean accuracy from {cvEvalIters} {cvSteps}-fold CVs:\t{cvResults.mean()}")

    # Set vars
    l_rateStr: str = str(l_rate).replace(".", "")
    # Dump model using pickle
    outName = f"{earlystop}_{l_rateStr}_{maxbins}_{maxiter}_{normFeat.shape[1]}features_{normFeat.shape[0]}samples.pckl"

    if len(outPrefix) > 0:
        outName = f"{outPrefix}.{outName}"
    else:
        outName = f"histgradientboosting.{outName}"
    modelPath = os.path.join(modelsDir, outName)

    # Note the if the minimum supported version of Python is 3.8
    # Then protocol=5 should be used, which is faster at loading big objects
    pfd = open(modelPath, "wb")
    pickle.dump(model, pfd, protocol=5)
    pfd.close()

    # sys.exit("DEBUG :: hgb_grid_search_from_raw_tr_data.py")


if __name__ == "__main__":
    main()
