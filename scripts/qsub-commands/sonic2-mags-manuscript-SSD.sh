#/usr/bin/bash
#PBS -S /usr/bin/bash
#PBS -N sonic2-mags-manuscript-SSD
#PBS -l ncpus=128
#PBS -V
#PBS -M salvocos@k.u-tokyo.ac.jp
#PBS -m abe
#PBS -e /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stderr
#PBS -o /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs/qsub_logs/stdout
# set -e
# set -u
# cd “${PBS_O_WORKDIR:-$(pwd)}”
cd /home/salvocos/tmp/sonicparanoid_runs/qfo2020-runs

date

 ### Diamond runs [essentials and hybrid]#####

#### Fast ####

echo $'\n@@@@ sonic2-mags-dm-hybrid-fast-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-fast -p s2-mags-dm-fast-thr75 -ot --aln-tool diamond --threads 128 -m fast -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mags-dm-graph-fast @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-fast -p s2-mags-dm-fast-graph -ot --aln-tool diamond --threads 128 -m fast -op -noidx -sa
wait
echo $'\n'

# echo $'\n@@@@ sonic2-mags-dm-graph-ca-fast @@@@\n'
# sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-fast -p s2-mags-dm-fast-graph-ca -ow --aln-tool diamond --threads 128 -m fast -op -noidx -sa -ca
# wait
# echo $'\n'


#### Default ####

echo $'\n@@@@ sonic2-mags-dm-hybrid-def-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-def -p s2-mags-dm-def-thr75 -ot --aln-tool diamond --threads 128 -m default -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mags-dm-graph-def @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-def -p s2-mags-dm-def-graph -ot --aln-tool diamond --threads 128 -m default -op -noidx -sa
wait
echo $'\n'

# echo $'\n@@@@ sonic2-mags-dm-graph-ca-def @@@@\n'
# sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-def -p s2-mags-dm-def-graph-ca -ow --aln-tool diamond --threads 128 -m default -op -noidx -sa -ca
# wait
# echo $'\n'


#### Sensitive ####

echo $'\n@@@@ sonic2-mags-dm-hybrid-sens-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-sens -p s2-mags-dm-sens-thr75 -ot --aln-tool diamond --threads 128 -m sensitive -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mags-dm-graph-sens @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-sens -p s2-mags-dm-sens-graph -ot --aln-tool diamond --threads 128 -m sensitive -op -noidx -sa
wait
echo $'\n'

# echo $'\n@@@@ sonic2-mags-dm-graph-ca-sens @@@@\n'
# sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-dm-hybrid-sens -p s2-mags-dm-sens-graph-ca -ow --aln-tool diamond --threads 128 -m sensitive -op -noidx -sa -ca
# wait
# echo $'\n'


 #### MMseqs runs #####

#### Default ####

echo $'\n@@@@ sonic2-mags-mm-hybrid-def-thr75 @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-mm-hybrid-def -p s2-mags-mm-def-thr75 -ot --threads 128 -m default -op -noidx --min-arch-merging-cov 0.75
wait
echo $'\n'

echo $'\n@@@@ sonic2-mags-mm-graph-def @@@@\n'
sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-mm-hybrid-def -p s2-mags-mm-def-graph -ot --threads 128 -m default -op -noidx -sa
wait
echo $'\n'

# echo $'\n@@@@ sonic2-mags-mm-graph-ca-def @@@@\n'
# sonicparanoid -i /home/salvocos/tmp/sonicparanoid_runs/mags-manuscript-run/reference_2000_mags -o /ssd_home/mags-manuscript-run/sonic2-mags-mm-hybrid-def -p s2-mags-mm-def-graph-ca -ow --threads 128 -m default -op -noidx -sa -ca
# wait
# echo $'\n'


# All runs completed
date