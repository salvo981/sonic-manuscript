# This script will generate the data for essential executiont times
# using the script in sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ca_sonic136-mmseqs-qfo20-complete-ismb21-ml5-most-sensitive.tsv tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ra_sonic136-mmseqs-qfo20-essentials-ismb21-ml5-most-sensitive.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-mmseqs-essentials-msens-ismb.extime.datapoint.tsv
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ca_sonic136-mmseqs-qfo20-complete-ismb21-ml5-sensitive.tsv tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ra_sonic136-mmseqs-qfo20-essentials-ismb21-ml5-sensitive.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-mmseqs-essentials-sens-ismb.extime.datapoint.tsv
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ca_sonic136-mmseqs-qfo20-complete-ismb21-ml5-default.tsv tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ra_sonic136-mmseqs-qfo20-essentials-ismb21-ml5-default.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-mmseqs-essentials-default-ismb.extime.datapoint.tsv
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ca_sonic136-mmseqs-qfo20-complete-ismb21-ml5-fast.tsv tables/aln-time-files/qfo2020/aln_ex_times_mmseqs_ra_sonic136-mmseqs-qfo20-essentials-ismb21-ml5-fast.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-mmseqs-essentials-fast-ismb.extime.datapoint.tsv
wait


##### DIAMOND RUNS #####
julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_diamond_ca_sonic136-dmnd-qfo20-complete-ismb21-ml5-most-sensitive.tsv tables/aln-time-files/qfo2020/aln_ex_times_diamond_ra_sonic136-dmnd-qfo20-essentials-ismb21-ml5-most-sensitive.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-dmnd-essentials-msens-ismb.extime.datapoint.tsv
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_diamond_ca_sonic136-dmnd-qfo20-complete-ismb21-ml5-sensitive.tsv tables/aln-time-files/qfo2020/aln_ex_times_diamond_ra_sonic136-dmnd-qfo20-essentials-ismb21-ml5-sensitive.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-dmnd-essentials-sens-ismb.extime.datapoint.tsv
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_diamond_ca_sonic136-dmnd-qfo20-complete-ismb21-ml5-default.tsv tables/aln-time-files/qfo2020/aln_ex_times_diamond_ra_sonic136-dmnd-qfo20-essentials-ismb21-ml5-default.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-dmnd-essentials-default-ismb.extime.datapoint.tsv
wait

julia ~/work_repos/sonic-manuscript/scripts/julia/create_essential_extimes_datapoints.jl tables/aln-time-files/qfo2020/aln_ex_times_diamond_ca_sonic136-dmnd-qfo20-complete-ismb21-ml5-fast.tsv tables/aln-time-files/qfo2020/aln_ex_times_diamond_ra_sonic136-dmnd-qfo20-essentials-ismb21-ml5-fast.tsv tables/snapshot-qfo2020-sonic136-azur.tsv --meta-data tables/qfo2020processed-tbl.tsv --aai-tbl tables/aai_computation_comparem/aai_qfo2020.tsv --output-tbl output/sonic136-dmnd-essentials-fast-ismb.extime.datapoint.tsv

